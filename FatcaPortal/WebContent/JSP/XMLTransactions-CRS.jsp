<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.newgen.bean.*"%>
<%@page import="java.util.*"%>
<%@page import="com.newgen.bean.GlobalVariables"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: <%=GlobalVariables.projectName%> Inland Revenue
	Department ::
</title>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/script.js"></script>
<script src="${pageContext.request.contextPath}/script/Common.js"></script>
</head>
<%!String reportingYearFromSession = null;%>
<script>
	function submitStatusform() {

		document.reportStatusform.submit();

	}
</script>


<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", -1);
	String UserName = "";
	String reportName = "";
	String submissionDate = "";
	int stageSeq = 0;
	Calendar now = Calendar.getInstance(); // Gets the current date and time
	int year = now.get(Calendar.YEAR);
	int startYear = GlobalVariables.reportingStartYear;
	String userType = "";
	String userSessionID = "";
	String userEmail = "";
	String complianceType = "";
	HttpSession session1 = request.getSession(false);
	UserName = (String) session1.getAttribute("UserName");
	userType = (String) session1.getAttribute("UserType");
	userEmail = (String) session1.getAttribute("UserEmail");
	reportingYearFromSession = (String) session1
			.getAttribute("ReportingYear");
	complianceType = (String) session1.getAttribute("ComplianceType");
	Map<String, String> loginUserMap = GlobalVariables.loginUserMap;
	userSessionID = loginUserMap.get(userEmail);
	if (null != userSessionID
			&& (!(userSessionID.equalsIgnoreCase(session1.getId())))
			|| null == UserName) {
		response.sendRedirect(request.getContextPath()
				+ "/JSP/SessionExpired.jsp");
	}  else if (null == userSessionID) {
		response.sendRedirect(request.getContextPath()
				+ "/JSP/SessionExpired.jsp");
	}

	ArrayList<TransactionXMLBean> XMLBeans = null;
	XMLBeans = (ArrayList<TransactionXMLBean>) request
			.getAttribute("XMLArrayList");
	String fistatus = "";
	String fistage = "";
	String status = "";
	String stage = "";
	if (XMLBeans != null && XMLBeans.size() > 0) {
		TransactionXMLBean bean = XMLBeans.get(0);
		reportName = bean.getFileName();
		submissionDate = bean.getUploadedDate();
		status = bean.getFileStatus();
		stage = bean.getFileStage();
		fistage = bean.getFiStage();
		fistatus = bean.getFiStatus();

	}
%>

<body>
	<table width="1003" border="0" cellspacing="0" cellpadding="0"
		align="center" bgcolor="#FFFFFF">
		<tr>
			<td background="${pageContext.request.contextPath}/images/bg-g-l.gif"
				width="19" height="1"></td>
			<td><table width="965" border="0" cellspacing="0"
					cellpadding="0">
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td
													background="${pageContext.request.contextPath}/images/bg.png"
													height="140"><table width="965" border="0"
														cellspacing="0" cellpadding="0">
														<tr>
															<td style="padding: 0px 0px 3px 10px" width="130"><img
																src="${pageContext.request.contextPath}/images/logo.png"
																width="118" height="135" /></td>
															<td valign="top"><table width="835" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>
																		<td colspan="2"><table border="0" cellspacing="0"
																				cellpadding="0" align="right">
																				<tr>
																					<td width="35" align="right"><img
																						src="${pageContext.request.contextPath}/images/us.png"
																						width="25" height="25" /></td>
																					<td class="logd1" style="text-align: left;"
																						width="150"><%=UserName%> is <strong>&nbsp;&nbsp;Logged
																							in </strong>&nbsp;</td>
																					<td class="logd" width="60">
																						<%-- <a href="${pageContext.request.contextPath}/JSP/logout.jsp"
																						class="logd">Logout</a> --%> <a
																						href="javascript:void();"
																						onclick='return logoutUser("jsp");' class="logd">Logout</a>
																					</td>

																				</tr>
																			</table></td>
																	</tr>
																	<tr>
																		<td style="padding: 2px 0px 0px 20px"><img
																			src="${pageContext.request.contextPath}/images/slogan.png"
																			width="652" height="" /></td>
																		<td style="padding: 8px 10px 5px 20px">
																		<img 	src="${pageContext.request.contextPath}/images/flag.png"
																			width="118" height="" />
																			&nbsp;
																			</td>
																	</tr>
																	<tr>
																		<td>&nbsp;</td>
																	</tr>
																</table></td>
														</tr>
													</table></td>
											</tr>
										</table></td>
								</tr>

							</table></td>
					</tr>
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">

								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td align="center"><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/JSP/home.jsp"
															class="menulink"><strong>Home</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<%-- <td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Submit
																	Report</strong></a>
															<ul>
																<li><a
																	href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Upload
																		XML</a></li>
																<li><a href="upload-xml.html">Upload XLS/CSV </a></li>
																<li><a
																	href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																		Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li>
															</ul></li>
													</ul></td> --%>


												<td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Submit
																	Report</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Upload
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<%
																	} else if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Upload
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Upload
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Upload
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	}
																%>


															</ul></li>
													</ul></td>


												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<!-- <td><ul class="drop_menu">
														<li><a href='javascript:void();'><strong>Report
																	Status</strong></a></li>
													</ul></td> -->


												<td><ul id="reportTab" class="reportTab">
														<li><a class="menulink" style="background-color: #2487a2" href="#"><strong>Report
																	Status</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<%
																	} else if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>
																<%
																	} else {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>

																<%
																	}
																%>

															</ul></li>
													</ul></td>


												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Contact
																	Us</strong></a></li>
													</ul></td>
											</tr>

										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>

									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td width="25"></td>
												<td valign="top"><table width="900" border="0"
														cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td valign="top"><table width="900" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g-t-m.png"
																			class="bgtext1">FI Reporting Life Cycle</td>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td
																			background="${pageContext.request.contextPath}/images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td><table width="882" border="0" cellspacing="0"
																				cellpadding="0">
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td style="text-align: justify;">This section
																						shows your FI's overall position/stage in the
																						current Reporting Cycle. To understand more about
																						the different stages, please click on the bubble
																						or stage indicator. Every Reporting Cycle, you
																						should strive to reach "Report Successful" stage
																						at the earliest.</td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td>
																						<form
																							action="${pageContext.request.contextPath}/ReportStatusCRS"
																							method="post" name="reportStatusform">
																							<table width="840" border="0" cellspacing="0"
																								cellpadding="0">
																								<tr>
																									<td width="100">Reporting Year</td>
																									<td width="220"><select
																										name="reporting_year" id="reporting_year"
																										class="form-control"
																										style="width: 200px; padding: 0px 2px 2px 5px;">
																											<!-- <option value="">-------Select-----</option> -->
																											<%
																												year = year - 1;
																												while (year >= startYear) {
																											%>
																											<option value="<%=year%>"><%=year%></option>
																											<%
																												year--;
																												}
																											%>
																											<script>
																												var reportingYear =
																											<%=reportingYearFromSession%>
																												;
																												var formObj = document
																														.getElementById('reporting_year');
																												for ( var i = 0; i < formObj.length; i++) {
																													if (formObj[i].value == reportingYear) {
																														formObj[i].selected = true;
																														break;
																													}
																												}
																											</script>

																									</select></td>
																									<td><a href="javascript: void();"
																										onclick="return submitStatusform();"><img
																											src="${pageContext.request.contextPath}/images/search.png"
																											width="31" height="24" border="0"
																											style="vertical-align: middle;" /></a></td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																								</tr>
																							</table>
																						</form>
																					</td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td><table width="800" border="0"
																							cellspacing="0" cellpadding="0" align="center">
																							<tr
																								style="padding-left: 30px; padding-right: 30px">


																								<%
																									if (null == reportName || reportName.equalsIgnoreCase("")
																											|| !(reportName.length() > 0)) {
																								%>
																								<td style="width: 120px;">&nbsp;</td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/upload1.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/pv2.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/ird1.png"
																									width="114" height="99" /></td>

																								<%-- <td align="center"><img
																									src="${pageContext.request.contextPath}/images/irs1.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/rfe1.png"
																									width="114" height="99" /></td> --%>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/r-succssesful1.png"
																									width="114" height="99" /></td>
																								<td style="width: 100px;">&nbsp;</td>
																								<%
																									} else if (fistage.equalsIgnoreCase("Portal Validation")) {
																										if (fistatus.length() > 0
																												&& fistatus.toUpperCase().contains("SUCCESS")) {
																											stageSeq = 21;
																								%>
																								<td style="width: 120px;">&nbsp;</td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/upload.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/pv1.png"
																									width="114" height="99" /></td>

																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/ird_y.png"
																									width="114" height="99" /></td>
																								<%-- <td align="center"><img
																									src="${pageContext.request.contextPath}/images/irs1.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/rfe1.png"
																									width="114" height="99" /></td> --%>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/r-succssesful1.png"
																									width="114" height="99" /></td>
																								<td style="width: 100px;">&nbsp;</td>
																								<%
																									} else if (fistatus.length() > 0
																												&& fistatus.toUpperCase().contains("INPROGRESS")) {
																											stageSeq = 22;
																								%>
																								<td style="width: 120px;">&nbsp;</td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/upload.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/pv1_y.png"
																									width="114" height="99" /></td>

																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/ird1.png"
																									width="114" height="99" /></td>
																								<%-- <td align="center"><img
																									src="${pageContext.request.contextPath}/images/irs1.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/rfe1.png"
																									width="114" height="99" /></td> --%>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/r-succssesful1.png"
																									width="114" height="99" /></td>
																								<td style="width: 100px;">&nbsp;</td>
																								<%
																									} else

																										{
																											stageSeq = 20;
																								%>
																								<td style="width: 120px;">&nbsp;</td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/upload.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/pv.png"
																									width="114" height="99" /></td>

																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/ird1.png"
																									width="114" height="99" /></td>
																								<%-- <td align="center"><img
																									src="${pageContext.request.contextPath}/images/irs1.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/rfe1.png"
																									width="114" height="99" /></td> --%>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/r-succssesful1.png"
																									width="114" height="99" /></td>
																								<td style="width: 100px;">&nbsp;</td>
																								<%
																									}
																									} else if (fistage.equalsIgnoreCase("IRD Verification")) {
																										if (fistatus.length() > 0
																												&& fistatus.toUpperCase().contains("SUCCESS")) {
																											stageSeq = 31;
																								%>
																								<td style="width: 120px;">&nbsp;</td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/upload.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/pv1.png"
																									width="114" height="99" /></td>

																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/ird.png"
																									width="114" height="99" /></td>
																								<%-- <td align="center"><img
																									src="${pageContext.request.contextPath}/images/irs_y.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/rfe1.png"
																									width="114" height="99" /></td> --%>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/r-succssesful_y.png"
																									width="114" height="99" /></td>
																								<td style="width: 100px;">&nbsp;</td>
																								<%
																									} else if (fistatus.length() > 0
																												&& fistatus.toUpperCase().contains("INPROGRESS")) {
																											stageSeq = 32;
																								%>
																								<td style="width: 120px;">&nbsp;</td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/upload.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/pv1.png"
																									width="114" height="99" /></td>

																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/ird_y.png"
																									width="114" height="99" /></td>
																								<%-- <td align="center"><img
																									src="${pageContext.request.contextPath}/images/irs1.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/rfe1.png"
																									width="114" height="99" /></td> --%>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/r-succssesful1.png"
																									width="114" height="99" /></td>
																								<td style="width: 100px;">&nbsp;</td>
																								<%
																									} else {
																											stageSeq = 30;
																								%>
																								<td style="width: 120px;">&nbsp;</td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/upload.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/pv1.png"
																									width="114" height="99" /></td>

																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/ird2.png"
																									width="114" height="99" /></td>
																								<%-- <td align="center"><img
																									src="${pageContext.request.contextPath}/images/irs1.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/rfe1.png"
																									width="114" height="99" /></td> --%>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/r-succssesful1.png"
																									width="114" height="99" /></td>
																								<td style="width: 100px;">&nbsp;</td>
																								<%
																									}
																									} else if (fistage.equalsIgnoreCase("REPORT SUCCESSFUL")) {
																										if (fistatus.length() > 0
																												&& (fistatus.toUpperCase().contains("SUCCESSFUL"))) {
																											stageSeq = 41;
																								%>
																								<td style="width: 120px;">&nbsp;</td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/upload.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/pv1.png"
																									width="114" height="99" /></td>

																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/ird.png"
																									width="114" height="99" /></td>
																								<%-- <td align="center"><img
																									src="${pageContext.request.contextPath}/images/irs.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/rfe_y.png"
																									width="114" height="99" /></td> --%>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/r-succssesful.png"
																									width="114" height="99" /></td>
																								<td style="width: 100px;">&nbsp;</td>

																								<%
																									} else if (fistatus.toUpperCase().contains("INPROGRESS")) {
																											stageSeq = 42;
																								%>
																								<td style="width: 120px;">&nbsp;</td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/upload.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/pv1.png"
																									width="114" height="99" /></td>

																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/ird.png"
																									width="114" height="99" /></td>
																								<%-- <td align="center"><img
																									src="${pageContext.request.contextPath}/images/irs_y.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/rfe1.png"
																									width="114" height="99" /></td> --%>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/r-succssesful_y.png"
																									width="114" height="99" /></td>
																								<td style="width: 100px;">&nbsp;</td>

																								<%
																									} else {
																											stageSeq = 40;
																								%>
																								<td style="width: 120px;">&nbsp;</td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/upload.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/pv1.png"
																									width="114" height="99" /></td>

																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/ird.png"
																									width="114" height="99" /></td>
																								<%-- <td align="center"><img
																									src="${pageContext.request.contextPath}/images/irs2.png"
																									width="114" height="99" /></td>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/rfe1.png"
																									width="114" height="99" /></td> --%>
																								<td align="center"><img
																									src="${pageContext.request.contextPath}/images/r-succssesful_r.png"
																									width="114" height="99" /></td>
																								<td style="width: 100px;">&nbsp;</td>
																								<%
																									}
																									}
																								%>


																							</tr>
																						</table></td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td><table width="650" border="0"
																							cellspacing="0" cellpadding="0" align="center"
																							style="border: 2px solid #a1a1a1; padding: 10px 10px; background: #e9e9e9; width: 650px; border-radius: 10px;">
																							<!-- <tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>
																										Portal Validation :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">submission
																									for the current Reporting Cycle is under Portal
																									Validation</td>
																							</tr> -->
																							<%
																								if (stageSeq == 22) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>
																										Portal Validation :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">submission
																									for the current Reporting Cycle is under Portal
																									Validation</td>
																							</tr>
																							<%
																								} else if (stageSeq == 21) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>Portal
																										Validation :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Your
																									submission for the current Reporting Cycle is
																									under verification by IRD. You may be contacted
																									by the IRD and/or receive email notification if
																									your submission is found to have errors or
																									further clarification is required.</td>
																							</tr>
																							<%
																								} else if (stageSeq == 20) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>Portal
																										Validation :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Your
																									submission for the current Reporting Cycle is
																									under Portal Validation. Please see the report
																									by clicking link in the below table and upload
																									updated xml</td>
																							</tr>
																							<%
																								} else if (stageSeq == 30) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>IRD
																										Verification :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Your
																									submission for the current Reporting Cycle is
																									under verification by IRD.Please see the report
																									by clicking link in the below table and upload
																									updated xml.</td>
																							</tr>
																							<%
																								} else if (stageSeq == 31) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>IRD
																										Verification :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Your
																									submission for the current Reporting Cycle is
																									under verification by IRS. You may be contacted
																									by the IRS and/or receive email notification if
																									your submission is found to have errors or
																									further clarification is required.</td>
																							</tr>
																							<%
																								} else if (stageSeq == 32) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>IRD
																										Verification :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Your
																									submission for the current Reporting Cycle is
																									under verification by IRD.Please see the report
																									by clicking link in the below table and upload
																									updated xml.</td>
																							</tr>
																							<%
																								} else if (stageSeq == 40) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>Report
																										Successful :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Your
																									submission for the current Reporting Cycle has
																									been submitted to IRS</td>
																							</tr>
																							<%
																								} else if (stageSeq == 41) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>
																										Report Successful :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">submission
																									for the current Reporting Cycle has been
																									completed successfully</td>
																							</tr>
																							<%
																								} else if (stageSeq == 42) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>
																										Report Successful :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">submission
																									for the current Reporting Cycle is under
																									submitted to IRS</td>
																							</tr>

																							<%
																								}
																							%>
																						</table></td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																			</table></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td><img
																			src="${pageContext.request.contextPath}/${pageContext.request.contextPath}/images/c-b-l.png"
																			width="9" height="5" /></td>
																		<td
																			style="background-image: url(${pageContext.request.contextPath}/images/g2.png)"></td>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-r.png"
																			width="9" height="5" /></td>
																	</tr>
																</table></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td><table width="900" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g-t-m.png"
																			class="bgtext1">Submission History</td>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td
																			background="${pageContext.request.contextPath}/images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td><table width="750" border="0" cellspacing="0"
																				cellpadding="0" align="center">
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td><table width="750" border="0"
																							cellspacing="0" cellpadding="0">
																							<tr>
																								<td width="9"><img
																									src="${pageContext.request.contextPath}/images/g-t-l.png"
																									width="9" height="29" /></td>
																								<td
																									background="${pageContext.request.contextPath}/images/g-t-m.png"
																									class="bgtext1">Latest Submission</td>
																								<td width="9"><img
																									src="${pageContext.request.contextPath}/images/g-t-r.png"
																									width="9" height="29" /></td>
																							</tr>
																							<tr>
																								<td
																									background="${pageContext.request.contextPath}/images/g.png"
																									style="background-repeat: repeat-y;"></td>
																								<td><table width="750" border="0"
																										cellspacing="0" cellpadding="0">
																										<tr>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																										</tr>
																										<tr>
																											<td colspan="4">This section shows the
																												historic status changes on each of your
																												submissions. The status of your latest
																												submission is also depicted here. Note: This
																												section shows the status of your individual
																												submissions and not the stage where your FI
																												lies in the current Reporting Cycle.</td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																										</tr>
																										<tr>
																											<td width="130">Stage and Status</td>
																											<td>&nbsp;</td>
																											<%-- <td align="center"><img
																												src="${pageContext.request.contextPath}/images/upload1.png"
																												width="114" height="99" /></td> --%>
																											<%
																												if (null == reportName || reportName.equalsIgnoreCase("")
																														|| !(reportName.length() > 0)) {
																											%>
																											<td align="center"><img
																												src="${pageContext.request.contextPath}/images/upload1.png"
																												width="114" height="99" /></td>
																											<%
																												} else if (stage.equalsIgnoreCase("Portal Validation")) {
																													if (status.length() > 0
																															&& status.toUpperCase().contains("SUCCESS")) {
																											%>
																											<td><a href="#"><img
																													src="${pageContext.request.contextPath}/images/pv1.png"
																													width="114" height="99" border="0" /></a></td>
																											<%
																												} else if (status.length() > 0
																															&& status.toUpperCase().contains("INPROGRESS")) {
																											%>

																											<td><a href="#"><img
																													src="${pageContext.request.contextPath}/images/pv1_y.png"
																													width="114" height="99" border="0" /></a></td>
																											<%
																												} else {
																											%>

																											<td><a href="#"><img
																													src="${pageContext.request.contextPath}/images/pv.png"
																													width="114" height="99" border="0" /></a></td>
																											<%
																												}
																												} else if (stage.equalsIgnoreCase("IRD Verification")) {
																													if (status.length() > 0
																															&& status.toUpperCase().contains("SUCCESS")) {
																											%>
																											<td><a href="#"><img
																													src="${pageContext.request.contextPath}/images/ird.png"
																													width="114" height="99" border="0" /></a></td>

																											<%
																												} else if (status.toUpperCase().contains("INPROGRESS")) {
																											%>
																											<td><a href="#"><img
																													src="${pageContext.request.contextPath}/images/ird_y.png"
																													width="114" height="99" border="0" /></a></td>
																											<%
																												} else {
																											%>
																											<td><a href="#"><img
																													src="${pageContext.request.contextPath}/images/ird2.png"
																													width="114" height="99" border="0" /></a></td>
																											<%
																												}
																												} else if (stage.equalsIgnoreCase("REPORT SUCCESSFUL")) {
																													if (status.length() > 0
																															&& (status.toUpperCase().contains("SUCCESSFUL"))) {
																											%>
																											<td><a href="#"><img
																													src="${pageContext.request.contextPath}/images/r-succssesful.png"
																													width="114" height="99" border="0" /></a></td>

																											<%
																												} else if (status.toUpperCase().contains("INPROGRESS")) {
																											%>
																											<td><a href="#"><img
																													src="${pageContext.request.contextPath}/images/irs_y.png"
																													width="114" height="99" border="0" /></a></td>
																											<%
																												} else {
																											%>
																											<td><a href="#"><img
																													src="${pageContext.request.contextPath}/images/irs2.png"
																													width="114" height="99" border="0" /></a></td>
																											<%
																												}
																												}
																											%>




																											<td width="400"><table width="400"
																													border="0" cellspacing="0" cellpadding="0">
																													<tr>
																														<td>&nbsp;</td>
																														<td>&nbsp;</td>
																													</tr>
																													<tr>
																														<td>Report Name</td>
																														<td><input type="text"
																															readonly="true" value="<%=reportName%>"
																															name="user" class="form-control"
																															style="padding: 0px 2px 2px 5px; width: 200px;"></td>
																													</tr>
																													<tr>
																														<td>&nbsp;</td>
																														<td>&nbsp;</td>
																													</tr>
																													<tr>
																														<td>Submission Date</td>
																														<td><input type="text"
																															readonly="true"
																															value="<%=submissionDate%>" name="user"
																															class="form-control"
																															/ style="padding: 0px 2px 2px 5px; width: 200px;"></td>
																													</tr>
																												</table></td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																										</tr>
																									</table></td>
																								<td
																									background="${pageContext.request.contextPath}/images/g1.png"
																									style="background-repeat: repeat-y;"></td>
																							</tr>
																							<tr>
																								<td colspan="3"
																									style="background-image: url(${pageContext.request.contextPath}/images/g21.png); background-repeat: repeat-x;"
																									height="1"></td>
																							</tr>
																						</table></td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td>
																						<div id='transactionBatchTable'
																							name='transactionBatchTable'>
																							<table width="770" border="1"
																								style="table-layout: fixed;" cellspacing="0"
																								cellpadding="0">
																								<tr class="bg1">
																									<td width="25%" class="ipm_n">Report Name</td>
																									<td width="15%" class="ipm_n">Submitted by</td>
																									<td width="10%" class="ipm_n">Type</td>
																									<td width="15%" class="ipm_n">Stage</td>
																									<td width="15%" class="ipm_n">Status (?)</td>
																									<td width="20%" class="ipm_n">Date &amp;
																										Time</td>
																								</tr>
																								<%
																									try {

																										int pageCount = (Integer) request.getAttribute("pageCount");
																										String totalCount = (String) request.getAttribute("totalCount");
																										String nextFlag = (String) request.getAttribute("nextFlag");
																										int prevPage = pageCount - 1;
																										int nextPage = pageCount + 1;
																										for (TransactionXMLBean obj : XMLBeans) {
																											//TransactionXMLBean obj= new  TransactionXMLBean();
																								%>
																								<tr>
																									<td class="ipm_n2"><%=obj.getFileName()%></td>
																									<td class="ipm_n2"><%=obj.getUploadedBy()%></td>
																									<td class="ipm_n2"><%=obj.getFiletype()%></td>
																									<td class="ipm_n2"><%=obj.getFileStage()%></td>
																									<%
																										//if (obj.getFileStatus().equalsIgnoreCase("failure")) {
																												if (obj.getFileStatus().length() > 0) {

																													/* System.out.println("obj.getFileStatus().length is >0 "); */
																													if (obj.getFileStatus().toUpperCase().contains("FAIL")) {
																									%>
																									<td class="ipm_n2"><a
																										href="${pageContext.request.contextPath}/Download?path=<%=obj.getHtmlFilepath() %>"
																										style="color: #015f7a;"> <span
																											style="color: #015f7a;"><%=obj.getFileStatus()%></span></a></td>
																									<%
																										} else {
																									%>
																									<td class="ipm_n2"><%=obj.getFileStatus()%></td>
																									<%
																										}
																												}
																									%>
																									<%-- <td class="ipm_n2"><%=obj.getFileStatus()%></td> --%>
																									<%-- <%
																										}
																												//}
																									%> --%>
																									<td width="120"><%=obj.getUploadedDate()%></td>
																								</tr>
																								<%
																									}
																								%>
																								<tr>
																									<td colspan=5>&nbsp;</td>
																									<td align="right">
																										<%
																											if (prevPage >= 0) {
																										%> <a href="javascript:void();"
																										onclick="getTransactionBatch('<%=prevPage%>','<%=totalCount%>','crs');">Prev</a>
																										<%
																											} else {
																										%> Prev <%
																											}
																										%> | <%
																											if (nextFlag != null && nextFlag.equalsIgnoreCase("TRUE")) {
																										%> <a href="javascript:void();"
																										onclick="getTransactionBatch('<%=nextPage%>','<%=totalCount%>','crs');">Next</a>
																										<%
																											} else {
																										%> Next <%
																											}
																										%>

																									</td>
																								</tr>
																							</table>
																						</div>

																					</td>
																				</tr>
																				<%
																					} catch (Exception e) {
																						System.out.println("Exception in Looop : " + e);
																						e.printStackTrace();
																					}
																				%>

																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																			</table></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td colspan="3"
																			style="background-image: url(${pageContext.request.contextPath}/images/g21.png); background-repeat: repeat-x;"
																			height="1"></td>
																	</tr>
																</table></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
													</table></td>
												<td width="20"></td>

											</tr>
										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>

								<tr>
									<td align="center"><img
										src="${pageContext.request.contextPath}/images/line.png"
										width="921" height="14" /></td>
								</tr>
								<%-- <tr>
									<td class="hedr">For Govt. of <%=com.newgen.bean.GlobalVariables.projectName%>
										Powered by Newgen Software Inc.
									</td>
								</tr> --%>

								<tr>
									<td>&nbsp;</td>
								</tr>
							</table></td>
					</tr>

				</table></td>
			<td background="${pageContext.request.contextPath}/images/bg-g-r.gif"
				height="1" width="19"></td>
		</tr>
	</table>
	<script type="text/javascript">
		var menu1 = new menu.dd("menu1");
		menu1.init("menu", "menuhover");
		  var reportMenu = new menu.dd("reportMenu");
		  reportMenu.init("reportTab", "menuhover");  
	</script>
</body>
</html>
