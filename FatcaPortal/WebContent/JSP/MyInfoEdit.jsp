<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.newgen.bean.FIRegistration"%>
<%@page import="java.io.File"%>
<%-- <%@page import="com.newgen.util.ClsMessageHandler"%>
<%@page import="com.newgen.util.ClsUtil"%> --%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.io.IOException"%>
<%@page import="com.newgen.bean.*"%>
<%@page import="java.util.*"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<script>
	window.history.forward(-1);
	function submitform() {

		var doc = document;

		if (!validate2('MyInfoEdit')) {

			return false;
		}/*  else if (doc.getElementById("isConformPassword").value == ""
				|| doc.getElementById("isConformPassword").value == "false")
			return false;
 */
 else if(!confirmPassword_editInfo())
		return false;
		
		var Password = encrptPassword1("Old_fi_password");
		doc.getElementById("Old_fi_password").value = Password;
		encrptPassword();
		executeServlet("userEdit");
		//alert(response);

	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: <%=com.newgen.bean.GlobalVariables.projectName%>
	Inland Revenue Department ::
</title>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/Common.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/jquery-ui.css" />
<script src="${pageContext.request.contextPath}/script/wdgeneral.js"></script>
<script src="${pageContext.request.contextPath}/script/blowfish.js"></script>

<script src="${pageContext.request.contextPath}/script/jquery-1.9.1.js"></script>
<script src="${pageContext.request.contextPath}/script/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/script/Common.js"></script>
<script src="${pageContext.request.contextPath}/script/Manual.js"></script>
<script src="${pageContext.request.contextPath}/script/script.js"></script>
<script src="${pageContext.request.contextPath}/script/md5.js"></script>

<%!String fiGIIN = null;
	String userCompliance = null;%>

<%
	response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1 
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0); //prevents caching at the proxy server

	String userType = "";
	String userSessionID = "";
	String userEmail = "";
	String userName = "";
	String complianceType = "";
	HttpSession session1 = request.getSession(false);
	userName = (String) session1.getAttribute("UserName");
	userType = (String) session1.getAttribute("UserType");
	userEmail = (String) session1.getAttribute("UserEmail");
	complianceType = (String) session1.getAttribute("ComplianceType");
	Map<String, String> loginUserMap = GlobalVariables.loginUserMap;
	userSessionID = loginUserMap.get(userEmail);
	if (null != userSessionID
			&& (!(userSessionID.equalsIgnoreCase(session1.getId())))
			|| null == userName) {
		response.sendRedirect(request.getContextPath()
				+ "/JSP/SessionExpired.jsp");
	} else if (null == userSessionID) {
		response.sendRedirect(request.getContextPath()
				+ "/JSP/SessionExpired.jsp");
	}
	//String Otpmsg = (String) request.getAttribute("OTPSuccessMsg");
	String Otpmsg = "";
	//System.out.println(Otpmsg);
	if (Otpmsg == null) {
		Otpmsg = "";
	}
	FIRegistration obj = (FIRegistration) request
			.getAttribute("fiUserObject");
	fiGIIN = obj.getGiin();
	userCompliance = complianceType;
%>

<script>
	$(function() {
		$("#tabs").tabs();
	});
	
	function setEditGIIN(){
		
		var editGIIN="<%=fiGIIN%>";
		var userCompliance="<%=userCompliance%>";
		if (userCompliance.toUpperCase() != "CRS") {
			setGIIN(editGIIN, "fi_giin");
		}
		makeReadOnly(userCompliance);
	}
</script>
</head>
<body onload="setEditGIIN(); ">
	<input type="hidden" id="isConformPassword" name="isConformPassword"></input>



	<table width="1003" border="0" cellspacing="0" cellpadding="0"
		align="center" bgcolor="#FFFFFF">
		<tr>
			<td background="${pageContext.request.contextPath}/images/bg-g-l.gif"
				width="19" height="1"></td>
			<td><table width="965" border="0" cellspacing="0"
					cellpadding="0">
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td
													background="${pageContext.request.contextPath}/images/bg.png"
													height="140"><table width="965" border="0"
														cellspacing="0" cellpadding="0">
														<tr>
															<td style="padding: 0px 0px 3px 10px" width="130"><img
																src="${pageContext.request.contextPath}/images/logo.png"
																width="118" height="135" /></td>
															<td valign="top"><table width="835" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>


																		<td colspan="2"><table border="0" cellspacing="0"
																				cellpadding="0" align="right">
																				<tr>
																					<td width="35" align="right"><img
																						src="${pageContext.request.contextPath}/images/us.png"
																						width="25" height="25" /></td>
																					<td class="logd1" style="text-align: left;"
																						width="150"><%=userName%> is <strong>&nbsp;&nbsp;Registrado en</strong>&nbsp;</td>
																					<td class="logd" width="60">
																						<%-- <a 	href="${pageContext.request.contextPath}/JSP/logout.jsp"
																						class="logd">Logout</a> --%> <a
																						href="javascript:void();"
																						onclick='return logoutUser("jsp");' class="logd">Cerrar sesi�n</a>
																					</td>

																				</tr>
																			</table></td>
																	</tr>
																	<tr>
																		<td style="padding: 2px 0px 0px 20px"><img
																			src="${pageContext.request.contextPath}/images/slogan.png"
																			width="652" height="" /></td>
																		<td style="padding: 8px 10px 5px 20px">
																		<img 	src="${pageContext.request.contextPath}/images/flag.png"
																			width="118" height="" />
																			&nbsp;
																			</td>
																	</tr>
																	<!-- <tr>
																		<td>&nbsp;</td>
																	</tr> -->
																</table></td>
														</tr>
													</table></td>
											</tr>
										</table></td>
								</tr>

							</table></td>
					</tr>
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td>

										<table width="965" border="0" cellspacing="0" cellpadding="0">
											<tr>


												<%
													if (userType != null
															&& (userType.equalsIgnoreCase("admin") || userType
																	.equalsIgnoreCase("fi admin"))) {
												%>
												<td align="center"><ul class="drop_menu_admin">
														<li><a
															href="${pageContext.request.contextPath}/JSP/home.jsp"
															class="menulink"><strong>Casa</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td align="center"><ul class="drop_menu_admin">
														<li><a
															href="${pageContext.request.contextPath}/JSP/admin-approval.jsp"
															class="menulink"><strong>Aprobaci�n del usuario</strong></a></li>
													</ul></td>
												<%-- <td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>

												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/ReportStatus'><Strong>Report
																	Status</Strong></a></li>
													</ul></td> --%>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu_admin">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu_admin">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Cont�ctenos</strong></a></li>
													</ul></td>

												<%
													} else {
												%>

												<td align="center"><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/JSP/home.jsp"
															class="menulink"><strong>Casa</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>

												<td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Enviar Informe</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<%
																	} else if (null != complianceType
																				&& complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	}
																%>


															</ul></li>
													</ul></td>

												<%-- <td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Submit
																	Report</strong></a>
															<ul>
																<li><a
																	href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Upload
																		XML</a></li>
																<li><a href="#">Upload XLS/CSV </a></li>
																<li><a
																	href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																		Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li>
															</ul></li>
													</ul></td> --%>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<%-- <td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/ReportStatus'><strong>Report
																	Status</strong></a></li>
													</ul></td> --%>

												<td><ul id="reportTab" class="reportTab">
														<li><a class="menulink" href="#"><strong>Report
																	Status</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<%
																	} else if (null != complianceType
																				&& complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>
																<%
																	} else {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>

																<%
																	}
																%>

															</ul></li>
													</ul></td>

												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Cont�ctenos</strong></a></li>
													</ul></td>

												<%
													}
												%>


											</tr>

										</table>
									</td>
								</tr>

								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td width="60"></td>
												<td>
													<!-- <div id="tabs"> -->

													<div id="tabs-2">
														<form name="MyInfoEditForm" method="post">
															<input type="hidden" id="giin" name="giin" />
															<p>
																<table width="860" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g-t-m.png"
																			class="bgtext1">Financial Institution Details</td>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td
																			background="${pageContext.request.contextPath}/images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td><table width="840" border="0" cellspacing="0"
																				cellpadding="0" align="center">
																				<tr>
																					<td colspan="5" id="fi_errormsg">&nbsp;</td>
																				</tr>
																				<tr>
																					<td><table width="840" border="0"
																							cellspacing="0" cellpadding="0">


																							<tr>
																								<td>&nbsp;</td>
																								<td colspan="4">&nbsp;</td>
																							</tr>
																							<tr>
																								<td width="100">GIIN/IN<span
																									style="color: #F00;">*</span>
																								</td>
																								<%
																									if ("CRS".equalsIgnoreCase(complianceType)) {
																								%>
																								<td valign="top"><input type="text"
																									name="fi_in" id="fi_in"
																									value="<%=obj.getGiin()%>" class="form-control"
																									style="width: 175px; padding: 0px 2px 2px 5px; text-decoration: none;" />
																									&nbsp;<span style="vertical-align: top;">
																										IN Type</span><span
																									style="color: #F00; vertical-align: top">*</span>
																									&nbsp;&nbsp; <input type="text"
																									name="fi_inType" id="fi_inType"
																									value="<%=obj.getInType()%>"
																									class="form-control"
																									style="width: 150px; padding: 0px 2px 2px 5px; text-decoration: none;" />
																								</td>
																								<%
																									} else {
																								%>
																								<td valign="top"><input type="hidden"
																									name="fi_giin" id="fi_giin" /> <input
																									type="text" name="edit_text_giin1"
																									id="edit_text_giin1" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplhanumeric");'
																									onpaste='return validateInput(this.id);' /> <input
																									type="text" name="edit_text_giin2"
																									id="edit_text_giin2" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplhanumeric");'
																									onpaste='return validateInput(this.id);' /><input
																									type="text" name="edit_text_giin3"
																									id="edit_text_giin3" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplhanumeric");'
																									onpaste='return validateInput(this.id);' /><input
																									type="text" name="edit_text_giin4"
																									id="edit_text_giin4" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplhanumeric");'
																									onpaste='return validateInput(this.id);' /><input
																									type="text" name="edit_text_giin5"
																									id="edit_text_giin5" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplhanumeric");'
																									onpaste='return validateInput(this.id);' /><input
																									type="text" name="edit_text_giin6"
																									id="edit_text_giin6" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplhanumeric");'
																									onpaste='return validateInput(this.id);' />.<input
																									type="text" name="edit_text_giin7"
																									id="edit_text_giin7" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplhanumeric");'
																									onpaste='return validateInput(this.id);' /><input
																									type="text" name="edit_text_giin8"
																									id="edit_text_giin8" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplhanumeric");'
																									onpaste='return validateInput(this.id);' /><input
																									type="text" name="edit_text_giin9"
																									id="edit_text_giin9" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplhanumeric");'
																									onpaste='return validateInput(this.id);' /><input
																									type="text" name="edit_text_giin10"
																									id="edit_text_giin10" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplhanumeric");'
																									onpaste='return validateInput(this.id);' /><input
																									type="text" name="edit_text_giin11"
																									id="edit_text_giin11" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplhanumeric");'
																									onpaste='return validateInput(this.id);' />.<input
																									type="text" name="edit_text_giin12"
																									id="edit_text_giin12" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplha");'
																									onpaste='return validateInput(this.id);' /><input
																									type="text" name="edit_text_giin13"
																									id="edit_text_giin13" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"aplha");'
																									onpaste='return validateInput(this.id);' />.<input
																									type="text" name="edit_text_giin14"
																									id="edit_text_giin14" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"numeric");'
																									onpaste='return validateInput(this.id);' /><input
																									type="text" name="edit_text_giin15"
																									id="edit_text_giin15" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"numeric");'
																									onpaste='return validateInput(this.id);' /><input
																									type="text" name="edit_text_giin16"
																									id="edit_text_giin16" maxlength="1"
																									style="width: 20px; text-align: center;"
																									class="form-control"
																									onkeyup="autoSetFocus(event,this,'giin')"
																									onKeypress='return validateKey(this.id,"numeric");'
																									onpaste='return validateInput(this.id);' /></td>
																								<%
																									}
																								%>
																								<td>&nbsp;</td>
																								<td valign="top" width="100">Name<span
																									style="color: #F00;">*</span>:
																								</td>
																								<td valign="top"><input type="text"
																									name="fi_name" id="fi_name"
																									value="<%=obj.getFiName()%>"
																									class="form-control"
																									style="width: 200px; padding: 0px 2px 2px 5px;" /></td>
																							</tr>
																							<tr>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																							</tr>
																							<tr>
																								<td rowspan="3" valign="top">Direcci�n<span
																									style="color: #F00;">*</span>:
																								</td>
																								<td rowspan="3" valign="top"><textarea
																										id="fi_address" name="fi_address"
																										style="width: 400px; height: 68px; padding: 0px 2px 2px 5px;"
																										class="form-control" maxlength="2000"
																										onKeypress='return validateKey(this.id,event);'
																										onpaste='return validateInput(this.id);'><%=obj.getFiAddress()%></textarea></td>
																								<td>&nbsp;</td>
																								<td valign="top">Nacionalidad<span
																									style="color: #F00;">*</span>:
																								</td>
																								<td valign="top"><input type="text"
																									name="fi_country" id="fi_country"
																									class="form-control"
																									value="<%=obj.getFiCountry()%>"
																									style="width: 200px; padding: 0px 2px 2px 5px;"
																									readonly /></td>
																							</tr>
																							<tr>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																							</tr>
																							<tr>
																								<td>&nbsp;</td>
																								<td valign="top">Identificaci�n de correo<span
																									style="color: #F00;">*</span>:
																								</td>
																								<td valign="top"><input type="text"
																									onblur="return checkFormat(this)"
																									value='<%=obj.getFiEmail()%>' name="fi_email"
																									id="fi_email" class="form-control"
																									style="background: #ffffb3 url(${pageContext.request.contextPath}/images/mail2.png) no-repeat center right;padding:0px 2px 2px 5px; height:25px; width:200px; padding:0px 2px 2px 5px;" /></td>
																							</tr>

																							<tr>
																								<td colspan="5" height="5"></td>
																							</tr>

																						</table></td>
																				</tr>
																			</table></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-l.png"
																			width="9" height="5" /></td>
																		<td
																			style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-r.png"
																			width="9" height="5" /></td>
																	</tr>
																</table>
																<br />
																<table width="860" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g-t-m.png"
																			class="bgtext1">User Details</td>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td
																			background="${pageContext.request.contextPath}/images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td><table width="840" border="0" cellspacing="0"
																				cellpadding="0" align="center">
																				<tr>
																					<td><table width="840" border="0"
																							cellspacing="0" cellpadding="0">
																							<tr>
																								<td colspan="5" id="user_errormsg">&nbsp;</td>
																							</tr>

																							<tr>
																								<td valign="top" width="100">Identificaci�n de correo<span
																									style="color: #F00;">*</span></td>
																								<td><input type="text" name="user_email"
																									readonly="readonly"
																									onblur="return checkFormat(this)"
																									id="user_email" class="form-control"
																									value='<%=obj.getUserEmailId()%>'
																									style="background: #ffffb3 url(${pageContext.request.contextPath}/images/mail2.png) no-repeat center right;padding:0px 2px 2px 5px; height:25px; width:200px; padding:0px 2px 2px 5px;" /></td>
																								<td>&nbsp;</td>
																								<td valign="top" width="100">Nombre<span
																									style="color: #F00;">*</span>:
																								</td>
																								<td valign="top"><input type="text"
																									name="user_name" id="user_name"
																									class="form-control"
																									value='<%=obj.getUserName()%>'
																									style="width: 200px; padding: 0px 2px 2px 5px;"
																									onKeypress='return validateKey(this.id,event);'
																									onpaste='return validateInput(this.id);' /></td>
																							</tr>
																							<tr>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																							</tr>
																							<tr>
																								<td rowspan="3" valign="top">Direcci�n<span
																									style="color: #F00;">*</span>:
																								</td>
																								<td rowspan="3" valign="top"><textarea
																										id="user_address" name="user_address"
																										style="width: 400px; height: 68px;"
																										2000" padding: 0px 2px 2px 5px;"
																											class="form-control"
																										readonly><%=obj.getUserAddress()%></textarea></td>
																								<td>&nbsp;</td>
																								<td valign="top">Nacionalidad<span
																									style="color: #F00;">*</span>:
																								</td>
																								<td valign="top"><input type="text"
																									name="user_country" id="user_country"
																									value="<%=obj.getUserCountry()%>"
																									class="form-control"
																									style="width: 200px; padding: 0px 2px 2px 5px;"
																									readonly /></td>
																							</tr>
																							<tr>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																							</tr>
																							<tr>
																								<td>&nbsp;</td>
																								<td valign="top">Tel�fono<span
																									style="color: #F00;">*</span>:
																								</td>
																								<td valign="top"><input type="text"
																									onblur="return checkFormat(this);"
																									name="user_phone" id="user_phone"
																									class="form-control"
																									value='<%=obj.getUserPhone()%>'
																									style="width: 200px; padding: 0px 2px 2px 5px;"
																									onKeypress='return validateKey(this.id,event);'
																									onpaste='return validateInput(this.id);' /></td>
																							</tr>
																							<tr>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																							</tr>
																							<tr>
																								<td valign="top">TIN<span
																									style="color: #F00;">*</span></td>
																								<td><input name="user_empid"
																									id="user_empid" type="text"
																									class="form-control"
																									value='<%=obj.getUserEmployeeID()%>'
																									style="width: 200px; padding: 0px 2px 2px 5px;"
																									onKeypress='return validateKey(this.id,event);'
																									onpaste='return validateInput(this.id);' /></td>
																								<td>&nbsp;</td>
																								<td valign="top">Posici�n<span
																									style="color: #F00;">*</span>:
																								</td>
																								<td valign="top"><input type="text"
																									name="user_designation" id="user_designation"
																									class="form-control"
																									value='<%=obj.getUserDesignation()%>'
																									style="width: 200px; padding: 0px 2px 2px 5px;" /></td>
																							</tr>
																							<tr>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																								</tr>
																								<tr>
																									<td valign="top">G�nero<span
																										style="color: #F00;">*</span></td>


																									<td><input type="text" name="user_gender"
																										id="user_gender"
																										value="<%=obj.getUserGender()%>"
																										class="form-control"
																										style="width: 200px; padding: 0px 2px 2px 5px;"
																										readonly /> <!-- <select name="user_gender"
																										id="user_gender" class="form-control"
																										style="width: 210px; padding: 0px 2px 2px 5px;">
																											<option value="">---Select---</option>
																											<option value="M">Male</option>
																											<option value="F">Female</option>

																									</select> --></td>
																									<td>&nbsp;</td>
																									<td valign="top">Fecha de nacimiento<span
																										style="color: #F00;">*</span>:
																									</td>
																									<!-- <td valign="top">Position<span
																										style="color: #F00;">*</span>:
																									</td> -->

																									<td><input type="text" name="userDOB"
																										id="userDOB" class="form-control"
																										readonly="readonly"
																										style="padding: 0px 2px 2px 5px; width: 200px;"
																										value="<%=obj.getUserDOB()%>" /></td>
																								</tr>
																							<tr>
																								<td colspan="5" height="5"></td>
																							</tr>

																						</table></td>
																				</tr>
																			</table></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-l.png"
																			width="9" height="5" /></td>
																		<td
																			style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-r.png"
																			width="9" height="5" /></td>
																	</tr>
																</table>
																<br />
																<table width="860" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g-t-m.png"
																			class="bgtext1">Supporting Documents</td>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td
																			background="${pageContext.request.contextPath}/images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td><table width="840" border="0" cellspacing="0"
																				cellpadding="0" align="center">
																				<tr>
																					<td><table width="840" border="0"
																							cellspacing="0" cellpadding="0">

																							<tr>
																								<td valign="top" width="100"><%=obj.getUserDocType1()%><span
																									style="color: #F00;">*</span></td>

																								<td valign="top" width="100"><%=obj.getUserDocType2()%><span
																									style="color: #F00;">*</span></td>

																							</tr>
																							<tr>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>

																							</tr>
																							<tr>
																								<%
																									String docPath1 = obj.getUploadFileLocation() + File.separator
																											+ obj.getUserDocType1File1();

																									String docPath2 = obj.getUploadFileLocation() + File.separator
																											+ obj.getUserDocType2File2();
																								%>
																								<td><a
																									href="${pageContext.request.contextPath}/Download?path=<%=docPath1%>">
																										<img
																										src="${pageContext.request.contextPath}/images/PDF.jpg"
																										width="70" height="70" border="0" />
																								</a></td>

																								<td><a
																									href="${pageContext.request.contextPath}/Download?path=<%=docPath2%>">
																										<img
																										src="${pageContext.request.contextPath}/images/PDF.jpg"
																										width="70" height="70" border="0" />
																								</a></td>
																							</tr>
																							<tr>
																								<td colspan="2" height="5"></td>
																							</tr>

																						</table></td>
																				</tr>
																			</table></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-l.png"
																			width="9" height="5" /></td>
																		<td
																			style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-r.png"
																			width="9" height="5" /></td>
																	</tr>
																</table>
																<br />
																
																<table width="860" border="0" cellspacing="0"
																		cellpadding="0">
																		<tr>
																			<td width="9"><img
																				src="${pageContext.request.contextPath}/images/g-t-l.png"
																				width="9" height="29" /></td>
																			<td
																				background="${pageContext.request.contextPath}/images/g-t-m.png"
																				class="bgtext1">Change Password</td>
																			<td width="9"><img
																				src="${pageContext.request.contextPath}/images/g-t-r.png"
																				width="9" height="29" /></td>
																		</tr>
																		<tr>
																			<td
																				background="${pageContext.request.contextPath}/images/g.png"
																				style="background-repeat: repeat-y;"></td>
																			<td><table width="840" border="0" cellspacing="0"
																		cellpadding="0" align="center">
																		
																		<tr>
																			<!-- <td colspan="5" id="errorAuthPassword">&nbsp;</td> -->
																			<td colspan="5" id="Old_fi_password_errorMsg">&nbsp;</td>
																		</tr>
																		<tr>
																			<td colspan="5" id="">&nbsp;</td>
																		</tr>

																		<tr>

																			<td valign="top" width="125">Introducir contrase�a antigua<span
																				style="color: #F00;">*</span></td>
																			<td><input type="password" class="form-control"
																				name="Old_fi_password" id="Old_fi_password"
																				style="background: white url(${pageContext.request.contextPath}/images/pwd.png) no-repeat center right;padding:0px 2px 2px 5px;height:25px; width:170px;"
																				onKeypress='return validateKey(this.id,event);'
																				onpaste='return validateInput(this.id);' /></td>
																			<!-- onblur="return checkPasswordFromDB()" -->
																		</tr>

																		<tr>
																			<td>&nbsp;</td>

																		</tr>
																		<tr>
																			<td colspan="" id="fi_password_errorMsg">&nbsp;</td>
																			<td colspan="" id="fi_confirm_password2_errorMsg">&nbsp;</td>
																		</tr>
																		<tr>
																			<td colspan="5" id="">&nbsp;</td>
																		</tr>
																		<tr>

																			<td valign="top" width="125">nueva contrase�a<span
																				style="color: #F00;">*</span></td>
																			<!-- onblur="return isConfirmPassword('myEditInformation');" -->
																			<!-- onblur="return isConfirmPassword('myEditInformation');" -->
																			<td><input type="password" class="form-control"
																				name="fi_password" id="fi_password"
																				style="background: white url(${pageContext.request.contextPath}/images/pwd.png) no-repeat center right;padding:0px 2px 2px 5px;height:25px; width:170px;"
																				onKeypress='return validateKey(this.id,event);'
																				onpaste='return validateInput(this.id);' /></td>
																			<td>&nbsp;</td>
																			<td valign="top" width="145">Confirm Password<span
																				style="color: #F00;">*</span></td>
																			<td><input type="password" class="form-control"
																				name="fi_confirm_password2"
																				id="fi_confirm_password2"
																				style="background: white url(${pageContext.request.contextPath}/images/pwd.png) no-repeat center right;padding:0px 2px 2px 5px;height:25px; width:170px;"
																				onKeypress='return validateKey(this.id,event);'
																				onpaste='return validateInput(this.id);' /></td>
																		</tr>
																		<tr>
																			<td>&nbsp;</td>
																			<td>&nbsp;</td>
																			<td>&nbsp;</td>
																			<td>&nbsp;</td>
																			<td>&nbsp;</td>
																		</tr>

																		<tr>
																			<!-- style='display: none' -->
																			<td colspan="5" align="center"><a
																				href="javascript: void();"
																				onclick="return submitform();" name="adminUserEdit"
																				id="adminUserEdit"> <img
																					src="${pageContext.request.contextPath}/images/submit.png"
																					width="65" height="24" border="0" /></a></td>
																		</tr>
																		<tr>
																			<td colspan="5" align="center">&nbsp;</td>
																		</tr>


																	</table></td>
																			<td
																				background="${pageContext.request.contextPath}/images/g1.png"
																				style="background-repeat: repeat-y;"></td>
																		</tr>
																		<tr>
																			<td><img
																				src="${pageContext.request.contextPath}/images/c-b-l.png"
																				width="9" height="5" /></td>
																			<td
																				style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																			<td><img
																				src="${pageContext.request.contextPath}/images/c-b-r.png"
																				width="9" height="5" /></td>
																		</tr>
																	</table>
																
																<%-- <table width="840" border="0" cellspacing="0"
																	cellpadding="0" align="center">
																	<tr>
																		<!-- <td colspan="5" id="errorAuthPassword">&nbsp;</td> -->
																		<td colspan="5" id="Old_fi_password_errorMsg">&nbsp;</td>
																	</tr>
																	<tr>
																		<td colspan="5" id="">&nbsp;</td>
																	</tr>

																	<tr>

																		<td valign="top" width="125">Enter Old Password<span
																			style="color: #F00;">*</span></td>
																		<td><input type="password" class="form-control"
																			name="Old_fi_password" id="Old_fi_password"
																			style="background: white url(${pageContext.request.contextPath}/images/pwd.png) no-repeat center right;padding:0px 2px 2px 5px;height:25px; width:170px;"
																			onKeypress='return validateKey(this.id,event);'
																			onpaste='return validateInput(this.id);'
																			/></td>
																			<!-- onblur="return checkPasswordFromDB()"  -->
																	</tr>

																	<tr>
																		<td>&nbsp;</td>

																	</tr>
																	<tr>
																		<td colspan="" id="fi_password_errorMsg">&nbsp;</td>
																		<td colspan="" id="fi_confirm_password2_errorMsg">&nbsp;</td>
																	</tr>
																	<tr>
																		<td colspan="5" id="">&nbsp;</td>
																	</tr>
																	<tr>

																		<td valign="top" width="125">New Password<span
																			style="color: #F00;">*</span></td>
																		<td><input type="password" class="form-control"
																			name="fi_password" id="fi_password"
																			
																			style="background: white url(${pageContext.request.contextPath}/images/pwd.png) no-repeat center right;padding:0px 2px 2px 5px;height:25px; width:170px;"
																			onKeypress='return validateKey(this.id,event);'
																			onpaste='return validateInput(this.id);' /></td>
																			<!-- onblur="return isConfirmPassword('myEditInformation');" -->
																		<td>&nbsp;</td>
																		<td valign="top" width="145">Confirm Password<span
																			style="color: #F00;">*</span></td>
																		<td><input type="password" class="form-control"
																			name="fi_confirm_password2" id="fi_confirm_password2"
																			style="background: white url(${pageContext.request.contextPath}/images/pwd.png) no-repeat center right;padding:0px 2px 2px 5px;height:25px; width:170px;"
																			
																			onKeypress='return validateKey(this.id,event);'
																			onpaste='return validateInput(this.id);' /></td>
																			<!-- onblur="return isConfirmPassword('myEditInformation');" -->
																	</tr>
																	<tr>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																	</tr>

																	<tr>
																	<!-- style='display: none' -->
																		<td colspan="5" align="center"><a
																			 href="javascript: void();"
																			onclick="return submitform();" name="adminUserEdit"
																			id="adminUserEdit"> <img
																				src="${pageContext.request.contextPath}/images/submit.png"
																				width="65" height="24" border="0" /></a></td>
																	</tr>
																	<tr>
																		<td colspan="5" align="center">&nbsp;</td>
																	</tr>


																</table> --%>
															</p>
														</form>
													</div> <!-- </div> -->
												</td>
												<td width="30"></td>
											</tr>
										</table></td>
								</tr>
								<tr>
									<td height="40">&nbsp;</td>
								</tr>
								<tr>
									<td align="center"><img
										src="${pageContext.request.contextPath}/images/line.png"
										width="921" height="14" /></td>
								</tr>
								<%-- <tr>
									<td class="hedr">For Govt. of <%=com.newgen.bean.GlobalVariables.projectName%>
										Powered by Newgen Software Inc.
									</td>
								</tr> --%>

								<tr>
									<td>&nbsp;</td>
								</tr>
							</table></td>
					</tr>

				</table></td>
			<td background="${pageContext.request.contextPath}/images/bg-g-r.gif"
				height="1" width="19"></td>
		</tr>
	</table>
	<script type="text/javascript">
		var menu1 = new menu.dd("menu1");
		menu1.init("menu", "menuhover");
		var reportMenu = new menu.dd("reportMenu");
		reportMenu.init("reportTab", "menuhover");
	</script>
</body>
<script>
	function encrptPassword() {
		var doc = document;
		try {
			var uiPwd = doc.getElementById("fi_password").value;
			var hashedPwd = CryptoJS.MD5(uiPwd);
			doc.getElementById("fi_password").value = hashedPwd;
		} catch (e) {
			alert("excepci�n:" + e);
		}
	}
</script>
</html>
