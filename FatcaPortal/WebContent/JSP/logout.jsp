<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.newgen.bean.*"%>
<%@page import="java.util.*"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>:: <%=com.newgen.bean.GlobalVariables.projectName%>
	Inland Revenue Department ::
</title>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<script type="text/javascript">
	window.history.forward(-1);
</script>
</head>
<body>
	<%
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", -1);
		if (session != null)
			session.invalidate();

		response.sendRedirect(request.getContextPath());
	%>





</body>
</html>