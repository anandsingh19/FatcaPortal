<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.newgen.bean.GlobalVariables"%>
<%@page import="java.util.Map"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: <%=com.newgen.bean.GlobalVariables.projectName%>
	Inland Revenue Department ::
</title>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<script>
	window.history.forward(-1);
</script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/script.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/Common.js"></script>
</head>
<body>
	<%
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", -1);
		String userType = "";
		String userSessionID = "";
		String userEmail = "";
		String userName = "";
		String complianceType = "";
		HttpSession session1 = request.getSession(false);
		userName = (String) session1.getAttribute("UserName");
		userType = (String) session1.getAttribute("UserType");
		userEmail = (String) session1.getAttribute("UserEmail");
		complianceType = (String) session1.getAttribute("ComplianceType");
		Map<String, String> loginUserMap = GlobalVariables.loginUserMap;
		userSessionID = loginUserMap.get(userEmail);
		//out.println("complianceType--> " + complianceType);
		if (null != userSessionID
				&& (!(userSessionID.equalsIgnoreCase(session1.getId())))
				|| null == userName) {
			response.sendRedirect(request.getContextPath()
					+ "/JSP/SessionExpired.jsp");
		} else if (null == userSessionID) {
			response.sendRedirect(request.getContextPath()
					+ "/JSP/SessionExpired.jsp");
		}
	%>
	<script>
		function uploadManual() {
			//alert();
			document.usermanualform.submit();
			return true;
		}
	</script>
	<table width="1003" border="0" cellspacing="0" cellpadding="0"
		align="center" bgcolor="#FFFFFF">
		<tr>
			<td background="${pageContext.request.contextPath}/images/bg-g-l.gif"
				width="19" height="1"></td>
			<td><table width="965" border="0" cellspacing="0"
					cellpadding="0">
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td>
									<table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td
													background="${pageContext.request.contextPath}/images/bg.png"
													height="140"><table width="965" border="0"
														cellspacing="0" cellpadding="0">
														<tr>
															<td style="padding: 0px 0px 3px 10px" width="130"><img
																src="${pageContext.request.contextPath}/images/logo.png"
																width="118" height="135" /></td>
															<td valign="top"><table width="835" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>
																		<td colspan="2">
																			<table border="0" cellspacing="0" cellpadding="0"
																				align="right">
																				<tr>
																					<td width="35" align="right"><img
																						src="${pageContext.request.contextPath}/images/us.png"
																						width="25" height="25" /></td>
																					<td class="logd1" style="text-align: left;"
																						width="150"><%=userName%> is <strong>&nbsp;&nbsp;Registrado en</strong>&nbsp;</td>
																					<td class="logd" width="60"><a
																						href="javascript:void();"
																						onclick='return logoutUser("jsp");' class="logd">Cerrar sesi�n</a></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 2px 0px 0px 20px"><img
																			src="${pageContext.request.contextPath}/images/slogan.png"
																			width="652" height="" /></td>
																		<td style="padding: 8px 10px 5px 20px">
																		<img 	src="${pageContext.request.contextPath}/images/flag.png"
																			width="118" height="" />
																			&nbsp;
																			</td>
																	</tr>
																	<!-- <tr>
																		<td>&nbsp;</td>
																	</tr><tr>
																		<td>&nbsp;</td>
																	</tr> -->
																</table></td>
														</tr>
													</table></td>
											</tr>
										</table></td>
								</tr>
							</table></td>
					</tr>
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td>
										<table width="965" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<%
													String styleClass ="";
													if (null != userType
															&& (userType.equalsIgnoreCase("admin") || userType
																	.equalsIgnoreCase("fi admin"))) {
														if (userType.equalsIgnoreCase("admin"))//added by shipra
																 styleClass="drop_menu IRDadmin";
														else
																styleClass="drop_menu FIadmin";//END
												%>
												<td align="center"><ul class="<%=styleClass%> selected">
														<li><a href="javascript:void();" class="menulink"><strong>Casa</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td align="center"><ul class="<%=styleClass%>">
														<li><a
															href="${pageContext.request.contextPath}/JSP/admin-approval.jsp"
															class="menulink"><strong>Aprobaci�n del usuario</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>

												<td><ul class="<%=styleClass%>">
														<li><a
															href='${pageContext.request.contextPath}/JSP/customReport.jsp'><Strong>Informe del administrador
																	</Strong></a></li>
													</ul></td>
													<!-- --------------Added by Shipra----------- -->
													<%
													if (null != userType && !( userType.equalsIgnoreCase("fi admin")) && !( userType.equalsIgnoreCase("admin"))) {
													%>
														<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>

												<td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Enviar Informe</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a></li>
																	</ul></li>
																<%
																	} else if (null != complianceType
																				&& complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> </li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																	</ul></li>
																<%
																	}
																%>
															</ul></li>
													</ul></td>
													<%
													}else{
													%>
														<td width="3" style="display:none"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>

												<td style="display:none"><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Enviar Informe</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> </li>
																	</ul></li>
																<%
																	} else if (null != complianceType
																				&& complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> </li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																	</ul></li>
																<%
																	}
																%>
															</ul></li>
													</ul></td>
													<%
													}
													%>
													<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<%-- <td>
													<ul id="reportTab" class="reportTab">
														<li><a class="menulink" href="#"><strong>Report
																	Status</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<%
																	} else if (null != complianceType
																				&& complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>
																<%
																	} else {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>
																<%
																	}
																%>
															</ul></li>
													</ul>
												</td> --%>
												<!-- ------------END---------- -->
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="<%=styleClass%>">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="<%=styleClass%>">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Cont�ctenos</strong></a></li>
													</ul></td>
												<%
													} else {
												%>
												<td align="center"><ul class="drop_menu selected">
														<li><a href="javascript:void();" class="menulink"><strong>Casa</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Enviar Informe</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->

																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<%
																	} else if (null != complianceType
																				&& complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	}
																%>
															</ul></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td>
													<ul id="reportTab" class="reportTab">
														<li><a class="menulink" href="#"><strong>Informe Estado</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<%
																	} else if (null != complianceType
																				&& complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>
																<%
																	} else {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>
																<%
																	}
																%>
															</ul></li>
													</ul>
												</td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Cont�ctenos</strong></a></li>
													</ul></td>
												<%
													}
												%>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td width="25"></td>
												<td width="200" valign="top"><table width="200"
														border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td valign="top"><table width="200" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g-t-m.png"
																			class="bgtext1">Helpful Links</td>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td
																			background="${pageContext.request.contextPath}/images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td><table width="180" border="0" cellspacing="0"
																				cellpadding="0" align="center">
																				<tr>
																					<td><table width="180" border="0"
																							cellspacing="0" cellpadding="0">
																							<%-- <tr>
																								<td class="arrw_txt1"><a
																									href="http://www.irs.gov" target="_blank"
																									class="arrw_txt1">IRS Website</a></td>
																							</tr>
																							<tr>
																								<td class="arrw_txt1"><a
																									href="http://www.ab.gov.ag/" target="_blank"
																									class="arrw_txt1"><%=com.newgen.bean.GlobalVariables.projectName%>-
																										US IGA</a></td>
																							</tr>
																							<tr>
																								<td class="arrw_txt1"><a
																									href="http://www.irs.gov" target="_blank"
																									class="arrw_txt1">XML V1.1 Format</a></td>
																							</tr>
																							<tr>
																								<!-- <td class="arrw_txt">City Maintenance</td> -->
																							</tr> --%>
																							<tr>
																								<td class="arrw_txt1"><a
																									href="<%=GlobalVariables.helpLink1_url%>"
																									target="_blank" class="arrw_txt1"><%=GlobalVariables.helpLink1_label%></a></td>
																							</tr>
																							<tr>
																								<!-- href="http://www.ab.gov.ag/"
																							 target="_blank" -->

																								<td class="arrw_txt1"><a target="_blank"
																									href="<%=GlobalVariables.helpLink2_url%>"
																									class="arrw_txt1"><%=GlobalVariables.helpLink2_label%>
																								</a></td>
																							</tr>
																							<tr>
																								<td class="arrw_txt1"><a
																									href="<%=GlobalVariables.helpLink3_url%>"
																									target="_blank" class="arrw_txt1"><%=GlobalVariables.helpLink3_label%></a></td>
																							</tr>
																					<!-- 	/* Change for All by Shubham on 28072016 'code added' - starts */ -->
																							<tr>
																								<td class="arrw_txt1"><a	
																									href="<%=GlobalVariables.helpLink4_url%>"
																									target="_blank" class="arrw_txt1"><%=GlobalVariables.helpLink4_label%></a></td>
																							</tr>
																							<%-- <tr>
																								<td class="arrw_txt1">
																									<form
																	action="${pageContext.request.contextPath}/CustomReport?reportType=userManual"
																	name="usermanualform" method="post">
																	<a id="usermanuallink" style="text-decoration: none;"
																		href="javascript:void();"
																		onclick=" return uploadManual();"><%=GlobalVariables.helpLink5_label%> </a>
																</form>
																								</td>
																							</tr> --%>
																							<!-- /* Change for All by shubham on 28072016 'code added' - end */ -->
																							<tr>
																								<!-- <td class="arrw_txt">City Maintenance</td> -->
																							</tr>
																						</table></td>
																				</tr>
																			</table></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-l.png"
																			width="9" height="5" /></td>
																		<td
																			style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-r.png"
																			width="9" height="5" /></td>
																	</tr>
																</table></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td><a
																href="${pageContext.request.contextPath}/MyInfoEdit?event=get"><img
																	src="${pageContext.request.contextPath}/images/edit.png"
																	 border="0" /></a> <%-- <a href='${pageContext.request.contextPath}/JSP/MyInfoEdit.jsp'><img
																	src="${pageContext.request.contextPath}/images/edit.png"
																	width="200" height="30" border="0" /></a> --%></td>
														</tr>
														<tr>
															<td>
																<div id="UploadMsg">
																	<%
																		String msg = (String) request.getAttribute("msg");
																		//System.out.println("MSG=" + msg);
																		if (msg == null) {
																			msg = "&nbsp;";
																		}
																	%>
																	<p style='color: green'><%=msg%></p>
																</div>
															</td>

														</tr>
													</table></td>
												<td width="20"></td>
												<td width="695"><table width="695" border="0"
														cellspacing="0" cellpadding="0">
														<tr>
															<td class="bgtext4"><strong><span
																	style="font-size: 20px; font-weight: bold; color: #064557;">Bienvenido al portal de Intercambio Autom�tico
																	 de Informaci�n FATCA de la DGII. 
																</span></strong><br /> <br /> 
																	El Gobierno de Republica Dominicana ha firmado un Acuerdo Intergubernamental para intercambiar 
																	informaci�n basada en las directrices de la FATCA y la CRS. En virtud de la Ley FATCA,
																	 las entidades pertinentes deben presentar archivos sobre las cuentas financieras y los activos
																	  financieros de personas determinadas en los Estados Unidos o compa��as en las que los ciudadanos
																	   estadounidenses tengan una participaci�n mayoritaria o sustancial. Es obligatorio para las instituciones
																	    financieras dependientes del CRS declarar las a los respectivos gobiernos, dicha iniciativa tiene 
																	    como objetivo combatir la evasi�n fiscal y aumentar el cumplimiento voluntario de impuestos. Visita 
																<a href="http://www.dgii.gov.do/Paginas/inicio.aspx"
																style="color: #015f7a;" target="_blank"><span
																	style="color: #015f7a;">www.dgii.gov.do</span></a>, <a href="http://www.dgii.gov.do/Paginas/inicio.aspx"
																style="color: #015f7a;" target="_blank"><span
																	style="color: #015f7a;">www.irs.gov</span></a>
																	  and <a href="https://www.irs.gov/"
																style="color: #015f7a;" target="_blank"><span
																	style="color: #015f7a;">www.oecd.org/tax/automatic-exchange</span></a>.
																	.<br />
																<br />Pulse el botón de Menú para Enviar enviar los datos, ya sea cargando un archivo
																XML o diligenciando  el formulario en línea. Una vez enviado, puede monitorear  el
																estado del informe mediante la pestana  “Estado del informe”, de ser necesario recibirá
																notificaciones  si se requiere aclaraciones/ correcciones futuras. 
																 <br>
																 <br>
																		En la pestaña de  Preguntas Frecuentes encontrara  Manuales de Usuario para su conveniencia. 
																		Para obtener información de contacto, consulte la página de Contáctenos.
																<%-- <a href="mailto:ird@<%=com.newgen.bean.GlobalVariables.projectName%>.gov"
																style="color: #015f7a;"><span
																	style="color: #015f7a;">ird@<%=com.newgen.bean.GlobalVariables.projectName%>.gov
																</span></a> --%>
																</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<%-- <tr>
															<td><video width="600" controls> <source
																	src="${pageContext.request.contextPath}/video/mov_bbb.mp4"
																	type="video/mp4" /> <source
																	src="${pageContext.request.contextPath}/video/mov_bbb.ogg"
																	type="video/ogg" /> Your browser does not support
																HTML5 video. </video></td>
														</tr> --%>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
													</table></td>
												<td width="25">&nbsp;</td>
											</tr>
										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="center"><img
										src="${pageContext.request.contextPath}/images/line.png"
										width="921" height="14" /></td>
								</tr>
								<%-- <tr>
									<td class="hedr">For Govt. of <%=com.newgen.bean.GlobalVariables.projectName%>
										Powered by Newgen Software Inc.
									</td>
								</tr> --%>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</table></td>
					</tr>
				</table></td>
			<td background="${pageContext.request.contextPath}/images/bg-g-r.gif"
				height="1" width="19"></td>
		</tr>
	</table>
	<script type="text/javascript">
		var menu1 = new menu.dd("menu1");
		menu1.init("menu", "menuhover");
		var reportMenu = new menu.dd("reportMenu");
		reportMenu.init("reportTab", "menuhover");
		</script>
</body>
</html>