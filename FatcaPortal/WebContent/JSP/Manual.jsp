
<!-- ------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : Account Registration module
 File Name                                                   : account-registration
 Author                                                      : Khushdil Kaushik
 Date (DD/MM/YYYY)                                           : 16/10/2015
 Description                                                 : FI user/Admin registration Form
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    		Change Description
 11600,11598		27/06/2016     Khushdil Kaushik 	called new function written
11599		27/06/2016     Khushdil Kaushik 	removed href from anchor tag 	
 ------------------------------------------------------------------------------------------------------
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Map"%>
<%@page import="com.newgen.bean.GlobalVariables"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>




<script type="text/javascript">
	window.history.forward(-1);
</script>
<%!String reportingYear = null;
	String submissionType = null;%>

<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", -1);
	String userType = "";
	String userSessionID = "";
	String userEmail = "";
	String userName = "";
	String complianceType = "";
	HttpSession session1 = request.getSession(false);
	userName = (String) session1.getAttribute("UserName");
	userType = (String) session1.getAttribute("UserType");
	userEmail = (String) session1.getAttribute("UserEmail");
	complianceType = (String) session1.getAttribute("ComplianceType");

	Map<String, String> loginUserMap = GlobalVariables.loginUserMap;
	userSessionID = loginUserMap.get(userEmail);

	if (null != userSessionID
			&& (!(userSessionID.equalsIgnoreCase(session1.getId())) || null == userName)) {

		response.sendRedirect(request.getContextPath()
				+ "/JSP/SessionExpired.jsp");

	} else if (null == userSessionID) {
		response.sendRedirect(request.getContextPath()
				+ "/JSP/SessionExpired.jsp");
	}
	reportingYear = (String) request.getAttribute("reportingYear");
	submissionType = (String) request.getAttribute("submissionType");
	// String usersesion = (String) session.getAttribute("UserType");
	//	if (null == usersesion || usersesion.length() == 0) {
	////	response.sendRedirect(request.getContextPath());
	//}
%>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: <%=com.newgen.bean.GlobalVariables.projectName%>
	Inland Revenue Department ::

</title>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/Common.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/jquery-ui.css" />



<%-- <script src="${pageContext.request.contextPath}/script/jquery-1.9.1.js"></script> --%>
<script src="${pageContext.request.contextPath}/script/jquery-1.10.2.js"></script>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/script.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/GlobalVariable.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/Manual.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/Common.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/ManualForNIL.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/jquery-ui.js"></script>
	<script src="${pageContext.request.contextPath}/script/JSMessages.js"></script>



<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->

<script>


$(document).ready(function(){
	//alert("asdasda--aadesh");
	$(".hide_spon,.show_spon").change(function(){
		$('.spon').toggle();
	});	
	$(".hide_inter,.show_inter").change(function(){
		$('.inter').toggle();
		});	
	
	$("#accDOB").datepicker({
        maxDate: '0',
    changeMonth: true,//this option for allowing user to select month
    changeYear: true //this option for allowing user to select from year range
  });
	
});  



/* function toggleSpInter(){
	alert("yea")
$(document).ready(function(){
	//alert("asdasda--aadesh");
	$(".hide_spon,.show_spon").change(function(){
		$('.spon').toggle();
	});	
	$(".hide_inter,.show_inter").change(function(){
		$('.inter').toggle();
		});	
	
});  
} */


</script>

</head>
<body onload="getDataOnLoad();enableButtons();">
	<input type="hidden" name="reportingYear" id="reportingYear"
		value=<%=reportingYear%>>
	<input type="hidden" name="submissionType" id="submissionType"
		value=<%=submissionType%>>
	<input type="hidden" name="isNilReport" id="isNilReport" value="N">
	<%-- <%
		//HttpSession sesion = request.getSession(false);
			String userType = "";
			userType = (String) session.getAttribute("UserType");
			if (null == userType || userType.length() == 0) {
				response.sendRedirect(request.getContextPath());
			}
	%> --%>
	<table width="1003" border="0" cellspacing="0" cellpadding="0"
		align="center" bgcolor="#FFFFFF">
		<tr>
			<td background="${pageContext.request.contextPath}/images/bg-g-l.gif"
				width="19" height="1"></td>
			<td><table width="965" border="0" cellspacing="0"
					cellpadding="0">
					<tr>


						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td
													background="${pageContext.request.contextPath}/images/bg.png"
													height="140"><table width="965" border="0"
														cellspacing="0" cellpadding="0">
														<tr>
															<td style="padding: 0px 0px 3px 10px" width="130"><img
																src="${pageContext.request.contextPath}/images/logo.png"
																width="118" height="135" /></td>
															<td valign="top"><table width="835" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>


																		<td colspan="2"><table border="0" cellspacing="0"
																				cellpadding="0" align="right">
																				<tr>





																					<td width="35" align="right"><img
																						src="${pageContext.request.contextPath}/images/us.png"
																						width="25" height="25" /></td>






																					<td class="logd1" style="text-align: left;"
																						width="150"><%=userName%> is <strong>&nbsp;&nbsp;Registrado en </strong>&nbsp;</td>
																					<td class="logd" width="60"><a
																						href="${pageContext.request.contextPath}/JSP/logout.jsp"
																						class="logd">Cerrar sesi�n</a></td>



																				</tr>




																			</table></td>

																	</tr>
																	<tr>
																		<td style="padding: 2px 0px 0px 20px"><img
																			src="${pageContext.request.contextPath}/images/slogan.png"
																			width="652" height="" /></td>



																		<td style="padding: 8px 10px 5px 20px"><img
																			src="${pageContext.request.contextPath}/images/flag.png"
																			width="118" height="" /> &nbsp;</td>

																	</tr>
																	<!-- <tr>
																		<td>&nbsp;</td>
																	</tr><tr>
																		<td>&nbsp;</td>
																	</tr> -->
																</table></td>
														</tr>
													</table></td>
											</tr>
										</table></td>
								</tr>

							</table></td>
					</tr>
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">

								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td align="center"><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/JSP/home.jsp"
															class="menulink"><strong>Casa</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>



												<%
													if (userType != null && userType.equalsIgnoreCase("admin")) {
												%>
												<td align="center"><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/JSP/view.jsp"
															class="menulink"><strong>Aprobaci�n del usuario</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>



												<%
													} else {
												%>

												<%-- <td><ul class="menu" id="menu">


														<li><a href="#" class="menulink"><strong>Submit
																	Report</strong></a>
															<ul>
																<li><a
																	href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Upload
																		XML</a></li>
																<li><a href="#">Upload XLS/CSV </a></li>


																<li><a href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill Manually Online</a>
																	<!-- <ul>


																		<li><a href="#">Fill New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> -->
																	</li>
															</ul></li>
													</ul></td> --%>

												<td><ul class="menu" id="menu">
														<li><a href="#" style="background-color: #2487a2"
															class="menulink"><strong>Enviar reporte</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<%
																	} else if (null != complianceType
																				&& complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	}
																%>




															</ul></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>

												<%
													}
												%>
												<%-- <td><ul class="drop_menu">


														<li><a href='${pageContext.request.contextPath}/ReportStatus'><strong>Report Status</strong></a></li>
													</ul></td> --%>
												<td><ul id="reportTab" class="reportTab">
														<li><a class="menulink" href="#"><strong>Estado del informe</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<%
																	} else if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>
																<%
																	} else {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>

																<%
																	}
																%>

															</ul></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Cont�ctenos</strong></a></li>
													</ul></td>
											</tr>

										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>

									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td width="25"></td>
												<td valign="top"><table width="900" border="0"
														cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td valign="top"><table width="900" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g-t-m.png"
																			class="bgtext11">Rellenar datos manualmente</td>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td
																			background="${pageContext.request.contextPath}/images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td><table width="882" border="0" cellspacing="0"
																				cellpadding="0">
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td style="text-align: justify;">Formulario en Linea Utilice
																					esta p�gina para enviar su informe llenando
																					el formulario en l�nea </td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td valign="top" colspan="5" align="left"><input
																						type="checkbox" id="nilReportChk"
																						name="nilReportChk"
																						onclick="nilReportCheck(this);" /> Enviar como Informe NIL &nbsp;</td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>



																					<td><table width="882" border="0"
																							cellspacing="0" cellpadding="0">
																							<tr>
																								<td width="9"><img
																									src="${pageContext.request.contextPath}/images/g-t-l.png"
																									width="9" height="29" /></td>
																								<td
																									background="${pageContext.request.contextPath}/images/g-t-m.png"
																									class="bgtext1">PARTE 1: IDENTIFICACI�N DEL DECLARANTE</td>
																								<td width="9"><img
																									src="${pageContext.request.contextPath}/images/g-t-r.png"
																									width="9" height="29" /></td>
																							</tr>
																							<tr>
																								<td
																									background="${pageContext.request.contextPath}/images/g.png"
																									style="background-repeat: repeat-y;"></td>
																								<td><table width="854" border="0"
																										cellspacing="0" cellpadding="0" align="center">
																										<tr>
																											<td><table width="864" border="0"
																													cellspacing="0" cellpadding="0">
																													<tr>
																														<td valign="top"><table width="864"
																																border="0" cellspacing="0"
																																cellpadding="0">
																																<tr>
																																	<td width="351" id="filerGiinErrormsg">&nbsp;</td>
																																	<td width="482">&nbsp;</td>
																																	<td width="31">&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>1. Nombre de Declarante<span
																																		style="color: #F00;">*</span></td>
																																	<td><input type="text"
																																		name="filerName_manual"
																																		id="filerName_manual"
																																		class="form-control"
																																		style="padding: 0px 2px 2px 5px; width: 470px;"
																																		onKeypress='return validateKey(this.id,"aplha");'
																																		onpaste='return validateInput(this.id,event,"alpha");'></td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>2. Direcci�n de Declarante�<span
																																		style="color: #F00;">* <a
																																			href="#" class="tooltip"> <img
																																				src="${pageContext.request.contextPath}/images/whts.png"
																																				width="20" height="20" border="0"><span>
																																					<img class="callout"
																																					src="${pageContext.request.contextPath}/images/callout.gif" />
																																					Direcci�n actual seg�n se indica en la Hora de registro




																																			</span></a> 


																																	</span>
																																	</td>

																																	<td><input type="text"
																																		name="filerAddres_manual"
																																		id="filerAddres_manual"
																																		class="form-control"
																																		style="padding: 0px 2px 2px 5px; width: 470px;"
																																		onKeypress='return validateKey(this.id,"address");'
																																		onpaste='return validateInput(this.id,event,"address");'></td>
																																	<td width="31" align="center">
																																		<%-- <a
																																		href="#"><span
																																			style="padding-top: 0px; vertical-align: text-bottom;"><img
																																				src="${pageContext.request.contextPath}/images/whts.png"
																																				width="20" height="20" border="0"></span>
																																				</a>  --%> &nbsp;
																																	</td>
																																</tr>
																																<tr>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>3. C�digo de Pa�s�<span
																																		style="color: #F00;">*</span></td>




																																	<td>
																																		<!-- Change for all by SHUBHAM on 02082016 'width incresed to 250px' starts -->

																																		<select name="filerCountry_manual"
																																		id="filerCountry_manual"
																																		class="form-control"
																																		style="width: 250px;">

																																			<option value="">---Seleccionar----</option>
																																			<option value="US">US</option>
																																			<option value="IN">IN</option>
																																			<option value="UK">UK</option>
																																	</select> <!-- Change for all by SHUBHAM on 02082016 end -->
																																	</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>4. GIIN <span
																																		style="color: #F00;">* <a
																																			href="#" class="tooltip"> <img
																																				src="${pageContext.request.contextPath}/images/whts.png"
																																				width="20" height="20" border="0"><span>
																																					<img class="callout"
																																					src="${pageContext.request.contextPath}/images/callout.gif" />
																																				Un �nico d�gito de 19 d�gitos
																																				"Global N�mero de Identificaci�n Intermedio"
																																				Emitido por el IRS a cada uno de FATCA registr� FF</span></a>
																																	</span><input type="hidden" name="giin"
																																		id="giin">

																																	</td>
																																	<td><input type="text"
																																		name="textfield" id="text1"
																																		maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />&nbsp;
																																		<input type="text" name="textfield"
																																		id="text2" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />&nbsp;
																																		<input type="text" name="textfield"
																																		id="text3" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />&nbsp;
																																		<input type="text" name="textfield"
																																		id="text4" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />&nbsp;
																																		<input type="text" name="textfield"
																																		id="text5" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />&nbsp;
																																		<input type="text" name="textfield"
																																		id="text6" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />&nbsp;&nbsp;.&nbsp;
																																		<input type="text" name="textfield"
																																		id="text7" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />&nbsp;
																																		<input type="text" name="textfield"
																																		id="text8" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />&nbsp;
																																		<input type="text" name="textfield"
																																		id="text9" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />&nbsp;
																																		<input type="text" name="textfield"
																																		id="text10" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />&nbsp;
																																		<input type="text" name="textfield"
																																		id="text11" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />&nbsp;&nbsp;.&nbsp;
																																		<input type="text" name="textfield"
																																		id="text12" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplha");'
																																		onpaste='return validateInput(this.id,event,"alpha");' />&nbsp;
																																		<input type="text" name="textfield"
																																		id="text13" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"aplha");'
																																		onpaste='return validateInput(this.id,event,"alpha");' />&nbsp;&nbsp;.&nbsp;
																																		<input type="text" name="textfield"
																																		id="text14" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"numeric");'
																																		onpaste='return validateInput(this.id,event,"numeric");' />&nbsp;
																																		<input type="text" name="textfield"
																																		id="text15" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"numeric");'
																																		onpaste='return validateInput(this.id,event,"numeric");' />&nbsp;
																																		<input type="text" name="textfield"
																																		id="text16" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'giin')"
																																		onKeypress='return validateGiinField(this.id,"numeric");'
																																		onpaste='return validateInput(this.id,event,"numeric");' /></td>
																																	<td width="30" align="center">&nbsp;
																																		<%-- <a

																																		href="#" class="tooltip">

																																			<img 
																																				src="${pageContext.request.contextPath}/images/whts.png"
																																				width="20" height="20" border="0"><span>
																								<img class="callout"
																								src="${pageContext.request.contextPath}/images/callout.gif" />
																								<strong>Text</strong><br /> Lorem Ipsum is
																								simply dummy text of the printing and
																								typesetting industry. Lorem Ipsum has been the
																								industry's standard dummy text ever since the
																								1500s, when an unknown printer took a galley of
																								type and scrambled it to make a type specimen
																								book. It has survived not only five centuries,
																								but also the leap into electronic typesetting,
																								remaining essentially unchanged.
																						</span></a> --%>
																																	</td>
																																</tr>
																																<tr>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>5. Categor�a de Declarante<span
																																		style="color: #F00;">*</span></td>
																																	<td>
																																		<!-- Change for all by shubham on 02082016 'width incresed to 250px' starts -->
																																		<!-- changes done by shubham on 12 06 2017 for filer category -->
																																		<select name="filerCategory_manual"
																																		id="filerCategory_manual"
																																		class="form-control"
																																		style="width: 250px;">
																																			<option value="">---Seleccionar----</option>
																																			<option value="FATCA 601">FATCA
																																				601</option>
																																			<option value="FATCA 602">FATCA
																																				602</option>
																																			<option value="FATCA 603">FATCA
																																				603</option>
																																			<option value="FATCA 604">FATCA
																																				604</option>
																																			<option value="FATCA 605">FATCA
																																				605</option>
																																			<option value="FATCA 606">FATCA
																																				606</option>
																																	</select> <!-- Change for all by KD on 02082016 end -->

																																	</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>6. �Es Entidad Patrocinada?</td>
																																	<td><input type="radio"
																																		name="isSponsoredEntity"
																																		id="sponsoredEntity_yes" value="yes"
																																		onclick="displayModules('isSponsoredEntity')"
																																		class="show_spon" />Si&nbsp; <input
																																		type="radio" name="isSponsoredEntity"
																																		id="sponsoredEntity_no"
																																		checked="checked" value="no"
																																		onclick="displayModules('isSponsoredEntity');"
																																		class="hide_spon" />No</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>

																																<tr class="spon" style="display: none;">
																																	<td>&bull; GIIN <span
																																		style="color: #F00;">*</span><input
																																		type="hidden" name="sponEntitygiin"
																																		id="sponEntitygiin"></td>
																																	<td><input type="text"
																																		name="textfield" id="text_giin1"
																																		maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin2" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin3" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin4" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin5" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin6" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"aplhanumeric");' />.
																																		<input type="text" name="textfield"
																																		id="text_giin7" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin8" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin9" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin10" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin11" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />.
																																		<input type="text" name="textfield"
																																		id="text_giin12" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplha");'
																																		onpaste='return validateInput(this.id,event,"alpha");' />
																																		<input type="text" name="textfield"
																																		id="text_giin13" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"aplha");'
																																		onpaste='return validateInput(this.id,event,"alpha");' />.
																																		<input type="text" name="textfield"
																																		id="text_giin14" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"numeric");'
																																		onpaste='return validateInput(this.id,event,"numeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin15" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"numeric");'
																																		onpaste='return validateInput(this.id,event,"numeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin16" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'sponEntitygiin')"
																																		onKeypress='return validateGiinField(this.id,"numeric");'
																																		onpaste='return validateInput(this.id,event,"numeric");' />
																																		<a href="javascript:void();"
																																		onclick="return validateFSI_Giin('Sponsor');"><img
																																			src="${pageContext.request.contextPath}/images/search.png"
																																			width="31" height="24" border="0"
																																			style="vertical-align: middle;" /></a></td>
																																	<td>&nbsp;</td>
																																</tr>


																																<tr class="spon" style="display: none;">
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>

																																</tr>



																																<tr class="spon" style="display: none;">
																																	<td>&bull; Nombre de Entidad Patrocinada <span style="color: #F00;">*
																																			<a class="tooltip"> <img
																																				src="${pageContext.request.contextPath}/images/whts.png"
																																				width="20" height="20" border="0"><span>
																																					<img class="callout"
																																					src="${pageContext.request.contextPath}/images/callout.gif" />
																																					En caso de que la cuenta pertenezca a un FI patrocinado


																																			</span></a>

																																	</span>
																																	</td>
																																	<td><input type="text"
																																		name="sponsoredEntityName"
																																		id="sponsoredEntityName"
																																		class="form-control readOnlyClass"
																																		style="padding: 0px 2px 2px 5px; width: 470px;"
																																		onKeypress='return validateKey(this.id,"aplha");'
																																		onpaste='return validateInput(this.id,event,"alpha");'></td>
																																	<td width="31" align="center">&nbsp;
																																		<%-- <a
																																		href="#"><span
																																			style="padding-top: 0px; vertical-align: text-bottom;"><img
																																				src="${pageContext.request.contextPath}/images/whts.png"
																																				width="20" height="20" border="0"></span>
																																				</a> --%>
																																	</td>
																																</tr>
																																<tr class="spon" style="display: none;">
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr class="spon" style="display: none;">
																																	<td>&bull; Direcci�n de Entidad Patrocinada<span style="color: #F00;">*</span>
																																	</td>
																																	<td><input type="text"
																																		name="sponEntityAddress"
																																		id="sponEntityAddress"
																																		class="form-control readOnlyClass"
																																		style="padding: 0px 2px 2px 5px; width: 470px;"
																																		onKeypress='return validateKey(this.id,"address");'
																																		onpaste='return validateInput(this.id,event,"address");'></td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr class="spon" style="display: none;">
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr class="spon" style="display: none;">
																																	<td>&bull; Filer Categor�a de Entidad Patrocinada<span
																																		style="color: #F00;">*</span>
																																	</td>
																																	<td>
																																	<!-- Change for all by KD on 02082016 'width incresed to 250px' starts -->
																																	<!-- changes done by shubham on 12 06 2017 for filer category of sponsor -->
																																	<select name=sponEntityFilerCategory
																																		id="sponEntityFilerCategory"
																																		class="form-control"
																																		style="width: 250px;">
																																			<option value="">------Seleccionar-------</option>
																																			<option value="FATCA 607">FATCA 607</option>
																																			<option value="FATCA 608">FATCA 608</option>
																																			<option value="FATCA 609">FATCA 609</option>

																																	</select>
																																	<!-- Change for all by KD on 02082016 end -->
																																	
																																	</td>
																																	
																																</tr>
																																
																																<tr class="spon" style="display: none;">
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr class="spon" style="display: none;">
																																	<td>&bull; C�digo de Pa�s de Entidad Patrocinada <span
																																		style="color: #F00;">*</span>
																																	</td>
																																	<td>
																																	<!-- Change for all by KD on 02082016 'width incresed to 250px' starts -->
																																	
																																	<select name=sponEntityCountry
																																		id="sponEntityCountry"
																																		class="form-control"
																																		style="width: 250px;">
																																			<option value="">---Seleccionar-----</option>

																																	</select>
																																	<!-- Change for all by KD on 02082016 end -->
																																	
																																	</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr class="spon" style="display: none;">
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																
																																
																																</tr>


																																<tr>
																																	<td>7. �Es Intermediario?</td>
																																	<td><input type="radio"
																																		name="isIntermediary"
																																		id="isIntermediary_yes" value="yes"
																																		onclick="displayModules('isIntermediary')"
																																		class="show_inter" />Si&nbsp; <input
																																		type="radio" name="isIntermediary"
																																		id="intermediary_no" value="no"
																																		checked="checked"
																																		onclick="displayModules('isIntermediary')"
																																		class="hide_inter" />No</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>

																																<tr class="inter" style="display: none;">
																																	<td>&bull; GIIN <span
																																		style="color: #F00;">*</span><input
																																		type="hidden" name="interEntityGiin"
																																		id="interEntityGiin"></td>
																																	<td><input type="text"
																																		name="textfield" id="text_giin_inter1"
																																		maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin_inter2" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin_inter3" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin_inter4" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin_inter5" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin_inter6" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />.
																																		<input type="text" name="textfield"
																																		id="text_giin_inter7" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin_inter8" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin_inter9" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin_inter10" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin_inter11" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplhanumeric");'
																																		onpaste='return validateInput(this.id,event,"alphanumeric");' />.
																																		<input type="text" name="textfield"
																																		id="text_giin_inter12" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplha");'
																																		onpaste='return validateInput(this.id,event,"alpha");' />
																																		<input type="text" name="textfield"
																																		id="text_giin_inter13" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"aplha");'
																																		onpaste='return validateInput(this.id,event,"alpha");' />.
																																		<input type="text" name="textfield"
																																		id="text_giin_inter14" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"numeric");'
																																		onpaste='return validateInput(this.id,event,"numeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin_inter15" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"numeric");'
																																		onpaste='return validateInput(this.id,event,"numeric");' />
																																		<input type="text" name="textfield"
																																		id="text_giin_inter16" maxlength="1"
																																		style="width: 20px; text-align: center;"
																																		class="form-control"
																																		onkeyup="autoSetFocus2(event,this,'interEntityGiin')"
																																		onKeypress='return validateGiinField(this.id,"numeric");'
																																		onpaste='return validateInput(this.id,event,"numeric");' />
																																		<a href="javascript:void();"
																																		onclick="return validateFSI_Giin('Intermediary');"><img
																																			src="${pageContext.request.contextPath}/images/search.png"
																																			width="31" height="24" border="0"
																																			style="vertical-align: middle;" /></a></td>
																																	<td>&nbsp;</td>
																																</tr>

																																<tr class="inter" style="display: none;">
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>

																																<tr class="inter" style="display: none;">
																																	<td>&bull; Nombre de Intermediario�<span
																																		style="color: #F00;">* <a
																																			class="tooltip"> <img
																																				src="${pageContext.request.contextPath}/images/whts.png"
																																				width="20" height="20" border="0"><span>
																																					<img class="callout"
																																					src="${pageContext.request.contextPath}/images/callout.gif" />
																																					En caso de que la cuenta pertenezca a un Intermediario


																																			</span></a>
																																	</span></td>
																																	<td><input type="text"
																																		name="interEntityName"
																																		id="interEntityName"
																																		class="form-control readOnlyClass"
																																		style="padding: 0px 2px 2px 5px; width: 470px;"
																																		onKeypress='return validateKey(this.id,"aplha");'
																																		onpaste='return validateInput(this.id,event,"alpha");'></td>
																																	<td width="31" align="center">&nbsp;
																																		<%-- <a
																																		href="#"><span
																																			style="padding-top: 0px; vertical-align: text-bottom;"><img
																																				src="${pageContext.request.contextPath}/images/whts.png"
																																				width="20" height="20" border="0"></span></a> --%>
																																	</td>
																																</tr>
																																<tr class="inter" style="display: none;">
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr class="inter" style="display: none;">
																																	<td>&bull; Direcci�n de Intermediario
																																		<span style="color: #F00;">*</span>
																																	</td>
																																	<td><input type="text"
																																		name="interEntityAddress"
																																		id="interEntityAddress"
																																		class="form-control readOnlyClass"
																																		style="padding: 0px 2px 2px 5px; width: 470px;"
																																		onKeypress='return validateKey(this.id,"address");'
																																		onpaste='return validateInput(this.id,event,"address");'></td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr class="inter" style="display: none;">
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr class="inter" style="display: none;">
																																	<td>&bull; C�digo de Pa�s de Intermediario<span
																																		style="color: #F00;">*</span>
																																	</td>
																																	<td>
																																	<!-- Change for all by KD on 02082016 'width incresed to 250px' starts -->
																																	
																																	<select
																																		name="interEntityCountry"
																																		id="interEntityCountry"
																																		class="form-control"
																																		style="width: 250px;">
																																			<option value="">---Seleccionar-----</option>
																																			<option value="USA">USA</option>
																																			<option value="IN">IN</option>
																																	</select>
																																	<!-- Change for all by KD on 02082016 end-->
																																	
																																	</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr class="inter" style="display: none;">
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>





																															</table></td>
																													</tr>
																												</table></td>
																										</tr>
																									</table></td>
																								<td
																									background="${pageContext.request.contextPath}/images/g1.png"
																									style="background-repeat: repeat-y;"></td>
																							</tr>
																							<tr>
																								<td colspan="3"
																									style="background-image:url(${pageContext.request.contextPath}/images/g21.png); background-repeat:repeat-x;"
																									height="1"></td>
																							</tr>
																						</table></td>
																				</tr>

																				<tr>
																					<td>&nbsp;</td>
																				</tr>




																				<tr>



																					<td><div id="acctHolder_div">
																							<table width="882" border="0" cellspacing="0"
																								cellpadding="0">
																								<tr>


																									<td width="9"><img
																										src="${pageContext.request.contextPath}/images/g-t-l.png"
																										width="9" height="29" /></td>
																									<td
																										background="${pageContext.request.contextPath}/images/g-t-m.png"
																										class="bgtext1">PARTE 2: INFORMACI�N DE TITULAR DE CUENTA O BENEFICIARIO</td>
																									<td width="9"><img
																										src="${pageContext.request.contextPath}/images/g-t-r.png"
																										width="9" height="29" /></td>
																								</tr>
																								<tr>
																									<td
																										background="${pageContext.request.contextPath}/images/g.png"
																										style="background-repeat: repeat-y;"></td>
																									<td><table width="854" border="0"
																											cellspacing="0" cellpadding="0"
																											align="center">
																											<tr>


																												<td><table width="864" border="0"
																														cellspacing="0" cellpadding="0">
																														<tr>

																															<td valign="top"><table width="864"
																																	border="0" cellspacing="0"
																																	cellpadding="0">
																																	<tr>
																																		<td width="351"
																																			id="isIndividualOREntityErrorMsg">&nbsp;</td>
																																		<td width="482">&nbsp;</td>
																																		<td width="31">&nbsp;</td>
																																	</tr>
																																	<tr>


																																		<td>TIPO DE TITULAR</td>
																																		<td><input type="radio"
																																			name="isIndividualOREntity"
																																			id="individual_radio"
																																			value="Individual" checked="true"
																																			onclick="Check_AccountHolder_Type('Individual');" />Individuo&nbsp;<input
																																			type="radio"
																																			name="isIndividualOREntity"
																																			id="Entity_radio" value="Entity"
																																			onclick="Check_AccountHolder_Type('Entity');" />Entidad</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>



																																		<td width="351">&nbsp;</td>
																																		<td width="482">&nbsp;</td>
																																		<td width="31">&nbsp;</td>
																																	</tr>



																																	<tr>

																																		<td width="351">&nbsp;</td>
																																		<td width="482">&nbsp;</td>
																																		<td width="31">&nbsp;</td>
																																	</tr>


																																	<tr>
																																		<td>1. Nombre� <span
																																			style="color: #F00;">*</span></td>
																																		<td><input type="text"
																																			name="IndividualOREntityName"
																																			id="IndividualOREntityName"
																																			class="form-control"
																																			style="padding: 0px 2px 2px 5px; width: 470px;"
																																			onKeypress='return validateKey(this.id,"aplha");'
																																			onpaste='return validateInput(this.id,event,"alpha");'></td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>2. Direcci�n <span
																																			style="color: #F00;">* </span> <%--<a
																																		 class="tooltip"> <img
																																			src="${pageContext.request.contextPath}/images/whts.png"
																																			width="20" height="20" border="0"><span>
																																				<img class="callout"
																																				src="${pageContext.request.contextPath}/images/callout.gif" />
																																				Address of the account holder whose
																																				data is being submitted. Same as in
																																				the books of FI
																																		</span></a>--%>

																																		</td>


																																		<td><input type="text"
																																			name="IndividualOREntityAddress"
																																			id="IndividualOREntityAddress"
																																			class="form-control"
																																			style="padding: 0px 2px 2px 5px; width: 470px;"
																																			onKeypress='return validateKey(this.id,"address");'
																																			onpaste='return validateInput(this.id,event,"address");'></td>
																																		<td width="31" align="center">&nbsp;

																																		</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>3. C�digo de Pa�s&nbsp;<span
																																			style="color: #F00;">*</span></td>
																																		<td>
																																			<!-- Change for all by KD on 02082016 'width incresed to 250px' starts -->
																																			<select
																																			name="IndividualOREntityCountry"
																																			id="IndividualOREntityCountry"
																																			class="form-control"
																																			style="width: 250px;">
																																				<option value="USA">USA</option>
																																				<option value="IN">IN</option>

																																		</select> <!-- Change for all by KD on 02082016 end -->
																																		</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>










																																		<td>4. TIN&nbsp;<span
																																			style="color: #F00;">* </span> <a
																																			class="tooltip"> <img
																																				src="${pageContext.request.contextPath}/images/whts.png"
																																				width="20" height="20" border="0"><span>
																																					<img class="callout"
																																					src="${pageContext.request.contextPath}/images/callout.gif" />
																																					N�mero de identificaci�n fiscal emitido Cada contribuyente
																																					por su impuesto autoridad. Utilice el formato '123456789' solamente.
																																			</span></a>
																																		</td>



																																		<!-- to fix tin length to 9 changes done by ; on 12 06 2017-->
																																		<td><input type="text"
																																			name="IndividualOREntityTin"
																																			id="IndividualOREntityTin"
																																			class="form-control"
																																			style="padding: 0px 2px 2px 5px; width: 470px;"
																																			onKeypress='return validateKey(this.id,"numeric");'
																																			onpaste='return validateInput(this.id,event,"numeric");'></td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>








																																		<td>5. Fecha de Nacimiento (o Fecha de Incorporaci�n)&nbsp;<span
																																			style="color: #F00;">* </span>
																																		</td>

																																		<td><input type="text"
																																			name="accDOB" id="accDOB"
																																			class="form-control"
																																			readonly="readonly"
																																			style="padding: 0px 2px 2px 5px; width: 470px;"
																																			onKeypress='return validateKey(this.id,"aplhanumeric");'
																																			onpaste='return validateInput(this.id,event,"alphanumeric");'></td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>








																																		<td>6. ID de Cliente o CIF</td>
																																		<td><input type="text"
																																			name="AccHolderCIF" id="AccHolderCIF"
																																			class="form-control"
																																			style="padding: 0px 2px 2px 5px; width: 470px;"
																																			onKeypress='return validateKey(this.id,"alpha");'
																																			onpaste='return validateInput(this.id,event,"alpha");'></td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr id="entityType_tr"
																																		style="display: none;">








																																		<td>7. Tipo de Entidad:&nbsp;<span
																																			style="color: #F00;">* </span>

																																		</td>
																																		<td>
																																			<!-- Change for all by KD on 02082016 'width incresed to 250px' starts -->
																																			<select
																																			name="ActHolderEntityType_manual"
																																			id="ActHolderEntityType_manual"
																																			class="form-control"
																																			style="width: 250px;">
																																				<option value="">--Seleccionar-----</option>
																																				<option value="Owner-Document FFI">Documento de propietario
																																					FFI</option>

																																				<option value="Owner-Document FFI2">Documento de propietario
																																					FFI2</option>

																																				<option value="Owner-Document FFI3">Documento de propietario
																																					FFI3</option>
																																		</select> <!-- Change for all by KD on 02082016 end -->
																																		</td>


																																		<td width="31" align="center">&nbsp;
																																			<%-- <a

																																		href="#"><span
																																			style="padding-top: 0px; vertical-align: text-bottom;"><img
																																				src="${pageContext.request.contextPath}/images/whts.png"
																																				width="20" height="20" border="0"></span></a> --%>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																</table></td>
																														</tr>
																													</table></td>
																											</tr>











																										</table></td>
																									<td
																										background="${pageContext.request.contextPath}/images/g1.png"
																										style="background-repeat: repeat-y;"></td>
																								</tr>
																								<tr>


																									<td colspan="3"
																										style="background-image:url(${pageContext.request.contextPath}/images/g21.png); background-repeat:repeat-x;"
																										height="1"></td>
																								</tr>
																							</table>
																						</div></td>






																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td>
																						<div id="sustantial_Owner_Div">

																							<table width="882" border="0" cellspacing="0"
																								cellpadding="0">
																								<tr>
																									<td width="9"><img
																										src="${pageContext.request.contextPath}/images/g-t-l.png"
																										width="9" height="29" /></td>
																									<td
																										background="${pageContext.request.contextPath}/images/g-t-m.png"
																										class="bgtext1">PART 3: INFORMACI�N DE CLIENTE</td>
																									<td width="9"><img
																										src="${pageContext.request.contextPath}/images/g-t-r.png"
																										width="9" height="29" /></td>
																								</tr>
																								<tr>
																									<td
																										background="${pageContext.request.contextPath}/images/g.png"
																										style="background-repeat: repeat-y;"></td>
																									<td><table width="854" border="0"
																											cellspacing="0" cellpadding="0"
																											align="center">
																											<tr>
																												<td><table width="864" border="0"
																														cellspacing="0" cellpadding="0">
																														<tr>
																															<td valign="top"><table width="864"
																																	border="0" cellspacing="0"
																																	cellpadding="0">
																																	<tr>
																																		<td width="351" id="accErrorMsg">&nbsp;</td>
																																		<td width="482">&nbsp;</td>
																																		<td width="31">&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>1. Nro. De Cuenta<span
																																			style="color: #F00;">*</span></td>
																																		<td>
																																			<!-- <input type="text"
																																			name="user" class="form-control"
																																			/ style="padding: 0px 2px 2px 5px; width: 10px;"> -->
																																			<input type="text"
																																			name="finance_ActNumber"
																																			id="finance_ActNumber"
																																			class="form-control"
																																			style="padding: 0px 2px 2px 5px; width: 450px;"
																																			onKeypress='return validateKey(this.id,"aplhanumeric");'
																																			onpaste='return validateInput(this.id,event,"alphanumeric");'>
																																		</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>2. C�digo de Moneda <span
																																			style="color: #F00;">*</span></td>
																																		<td>
																																			<!-- Change for all by KD on 02082016 'width incresed to 250px' starts -->

																																			<select name="finance_Country"
																																			id="finance_Country"
																																			class="form-control"
																																			style="width: 250px;">
																																				<option value="">------Seleccionar----</option>
																																				<option value="INR">INR</option>
																																				<option value="DO">DOLLAR</option>

																																		</select> <!-- Change for all by KD on 02082016 end -->
																																		</td>
																																		<td width="31" align="center">&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>3. Saldo de Cuenta <span
																																			style="color: #F00;">*</span></td>
																																		<td><input type="text"
																																			name="finance_balance"
																																			id="finance_balance"
																																			class="form-control"
																																			onblur='return vaidateDecimal(this.id);'
																																			style="padding: 0px 2px 2px 5px; width: 470px;"
																																			onKeyPress='return fun_AllowOnlyAmountAndDot(this.id);'
																																			onpaste='return validateInput(this.id,event,"float");' />
																																		</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>4a. Inter�s</td>
																																		<td><input type="text"
																																			name="finance_Interest"
																																			id="finance_Interest"
																																			class="form-control"
																																			placeholder="0.00"
																																			onblur='return vaidateDecimal(this.id);'
																																			style="padding: 0px 2px 2px 5px; width: 470px;"
																																			onKeypress='return fun_AllowOnlyAmountAndDot(this.id);'
																																			onpaste='return validateInput(this.id,event,"float");'></td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>4b. Dividendos</td>
																																		<td><input type="text"
																																			name="finance_Dividend"
																																			id="finance_Dividend"
																																			class="form-control"
																																			placeholder="0.00"
																																			onblur='return vaidateDecimal(this.id);'
																																			style="padding: 0px 2px 2px 5px; width: 470px;"
																																			onKeypress='return fun_AllowOnlyAmountAndDot(this.id);'
																																			onpaste='return validateInput(this.id,event,"float");'></td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>4c. Ingresos Brutos/Reembolsos </td>
																																		<td><input type="text"
																																			name="finance_Redemption"
																																			id="finance_Redemption"
																																			class="form-control"
																																			placeholder="0.00"
																																			onblur='return vaidateDecimal(this.id);'
																																			style="padding: 0px 2px 2px 5px; width: 470px;"
																																			onKeypress='return fun_AllowOnlyAmountAndDot(this.id);'
																																			onpaste='return validateInput(this.id,event,"float");'></td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>4d. Otros</td>
																																		<td><input type="text"
																																			name="finance_Others"
																																			id="finance_Others"
																																			class="form-control"
																																			placeholder="0.00"
																																			onblur='return vaidateDecimal(this.id);'
																																			style="padding: 0px 2px 2px 5px; width: 470px;"
																																			onKeypress='return fun_AllowOnlyAmountAndDot(this.id);'
																																			onpaste='return validateInput(this.id,event,"float");'></td>
																																		<td>&nbsp;</td>
																																	</tr>
																																	<tr>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																		<td>&nbsp;</td>
																																	</tr>
																																</table></td>
																														</tr>
																													</table></td>
																											</tr>
																										</table></td>
																									<td
																										background="${pageContext.request.contextPath}/images/g1.png"
																										style="background-repeat: repeat-y;"></td>
																								</tr>
																								<tr>
																									<td colspan="3"
																										style="background-image:url(${pageContext.request.contextPath}/images/g21.png); background-repeat:repeat-x;"
																										height="1"></td>
																								</tr>
																							</table>

																						</div>

																					</td>
																				</tr>


																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr id="Part3_tr" style="display: none;">
																					<td>


																						<table width="882" border="0" cellspacing="0"
																							cellpadding="0">
																							<tr>
																								<td width="9"><img
																									src="${pageContext.request.contextPath}/images/g-t-l.png"
																									width="9" height="29" /></td>
																								<td
																									background="${pageContext.request.contextPath}/images/g-t-m.png"
																									class="bgtext1">PARTE 4: IDENTIFICACI�N INFORMACI�N DE LOS
																									PROPIETARIOS USADOS QUE SE ESPECIFICAN PERSONAS DE
																									LOS ESTADOS UNIDOS (PROPIETARIO SUBSTANCIAL)</td>
																								<td width="9"><img
																									src="${pageContext.request.contextPath}/images/g-t-r.png"
																									width="9" height="29" /></td>
																							</tr>
																							<tr>
																								<td
																									background="${pageContext.request.contextPath}/images/g.png"
																									style="background-repeat: repeat-y;"></td>
																								<td><table width="854" border="0"
																										cellspacing="0" cellpadding="0" align="center">
																										<tr>
																											<td><table width="864" border="0"
																													cellspacing="0" cellpadding="0">
																													<tr>
																														<td valign="top"><table width="854"
																																border="0" cellspacing="0"
																																cellpadding="0">
																																<tr>
																																	<td width="351" id="errorMsgEntitySo">&nbsp;</td>
																																	<td width="482">&nbsp;</td>
																																	<td width="31">&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>1. Nombre del propietario&nbsp;<span
																																		style="color: #F00;">* </span></td>
																																	<td><input type="text"
																																		id="OwnerName_manual"
																																		name="OwnerName_manual"
																																		class="form-control"
																																		style="padding: 0px 2px 2px 5px; width: 470px;"
																																		onKeypress='return validateKey(this.id,"alpha");'
																																		onpaste='return validateInput(this.id);'></td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>2. Direcci�n&nbsp;<span
																																		style="color: #F00;">* </span></td>
																																	<td><input type="text"
																																		name="OwnerAddress_manual"
																																		id="OwnerAddress_manual"
																																		class="form-control"
																																		style="padding: 0px 2px 2px 5px; width: 470px;"
																																		onKeypress='return validateKey(this.id,"address");'
																																		onpaste='return validateInput(this.id,event,"address");'></td>
																																	<td>&nbsp;</td>
																																	<%-- <td width="31" align="center"><a
																																		href="#"><span
																																			style="padding-top: 0px; vertical-align: text-bottom;"><img
																																				src="${pageContext.request.contextPath}/images/whts.png"
																																				width="20" height="20" border="0"></span></a></td> --%>
																																</tr>
																																<tr>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>3. C�digo de pa�s&nbsp;<span
																																		style="color: #F00;">* </span></td>
																																	<td>
																																		<!-- Change for all by KD on 02082016 'width incresed to 250px' starts -->
																																		<select name="OwnerCountry_manual"
																																		id="OwnerCountry_manual"
																																		class="form-control"
																																		style="width: 250px;">
																																			<option value="1206">1206</option>
																																			<option value="1207">1207</option>
																																			<option value="1208">1208</option>
																																	</select> <!-- Change for all by KD on 02082016 end -->
																																	</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																																<!-- validation on tin by shubham 12 06 2017 -->
																																<tr>
																																	<td>4. TIN del propietario&nbsp;<span
																																		style="color: #F00;">* </span></td>
																																	<td><input type="text"
																																		name="OwnerTin_manual"
																																		id="OwnerTin_manual"
																																		class="form-control"
																																		style="padding: 0px 2px 2px 5px; width: 470px;"
																																		onKeypress='return validateKey(this.id,"numeric");'
																																		onpaste='return validateInput(this.id,event,"numeric");' /></td>
																																	<td>&nbsp;</td>
																																</tr>
																																<tr>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																	<td>&nbsp;</td>
																																</tr>
																															</table></td>
																													</tr>
																												</table></td>
																										</tr>
																									</table> <!-- <tr id="substantial_table_tr" style="display: none;"> -->
																									<div id="substantial_table_tr"
																										style="display: none;">
																										<table width="854" border="0"
																											style="margin-left: 8px" cellspacing="0"
																											cellpadding="0">
																											<tr>
																												<td>&nbsp;</td>
																											</tr>
																											<!-- <tr>
																								<td>&nbsp;</td>
																							</tr> -->
																											<tr>
																												<td align="left"
																													style="padding: 10px 10px 10px 0px"><a
																													id="addEntity" href="javascript: void();"
																													onclick="return addGridData('SubstantialOwner');"><img
																														src="${pageContext.request.contextPath}/images/add.png"
																														width="34" height="20" border="0" /></a>&nbsp;<a
																													id="modifyEntity" href="javascript:void();"
																													onclick="return modifiedRows('ownerList_table','ownerListtableRadio');"><img
																														src="${pageContext.request.contextPath}/images/modify.png"
																														width="100" height="20" border="0" /></a>&nbsp;<a
																													id="deleteEntity" href="javascript:void();"
																													onclick="return deleteRow('ownerList_table','ownerListtableRadio');"><img
																														src="${pageContext.request.contextPath}/images/delete.png"
																														width="50" height="20" border="0" /></a></td>
																											</tr>
																											<tr>
																												<td id="totalSoCount">&nbsp;</td>
																											</tr>

																											<tr>


																												<td><table width="854" border="1"
																														cellspacing="0" cellpadding="0"
																														id="ownerList_table">
																														<tr class="bg1">
																															<td class="ipm_n">N�mero de cuenta</td>


																															<td class="ipm_n">Nombre de la entidad</td>


																															<td class="ipm_n">Nombre del due�o</td>


																															<td class="ipm_n">Propietario TIN</td>


																															<td class="ipm_n">Seleccionar</td>
																														</tr>



																													</table></td>


																											</tr>
																										</table>
																										&nbsp;
																									</div></td>
																								<td
																									background="${pageContext.request.contextPath}/images/g1.png"
																									style="background-repeat: repeat-y;"></td>
																							</tr>
																							<tr>
																								<td colspan="3"
																									style="background-image:url(${pageContext.request.contextPath}/images/g21.png); background-repeat:repeat-x;"
																									height="1"></td>
																							</tr>
																						</table>



																					</td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>




																				<tr>
																					<td>

																						<div id="act_grid_div">

																							<table width="882" border="0" cellspacing="0"
																								cellpadding="0">
																								<tr>
																									<td align="left"
																										style="padding: 10px 10px 10px 0px"><a
																										id="FIAdd" href="javascript: void();"
																										onclick="return addGridData('accountInformation'); "><img
																											src="${pageContext.request.contextPath}/images/add.png"
																											width="50" height="20" border="0" /></a>&nbsp;<a
																										id="FIDelete" href="javascript:void()"
																										onclick="return modifiedRows('manual_Data_Grid_Table','accountInformationGroupRadio');"><img
																											src="${pageContext.request.contextPath}/images/modify.png"
																											width="90" height="20" border="0" /></a>&nbsp;<a
																										id="FIModify" href="javascript:void();"
																										onclick="return deleteRow('manual_Data_Grid_Table','accountInformationGroupRadio');"><img
																											src="${pageContext.request.contextPath}/images/delete.png"
																											width="60" height="20" border="0" /></a></td>
																								</tr>
																								<tr>
																									<td><table width="882" border="0"
																											cellspacing="0" cellpadding="0">
																											<tr>
																												<td width="400" id="totalAccountCount"></td>
																												<td colspan="3" width="300">&nbsp;</td>



																											</tr>
																										</table></td>
																								</tr>
																								<tr>
																									<td>
																										<table id="manual_Data_Grid_Table" width="882"
																											border="1" cellspacing="0" cellpadding="0">
																											<tr class="bg1">
																												<td class="ipm_n">GIIN de Declarante</td>

																												<td class="ipm_n">GIIN de Patrocinador</td>

																												<td class="ipm_n">GIIN del Intermediario</td>

																												<td class="ipm_n">Nombre de Cliente</td>

																												<td class="ipm_n">TIN de Cliente</td>

																												<td class="ipm_n">N�mero de Cuenta</td>

																												<td class="ipm_n">Seleccionar</td>
																											</tr>



																										</table>
																									</td>
																							</table>
																						</div>
																					</td>






																				</tr>

																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>

																				<tr>

																					<td align="center"><a id="saveManualData"
																						href="javascript:void();"
																						onclick="return saveManualGridData('save');"
																						style="text-decoration: none"> <img
																							src="${pageContext.request.contextPath}/images/save.png"
																							width="65" height="24" border="0" />
																					</a>&nbsp;&nbsp; <a id="submitManualData"
																						href="javascript:void();"
																						onclick="return saveManualGridData('submit');"
																						style="text-decoration: none; display: none;">
																							<img
																							src="${pageContext.request.contextPath}/images/submit1.png"
																							width="65" height="24" border="0" />
																					</a></td>

																				</tr>
																			</table></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-l.png"
																			width="9" height="5" /></td>
																		<td
																			style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-r.png"
																			width="9" height="5" /></td>
																	</tr>
																</table></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
													</table></td>
												<td width="20"></td>

											</tr>
										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>

								<tr>
									<td align="center"><img
										src="${pageContext.request.contextPath}/images/line.png"
										width="921" height="14" /></td>
								</tr>
								<tr>
									<td class="hedr">For <%=com.newgen.bean.GlobalVariables.projectName%>
										Revenue Authority Powered by Newgen Software Inc.
									</td>
								</tr>

								<tr>
									<td>&nbsp;</td>
								</tr>
							</table></td>
					</tr>

				</table></td>
			<td background="${pageContext.request.contextPath}/images/bg-g-r.gif"
				height="1" width="19"></td>
		</tr>
	</table>
	<script type="text/javascript">
		var menu1 = new menu.dd("menu1");
		menu1.init("menu", "menuhover");
		var reportMenu = new menu.dd("reportMenu");
		reportMenu.init("reportTab", "menuhover");
	</script>
</body>



</html>