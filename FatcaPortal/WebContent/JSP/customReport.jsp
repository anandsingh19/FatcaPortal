<!-- ------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - St Lucia ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : Repot Section
 File Name                                                   : customReport
 Author                                                      : Khushdil Kaushik
 Date (DD/MM/YYYY)                                           : 08/06/2016
 Description                                                 : a page where admin can generate its custome report  
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    Change Description
 
 ------------------------------------------------------------------------------------------------------
 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.newgen.bean.GlobalVariables"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Map"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: <%=GlobalVariables.projectName%> Inland Revenue
	Department ::
</title>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<script>
	window.history.forward(-1);
</script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/Common.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/jquery-ui.css" />
<script src="${pageContext.request.contextPath}/script/jquery-1.10.2.js"></script>

<%-- <script src="${pageContext.request.contextPath}/script/jquery-1.9.1.js"></script> --%>
<script src="${pageContext.request.contextPath}/script/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/script/Manual.js"></script>
<script src="${pageContext.request.contextPath}/script/Common.js"></script>
<script
	src="${pageContext.request.contextPath}/script/GlobalVariable.js"></script>
<script src="${pageContext.request.contextPath}/script/md5.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/script.js"></script>
</head>
<script>
	$(document).ready(function() {
		//alert("asdasda--aadesh");
		$(".hide_spon,.show_spon").change(function() {
			$('.spon').toggle();
		});
		$(".hide_inter,.show_inter").change(function() {
			$('.inter').toggle();
		});

		$("#fromDate").datepicker({
			maxDate : '0',
			changeMonth : true,//this option for allowing user to select month
			changeYear : true,//this option for allowing user to select from year range
			dateFormat : "d'/'M'/'yy"
		});
		$("#toDate").datepicker({
			maxDate : '0',
			changeMonth : true,//this option for allowing user to select month
			changeYear : true,//this option for allowing user to select from year range
			dateFormat : "d'/'M'/'yy"
		});
		/* $("[id^=toDate]").datepicker({
			maxDate : '0',
			changeMonth : true,//this option for allowing user to select month
			changeYear : true, //this option for allowing user to select from year range
			//dateFormat: "yy/mm/dd"
			dateFormat : "d'/'M'/'yy"
		}); */
	});
</script>
<body>
	<%
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", -1);
		Calendar now = Calendar.getInstance(); // Gets the current date and time
		int year = now.get(Calendar.YEAR);
		int startYear = GlobalVariables.reportingStartYear;
		String userType = "";
		String userSessionID = "";
		String userEmail = "";
		String userName = "";
		String userGIIN = "";
		String complianceType = "";
		HttpSession session1 = request.getSession(false);
		userName = (String) session1.getAttribute("UserName");
		complianceType = (String) session1.getAttribute("ComplianceType");//added by shipra 
		userType = (String) session1.getAttribute("UserType");
		userEmail = (String) session1.getAttribute("UserEmail");
		userGIIN = (String) session1.getAttribute("UserGIIN");
		Map<String, String> loginUserMap = GlobalVariables.loginUserMap;
		userSessionID = loginUserMap.get(userEmail);
		String luciaUSIGAPath = GlobalVariables.luciaUSIGA;
		if (null != userSessionID && (!(userSessionID.equalsIgnoreCase(session1.getId()))) || null == userName) {

			response.sendRedirect(request.getContextPath() + "/JSP/SessionExpired.jsp");
		} else if (null == userSessionID) {
			response.sendRedirect(request.getContextPath() + "/JSP/SessionExpired.jsp");
		}
		//String url = "http://www.irs.gov";
		//String url = "https://www.google.com";
	%>
	<table width="1003" border="0" cellspacing="0" cellpadding="0"
		align="center" bgcolor="#FFFFFF">
		<tr>
			<td background="${pageContext.request.contextPath}/images/bg-g-l.gif"
				width="19" height="1"></td>
			<td><table width="965" border="0" cellspacing="0"
					cellpadding="0">
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td
													background="${pageContext.request.contextPath}/images/bg.png"
													height="140"><table width="965" border="0"
														cellspacing="0" cellpadding="0">
														<tr>
															<td style="padding: 0px 0px 3px 10px" width="130"><img
																src="${pageContext.request.contextPath}/images/logo.png"
																width="118" height="135" /></td>
															<td valign="top"><table width="835" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>
																		<td colspan="2"><table border="0" cellspacing="0"
																				cellpadding="0" align="right">
																				<tr>
																					<td width="35" align="right"><img
																						src="${pageContext.request.contextPath}/images/us.png"
																						width="25" height="25" /></td>
																					<td class="logd1" style="text-align: left;"
																						width="150"><%=userName%> is <strong>&nbsp;&nbsp;Logged
																							in </strong>&nbsp;</td>
																					<td class="logd" width="60"><a
																						href="javascript:void();"
																						onclick='return logoutUser("jsp");' class="logd">Logout</a></td>
																				</tr>
																			</table></td>
																	</tr>
																	<tr>
																		<td style="padding: 2px 0px 0px 20px"><img
																			src="${pageContext.request.contextPath}/images/slogan.png"
																			width="652" height="" /></td>
																		<td style="padding: 8px 10px 5px 20px"><img
																			src="${pageContext.request.contextPath}/images/flag.png"
																			width="118" height="" /></td>
																	</tr>
																	<!-- <tr>
																		<td>&nbsp;</td>
																	</tr><tr>
																		<td>&nbsp;</td>
																	</tr> -->
																</table></td>
														</tr>
													</table></td>
											</tr>
										</table></td>
								</tr>
							</table></td>
					</tr>
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td>
										<table width="965" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<%
													String styleClass = "";
													if (null != userType && (userType.equalsIgnoreCase("admin") || userType.equalsIgnoreCase("fi admin"))) {
														if (userType.equalsIgnoreCase("admin"))//added by shipra
															styleClass = "drop_menu IRDadmin";
														else
															styleClass = "drop_menu FIadmin";//END
												%>
												<td align="center"><ul class="<%=styleClass%> ">
														<li><a
															href="${pageContext.request.contextPath}/JSP/home.jsp"
															class="menulink"><strong>Casa</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td align="center"><ul class="<%=styleClass%>">
														<li><a
															href="${pageContext.request.contextPath}/JSP/admin-approval.jsp"
															class="menulink"><strong>Aprobaci�n del usuario</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>

												<td><ul class="<%=styleClass%> selected">
														<li><a href="#"><Strong>Informe del administrador</Strong></a></li>
													</ul></td>
												<!-- --------------Added by Shipra----------- -->
												<%
													if (null != userType && !( userType.equalsIgnoreCase("fi admin")) && !( userType.equalsIgnoreCase("admin"))) {
												%>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Enviar Informe</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																					&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a></li>
																	</ul></li>
																<%
																	} else if (null != complianceType && complianceType.length() > 0
																					&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a></li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																	</ul></li>
																<%
																	}
																%>
															</ul></li>
													</ul></td>
												<%
													} else {
												%>
												<td width="3" style="display: none"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td style="display: none"><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Submit
																	Report</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																					&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a></li>
																	</ul></li>
																<%
																	} else if (null != complianceType && complianceType.length() > 0
																					&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a></li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																	</ul></li>
																<%
																	}
																%>
															</ul></li>
													</ul></td>
												<%
													}
												%>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<%-- <td>
													<ul id="reportTab" class="reportTab">
														<li><a class="menulink" href="#"><strong>Informe Estado</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0 && complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<%
																	} else if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>
																<%
																	} else {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>

																<%
																	}
																%>
															</ul></li>
													</ul>
												</td> --%>
												<!-- ------------END---------- -->
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="<%=styleClass%>">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="<%=styleClass%>">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Cont�ctenos</strong></a></li>
													</ul></td>
												<%
													} else {
												%>
												<td align="center"><ul class="drop_menu selected">
														<li><a href="javascript:void();" class="menulink"><strong>Casa</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Enviar Informe</strong></a>
															<ul>
																<li><a
																	href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																		XML</a></li>
																<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																<li><a
																	href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
															</ul></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/ReportStatus'><strong>Informe Estado</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Cont�ctenos</strong></a></li>
													</ul></td>
												<%
													}
												%>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td width="25"></td>
												<td width="200" valign="top"><table width="200"
														border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td valign="top">
																<table width="200" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g-t-m.png"
																			class="bgtext1">Informes de Administrador</td>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td
																			background="${pageContext.request.contextPath}/images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td><table width="180" border="0" cellspacing="0"
																				cellpadding="0" align="center">
																				<tr>
																					<td><table width="180" border="0"
																							cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="arrw_txt1"><a id="allUser"
																									href="#"
																									onclick="return showMyDIV('userReport_div');"
																									target="" class="arrw_txt1">Listado de Usuarios Registrados </a></td>
																							</tr>
																							<tr>
																								<!-- href="http://www.ab.gov.ag/"
																							 target="_blank" -->
																								<td class="arrw_txt1"><a target="" href="#"
																									onclick="return showMyDIV('userForGIIN_div');"
																									class="arrw_txt1">Informes de Usuarios Registrados por un Periodo Determinado</a></td>
																							</tr>
																							<tr>
																								<td class="arrw_txt1"><a href="#" target=""
																									onclick="return showMyDIV('yearlyReportStatus_div');"
																									class="arrw_txt1">Estado de Env�o del Informe Anual FATCA basado en GIIN</a></td>
																							</tr>
																							<tr>
																								<!-- <td class="arrw_txt">City Maintenance</td> -->
																							</tr>
																						</table></td>
																				</tr>
																			</table></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-l.png"
																			width="9" height="5" /></td>
																		<td
																			style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-r.png"
																			width="9" height="5" /></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<%-- <tr>
															<td><a
																href="${pageContext.request.contextPath}/MyInfoEdit?event=get"><img
																	src="${pageContext.request.contextPath}/images/edit.png"
																	width="200" height="30" border="0" /></a> <a href='${pageContext.request.contextPath}/JSP/MyInfoEdit.jsp'><img
																	src="${pageContext.request.contextPath}/images/edit.png"
																	width="200" height="30" border="0" /></a></td>
														</tr> --%>
														<tr>
															<td>
																<div id="UploadMsg">
																	<%
																		String msg = (String) request.getAttribute("msg");
																		//System.out.println("MSG=" + msg);
																		if (msg == null) {
																			msg = "&nbsp;";
																		}
																	%>
																	<p style='color: green'><%=msg%></p>
																</div>
															</td>
														</tr>
													</table></td>
												<td width="20"></td>
												<td width="695" valign="top">
													<div id="customReport_div">
														<table width="695" border="0" cellspacing="0"
															cellpadding="0">
															<tr>
																<td class="bgtext4"><strong><span
																		style="font-size: 20px; font-weight: bold; color: #064557;">Bienvenido A la secci�n de informes del administrador </span></strong><br /></td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
															</tr>
														</table>
													</div>
													<div id="userReport_div" style="display: none;">
														<table width="695" border="0" cellspacing="0"
															cellpadding="0">
															<tr>
																<td width="9"><img
																	src="${pageContext.request.contextPath}/images/g-t-l.png"
																	width="9" height="29" /></td>
																<td
																	background="${pageContext.request.contextPath}/images/g-t-m.png"
																	class="bgtext1">Usuarios registrados</td>
																<td width="9"><img
																	src="${pageContext.request.contextPath}/images/g-t-r.png"
																	width="9" height="29" /></td>
															</tr>
															<tr>
																<td
																	background="${pageContext.request.contextPath}/images/g.png"
																	style="background-repeat: repeat-y;"></td>
																<td><table width="695" border="0" cellspacing="0"
																		cellpadding="0" align="center">
																		<tr>
																			<td>
																				<form
																					action="${pageContext.request.contextPath}/CustomReport?reportType=allUser"
																					name="customReportForm" method="post">
																					<table width="695" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td colspan="4">&nbsp;</td>
																						</tr>
																						<tr>
																							<td colspan="4" id="errormsg_userReport">&nbsp;</td>
																						</tr>
																						<tr>
																							<td colspan="4">Dicho informe contiene los datos para todos los usuarios registrados en el sistema.
																							Haga clic en el bot�n "descargar" informe.</td>
																						</tr>
																						<tr>
																							<td colspan="4">&nbsp;</td>
																						</tr>
																						<tr>
																							<td align="center"><a id="customReport"
																								style="text-decoration: none;"
																								href="javascript:void();"
																								onclick=" return submitCustomeReportData(this.id);">&nbsp;&nbsp;<img
																									src="${pageContext.request.contextPath}/images/Download.png"
																									width="104" height="24" border="0"
																									style="vertical-align: middle;" /></a></td>
																						</tr>
																						<tr>
																							<td colspan="4">&nbsp;</td>
																						</tr>
																					</table>
																				</form>
																			</td>
																		</tr>
																	</table></td>
																<td
																	background="${pageContext.request.contextPath}/images/g1.png"
																	style="background-repeat: repeat-y;"></td>
															</tr>
															<tr>
																<td><img
																	src="${pageContext.request.contextPath}/images/c-b-l.png"
																	width="9" height="5" /></td>
																<td
																	style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																<td><img
																	src="${pageContext.request.contextPath}/images/c-b-r.png"
																	width="9" height="5" /></td>
															</tr>
														</table>
													</div>

													<div id="userForGIIN_div" style="display: none;">
														<table width="695" border="0" cellspacing="0"
															cellpadding="0">
															<tr>
																<td width="9"><img
																	src="${pageContext.request.contextPath}/images/g-t-l.png"
																	width="9" height="29" /></td>
																<td
																	background="${pageContext.request.contextPath}/images/g-t-m.png"
																	class="bgtext1">Informe de Usuario entre per�odos temporales.</td>
																<td width="9"><img
																	src="${pageContext.request.contextPath}/images/g-t-r.png"
																	width="9" height="29" /></td>
															</tr>
															<tr>
																<td
																	background="${pageContext.request.contextPath}/images/g.png"
																	style="background-repeat: repeat-y;"></td>
																<td><table width="695" border="0" cellspacing="0"
																		cellpadding="0" align="center">
																		<tr>
																			<td>
																				<form
																					action="${pageContext.request.contextPath}/CustomReport?reportType=userofGIIN"
																					name="customReportForm_giin" method="post">
																					<table width="695" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td colspan="4">&nbsp;</td>
																						</tr>
																						<tr>
																							<td colspan="4" id="errormsg_giinReport">&nbsp;</td>
																						</tr>
																						<tr>
																							<td colspan="4">Para obtener el estado de env�o del informe FATCA
																							entre los per�odos especificados, proporcione el per�odo de reporte
																							en el los campos "De" y "A" y en n�mero GIIN.
																							Haga clic en el bot�n "Descargar" para obtener el informe</td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																						</tr>
																						<tr>
																							<td valign="top" style="width: 42px">De<span
																								style="color: #F00;">*</span>:
																							</td>

																							<td valign="top"><input type="text"
																								name="fromDate" id="fromDate"
																								class="form-control" readonly="readonly"
																								style="padding: 0px 2px 2px 5px; width: 200px;"
																								onKeypress='return validateKey(this.id,"aplhanumeric");'
																								onpaste='return validateInput(this.id);' /></td>
																							<td valign="top">A<span
																								style="color: #F00;">*</span>:
																							</td>
																							<td valign="top"><input type="text"
																								name="toDate" id="toDate" class="form-control"
																								readonly="readonly"
																								style="padding: 0px 2px 2px 5px; width: 200px;"
																								onKeypress='return validateKey(this.id,"aplhanumeric");'
																								onpaste='return validateInput(this.id);' /></td>
																						</tr>
																						<tr>
																							<td colspan="4">&nbsp;</td>
																						</tr>
																						<!-- <tr>
																							<td valign="top">GIIN<span
																								style="color: #F00;">*</span>:
																							</td>
																							<td valign="top"><input type="text"
																								name="giin_customReport" id="giin_customReport"
																								class="form-control" maxlength="100"
																								style="width: 200px; padding: 0px 2px 2px 5px;" /></td>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																						</tr> -->
																						<tr>
																							<td colspan="4">&nbsp;</td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																							<td colspan="2" align="center"
																								style="padding-left: 180px"><a
																								id="customReport_giin"
																								style="text-decoration: none;"
																								href="javascript:void();"
																								onclick=" return submitCustomeReportData(this.id);"><img
																									src="${pageContext.request.contextPath}/images/Download.png"
																									width="104" height="24" border="0"
																									style="vertical-align: middle;" /></a></td>
																							<td>&nbsp;</td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																						</tr>
																					</table>
																				</form>
																			</td>
																		</tr>
																	</table></td>
																<td
																	background="${pageContext.request.contextPath}/images/g1.png"
																	style="background-repeat: repeat-y;"></td>
															</tr>
															<tr>
																<td><img
																	src="${pageContext.request.contextPath}/images/c-b-l.png"
																	width="9" height="5" /></td>
																<td
																	style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																<td><img
																	src="${pageContext.request.contextPath}/images/c-b-r.png"
																	width="9" height="5" /></td>
															</tr>
														</table>
													</div>
													<div id="yearlyReportStatus_div" style="display: none;">
														<table width="695" border="0" cellspacing="0"
															cellpadding="0">
															<tr>
																<td width="9"><img
																	src="${pageContext.request.contextPath}/images/g-t-l.png"
																	width="9" height="29" /></td>
																<td
																	background="${pageContext.request.contextPath}/images/g-t-m.png"
																	class="bgtext1">Estado de presentaci�n del Informe FATCA</td>
																<td width="9"><img
																	src="${pageContext.request.contextPath}/images/g-t-r.png"
																	width="9" height="29" /></td>
															</tr>
															<tr>
																<td
																	background="${pageContext.request.contextPath}/images/g.png"
																	style="background-repeat: repeat-y;"></td>
																<td><table width="695" border="0" cellspacing="0"
																		cellpadding="0" align="center">
																		<tr>
																			<td><form
																					action="${pageContext.request.contextPath}/CustomReport?reportType=yearlyReport"
																					name="customReportForm_yearly" method="post">
																					<table width="695" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td colspan="4">&nbsp;</td>
																						</tr>
																						<tr>
																							<td colspan="4" id="errormsg_yearlyReport">&nbsp;</td>
																						</tr>
																						<tr>
																							<td colspan="4">Para obtener el estado de env�o del informe
																							FATCA entre los per�odos especificados, proporcione el per�odo
																							de reporte en el los campos "De" y "A" y en n�mero GIIN.
																							Haga clic en el bot�n "Descargar" para obtener el informe</td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																						</tr>
																						<tr>
																							<td valign="top" style="width: 42px">De<span
																								style="color: #F00;">*</span>:
																							</td>
																							<td valign="top" style="width: 200px"><select
																								name="fromYear_custom_report"
																								id="fromYear_custom_report" class="form-control"
																								style="width: 180px">
																									<option value="Blank">--Seleccionar--</option>
																									<%
																										year = year - 1;
																										while (year >= startYear) {
																									%>
																									<option value="<%=year%>"><%=year%></option>
																									<%
																										year--;
																										}
																										year = now.get(Calendar.YEAR);
																									%>
																							</select></td>
																							<td valign="top" style="width: 42px">A<span
																								style="color: #F00;">*</span>:
																							</td>
																							<td valign="top" style="width: 360px"><select
																								name="toYear_custom_report"
																								id="toYear_custom_report" class="form-control"
																								style="width: 180px">
																									<option value="Blank">--Seleccionar--</option>
																									<%
																										year = year - 1;
																										while (year >= startYear) {
																									%>
																									<option value="<%=year%>"><%=year%></option>
																									<%
																										year--;
																										}
																									%>
																							</select></td>
																						</tr>
																						<tr>
																							<td colspan="4">&nbsp;</td>
																						</tr>
																						<tr>
																							<td valign="top">GIIN<span
																								style="color: #F00;"></span>:
																							</td>
																							<%
																								if (userType.equalsIgnoreCase("Fi admin")) {
																							%>
																							<td valign="top"><input type="text"
																								value="<%=userGIIN%>" readonly="readonly"
																								name="giinForYearlyReport"
																								id="giinForYearlyReport" class="form-control"
																								maxlength="100"
																								style="width: 200px; background-color: #ffffb3; pointer-events: none" /></td>
																							<%
																								} else {
																							%>
																							<td valign="top"><input type="text"
																								name="giinForYearlyReport"
																								id="giinForYearlyReport" class="form-control"
																								maxlength="100"
																								style="width: 170px; padding: 0px 2px 2px 5px;" /></td>
																							<%
																								}
																							%>
																							<td>&nbsp;</td>
																							<td><a id="customReport_yearly"
																								style="text-decoration: none;"
																								href="javascript:void();"
																								onclick=" return submitCustomeReportData(this.id);"><img
																									src="${pageContext.request.contextPath}/images/Download.png"
																									width="104" height="24" border="0"
																									style="vertical-align: middle;" /></a></td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																							<td>&nbsp;</td>
																						</tr>
																					</table>
																				</form></td>
																		</tr>
																	</table></td>
																<td
																	background="${pageContext.request.contextPath}/images/g1.png"
																	style="background-repeat: repeat-y;"></td>
															</tr>
															<tr>
																<td><img
																	src="${pageContext.request.contextPath}/images/c-b-l.png"
																	width="9" height="5" /></td>
																<td
																	style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																<td><img
																	src="${pageContext.request.contextPath}/images/c-b-r.png"
																	width="9" height="5" /></td>
															</tr>
														</table>
													</div>
												</td>
												<td width="25">&nbsp;</td>
											</tr>
										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="center"><img
										src="${pageContext.request.contextPath}/images/line.png"
										width="921" height="14" /></td>
								</tr>
								<%-- <tr>
									<td class="hedr">For Govt. of <%=com.newgen.bean.GlobalVariables.projectName%>
										Powered by Newgen Software Inc.
									</td>
								</tr> --%>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</table></td>
					</tr>
				</table></td>
			<td background="${pageContext.request.contextPath}/images/bg-g-r.gif"
				height="1" width="19"></td>
		</tr>
	</table>
	<script type="text/javascript">
		var menu = new menu.dd("menu");
		menu.init("menu", "menuhover");
		var reportMenu = new menu.dd("reportMenu");//added by shipra
		reportMenu.init("reportTab", "menuhover");//added by shipra
	</script>
</body>
</html>
