<%@page import="java.util.*"%>
<%@page import="com.newgen.bean.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: <%=com.newgen.bean.GlobalVariables.projectName%>
	Inland Revenue Department ::
</title>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/Common.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/script.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/GlobalVariable.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/Common.js"></script>

<style>
a.tooltip {
	outline: none;
}

a.tooltip strong {
	line-height: 30px;
}

a.tooltip:hover {
	text-decoration: none;
}

a.tooltip span {
	z-index: 10;
	display: none;
	padding: 14px 20px;
	margin-top: -30px;
	margin-left: 28px;
	width: 220px;
	line-height: 16px;
	text-align: justify;
}

a.tooltip:hover span {
	display: inline;
	position: absolute;
	color: #111;
	border: 1px solid #DCA;
	background: #fffAF0;
}

.callout {
	z-index: 20;
	position: absolute;
	top: 30px;
	border: 0;
	left: -12px;
}

/*CSS3 extras*/
a.tooltip span {
	border-radius: 4px;
	box-shadow: 5px 5px 8px #CCC;
}
</style>

<%
	Calendar now = Calendar.getInstance(); // Gets the current date and time
	int year = now.get(Calendar.YEAR);
	int startYear = GlobalVariables.reportingStartYear;
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", -1);

	String userType = "";
	String userSessionID = "";
	String userEmail = "";
	String userName = "";
	String complianceType = "";

	HttpSession session1 = request.getSession(false);
	userName = (String) session1.getAttribute("UserName");
	userType = (String) session1.getAttribute("UserType");
	userEmail = (String) session1.getAttribute("UserEmail");
	complianceType = (String) session1.getAttribute("ComplianceType");
	String submissionModeMsg = (String) request
			.getAttribute("SubmissionModeMsg");
	if (submissionModeMsg == null) {
		submissionModeMsg = "";
	}
	Map<String, String> loginUserMap = GlobalVariables.loginUserMap;
	userSessionID = loginUserMap.get(userEmail);
	if (null != userSessionID
			&& (!(userSessionID.equalsIgnoreCase(session1.getId())))
			|| null == userName) {
		response.sendRedirect(request.getContextPath()
				+ "/JSP/SessionExpired.jsp");
	} else if (null == userSessionID) {
		response.sendRedirect(request.getContextPath()
				+ "/JSP/SessionExpired.jsp");
	}
%>

<script>
	function CallbackFunction(event) {

		if (window.event) {

			if (window.event.clientX < 40 && window.event.clientY < 0) {

				//  alert("back button is clicked");    

			} else {

				//  alert("refresh button is clicked");
			}

		} else {

			// want some condition here so that I can differentiate between whether refresh button is clicked or back button is clicked.             
		}
	}
</script>
</head>

<body onbeforeunload="CallbackFunction();">
	<!-- <body> -->
	<table width="1003" border="0" cellspacing="0" cellpadding="0"
		align="center" bgcolor="#FFFFFF">
		<tr>
			<td background="${pageContext.request.contextPath}/images/bg-g-l.gif"
				width="19" height="1"></td>
			<td><table width="965" border="0" cellspacing="0"
					cellpadding="0">
					<tr>


						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td
													background="${pageContext.request.contextPath}/images/bg.png"
													height="140"><table width="965" border="0"
														cellspacing="0" cellpadding="0">
														<tr>
															<td style="padding: 0px 0px 3px 10px" width="130"><img
																src="${pageContext.request.contextPath}/images/logo.png"
																width="118" height="135" /></td>
															<td valign="top"><table width="835" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>


																		<td colspan="2"><table border="0" cellspacing="0"



																				cellpadding="0" align="right">
																				<tr>





																					<td width="35" align="right"><img
																						src="${pageContext.request.contextPath}/images/us.png"
																						width="25" height="25" /></td>



																					<td class="logd1" style="text-align: left;"
																						width="150"><%=userName%> is <strong>&nbsp;&nbsp;Registrado en</strong>&nbsp;</td>
																					<td class="logd" width="60">
																						<%-- <a 	href="${pageContext.request.contextPath}/JSP/logout.jsp"
																						class="logd">Logout</a> --%> <a
																						href="javascript:void();"
																						onclick='return logoutUser("jsp");' class="logd">Cerrar sesi�n</a>

																					</td>

																				</tr>



																			</table></td>
																	</tr>
																	<tr>
																		<td style="padding: 2px 0px 0px 20px"><img

																			src="${pageContext.request.contextPath}/images/slogan.png"
																			width="652" height="" /></td>


																		<td style="padding: 8px 10px 5px 20px">
																		<img 	src="${pageContext.request.contextPath}/images/flag.png"
																			width="118" height="" />

																			&nbsp;
																			</td>
																	</tr>
																	<!-- <tr>
																		<td>&nbsp;</td>


																	</tr> -->
																</table></td>
														</tr>
													</table></td>
											</tr>
										</table></td>
								</tr>

							</table></td>
					</tr>
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">

								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td align="center"><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/JSP/home.jsp"
															class="menulink"><strong>Home</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<%-- <td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Submit
																	Report</strong></a>
															<ul>
																<li><a
																	href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Upload
																		XML</a></li>
																<li><a
																	href="#">Upload
																		XLS/CSV </a></li>
																<li><a href="#">Fill Manually Online</a>
																	<!-- <ul>
																		<li><a href="#">Fill New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> -->
																	</li>
															</ul></li>
													</ul></td> --%>

												<td><ul class="menu" id="menu">
														<li><a href="#" style="background-color: #2487a2"
															class="menulink"><strong>Enviar reporte</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<%
																	} else if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	}
																%>


															</ul></li>
													</ul></td>

												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<%-- <td><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/ReportStatus"><strong>Report
																	Status</strong></a></li>
													</ul></td> --%>

												<td><ul id="reportTab" class="reportTab">
														<li><a class="menulink" href="#"><strong>Informe Estado</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<%
																	} else if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>
																<%
																	} else {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>

																<%
																	}
																%>

															</ul></li>
													</ul></td>

												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/FAQServlet?callFor=FAQ"><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Cont�ctenos</strong></a></li>
													</ul></td>
											</tr>

										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>

									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td width="25"></td>
												<td valign="top"><table width="700" border="0"
														cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td valign="top"><table width="700" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g-t-m.png"
																			class="bgtext11">Online Form</td>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td
																			background="${pageContext.request.contextPath}/images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td>
																			<form
																				action="${pageContext.request.contextPath}/ManualServlet"
																				method="post" name="manualselection">
																				<input type="hidden" id="reportingMode"
																					value="Manual" name="reportingMode" />
																					<!-- Change for All by Shubham on 28072016 starts -->
																					<input type="hidden" id="complianceType"
																					value="FATCA" name="complianceType" />
																					<!-- Change for All by Shubham on 28072016 end -->



																				<table width="672" border="0" cellspacing="0"
																					cellpadding="0">

																					<tr>
																						<td colspan="3" style="color: red;"><span
																							id="errorMsgManualScreen">&nbsp;<%=submissionModeMsg%></span></td>
																					</tr>
																					<tr>
																						<td>&nbsp;</td>
																						<td>&nbsp;</td>
																						<td>&nbsp;</td>
																					</tr>

																					<tr>
																						<td colspan="3">Selecciones el A�o del reporte que desea
																						ser enviado mediante el formulario en l�nea.</td>
																					</tr>
																					<tr>
																						<td>&nbsp;</td>
																						<td>&nbsp;</td>
																						<td>&nbsp;</td>
																					</tr>
																					<tr>
																						<td width="300">Paso 1: Seleccione el A�o del Informe </td>
																						<td width="200"><select
																							name="Manual_Reporting_Year"
																							id="Manual_Reporting_Year"
																							onchange="getSubmissionType(this,'giinSubmissionType','Manual','Fatca');"
																							class="form-control" style="width: 180px;">
																								<option value="blank">---Seleccionar---</option>

																								<%
																									year = year - 1;
																									while (year >= startYear) {
																								%>
																								<option value="<%=year%>"><%=year%></option>
																								<%
																									year--;
																									}
																								%>
																						</select></td>
																						<td style="width: 170px;"><a href="#"
																							class="tooltip"> <img
																								src="${pageContext.request.contextPath}/images/whts.png"
																								width="20" height="20" border="0"> <span>
																									<img class="callout"
																									src="${pageContext.request.contextPath}/images/callout.gif" />
																									A�o civil para el que se debe presentar el informe
																							</span></a></td>
																					</tr>
																					<tr>
																						<td>&nbsp;</td>
																						<td>&nbsp;</td>
																						<td>&nbsp;</td>
																					</tr>
																					<tr>
																						<!-- <td>Step 2: Please select your Submission
																							Type</td>
																						<td width="200"><select
																							name="Manual_Submission_Type"
																							id="Manual_Submission_Type" class="form-control"
																							style="width: 180px;">
																								<option value="blank">---Select---</option>
																								<option value="New">New</option>
																								<option value="Update">Update</option>
																								<option value="Response to IRS request">Response
																									to IRS request</option>
																						</select></td> -->

																						<td>Paso 3: Seleccione el Tipo de Env�o </td>
																						<td width="200"><input type="text"
																							name="Manual_Submission_Type"
																							id="Manual_Submission_Type" class="form-control"
																							style="padding: 0px 2px 2px 5px; width: 170px;"
																							readonly;
																							onpaste='return validateInput(this.id);'>
																						</td>
																						<td style="width: 170px;"><a href="#"
																							class="tooltip"> <img
																								src="${pageContext.request.contextPath}/images/whts.png"
																								width="20" height="20" border="0"> <span>
																									<img class="callout"
																									src="${pageContext.request.contextPath}/images/callout.gif" />
																									Si el informe de presentaci�n XML es para
																									nuevos datos o si es una actualizaci�n de
																									cualquier informe presentado anteriormente


																							</span></a></td>
																					</tr>
																					<tr>
																						<td>&nbsp;</td>
																						<td>&nbsp;</td>
																						<td>&nbsp;</td>
																					</tr>
																					<tr>
																						<!-- <td>Is this a new submission or an
																						update?&nbsp;</td> -->
																						<td>&nbsp;</td>
																						<td width="200"><a href="javascript: void();"
																							onclick=" return getManualForm();"><img
																								src="${pageContext.request.contextPath}/images/ok.png"
																								 border="0"></a>&nbsp;<a
																							href="#"
																							onclick="resetManualSelectionScreen('Manual');"><img
																								src="${pageContext.request.contextPath}/images/reset.png"
																								 border="0"></a></td>
																						<td>&nbsp;</td>
																					</tr>
																					<tr>
																						<td>&nbsp;</td>
																						<td>&nbsp;</td>
																						<td>&nbsp;</td>
																					</tr>
																					<tr>
																						<td colspan="3">Una vez que haya seleccionado OK,
																						ser� redirigido a la p�gina de Formulario En L�nea </td>
																					</tr>
																					<tr>
																						<td>&nbsp;</td>
																						<td>&nbsp;</td>
																						<td>&nbsp;</td>
																					</tr>
																				</table>

																			</form>

																		</td>
																		<td
																			background="${pageContext.request.contextPath}/images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-l.png"
																			width="9" height="5" /></td>
																		<td
																			style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-r.png"
																			width="9" height="5" /></td>
																	</tr>
																</table></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
													</table></td>
												<td width="20"></td>

											</tr>
										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>

								<tr>
									<td align="center"><img
										src="${pageContext.request.contextPath}/images/line.png"
										width="921" height="14" /></td>
								</tr>

								<%-- <tr>
									<td class="hedr">For Govt. of <%=com.newgen.bean.GlobalVariables.projectName%>
										Powered by Newgen Software Inc.
									</td>
								</tr> --%>

								<tr>
									<td>&nbsp;</td>
								</tr>
							</table></td>
					</tr>

				</table></td>
			<td background="${pageContext.request.contextPath}/images/bg-g-r.gif"
				height="1" width="19"></td>
		</tr>
	</table>
	<script type="text/javascript">
		var menu1 = new menu.dd("menu1");
		menu1.init("menu", "menuhover");
		var reportMenu = new menu.dd("reportMenu");
		reportMenu.init("reportTab", "menuhover");
	</script>
</body>
</html>
