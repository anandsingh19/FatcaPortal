<!-- ------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : Contact US page
 File Name                                                   : contact-us
 Author                                                      : Khushdil Kaushik
 Date (DD/MM/YYYY)                                           : 14/12/2015
 Description                                                 : page having contact us informtion 
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    Change Description
 
 ------------------------------------------------------------------------------------------------------
 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.newgen.bean.*"%>
<%@page import="java.util.*"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: <%=com.newgen.bean.GlobalVariables.projectName%>
	Inland Revenue Department ::
</title>

<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/script.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/Common.js"></script>
</head>
<body>


	<%
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", -1);
		String userType = "";
		String userSessionID = "";
		String userEmail = "";
		String userName = "";
		HttpSession session1 = request.getSession(false);
		userName = (String) session1.getAttribute("UserName");
		userType = (String) session1.getAttribute("UserType");
		userEmail = (String) session1.getAttribute("UserEmail");

		Map<String, String> loginUserMap = GlobalVariables.loginUserMap;
		userSessionID = loginUserMap.get(userEmail);
		/* 		if (!(userSessionID.equalsIgnoreCase(session1.getId()))) {
		 response.sendRedirect(request.getContextPath());
		 }
		 *//* if (null == userType || userType.length() == 0) {
				response.sendRedirect(request.getContextPath());
			} */

		List<String> contactDataList = (List<String>) request
				.getAttribute("contactUS");
	%>

	<table width="1003" border="0" cellspacing="0" cellpadding="0"
		align="center" bgcolor="#FFFFFF">
		<tr>
			<td background="${pageContext.request.contextPath}/images/bg-g-l.gif"
				width="19" height="1"></td>
			<td><table width="965" border="0" cellspacing="0"
					cellpadding="0">
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td
													background="${pageContext.request.contextPath}/images/bg.png"
													height="140"><table width="965" border="0"
														cellspacing="0" cellpadding="0">
														<tr>
															<td style="padding: 0px 0px 3px 10px" width="130"><img
																src="${pageContext.request.contextPath}/images/logo.png"
																width="118" height="135" /></td>
															<td valign="top"><table width="835" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>
																		<td colspan="2">
																			<%
																				//if (null == userType || userType.length() == 0) {	
																				//if (null != session.getAttribute("UserName")) {
																				if (null != userName) {
																			%>
																			<table border="0" cellspacing="0" cellpadding="0"
																				align="right">
																				<tr>
																					<td width="35" align="right"><img
																						src="${pageContext.request.contextPath}/images/us.png"
																						width="25" height="25" /></td>
																					<td class="logd1" style="text-align: left;"
																						width="150"><%=userName%> is <strong>&nbsp;&nbsp;Registrado en</strong>&nbsp;</td>
																					<td class="logd" width="60">
																						<%-- <a href="${pageContext.request.contextPath}/JSP/logout.jsp"
																						class="logd">Logout</a>
																						 --%> <a href="javascript:void();"
																						onclick='return logoutUser("jsp");' class="logd">Cerrar sesi�n</a>
																					</td>

																				</tr>
																			</table> <%
 	} else {
 %>

																			<table border="0" cellspacing="0" cellpadding="0"
																				align="right">
																				<tr>
																					<td>&nbsp;</td>
																					<td>&nbsp;</td>
																					<td>&nbsp;</td>
																				</tr>
																			</table> <%
 	}
 %>

																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 2px 0px 0px 20px"><img
																			src="${pageContext.request.contextPath}/images/slogan.png"
																			width="652" height="" /></td>
																		<td style="padding: 8px 10px 5px 20px">
																		<img 	src="${pageContext.request.contextPath}/images/flag.png"
																			width="118" height="" />
																			&nbsp;
																			</td>
																	</tr>
																	<tr>
																		<td>&nbsp;</td>
																	</tr>
																</table></td>
														</tr>
													</table></td>
											</tr>
										</table></td>
								</tr>

							</table></td>
					</tr>
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">

								<tr>
									<td>
										<%
											//if (null == session.getAttribute("UserName")) {
											if (null == userName) {
										%>
										<table width="965" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center"><ul class="drop_menu_new">
														<li><a href="${pageContext.request.contextPath}"
															class="menulink"><strong>Home</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>




												<td><ul class="drop_menu_new">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu_new selected">
														<li><a href='#'><strong>Cont�ctenos</strong></a></li>
													</ul></td>
											</tr>

										</table> <%
 	} else {
 %>
										<table width="965" border="0" cellspacing="0" cellpadding="0">
											<tr>


												<%
													if (null != userSessionID
																&& (userSessionID.equalsIgnoreCase(session1.getId()))) {
															if (userType != null
																	&& (userType.equalsIgnoreCase("admin") || userType
																			.equalsIgnoreCase("fi admin"))) {
												%>
												<td align="center"><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/JSP/home.jsp"
															class="menulink"><strong>Home</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td align="center"><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/JSP/admin-approval.jsp"
															class="menulink"><strong>User Approval</strong></a></li>
													</ul></td>
													<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>

												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/JSP/customReport.jsp'><Strong>Informe del administrador
																	</Strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu selected">
														<li><a href='#'><strong>Cont�ctenos</strong></a></li>
													</ul></td>



												<%
													} else {
												%>
												<td align="center"><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/JSP/home.jsp"
															class="menulink"><strong>Casa</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Enviar Informe</strong></a>
															<ul>
																<li><a
																	href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																		XML</a></li>
																<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																<li><a
																	href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
															</ul></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/ReportStatus'><Strong>Report
																	Status</Strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu selected">
														<li><a href='#'><strong>Cont�ctenos</strong></a></li>
													</ul></td>

												<%
													}
														}

														else {
												%>

												<td><ul class="drop_menu">
														<li><a href='#'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu selected">
														<li><a
															href='${pageContext.request.contextPath}/JSP/contact-us.jsp'><strong>Cont�ctenos</strong></a></li>
													</ul></td>
												<%
													}
												%>


											</tr>

										</table> <%
 	}
 %>




									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>

									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td width="25"></td>
												<td valign="top"><table width="900" border="0"
														cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td><table width="882" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g-t-m.png"
																			class="bgtext1">Contact Us</td>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td
																			background="${pageContext.request.contextPath}/images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td><table width="854" border="0" cellspacing="0"
																				cellpadding="0" align="center">
																				<tr>
																					<td><table width="854" border="0"
																							cellspacing="0" cellpadding="0">
																							<tr>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																							</tr>
																							<tr>
																								<td valign="top">
																									<%
																										if (null != contactDataList && contactDataList.size() > 0) {
																											for (int i = 0; i < contactDataList.size(); i++) {
																												out.print(contactDataList.get(i));
																											}
																										}
																									%> <%-- <strong><u>Contact
																											Us</u></strong><br /> <br /> Inland Revenue Department.<br />
																									Ministry of Finance<br />Government of <%=com.newgen.bean.GlobalVariables.projectName%>
																									<br /> Woods Centre<br /> Friars Hill Road<br />
																									St John's <%=com.newgen.bean.GlobalVariables.projectName%>.<br />
																									<br /> <br /> <strong>T:</strong> (268)
																									468-9471<br /> <br /> <strong>M:</strong>
																									(268) 462-3175<br /> <br /> <br /> <strong>E-Mail:</strong>
																									irdinfo@ab.gov.ag, revenue@ab.gov.ag and
																									irdproperty@ab.gov.ag<br /> <br /> <strong>Hours
																										of operation</strong><br /> <br /> <strong>Opening
																										Hours:</strong> Mon-Thu: 8:00 am - 4:30 pm<br /> <br />
																									<strong>Fri:</strong> 8:00 am 3:00 pm<br /> <br />
																									<strong>Cashers open from:</strong> Mon-Thu:
																									8:30 am - 3:00 pm<br /> <br /> <strong>Fri:</strong>
																									8:30 am 2:00 pm --%>



																								</td>

																								<td valign="top">&nbsp;
																								
																								<%-- <img
																									src="${pageContext.request.contextPath}/images/cont.png"
																									width="250" height="212" /> --%>
																									
																									</td>
																							</tr>
																							<tr>
																								<td>&nbsp;</td>
																								<td>&nbsp;</td>
																							</tr>

																						</table></td>
																				</tr>
																			</table></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td colspan="3"
																			style="background-image:url(${pageContext.request.contextPath}/images/g21.png); background-repeat:repeat-x;"
																			height="1"></td>
																	</tr>
																</table></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
													</table></td>
												<td width="20"></td>

											</tr>
										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>

								<tr>
									<td align="center"><img
										src="${pageContext.request.contextPath}/images/line.png"
										width="921" height="14" /></td>
								</tr>
								<%-- <tr>
									<td class="hedr">For Govt. of <%=com.newgen.bean.GlobalVariables.projectName%>
										Powered by Newgen Software Inc.
									</td>
								</tr> --%>

								<tr>
									<td>&nbsp;</td>
								</tr>
							</table></td>
					</tr>

				</table></td>
			<td background="${pageContext.request.contextPath}/images/bg-g-r.gif"
				height="1" width="19"></td>
		</tr>
	</table>
	<script type="text/javascript">
		var menu = new menu.dd("menu");
		menu.init("menu", "menuhover");
	</script>
</body>
</html>
