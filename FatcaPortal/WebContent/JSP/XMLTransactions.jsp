<!-- ------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : FI report cycle(Report status)
 File Name                                                   : XML-Transaction-CRS
 Author                                                      : Deepak Sharma
 Date (DD/MM/YYYY)                                           : 16/10/2015
 Description                                                 : this Jsp used to show the report cycle of FI
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    		Change Description
 11608				27/06/2016     Khushdil Kaushik 		added tool tip 
 ------------------------------------------------------------------------------------------------------
 -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.newgen.bean.*"%>
<%@page import="java.util.*"%>
<%@page import="com.newgen.bean.GlobalVariables"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: <%=GlobalVariables.projectName%> Inland Revenue
	Department ::
</title>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="script/script.js"></script>
<script src="${pageContext.request.contextPath}/script/Common.js"></script>
</head>
<%!String reportingYearFromSession = null;%>
<script>
	function submitStatusform() {

		document.reportStatusform.submit();

	}
</script>


<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", -1);
	Calendar now = Calendar.getInstance(); // Gets the current date and time
	int year = now.get(Calendar.YEAR);
	int startYear = GlobalVariables.reportingStartYear;
	int stageSeq = 0;
	String userType = "";
	String userSessionID = "";
	String userEmail = "";
	String UserName = "";
	String complianceType = "";
	HttpSession session1 = request.getSession(false);
	UserName = (String) session1.getAttribute("UserName");
	userType = (String) session1.getAttribute("UserType");
	userEmail = (String) session1.getAttribute("UserEmail");
	reportingYearFromSession = (String) session1
			.getAttribute("ReportingYear");
	complianceType = (String) session1.getAttribute("ComplianceType");
	Map<String, String> loginUserMap = GlobalVariables.loginUserMap;
	userSessionID = loginUserMap.get(userEmail);
	if (null != userSessionID
			&& (!(userSessionID.equalsIgnoreCase(session1.getId())))
			|| null == UserName) {
		response.sendRedirect(request.getContextPath()
				+ "/JSP/SessionExpired.jsp");
	} else if (null == userSessionID) {
		response.sendRedirect(request.getContextPath()
				+ "/JSP/SessionExpired.jsp");
	}

	ArrayList<TransactionXMLBean> XMLBeans = null;
	XMLBeans = (ArrayList<TransactionXMLBean>) request
			.getAttribute("XMLArrayList");

	String reportName = "";
	String submissionDate = "";
	String fistatus = "";
	String fistage = "";
	String status = "";
	String stage = "";
	if (XMLBeans != null && XMLBeans.size() > 0) {
		TransactionXMLBean bean = XMLBeans.get(0);
		reportName = bean.getFileName();
		submissionDate = bean.getUploadedDate();
		status = bean.getFileStatus();
		stage = bean.getFileStage();
		fistage = bean.getFiStage();
		fistatus = bean.getFiStatus();

	}

%>

<body>
	<table width="1003" border="0" cellspacing="0" cellpadding="0"
		align="center" bgcolor="#FFFFFF">
		<tr>
			<td background="images/bg-g-l.gif" width="19" height="1"></td>
			<td><table width="965" border="0" cellspacing="0"
					cellpadding="0">
					<tr>

						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>


												<td background="images/bg.png" height="140"><table
														width="965" border="0" cellspacing="0" cellpadding="0">

														<tr>
															<td style="padding: 0px 0px 3px 10px" width="130"><img

																src="images/logo.png" width="118" height="135" /></td>
															<td valign="top"><table width="835" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>


																		<td colspan="2"><table border="0" cellspacing="0"



																				cellpadding="0" align="right">
																				<tr>





																					<td width="35" align="right"><img
																						src="${pageContext.request.contextPath}/images/us.png"
																						width="25" height="25" /></td>



																					<td class="logd1" style="text-align: left;"
																						width="150"><%=UserName%> is <strong>&nbsp;&nbsp;Registrado en </strong>&nbsp;</td>
																					<td class="logd" width="60">
																						<%-- <a href="${pageContext.request.contextPath}/JSP/logout.jsp"
																						class="logd">Logout</a> --%> <a
																						href="javascript:void();"
																						onclick='return logoutUser("jsp");' class="logd">Cerrar sesi�n</a>

																					</td>

																				</tr>



																			</table></td>
																	</tr>
																	<tr>
																		<td style="padding: 2px 0px 0px 20px"><img

																			src="${pageContext.request.contextPath}/images/slogan.png"
																			width="652" height="" /></td>


																		<td style="padding: 8px 10px 5px 20px">
																		<img 	src="${pageContext.request.contextPath}/images/flag.png"
																			width="118" height="" />

																			&nbsp;
																			</td>
																	</tr>



																	<tr>
																		<td>&nbsp;</td>

																	</tr>
																</table></td>
														</tr>
													</table></td>
											</tr>
										</table></td>
								</tr>

							</table></td>
					</tr>
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">

								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td align="center"><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/JSP/home.jsp"
															class="menulink"><strong>Casa</strong></a></li>
													</ul></td>
												<td width="3"><img src="images/w-bg.png" width="3"
													height="30" /></td>
												<%-- <td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Submit
																	Report</strong></a>
															<ul>
																<li><a
																	href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Upload
																		XML</a></li>
																<li><a href="upload-xml.html">Upload XLS/CSV </a></li>
																<li><a
																	href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																		Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li>
															</ul></li>
													</ul></td> --%>


												<td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Enviar Informe</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<%
																	} else if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	}
																%>


															</ul></li>
													</ul></td>

												<td width="3"><img src="images/w-bg.png" width="3"
													height="30" /></td>


												<td><ul id="reportTab" class="reportTab">
														<li><a class="menulink" style="background-color: #2487a2" href="#"><strong>Informe Estado</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<%
																	} else if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>
																<%
																	} else {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>

																<%
																	}
																%>

															</ul></li>
													</ul></td>



												<!-- <td><ul class="drop_menu">
														<li><a href='javascript:void();'><strong>Report
																	Status</strong></a></li>
													</ul></td> -->
												<td width="3"><img src="images/w-bg.png" width="3"
													height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img src="images/w-bg.png" width="3"
													height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Cont�ctenos</strong></a></li>
													</ul></td>
											</tr>

										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>

									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td width="25"></td>
												<td valign="top"><table width="900" border="0"
														cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td valign="top"><table width="900" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>
																		<td width="9"><img src="images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td background="images/g-t-m.png" class="bgtext1">FI
																			Reporting Life Cycle</td>
																		<td width="9"><img src="images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td background="images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td><table width="882" border="0" cellspacing="0"
																				cellpadding="0">
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td style="text-align: justify;">Esta secci�n muestra el
																					avance/fase del Informe actual. Para mayor informaci�n por
																					favor hacer clic en cada etapa. Para su terminaci�n debe alcanzar
																					la etapa de "Informe exitoso".</td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td>
																						<form
																							action="${pageContext.request.contextPath}/ReportStatus"
																							method="post" name="reportStatusform">
																							<table width="840" border="0" cellspacing="0"
																								cellpadding="0">
																								<tr>
																									<td width="100">A�o del Informe </td>
																									<td width="220"><select
																										name="reporting_year" id="reporting_year"
																										class="form-control"
																										style="width: 200px; padding: 0px 2px 2px 5px;">
																											<!-- <option value="">-------Select-----</option> -->
																											<%
																												year = year - 1;
																												while (year >= startYear) {
																											%>
																											<option value="<%=year%>"><%=year%></option>
																											<%
																												year--;
																												}
																											%>
																											<script>
																												var reportingYear =
																											<%=reportingYearFromSession%>
																												;
																												var formObj = document
																														.getElementById('reporting_year');
																												for ( var i = 0; i < formObj.length; i++) {
																													if (formObj[i].value == reportingYear) {
																														formObj[i].selected = true;
																														break;
																													}
																												}
																											</script>

																									</select></td>
																									<td><a href="javascript: void();"
																										onclick="return submitStatusform();"><img
																											src="${pageContext.request.contextPath}/images/search.png"
																											width="31" height="24" border="0"
																											style="vertical-align: middle;" /></a></td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																								</tr>
																							</table>
																						</form>
																					</td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td><table width="800" border="0"
																							cellspacing="0" cellpadding="0" align="center">
																							<tr>


																								<%
																								System.out.println(reportName);
																									if (reportName == null || reportName.equalsIgnoreCase("")
																											|| !(reportName.length() > 0)) {
																								%>
																								<td align="center"><img
																									src="images/upload1.png" width="114"
																									height="99" /></td>
																								<td align="center"><img
																									src="images/pv2.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/ird1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>
																								<%
																									} 
																									
																									else if (fistage.equalsIgnoreCase("Portal Validation")) {
																										if (fistatus.length() > 0
																												&& fistatus.toUpperCase().contains("SUCCESS")) {
																											stageSeq = 21;
																								%>

																								<td align="center"><img
																									src="images/upload.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/pv1.png" width="114" height="99" /></td>

																								<td align="center"><img
																									src="images/ird_y.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>
																								<%
																									} else if (fistatus.length() > 0
																												&& fistatus.toUpperCase().contains("INPROGRESS")) {
																											stageSeq = 22;
																								%>

																								<td align="center"><img
																									src="images/upload.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/pv1_y.png" width="114" height="99" /></td>

																								<td align="center"><img
																									src="images/ird1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>
																								<%
																									} else

																										{
																											stageSeq = 20;
																								%>

																								<td align="center"><img
																									src="images/upload.png" width="114" height="99" /></td>
																								<td align="center"><img src="images/pv.png"
																									width="114" height="99" /></td>

																								<td align="center"><img
																									src="images/ird1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>
																								<%
																									}
																									} else if (fistage.equalsIgnoreCase("IRD Verification")) {
																										if (fistatus.length() > 0
																												&& fistatus.toUpperCase().contains("SUCCESS")) {
																											stageSeq = 31;
																								%>

																								<td align="center"><img
																									src="images/upload.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/pv1.png" width="114" height="99" /></td>

																								<td align="center"><img
																									src="images/ird.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs_y.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>
																								<%
																									} else if (fistatus.length() > 0
																												&& fistatus.toUpperCase().contains("INPROGRESS")) {
																											stageSeq = 32;
																								%>

																								<td align="center"><img
																									src="images/upload.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/pv1.png" width="114" height="99" /></td>

																								<td align="center"><img
																									src="images/ird_y.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>
																								<%
																									} else {
																											stageSeq = 30;
																								%>
																								<td align="center"><img
																									src="images/upload.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/pv1.png" width="114" height="99" /></td>

																								<td align="center"><img
																									src="images/ird2.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>
																								<%
																									}
																									} else if (fistage.equalsIgnoreCase("Submitted to IRS")) {
																										if (fistatus.length() > 0
																												&& (fistatus.toUpperCase().contains("SUCCESSFUL"))) {
																											stageSeq = 41;
																								%>

																								<td align="center"><img
																									src="images/upload.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/pv1.png" width="114" height="99" /></td>

																								<td align="center"><img
																									src="images/ird.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe_y.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>

																								<%
																									} else if (fistatus.toUpperCase().contains("INPROGRESS")) {
																											stageSeq = 42;
																								%>

																								<td align="center"><img
																									src="images/upload.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/pv1.png" width="114" height="99" /></td>

																								<td align="center"><img
																									src="images/ird.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs_y.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>

																								<%
																									} else {
																											stageSeq = 40;
																								%>

																								<td align="center"><img
																									src="images/upload.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/pv1.png" width="114" height="99" /></td>

																								<td align="center"><img
																									src="images/ird.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs2.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>
																								<%
																									}
																									}else if (fistage.equalsIgnoreCase("RFE BY IRS")) {
																										if (fistatus.length() > 0
																												&& (fistatus.toUpperCase().contains("SUCCESSFUL"))) {
																											stageSeq = 61;
																								%>

																								<td align="center"><img
																									src="images/upload.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/pv1.png" width="114" height="99" /></td>

																								<td align="center"><img
																									src="images/ird.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe_y.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>

																								<%
																									} else if (fistatus.toUpperCase().contains("INPROGRESS")) {
																											stageSeq = 62;
																								%>

																								<td align="center"><img
																									src="images/upload.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/pv1.png" width="114" height="99" /></td>

																								<td align="center"><img
																									src="images/ird.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs_y.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe1.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>

																								<%
																									} else {
																											stageSeq = 63;
																								%>

																								<td align="center"><img
																									src="images/upload.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/pv1.png" width="114" height="99" /></td>

																								<td align="center"><img
																									src="images/ird.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/irs.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/rfe3.png" width="114" height="99" /></td>
																								<td align="center"><img
																									src="images/r-succssesful1.png" width="114"
																									height="99" /></td>
																								<%
																									}
																									}

																								%>


																							</tr>
																						</table></td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td><table width="650" border="0"
																							cellspacing="0" cellpadding="0" align="center"
																							style="border: 2px solid #a1a1a1; padding: 10px 10px; background: #e9e9e9; width: 650px; border-radius: 10px;">

																							<%
																								if (stageSeq == 22) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>
																										Portal Validation :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">submission
																									for the current Reporting Cycle is under Portal
																									Validation</td>
																							</tr>
																							<%
																								} else if (stageSeq == 21) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>Portal
																										Validation :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Tu Presentaci�n
																								del Ciclo de Informes actual es Bajo verificaci�n por
																								el IRD. Puede ser contactado Por el IRD y / o recibir
																								una notificaci�n por correo electr�nico si Su solicitud
																								se encuentra que tiene errores o Se requiere mayor aclaraci�n.</td>
																							</tr>
																							<%
																								} else if (stageSeq == 20) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>Portal de Validaci�n :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Tu Presentaci�n del Ciclo
																								de Informes actual es Bajo Validaci�n de portal. Consulte el
																								informe Haciendo clic en el enlace de la tabla
																								de abajo y subiendo Xml actualizado</td>
																							</tr>
																							<%
																								} else if (stageSeq == 30) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>Verificacion de IRD :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Tu Presentaci�n del
																								Ciclo de Informes actual es Bajo verificaci�n por IRD.
																								Por favor vea el informe Haciendo clic en el enlace de
																								la tabla de abajo y subiendo Xml actualizado.</td>
																							</tr>
																							<%
																								} else if (stageSeq == 31) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>Verificacion de IRD :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Tu Presentaci�n del Ciclo 
																								de Informes actual es Bajo verificaci�n por el IRS. 
																								Puede ser contactado Por el IRS y / o recibir una 
																								notificaci�n por correo electr�nico si Su solicitud 
																								se encuentra que tiene errores o Se requiere mayor aclaraci�n.</td>
																							</tr>
																							<%
																								} else if (stageSeq == 32) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>Verificacion de IRD :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Tu Presentaci�n 
																								del Ciclo de Informes actual es Bajo verificaci�n por IRD. 
																								Por favor vea el informe Haciendo clic en el enlace
																								de la tabla de abajo y subiendo Xml actualizado.</td>
																							</tr>
																							<%
																								} else if (stageSeq == 40) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>Presentada A la verificaci�n del IRS :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Tu Presentaci�n del Ciclo de Informes actual Presentado al IRS</td>
																							</tr>
																							<%
																								} else if (stageSeq == 41) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>
																										Presentado al IRS :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Sumisi�n Para el Ciclo de Informes actual ha sido Presentado al IR</td>
																							</tr>
																							<%
																								} else if (stageSeq == 42) {
																							%>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;"><strong>
																										Presentado al IRS :-</strong></td>
																							</tr>
																							<tr>
																								<td style="padding: 5px 5px 5px 5px;">Sumisi�n Para el Ciclo de Informes actual est� bajo Presentado al IRS</td>
																							</tr>

																							<%
																								}else if (stageSeq == 63) {
																									%>
																									<tr>
																										<td style="padding: 5px 5px 5px 5px;"><strong>RFE por el IRS :-</strong></td>
																									</tr>
																									<tr>
																										<td style="padding: 5px 5px 5px 5px;">Tu Presentaci�n del 
																										Ciclo de Informes actual es Verificado por el IRD. Se ha 
																										encontrado que su solicitud contiene errores. Consulte el 
																										informe de errores para mayor aclaraci�n..</td>
																									</tr>
																									<%
																								}
																							%>
																						</table></td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																			</table></td>
																		<td background="images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td><img src="images/c-b-l.png" width="9"
																			height="5" /></td>
																		<td style="background-image: url(images/g2.png)"></td>
																		<td><img src="images/c-b-r.png" width="9"
																			height="5" /></td>
																	</tr>
																</table></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td><table width="900" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td width="9"><img src="images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td background="images/g-t-m.png" class="bgtext1">Historial del Env�o</td>
																		<td width="9"><img src="images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td background="images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td><table width="750" border="0" cellspacing="0"
																				cellpadding="0" align="center">
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td><table width="750" border="0"
																							cellspacing="0" cellpadding="0">
																							<tr>
																								<td width="9"><img src="images/g-t-l.png"
																									width="9" height="29" /></td>
																								<td background="images/g-t-m.png"
																									class="bgtext1">�ltimo env�o </td>
																								<td width="9"><img src="images/g-t-r.png"
																									width="9" height="29" /></td>
																							</tr>
																							<tr>
																								<td background="images/g.png"
																									style="background-repeat: repeat-y;"></td>
																								<td><table width="750" border="0"
																										cellspacing="0" cellpadding="0">
																										<tr>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																										</tr>
																										<tr>
																											<td colspan="4">Esta secci�n muestra el
																											hist�ricos de los cambios realizados en
																											cada uno de sus env�os y se muestra el
																											estado del �ltimo reporte. Nota: Esta secci�n
																											muestra el estado de los reportes de env�os
																											individuales y no la fase en la que se encuentre
																											su Instituci�n financiera  en el Ciclo de Reporteo.</td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																										</tr>
																										<tr>
																											<td width="130">Etapa y Estado</td>
																											<td>&nbsp;</td>

																											<%
																												if (reportName == null || reportName.equalsIgnoreCase("")
																														|| !(reportName.length() > 0)) {
																											%>
																											<td align="center"><img
																												src="images/upload1.png" width="114"
																												height="99" /></td>
																											<%
																												} else if (stage.equalsIgnoreCase("Portal Validation")) {
																													if (status.length() > 0
																															&& status.toUpperCase().contains("SUCCESS")) {
																											%>
																											<td><a href="#"><img
																													src="images/pv1.png" width="114"
																													height="99" border="0" /></a></td>
																											<%
																												} else if (status.length() > 0
																															&& status.toUpperCase().contains("INPROGRESS")) {
																											%>

																											<td><a href="#"><img
																													src="images/pv1_y.png" width="114"
																													height="99" border="0" /></a></td>
																											<%
																												} else {
																											%>

																											<td><a href="#"><img
																													src="images/pv.png" width="114" height="99"
																													border="0" /></a></td>
																											<%
																												}
																												} else if (stage.equalsIgnoreCase("IRD Verification")) {
																													if (status.length() > 0
																															&& status.toUpperCase().contains("SUCCESS")) {
																											%>
																											<td><a href="#"><img
																													src="images/ird.png" width="114"
																													height="99" border="0" /></a></td>

																											<%
																												} else if (status.toUpperCase().contains("INPROGRESS")) {
																											%>
																											<td><a href="#"><img
																													src="images/ird_y.png" width="114"
																													height="99" border="0" /></a></td>
																											<%
																												} else {
																											%>
																											<td><a href="#"><img
																													src="images/ird2.png" width="114"
																													height="99" border="0" /></a></td>
																											<%
																												}
																												} else if (stage.equalsIgnoreCase("Submitted to IRS")) {
																													if (status.length() > 0
																															&& (status.toUpperCase().contains("SUCCESS"))) {
																											%>
																											<td><a href="#"><img
																													src="images/irs.png" width="114"
																													height="99" border="0" /></a></td>

																											<%
																												} else if (status.toUpperCase().contains("INPROGRESS")) {
																											%>
																											<td><a href="#"><img
																													src="images/irs_y.png" width="114"
																													height="99" border="0" /></a></td>
																											<%
																												} else {
																											%>
																											<td><a href="#"><img
																													src="images/irs2.png" width="114"
																													height="99" border="0" /></a></td>
																											<%
																												}
																												}
																												else if (stage.equalsIgnoreCase("RFE BY IRS")) {
																													if (status.length() > 0
																															&& (status.toUpperCase().contains("SUCCESS"))) {
																											%>
																											<td><a href="#"><img
																													src="images/rfe.png" width="114"
																													height="99" border="0" /></a></td>

																											<%
																												} else if (status.toUpperCase().contains("INPROGRESS")) {
																											%>
																											<td><a href="#"><img
																													src="images/rfe1.png" width="114"
																													height="99" border="0" /></a></td>
																											<%
																												} else {
																											%>
																											<td><a href="#"><img
																													src="images/rfe3.png" width="114"
																													height="99" border="0" /></a></td>
																											<%
																												}
																												}
																											%>




																											<td width="400"><table width="400"
																													border="0" cellspacing="0" cellpadding="0">
																													<tr>
																														<td>&nbsp;</td>
																														<td>&nbsp;</td>
																													</tr>
																													<tr>
																														<td>Nombre del Informe</td>
																														<td><input type="text"
																															readonly="true" value='<%=reportName%>'
																															name="user" class="form-control"
																															/ style="padding: 0px 2px 2px 5px; width: 200px;"></td>
																													</tr>
																													<tr>
																														<td>&nbsp;</td>
																														<td>&nbsp;</td>
																													</tr>
																													<tr>
																														<td>Fecha de Env�o</td>
																														<td><input type="text"
																															readonly="true"
																															value='<%=submissionDate%>' name="user"
																															class="form-control"
																															style="padding: 0px 2px 2px 5px; width: 200px;"></td>
																													</tr>
																												</table></td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																										</tr>
																										<tr>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																											<td>&nbsp;</td>
																										</tr>
																									</table></td>
																								<td background="images/g1.png"
																									style="background-repeat: repeat-y;"></td>
																							</tr>
																							<tr>
																								<td colspan="3"
																									style="background-image: url(images/g21.png); background-repeat: repeat-x;"
																									height="1"></td>
																							</tr>
																						</table></td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																				<tr>
																					<td>
																						<div id='transactionBatchTable'
																							name='transactionBatchTable'>
																							<table width="770" border="1"
																								style="table-layout: fixed;" cellspacing="0"
																								cellpadding="0">
																								<tr class="bg1">
																									<td width="25%" class="ipm_n">Nombre del Informe</td>
																									<td width="15%" class="ipm_n">Enviado por</td>
																									<td width="10%" class="ipm_n">Clase</td>
																									<td width="15%" class="ipm_n">Etapa</td>
																									<td width="15%" class="ipm_n">Estado (?)</td>
																									<td width="20%" class="ipm_n">Fecha y Hora</td>
																								</tr>
																								<%
																									try {

																										int pageCount = (Integer) request.getAttribute("pageCount");
																										String totalCount = (String) request.getAttribute("totalCount");
																										String nextFlag = (String) request.getAttribute("nextFlag");
																										int prevPage = pageCount - 1;
																										int nextPage = pageCount + 1;

																										/* System.out.println("nextFlag : " + nextFlag);
																																											System.out.println("totalCount : " + totalCount);
																										System.out.println("pageCount : " + pageCount);
																										System.out.println("prevPage : " + prevPage);
																										System.out.println("nextPage : " + nextPage); */
																										for (TransactionXMLBean obj : XMLBeans) {
																								%>
																								<!-- bug id 11608 -->
																								<tr>
																									<td class="ipm_n2"><a
																										data-toggle="tooltip"
																										title="<%=obj.getFileName()%>"><%=obj.getFileName()%></a></td>
																									<td class="ipm_n2"><a
																										data-toggle="tooltip"
																										title="<%=obj.getUploadedBy()%>"><%=obj.getUploadedBy()%></a></td>
																									<td class="ipm_n2"><a
																										data-toggle="tooltip"
																										title="<%=obj.getFiletype()%>"><%=obj.getFiletype()%></a></td>
																									<td class="ipm_n2"><a
																										data-toggle="tooltip"
																										title="<%=obj.getFileStage()%>"><%=obj.getFileStage()%></a></td>
																									<%
																										//if (obj.getFileStatus().equalsIgnoreCase("failure")) {
																												if (obj.getFileStatus().length() > 0) {

																													/* System.out.println("obj.getFileStatus().length is >0 "); */
																													if (obj.getFileStatus().toUpperCase().contains("FAIL")) {
																									%>
																									<td class="ipm_n2"><a
																										href="${pageContext.request.contextPath}/Download?path=<%=obj.getHtmlFilepath() %>"
																										style="color: #015f7a;"> <span
																											style="color: #015f7a;"><%=obj.getFileStatus()%></span></a></td>
																									<%
																										} else {
																									%>
																									<td class="ipm_n2"><a
																										data-toggle="tooltip"
																										title="<%=obj.getFileStatus()%>"><%=obj.getFileStatus()%></a></td>
																									<%
																										}
																									%>
																									<%-- <td class="ipm_n2"><%=obj.getFileStatus()%></td> --%>
																									<%
																										}
																												//}
																									%>
																									<td width="120"><a data-toggle="tooltip"
																										title="<%=obj.getUploadedDate()%>"><%=obj.getUploadedDate()%></a></td>

																								</tr>
																								<!-- bug id 11608 -->
																								<%
																									}
																								%>
																								
																								<tr>
																									<td colspan=5>&nbsp;</td>
																									<td align="right">
																										<%
																											if (prevPage >= 0) {
																										%> <a href="javascript:void();"
																										onclick="getTransactionBatch('<%=prevPage%>','<%=totalCount%>','Fatca');">anterior</a>
																										<%
																											} else {
																										%> Prev <%
																											}
																										%> | <%
																											if (nextFlag != null && nextFlag.equalsIgnoreCase("TRUE")) {
																										%> <a href="javascript:void();"
																										onclick="getTransactionBatch('<%=nextPage%>','<%=totalCount%>','Fatca');">siguiente</a>
																										<%
																											} else {
																										%> Next <%
																											}
																										%>

																									</td>
																								</tr>
																							</table>
																						</div>

																					</td>
																				</tr>
																				<%
																					} catch (Exception e) {
																						System.out.println("Exception in Looop : " + e);
																						e.printStackTrace();
																					}
																				%>

																				<tr>
																					<td>&nbsp;</td>
																				</tr>
																			</table></td>
																		<td background="images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td colspan="3"
																			style="background-image: url(images/g21.png); background-repeat: repeat-x;"
																			height="1"></td>
																	</tr>
																</table></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
													</table></td>
												<td width="20"></td>

											</tr>
										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>

								<tr>
									<td align="center"><img src="images/line.png" width="921"
										height="14" /></td>
								</tr>

								<%-- <tr>
									<td class="hedr">For Govt. of <%=com.newgen.bean.GlobalVariables.projectName%>
										Powered by Newgen Software Inc.
									</td>
								</tr> --%>

								<tr>
									<td>&nbsp;</td>
								</tr>
							</table></td>
					</tr>

				</table></td>
			<td background="images/bg-g-r.gif" height="1" width="19"></td>
		</tr>
	</table>
	<script type="text/javascript">
		var menu1 = new menu.dd("menu1");
		menu1.init("menu", "menuhover");
		  var reportMenu = new menu.dd("reportMenu");
		  reportMenu.init("reportTab", "menuhover");  
	</script>
</body>
</html>
