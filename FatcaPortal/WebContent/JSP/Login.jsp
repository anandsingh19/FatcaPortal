
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.newgen.bean.GlobalVariables"%>
<%-- <%@page import="com.newgen.util.ClsMessageHandler"%>
<%@page import="com.newgen.util.ClsUtil"%> --%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.io.IOException"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<%!private void preCompileJsp(PageContext pageContext, JspWriter out,
			HttpServletRequest request, HttpServletResponse response,
			String uripath) throws IOException, ServletException {
		// getting jsp files list for pre compilation      
		Set jspFilesSet = pageContext.getServletContext().getResourcePaths(
				uripath);
		for (Iterator jspFilesSetItr = jspFilesSet.iterator(); jspFilesSetItr
				.hasNext();) {
			String uri = (String) jspFilesSetItr.next();
			if (uri.endsWith(".jsp")) {
				RequestDispatcher rd = getServletContext()
						.getRequestDispatcher(uri);
				if (rd == null)
					throw new Error(uri + " - not found");
				rd.include(request, response);
			} else if (uri.endsWith("/")) {
				preCompileJsp(pageContext, out, request, response, "/JSP/");
			}
		}
	}%>
<%
	HttpServletRequest req = new HttpServletRequestWrapper(request) {
		public String getQueryString() {
			// setting the pre compile parameter           
			return "jsp_precompile";
		};
	};
	preCompileJsp(pageContext, out, req, response, "/");
%>
<%
	HttpSession sesion = request.getSession(true);
	if (!session.isNew()) {
		//session.invalidate();
		session = request.getSession(true); // get a new session
	}
	//ClsUtil utility = new ClsUtil();
	//sesion.setAttribute("eToken", utility.GetEncryptKey());
	//out.println("etoken:"+sesion.getAttribute("eToken"));
	//utility = null;
%>
<head>
<%
	response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1 
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0); //prevents caching at the proxy server
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: <%=GlobalVariables.projectName%> Inland Revenue
	Department ::
</title>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<script src="${pageContext.request.contextPath}/script/wdgeneral.js"></script>
<script src="${pageContext.request.contextPath}/script/blowfish.js"></script>
<script src="${pageContext.request.contextPath}/script/Common.js"></script>
<script src="${pageContext.request.contextPath}/script/md5.js"></script>

<script type="text/javascript">
	window.history.forward(1);
	document.onkeydown = function() {

		//alert(window.event.keyCode);
		if (window.event.keyCode == 13) { // Capture and remap F5  
			document.getElementById("loginBtn").click();
		}
	}
</script>
<!-- Changes made by Alok on 21-11-2016 Start -->
<script>
var value;
 function myFunction1()
 {
	 value=myFunction();
 }
    function myFunction() { 
    	
        if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) 
    {
        	 alert("Tu navegador no es compatible");
        	window.location="Error.jsp"
    	
    }
    else if(navigator.userAgent.indexOf("Chrome") != -1 )
    {
    	return true;
    	

    	
       
    }
    else if(navigator.userAgent.indexOf("Safari") != -1)
    {
    	 alert("Tu navegador no es compatible");
    	window.location="Error.jsp"   
    }
    else if(navigator.userAgent.indexOf("Firefox") != -1 ) 
    {
    	 alert("Tu navegador no es compatible");
    	window.location="Error.jsp"
    }
    else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
    {
    	return true;
     
    }  
    else 
    {
       return false;
    }
    }
   
   
    </script>
    


</head>


<body onload="myFunction1()">
<!-- Changes made by Alok on 21-11-2016 End -->

	<%
		String errormsg = (String) request.getAttribute("msg");
		System.out.println("error on jsp--> " + errormsg);
		
		if (errormsg == null) {
			errormsg = "";
		}
	%>
	<table width="1003" border="0" cellspacing="0" cellpadding="0"
		align="center" bgcolor="#FFFFFF">
		<tr>
			<td background="${pageContext.request.contextPath}/images/bg-g-l.gif"
				width="19" height="1"></td>
			<td><table width="965" border="0" cellspacing="0"
					cellpadding="0">
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td
													background="${pageContext.request.contextPath}/images/bg.png"
													height="140"><table width="965" border="0"
														cellspacing="0" cellpadding="0">
														<tr>
															<td style="padding: 0px 0px 3px 10px" width="130"><img
																src="${pageContext.request.contextPath}/images/logo.png"
																width="118" height="135" /></td>
															<td valign="top"><table width="835" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>


																		<td colspan="2"><table border="0" cellspacing="0"
																				cellpadding="0" align="right">
																				<tr>
																					<td>&nbsp;</td>
																					<td>&nbsp;</td>
																					<td>&nbsp;</td>
																				</tr>
																			</table></td>
																	</tr>
																	<tr>
																		<td style="padding: 2px 0px 0px 20px"><img
																			src="${pageContext.request.contextPath}/images/slogan.png"
																			width="652" height="" /></td>
																		<td style="padding: 8px 10px 5px 20px"><img
																			src="${pageContext.request.contextPath}/images/flag.png"
																			width="118" height="" /> &nbsp;</td>
																	</tr>
																	<!-- <tr>
																		<td>&nbsp;</td>
																	</tr><tr>
																		<td>&nbsp;</td>
																	</tr> -->
																</table></td>
														</tr>
													</table></td>
											</tr>
										</table></td>
								</tr>

							</table></td>
					</tr>
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">

								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td valign="top"><table width="620" border="0"
														cellspacing="0" cellpadding="0">
														<tr>
															<td>&nbsp;</td>
														</tr>
														<!-- <tr>
															<td class="text" style="color: red;">The deadline
																date for submission is 16th September 2016 at 11.59 pm</td>
														</tr> -->
														<tr>
															<%-- <td class="text">Welcome to <%=GlobalVariables.projectName%>
																Inland Revenue Department Reporting Portal.
															</td> --%>
															<td class="text">Bienvenido al portal de informes de la DGII</td>
														</tr>
														<tr>



															<td class="text1">TEl Gobierno de Direcci�n General de Impuestos Internos ha firmado un
															Acuerdo Intergubernamental para intercambiar informaci�n basada en las directrices de la
															FATCA y la CRS. En virtud de dicha Ley FATCA Y  las entidades pertinentes deben presentar
															informes sobre las cuentas financieras y los activos financieros de personas determinadas
															en los Estados Unidos o compa��as  en las que los ciudadanos estadounidenses tengan una
															participaci�n mayoritaria o sustancial. Es obligatorio para las instituciones financieras
															dependientes del CRS declarar las a los respectivos gobiernos, dicha iniciativa tiene
															como objetivo combatir la evasi�n fiscal y aumentar el cumplimiento voluntario de impuestos. Visita <a
																href="http://www.gov.vc"
																target="_blank" style="color: #015f7a;"><span
																	style="color: #015f7a;">http://www.gov.vc</span></a> or <a
																href="https://www.irs.gov/"
																target="_blank" style="color: #015f7a;"><span
																	style="color: #015f7a;">www.irs.gov</span></a> para saber m�s.


															</td>
														</tr>
														<tr>
															<td height="10"></td>
														</tr>
														<tr>
															<td class="text1">El portal de Direcci�n General de Impuestos Internos le ofrece todas
															las herramientas necesarias para la presentaci�n de reportes los clientes reportables
															a FATCA y CRS.
															Para ingresar al portal como un nuevaentidad financiera, haga clic en Crear Cuenta o
															utilice la secci�n de Inicio de Sesi�npara cuentas existentes. . </td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>

														<%-- <tr>
															<td class="text2"><a
																href="${pageContext.request.contextPath}/JSP/account-registration.jsp"
																class="text2"><img
																	src="${pageContext.request.contextPath}/images/new_Account.png"></a></td>

														</tr> --%>
														<!-- <tr>
															<td height="50"></td>
														</tr> -->
														<tr>

															<td class="text1">Para obtener ayuda, consulte el apartado <a
																href="${pageContext.request.contextPath}/FAQServlet?callFor=FAQ"
																style="color: #015f7a;"><span
																	style="color: #015f7a;">Preguntas Frecuentes.</span></a></td>
															<%-- <td class="text1">For help, please see <a
																href="${pageContext.request.contextPath}/JSP/faq_new.jsp"
																style="color: #015f7a;"><span
																	style="color: #015f7a;">Frequently Asked
																		Questions.</span></a></td> --%>
														</tr>
														<tr>
															<td height="5"></td>
														</tr>
														<tr>
															<td class="text1">Para m�s preguntas, por favor <a
																href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'
																style="color: #015f7a;"><span
																	style="color: #015f7a;">Cont�ctenos</span></a></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
													</table></td>
												<td width="9">&nbsp;</td>
												<td valign="top"><table width="336" border="0"
														cellspacing="0" cellpadding="0">
														<tr>
															<td width="16"></td>
															<td><table width="304" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td height="40">&nbsp;</td>
																	</tr>
																	<tr>
																		<td width="304" valign="top" class="logn_bg">

																			<form
																				action="${pageContext.request.contextPath}/LoginServlet"
																				method="post" name="login">
																				<input type="hidden" id="code" name="code">
																				<table width="304" border="0" cellspacing="0"
																					cellpadding="0">

																					<tr>
																						<td colspan="2" height="40"></td>
																					</tr>

																					<tr>
																						<td style="color: red" align="center"
																							id="loginErrorMsg" colspan="2">&nbsp;<%=errormsg%></td>
																					</tr>




																					<tr>

																						<!--Change for all by Shubham on 28/072016 'user name changed to  Email ID' starts -->
																						<td class="Bodytxt" width="100">Email ID :</td>
																						<!--Change for all by Shubham on 28/072016 end -->

																						<td width="220"><input type="text"
																							tabindex="1" class="form-control" name="userName"
																							id="userName"
																							style="background: white url(${pageContext.request.contextPath}/images/user.png) no-repeat center right;padding:0px 2px 2px 5px;height:25px; width:170px;" /></td>
																					</tr>
																					<tr>
																						<td colspan="2" height="10"></td>
																					</tr>
																					<tr>
																						<td class="Bodytxt" width="100">Contrase�a :</td>
																						<td width="220"><input type="password"
																							tabindex="2" class="form-control" name="password"
																							id="password"
																							style="background: white url(${pageContext.request.contextPath}/images/pwd.png) no-repeat center right;padding:0px 2px 2px 5px;height:25px; width:170px;" /></td>
																					</tr>

																					<tr>
																						<td colspan="2" height="10"></td>
																					</tr>
																					<tr>
																						<td class="Bodytxt" width="100">Captcha :</td>
																						<td class="captcha" id="captcha"><img
																							src="${pageContext.request.contextPath}/Captchaimage"
																							id="imgCaptcha" style="margin: 0px 2px 10px;" />
																							<a id="refreshCaptch" href="#"
																							onclick="resetCaptcha('Login');return false;"><img
																								src="${pageContext.request.contextPath}/images/refresh.JPG"
																								border="0" id="imgRefresh"
																								style="margin: 0px 2px 10px;" height="20px" /></a>
																							<div style="clear: both"></div> <input
																							tabindex="3" type="text" id="txtInput"
																							style="width: 160px" class="form-control"
																							onDrag="alert('Copiar / Pegar se ha restringido');return false;"
																							onDrop="alert('Copiar / Pegar se ha restringido');return false;"
																							onpaste="alert('Copiar / Pegar se ha restringido');return false;" />
																							<!-- <input type="button" id="submitCaptcha"
																									class="btn" value="Submit"
																									onclick="checkCaptcha();"  />--></td>
																						<%-- <td width="220"><img
																							src="${pageContext.request.contextPath}/images/captcha.png"
																							width="100" height="31" /> &nbsp;<a href="#"><img
																								src="${pageContext.request.contextPath}/images/refresh.png"
																								width="25" height="31" border="0" /></a></td> --%>
																					</tr>

																					<tr>
																						<td colspan="2" height="10"></td>
																					</tr>
																					<tr>
																						<td width="100" align="right">&nbsp;</td>
																						<td width="220" class="f-pwd"><a tabindex="4"
																							href="${pageContext.request.contextPath}/JSP/forgotPassword.jsp"
																							class="f-pwd">�Se te olvid� tu contrase�a?</a></td>
																					</tr>
																					<tr>
																						<td colspan="2" height="10"></td>
																					</tr>
																					<tr>
																						<td>&nbsp;</td>
																						<td><a id="loginBtn"
																							href="javascript: void();" tabindex="5"
																							onclick="return checkCaptcha('','','Login');"><img
																								src="${pageContext.request.contextPath}/images/login.png"
																								width="132" height="24" border="0"></a></td>
																					</tr>

																				</table>


																			</form>


																		</td>
																	</tr>
																</table></td>
															<td width="16"></td>
														</tr>

														<tr>
															<td width="16"></td>
															<td align="center"><a
																href="${pageContext.request.contextPath}/JSP/account-registration.jsp"
																class="f-pwd"><img
																	src="${pageContext.request.contextPath}/images/new_Account.png"></a></td>
															<td width="16">&nbsp;</td>
														</tr>
													</table></td>
											</tr>
										</table></td>
								</tr>
								<!-- <tr>
									<td height="20">&nbsp;</td>
								</tr> -->
								<tr>
									<td align="center"><img
										src="${pageContext.request.contextPath}/images/line.png"
										width="921" height="10" /></td>
								</tr>


								<%-- 								<tr>
									<td class="hedr">For Govt. of <%=GlobalVariables.projectName%>
										Powered by Newgen Software Inc.
									</td>
								</tr>

 --%>
								<tr>
									<td><marquee
											style="background-color: #2487A2; color: white;"><%=GlobalVariables.supportedBrowsers%></marquee>
									</td>
								</tr>

								<tr>
									<td>&nbsp;</td>
								</tr>
							</table></td>
					</tr>

				</table></td>
			<td background="${pageContext.request.contextPath}/images/bg-g-r.gif"
				height="1" width="19"></td>
		</tr>
	</table>

</body>
<script>
	function encryptPassword(src) {
		var doc = document;
		var id = undefined;
		try {
			if (src == "login") {

				if (doc.getElementById("userName").value == null
						|| doc.getElementById("userName").value == "") {
					doc.getElementById("loginErrorMsg").innerHTML = "El nombre de usuario no puede estar en blanco.";
					doc.getElementById("loginErrorMsg").className = "errorMsg";
					return false;
				}
				if (doc.getElementById("password").value == null
						|| doc.getElementById("password").value == "") {
					doc.getElementById("loginErrorMsg").innerHTML = "La contrase�a no puede estar en blanco.";
					doc.getElementById("loginErrorMsg").className = "errorMsg";
					return false;
				}
				id = "password";
			}

			var uiPwd = doc.getElementById(id).value;
			var hashedPwd = CryptoJS.MD5(uiPwd);
			/* var bf = new Blowfish('DES');
			var eToken = '${sessionScope.eToken}'; 
			var sPwd = bf.encryptx(uiPwd, eToken);
			var uiPwd2 = encode_utf8(sPwd);*/
			doc.getElementById(id).value = hashedPwd;

			return true;
		} catch (e) {
			alert(e);
		}

	}
</script>

</html>
