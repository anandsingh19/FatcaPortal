<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>:: <%=com.newgen.bean.GlobalVariables.projectName%>
	Inland Revenue Department ::
</title>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<script type="text/javascript">
	window.history.forward(-1);
</script>
</head>
<body>
<% 
String source=(String)request.getAttribute("source");
//String source="registration";
if(null!=source && source.equalsIgnoreCase("logout"))
{
%>
<p> Has logrado salir con �xito. Por favor, haga clic en iniciar sesi�n<a href="${pageContext.request.contextPath}/index.jsp">login</a>
<%
}
else if(null!=source && source.equalsIgnoreCase("registration")){
%>
<p> Gracias por la solicitud. Su solicitud de registro ha sido enviada para su aprobaci�n al IRD. Recibir� una notificaci�n por correo electr�nico sobre la aprobaci�n de la solicitud. Por favor, ingrese haciendo clic<a href="${pageContext.request.contextPath}/index.jsp">login</a>
<%
}
else if(null!=source && source.equalsIgnoreCase("forgotpassword")){
%>
<p> Tu contrase�a ha sido cambiada exitosamente. Por favor, ingrese haciendo clic<a href="${pageContext.request.contextPath}/index.jsp">login</a>
<%
}
%>
</body>
</html>