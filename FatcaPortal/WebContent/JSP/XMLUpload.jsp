<%@page import="java.util.*"%>
<%@page import="com.newgen.bean.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: <%=com.newgen.bean.GlobalVariables.projectName%>
	Revenue Authority ::
</title>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/Common.css" />
<script src="${pageContext.request.contextPath}/script/jquery-1.9.1.js"></script>
<script src="${pageContext.request.contextPath}/script/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/script/Common.js"></script>
<script
	src="${pageContext.request.contextPath}/script/GlobalVariable.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/script.js"></script>
	
	<!-- Changes made by Alok on 21-11-2016 Start -->
	  <script type="text/javascript">
	 
            function validateFilename()
            {
                var thefile = document.getElementById('uploadFile');
                var fileName= thefile.value;
              if(fileName.includes("_Payload")){
            	  
            	  window.history.go(0);
                alert('Por favor, cambie el nombre del archivo que no debe contener Carga 鷗il');
              
                return false;
                
              }
          
            }
	  </script>
 <!-- Changes made by Alok on 21-11-2016 End -->
</head>
<script>
	function submitform() {
		//alert("submitform");
		var doc = document;

		if (!validate2('XMLUpload')) {
			//alert("submitform : false");
			return false;
		}

		document.xmluploadform.submit();

	}
</script>
<body onload='validateFilename'>
	<%
		Calendar now = Calendar.getInstance(); // Gets the current date and time
		int year = now.get(Calendar.YEAR);
		int startYear = GlobalVariables.reportingStartYear;
		String userType = "";
		String userSessionID = "";
		String userEmail = "";
		String userName = "";
		String complianceType = "";
		HttpSession session1 = request.getSession(false);
		userName = (String) session1.getAttribute("UserName");
		userType = (String) session1.getAttribute("UserType");
		userEmail = (String) session1.getAttribute("UserEmail");
		complianceType = (String) session1.getAttribute("ComplianceType");
		Map<String, String> loginUserMap = GlobalVariables.loginUserMap;
		userSessionID = loginUserMap.get(userEmail);
		if (null != userSessionID
				&& (!(userSessionID.equalsIgnoreCase(session1.getId())))
				|| null == userName) {
			response.sendRedirect(request.getContextPath()
					+ "/JSP/SessionExpired.jsp");
		} else if (null == userSessionID) {
			response.sendRedirect(request.getContextPath()
					+ "/JSP/SessionExpired.jsp");
		}

		else if (userType.equalsIgnoreCase("admin")
				|| userType.equalsIgnoreCase("fi admin")) {
			request.getRequestDispatcher("/JSP/admin-approval.jsp")
					.forward(request, response);
		}
	%>

	<table width="1003" border="0" cellspacing="0" cellpadding="0"
		align="center" bgcolor="#FFFFFF">
		<tr>
			<td background="${pageContext.request.contextPath}/images/bg-g-l.gif"
				width="19" height="1"></td>
			<td><table width="965" border="0" cellspacing="0"
					cellpadding="0">
					<tr>
						<td>
							<table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td>
									
									
									<table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td
													background="${pageContext.request.contextPath}/images/bg.png"
													height="140"><table width="965" border="0"
														cellspacing="0" cellpadding="0">
														<tr>
															<td style="padding: 0px 0px 3px 10px" width="130"><img
																src="${pageContext.request.contextPath}/images/logo.png"
																width="118" height="135" /></td>
															<td valign="top"><table width="835" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>


																		<td colspan="2">


																			<table border="0" cellspacing="0" cellpadding="0"
																				align="right">
																				<tr>





																					<td width="35" align="right"><img
																						src="${pageContext.request.contextPath}/images/us.png"
																						width="25" height="25" /></td>



																					<td class="logd1" style="text-align: left;"
																						width="150"><%=userName%> is <strong>&nbsp;&nbsp;Conectado</strong>&nbsp;</td>
																					<td class="logd" width="60"><a
																						href="javascript:void();"


																						onclick='return logoutUser("jsp");' class="logd">Cerrar sesi贸n</a></td>

																				</tr>
																			</table>

																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 2px 0px 0px 20px"><img

																			src="${pageContext.request.contextPath}/images/slogan.png"
																			width="652" height="" /></td>


																		<td style="padding: 8px 10px 5px 20px">
																		<img 	src="${pageContext.request.contextPath}/images/flag.png"
																			width="118" height="" />

																			&nbsp;
																			</td>
																	</tr>
																	<!-- <tr>
																		<td>&nbsp;</td>
																	</tr><tr>
																		<td>&nbsp;</td>
																	</tr> -->
																</table></td>
														</tr>
													</table></td>
											</tr>
										</table></td>
								</tr>

							</table></td>
					</tr>
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">

								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td align="center"><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/JSP/home.jsp"
															class="menulink"><strong>Casa</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>




												<%
													if (userType != null && userType.equalsIgnoreCase("admin")) {
												%>
												<td align="center"><ul class="drop_menu">
														<li><a
															href="${pageContext.request.contextPath}/JSP/admin-approval.jsp"
															class="menulink"><strong>Aprobaci贸n del usuario</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>



												<%
													} else {
												%>

												<%-- <td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Submit
																	Report</strong></a>
															<ul>
																<li><a href="#">Upload XML</a></li>
																<li><a href="#">Upload XLS/CSV </a></li>
																<li><a
																	href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																		Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li>
															</ul></li>
													</ul></td> --%>






												<td><ul class="menu" id="menu">
														<li><a href="#" style="background-color: #2487a2"
															class="menulink"><strong>Enviar reporte</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir XML</a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l铆nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<%
																	} else if (null != complianceType
																				&& complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir XML</a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir XML</a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l铆nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir XML</a></li>
																		<!-- <li><a href="#">Upload XLS/CSV </a></li> -->
																		<%-- <li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				Manually Online</a> <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul></li> --%>
																	</ul></li>
																<%
																	}
																%>


															</ul></li>
													</ul></td>




												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>

												<%
													}
												%>
												<%-- <td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/ReportStatus'><strong>Report
																	Status</strong></a></li>
													</ul></td> --%>


												<td><ul id="reportTab" class="reportTab">
														<li><a class="menulink" href="#"><strong>Informe Estado</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<%
																	} else if (null != complianceType && complianceType.length() > 0
																			&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>
																<%
																	} else {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>

																<%
																	}
																%>

															</ul></li>
													</ul></td>


												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
													</ul></td>
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>
												<td><ul class="drop_menu">
														<li><a
															href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Cont醕tenos</strong></a></li>
													</ul></td>
											</tr>

										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>

									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td width="25"></td>
												<td valign="top"><table width="700" border="0"
														cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td valign="top"><table width="700" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-l.png"
																			width="9" height="29" /></td>
																		<td
																			background="${pageContext.request.contextPath}/images/g-t-m.png"
																			class="bgtext11">Subir FATCA XML</td>
																		<td width="9"><img
																			src="${pageContext.request.contextPath}/images/g-t-r.png"
																			width="9" height="29" /></td>
																	</tr>
																	<tr>
																		<td
																			background="${pageContext.request.contextPath}/images/g.png"
																			style="background-repeat: repeat-y;"></td>
																		<td>

																			<form
																				action="${pageContext.request.contextPath}/XMLUpload"
																				enctype="multipart/form-data" method="post"
																				name="xmluploadform">
																				<input type="hidden" id="XMLtype" name="XMLtype"
																					value="FATCA" /> <input type="hidden"
																					id="reportingMode" value="XML" name="reportingMode" />
																				<table width="672" border="0" cellspacing="0"
																					cellpadding="0" align="center">
																					<tr>
																						<td colspan="2"><br /> Para subir el archivo XML  el formato prescrito,
																						 por favor seguir los siguientes pasos: Para mayor informaci贸n por favor
																						  consulte los enlaces de Ayuda en la P谩gina de Inicio.<br /> <br /></td>
																					</tr>
																					<tr>
																						<td colspan="2">



																							<div id="UploadMsg" style="color: red">
																								<%
																									String messageclr = "";
																									String msg = (String) request.getAttribute("msg");
																									//msg=msg+" Successfully ";

																									if (msg == null) {
																										msg = "&nbsp;";
																									} else {
																										/*Change for all by KD on 27072016 strats*/
																										if (msg.contains("progress"))
																											messageclr = "color:orange";
																										/*Change for all by KD on 27072016 end*/
																										else
																											messageclr = "color:red";
																									}
																								%>
																								<p style='<%=messageclr%>'><%=msg%></p>
																							</div>
																						</td>
																					</tr>
																					<tr>
																						<td style="width: 270px">Paso 1: Seleccione el A帽o de Informe &nbsp;</td>
																						<td><select name="reportingYear"
																							id="reportingYear" class="form-control"
																							onchange="return resetFields('Xml');"
																							style="width: 180px">

																								<option >--Seleccionar--</option>
																								<%
																									year = year - 1;
																									while (year >= startYear) {
																								%>
																								<option value="<%=year%>"><%=year%></option>
																								<%
																									year--;
																									}
																								%>
																						</select>&nbsp; <a class="tooltip" ><img
																								src="${pageContext.request.contextPath}/images/whts.png"
																								width="20" height="20" border="0" /> <span>
																									<img class="callout"
																									src="${pageContext.request.contextPath}/images/callout.gif" />
																									Calender year for which the report has to be
																									submitted
																							</span> </a> <br /> <br /></td>


																					</tr>
																					<tr><td>  Paso 2: Seleccione el Tipo de Env铆o  &nbsp;
																								</td>
																								<td><select
																									name="submissionType" id="submissionType"
																									 onchange="return getSubmissionType(this,'giinSubmissionType','Xml','fatca');"
																									class="form-control" style="width: 180px">
																									<option value="">--Seleccionar--</option>
																									<option value="Actual">Real</option>
																									<option value="Test">Prueba</option>
																								</select> &nbsp; <a class="tooltip" ><img
																									src="${pageContext.request.contextPath}/images/whts.png"
																									width="20" height="20" border="0" /><span>
																										<img class="callout"
																										src="${pageContext.request.contextPath}/images/callout.gif" />
																										Si la presentaci贸n XML es para fines de prueba O presentaci贸n de datos reales
																								</span> </a><br /> <br /> </td></tr>
																					<tr>
																						<%-- <td>Step 2: Is this a new submission or an
																							update?&nbsp; &nbsp;</td>
																						<td><span
																							style="padding-top: 0px; vertical-align: text-bottom;"><select
																								name="new_update" id="new_update"
																								class="form-control" style="width: 180px">
																									<option>--Select--</option>
																									<option value="New">New</option>
																									<option value="Update">Update</option>
																							</select></span>&nbsp; <a class="tooltip" ><img
																								src="${pageContext.request.contextPath}/images/whts.png"
																								width="20" height="20" border="0" /><span>
																									<img class="callout"
																									src="${pageContext.request.contextPath}/images/callout.gif" />
																									Whether the XML submission report is for new
																									data or it is update to any previously
																									submitted report
																							</span></a> <br /> <br /></td> --%>
																						<td>Paso 3: Nuevo Registro  o actualizaci贸n??&nbsp; &nbsp;</td>
																						<td width="200"><span
																							style="padding-top: 0px; vertical-align: text-bottom;"><input type="text"
																							name="new_update" id="new_update"
																							class="form-control"
																							style="padding: 0px 2px 2px 5px; width: 170px;"
																							readonly;
																							onpaste='return validateInput(this.id);' /></span>&nbsp; <a class="tooltip" ><img
																								src="${pageContext.request.contextPath}/images/whts.png"
																								width="20" height="20" border="0" /><span>
																									<img class="callout"
																									src="${pageContext.request.contextPath}/images/callout.gif" />
																									Si el informe de presentaci贸n XML es 
																									nuevo Datos o se trata de una actualizaci贸n
																									de Informe presentado

																							</span></a> <br /> <br />
																						</td>
																					</tr>
																					<tr>
																						<td>Paso 4: Seleccione el XML聽 &nbsp;
																							&nbsp;</td>
																						<td><span
																							style="padding-top: 0px; vertical-align: text-bottom;"><input
																						onchange="validateFilename()"		name="uploadFile" id="uploadFile" type="file"  accept=".xml" /></span>
																							<br /> <br /></td>
																					</tr>

																					<tr>
																						<td>&nbsp;</td>
																						<td align="left"><a
																							href="javascript: void();"
																							onclick="return submitform();"><img
																								src="${pageContext.request.contextPath}/images/upld.png"
																								width="65" height="20" border="0" /></a></td>
																					</tr>

																					<tr>
																						<td colspan="2">&nbsp;</td>

																					</tr>

																					<tr>
																						<td colspan="2">Una vez que la informaci贸n del archivo
																						XML ha sido enviada, estar谩 sometido a validaci贸n.
																						Para conocer el estado del informe por favor dirigirse a 鈥淓stado de Informe鈥�  </td>
																					</tr>






																				</table>
																			</form>

																		</td>
																		<td
																			background="${pageContext.request.contextPath}/images/g1.png"
																			style="background-repeat: repeat-y;"></td>
																	</tr>
																	<tr>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-l.png"
																			width="9" height="5" /></td>
																		<td
																			style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																		<td><img
																			src="${pageContext.request.contextPath}/images/c-b-r.png"
																			width="9" height="5" /></td>
																	</tr>
																</table></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
													</table></td>
												<td width="20"></td>

											</tr>
										</table></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>

								<tr>
									<td align="center"><img
										src="${pageContext.request.contextPath}/images/line.png"
										width="921" height="14" /></td>
								</tr>
								<%-- <tr>
									<td class="hedr">For <%=com.newgen.bean.GlobalVariables.projectName%> Revenue Authority
										Powered by Newgen Software Inc.
									</td>
								</tr> --%>

								<tr>
									<td>&nbsp;</td>
								</tr>
							</table></td>
					</tr>

				</table></td>
			<td background="${pageContext.request.contextPath}/images/bg-g-r.gif"
				height="1" width="19"></td>
		</tr>
	</table>
	<script type="text/javascript">
		var menu1 = new menu.dd("menu1");
		menu1.init("menu", "menuhover");
		var reportMenu = new menu.dd("reportMenu");
		reportMenu.init("reportTab", "menuhover");
	</script>
</body>
</html>
