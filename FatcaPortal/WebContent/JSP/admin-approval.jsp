<%@page import="com.newgen.bean.*"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.log4j.Logger"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: <%=com.newgen.bean.GlobalVariables.projectName%>
	Inland Revenue Department ::
</title>
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/images/favicon.ico" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/jquery-ui.css" />
<%-- <script src="${pageContext.request.contextPath}/script/jquery-1.9.1.js"></script> --%>
<script src="${pageContext.request.contextPath}/script/jquery-1.10.2.js"></script>
<script src="${pageContext.request.contextPath}/script/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/script/Common.js"></script>
<script
	src="${pageContext.request.contextPath}/script/GlobalVariable.js"></script>
<script src="${pageContext.request.contextPath}/script/Manual.js"></script>
<script
	src="${pageContext.request.contextPath}/script/GlobalVariable.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/script/script.js"></script><!--  ADDED BY SHIPRA -->
	
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/nstyle.css" />

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/Common.css" />
<script type="text/javascript">
	window.history.forward(-1);
</script>
<%!String searchFor = null;
	String searchIn = null;%>

<%
	Logger logger = Logger.getLogger("Admin-approval.jsp");
	String userType = "";
	String userSessionID = "";
	String userEmail = "";
	String userName = "";
	String complianceType = "";
	try{
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", -1);
		
		HttpSession session1 = request.getSession(false);
		userName = (String) session1.getAttribute("UserName");
		userType = (String) session1.getAttribute("UserType");
		userEmail = (String) session1.getAttribute("UserEmail");
		searchFor = (String) request.getAttribute("searchFor");
		searchIn = (String) request.getAttribute("searchIn");
		complianceType = (String) session1.getAttribute("ComplianceType");//added by shipra 
		
		if (null == searchFor || "" == searchFor) {
		searchFor = "Blank";
		}
		Map<String, String> loginUserMap = GlobalVariables.loginUserMap;
		userSessionID = loginUserMap.get(userEmail);

		if (null != userSessionID
		&& (!(userSessionID.equalsIgnoreCase(session1.getId())))
		|| null == userName) {
		response.sendRedirect(request.getContextPath()
			+ "/JSP/SessionExpired.jsp");
		} else if (null == userSessionID) {
		response.sendRedirect(request.getContextPath()
			+ "/JSP/SessionExpired.jsp");
		}
		/* if (null == userType || userType.length() == 0) {
	response.sendRedirect(request.getContextPath());
		} */
	}catch(Exception e)
	{
		logger.debug("Exception in admin approval JSp--> "+e.getMessage());
	}
%>
<script>
	document.onkeydown = function() {

		//alert(window.event.keyCode);
		if (window.event.keyCode == 116) { // Capture and remap F5  
			return false;
		}/*  else if (window.event.keyCode == 8) { // Capture and remap F5  
																																																	return false;
																																																} */
		/* if (window.event && window.event.keyCode == 505) { // New action for F5  
			return false; // Must return false or the browser will refresh anyway  
		} */
	};
	
	function submitSearchData() {
		//alert("inside submitSearchData ");
		document.AdminSearchUser.submit();
		return true;
	}
	function getUserList() {
		//alert("inside getUserList ");
		var searchFor ="<%=searchFor%>";
		var searchIn ="<%=searchIn%>";

		/* Change for ALL -By Shubham on 28072016 'code added'starts */
		getMasterDropDown("AdminApproval","Country");
		/* Change for ALL -By Shubham on 28072016 end */



		//alert("searchFor--> " + searchFor);
	//	alert("searchIn--> " + searchIn);
		getUserApprovalList('getPageCount', searchFor, searchIn);

	}

	function submit() {

		if (selectUserListRow('userApprovalTable', 'status') > -1) {
			var doc = document;
			var isValidate = false;
			var userType_toUpdate = "";
			var userGiin = "";

			if (!mandateAdminApprovalPage())
				return false;

			if (doc.getElementById("comments_adminApproval").value != null
					&& doc.getElementById("comments_adminApproval").value != "")
				isValidate = true;
			else {
				doc.getElementById("commentError").innerHTML = "Please enter Comment before Submission.";
				doc.getElementById("commentError").className = "errorMsg";
				return false;

			}
			if (doc.getElementById("status_adminApproval").value == "Delete"
					&& doc.getElementById("user_currentStatus").value == "Active") {
				userType_toUpdate = doc.getElementById("userType_toUpdate").value;

				if (userType_toUpdate.toUpperCase() == "FI ADMIN") {
					if (doc.getElementById("user_ComplianceType").value == "CRS") {
						userGiin = doc.getElementById("fiIN_adminApproval").value;
					} else {
						userGiin = doc.getElementById("GIIN_adminApproval").value;
					}
					if (isLastFiAdmin(userGiin)) {
						if (confirm("This is the last FI admin associated with the FI, so deletion of the FI admin will block the users associated with this FI. Are you sure want to delete this user?")) {
							isValidate = true;
						} else {
							return false;
						}
					} else {
						isValidate = true;
					}
				}

				//alert(userType_toUpdate);

			}

			/* else if (doc.getElementById("status_adminApproval").value == "Active"
					&& doc.getElementById("user_currentStatus").value == "Active") {

				if (doc.getElementById("user_LockedStatus").value == doc
						.getElementById("user_adminLockStatus").value) {
					alert("Please change User Status");
					return false;
				} else {
					doc.getElementById("user_LockedStatus").value = doc
							.getElementById("user_adminLockStatus").value;
				}
			} */

			if (isValidate) {
				//removeReadOnly("userAdminApproval");
				//return false;

				document.AdminUserEditUrl.submit();
				//executeServlet("admin-approval");

			} else {
				return false;
			}
			return true;
		} else {
			return false;
		}
	}
	$(function() {
		$("#tabs").tabs();
	});
	
	$(document).ready(function() {
		//alert("asdasda--aadesh");
		$(".hide_spon,.show_spon").change(function() {
			$('.spon').toggle();
		});
		$(".hide_inter,.show_inter").change(function() {
			$('.inter').toggle();
		});

		$("#userDOB_adminApproval").datepicker({
			maxDate : '0',
			changeMonth : true,//this option for allowing user to select month
			changeYear : true,//this option for allowing user to select from year range
			dateFormat : "d'/'M'/'yy"
		});

	});
</script>

</head>

<!-- <body onload="getUserApprovalList('getPageCount');"> -->
<body onload="getUserList();">




	<%-- <%
		//HttpSession session= request.getSession(false);
		String userType = "";
		userType = (String) session.getAttribute("UserType");

		if (null == userType || userType.length() == 0) {
			response.sendRedirect(request.getContextPath());
		}
	%> --%>

	<table width="1003" border="0" cellspacing="0" cellpadding="0"
		align="center" bgcolor="#FFFFFF">
		<tr>
			<td background="${pageContext.request.contextPath}/images/bg-g-l.gif"
				width="19" height="1"></td>
			<td><table width="965" border="0" cellspacing="0"
					cellpadding="0">
					<tr>


						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td
													background="${pageContext.request.contextPath}/images/bg.png"
													height="140">
													<table width="965" border="0" cellspacing="0"
														cellpadding="0">
														<tr>
															<td style="padding: 0px 0px 3px 10px" width="130"><a
																href="${pageContext.request.contextPath}/JSP/home.jsp">
																	<img
																	src="${pageContext.request.contextPath}/images/logo.png"
																	width="118" height="135" />
															</a></td>
															<td valign="top"><table width="835" border="0"
																	cellspacing="0" cellpadding="0">
																	<tr>


																		<td colspan="2"><table border="0" cellspacing="0"



																				cellpadding="0" align="right">
																				<tr>





																					<td width="35" align="right"><img
																						src="${pageContext.request.contextPath}/images/us.png"
																						width="25" height="25" /></td>



																					<td class="logd1" style="text-align: left;"
																						width="150"><%=userName%> is <strong>&nbsp;&nbsp;Registrado en</strong>&nbsp;</td>
																					<td class="logd" width="60">
																						<%-- <a	href="${pageContext.request.contextPath}/JSP/logout.jsp"
																						class="logd">Logout</a> --%> <a
																						href="javascript:void();"
																						onclick='return logoutUser("jsp");' class="logd">Cerrar sesi�n</a>

																					</td>

																				</tr>



																			</table></td>
																	</tr>
																	<tr>
																		<td style="padding: 2px 0px 0px 20px"><img

																			src="${pageContext.request.contextPath}/images/slogan.png"
																			width="652" height="" /></td>


																		<td style="padding: 8px 10px 5px 20px">
																		<img 	src="${pageContext.request.contextPath}/images/flag.png"
																			width="118" height="" />

																			&nbsp;
																			</td>
																	</tr>






																</table></td>
														</tr>

													</table> <%-- <table width="965" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td style="padding:3px 0px 3px 10px" width="130"><img src="${pageContext.request.contextPath}/images/logo.png" width="118" height="135" /></td>
              <td style="padding:0px 0px 0px 0px" align="center"><img src="${pageContext.request.contextPath}/images/slogan.png" width="652" height="40" /></td>
              <td style="padding:0px 0px 3px 10px" width="130"><img src="${pageContext.request.contextPath}/images/logo.png" width="118" height="135" /></td>
            </tr>
          </table> --%>
												</td>
											</tr>
										</table></td>
								</tr>

							</table></td>

					</tr>
					<tr>
						<td><table width="965" border="0" cellspacing="0"
								cellpadding="0">

								<tr>
									<!--  <td>&nbsp;</td> -->
									<td></td>

								</tr>
								<tr>
									<td><table width="965" border="0" cellspacing="0"
											cellpadding="0">
											<tr>
												<td colspan="3">
													<table width="965" border="0" cellspacing="0"
														cellpadding="0">
														<tr>


															<%
															String styleClass ="";
																if (userType != null && (userType.equalsIgnoreCase("admin") || userType
																	.equalsIgnoreCase("fi admin"))) {
																	if (userType.equalsIgnoreCase("admin"))//added by shipra
																		 styleClass="drop_menu IRDadmin";
																else
																		styleClass="drop_menu FIadmin";//----END
															%>

															<td align="center"><ul class="<%=styleClass%>">
																	<li><a
																		href="${pageContext.request.contextPath}/JSP/home.jsp"
																		class="menulink"><strong>Home</strong></a></li>
																</ul></td>
															<td width="3"><img
																src="${pageContext.request.contextPath}/images/w-bg.png"
																width="3" height="30" /></td>
															<td align="center"><ul
																	class="<%=styleClass%> selected">
																	<li><a href="#" class="menulink"><strong>Usuario Aprobaci�n</strong></a></li>
																</ul></td>
															<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>

												<td><ul class="<%=styleClass%>">
														<li><a
															href='${pageContext.request.contextPath}/JSP/customReport.jsp'><Strong>Informe del administrador
																	</Strong></a></li>
													</ul></td>
													<!-- --------------Added by Shipra----------- -->
													<%
													if (null != userType && !( userType.equalsIgnoreCase("fi admin")) && !( userType.equalsIgnoreCase("admin"))) {
													%>
														<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>

												<td><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Enviar Informe</strong></a>
															<ul>

																<%
																	if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a></li>
																	</ul></li>
																<%
																	} else if (null != complianceType
																				&& complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> </li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		
																	</ul></li>
																<%
																	}
																%>


															</ul></li>
													</ul></td>
													<%
													}else{
													%>
														<td width="3" style="display:none"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>

												<td style="display:none"><ul class="menu" id="menu">
														<li><a href="#" class="menulink"><strong>Enviar Informe</strong></a>
															<ul>

																<%
																	if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> </li>
																	</ul></li>
																<%
																	} else if (null != complianceType
																				&& complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Subir
																				XML </a></li>
																		
																	</ul></li>
																<%
																	} else {
																%>
																<li><a href="#">FATCA</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																				XML </a></li>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> </li>
																	</ul></li>
																<li><a href="#">CRS</a>
																	<ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/XMLUpload-CRS.jsp">Upload
																				XML </a></li>
																		
																	</ul></li>
																<%
																	}
																%>


															</ul></li>
													</ul></td>
													<%
													}
													%>
													
												<td width="3"><img
													src="${pageContext.request.contextPath}/images/w-bg.png"
													width="3" height="30" /></td>


												<%-- <td>
													<ul id="reportTab" class="reportTab">
														<li><a class="menulink" href="#"><strong>Informe Estado</strong></a>
															<ul>
																<%
																	if (null != complianceType && complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("fatca")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>


																<%
																	} else if (null != complianceType
																				&& complianceType.length() > 0
																				&& complianceType.equalsIgnoreCase("crs")) {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>
																<%
																	} else {
																%>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatus">FATCA</a></li>
																<li><a
																	href="${pageContext.request.contextPath}/ReportStatusCRS">CRS</a></li>

																<%
																	}
																%>

															</ul></li>
													</ul>
												</td> --%>
												<!-- ------------END---------- -->
															<td width="3"><img
																src="${pageContext.request.contextPath}/images/w-bg.png"
																width="3" height="30" /></td>
															<td><ul class="<%=styleClass%>">
																	<li><a
																		href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
																</ul></td>
															<td width="3"><img
																src="${pageContext.request.contextPath}/images/w-bg.png"
																width="3" height="30" /></td>
															<td><ul class="<%=styleClass%>">
																	<li><a
																		href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Contacto No</strong></a></li>
																</ul></td>

															<%
																} else {
															%>

															<td align="center"><ul class="drop_menu">
																	<li><a
																		href="${pageContext.request.contextPath}/JSP/home.jsp"
																		class="menulink"><strong>Casa</strong></a></li>
																</ul></td>
															<td width="3"><img
																src="${pageContext.request.contextPath}/images/w-bg.png"
																width="3" height="30" /></td>
															<td><ul class="menu" id="menu">
																	<li><a href="#" class="menulink"><strong>Enviar Informe</strong></a>
																		<ul>
																			<li><a
																				href="${pageContext.request.contextPath}/JSP/XMLUpload.jsp">Subir
																					XML</a></li>
																			<li><a href="#">Upload XLS/CSV </a></li>
																			<li><a
																				href="${pageContext.request.contextPath}/JSP/online-form.jsp">Llenar Manualmente en l�nea</a> <%-- <ul>
																		<li><a
																			href="${pageContext.request.contextPath}/JSP/Manual.jsp">Fill
																				New Form</a></li>
																		<li><a href="#">Update Previously Filled Form</a></li>
																	</ul> --%></li>
																		</ul></li>
																</ul></td>
															<td width="3"><img
																src="${pageContext.request.contextPath}/images/w-bg.png"
																width="3" height="30" /></td>
															<td><ul class="drop_menu">
																	<li><a
																		href='${pageContext.request.contextPath}/ReportStatus'><strong>Informe Estado</strong></a></li>
																</ul></td>
															<td width="3"><img
																src="${pageContext.request.contextPath}/images/w-bg.png"
																width="3" height="30" /></td>
															<td><ul class="drop_menu">
																	<li><a
																		href='${pageContext.request.contextPath}/FAQServlet?callFor=FAQ'><strong>PMF</strong></a></li>
																</ul></td>
															<td width="3"><img
																src="${pageContext.request.contextPath}/images/w-bg.png"
																width="3" height="30" /></td>
															<td><ul class="drop_menu">
																	<li><a
																		href='${pageContext.request.contextPath}/FAQServlet?callFor=ContactUS'><strong>Cont�ctenos</strong></a></li>
																</ul></td>

															<%
																}
															%>


														</tr>

													</table>
												</td>

											</tr>
											<tr>
												<td width="30"></td>
												<td><div id="tabs">
														<ul>
															<li><a href="#tabs-1"><strong>Secci�n de administraci�n - Aprobaci�n del usuario
</strong></a></li>
														</ul>
														<div id="tabs-1">

															<form
																action="${pageContext.request.contextPath}/UserApproval?option=searchUser"
																name="AdminSearchUser" method="post">
																<table width="860" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td style="width: 95px">Busca en</td>
																		<td style="width: 250px"><select name="searchIn"
																			id="searchIn" class="form-control"
																			style="width: 200px; padding: 0px 2px 2px 5px;">
																				<option value="Blank">---Seleccionar---</option>
																				<option value="GIIN">GIIN</option>
																				<option value="User Email">Correo Electr�nico</option>
																				<option value="Name">Nombre</option>
																				<script>
																					var searchIn ="<%=searchIn%>";
																					var formObj = document
																							.getElementById('searchIn');
																					for ( var i = 0; i < formObj.length; i++) {
																						if (formObj[i].value == searchIn) {
																							formObj[i].selected = true;
																							break;
																						}
																					}
																				</script>
																		</select></td>
																		<td style="width: 95px">Buscar</td>
																		<%
																			if (null == searchFor || "blank".equalsIgnoreCase(searchFor)) {
																																				searchFor = "";
																																			}
																		%>
																		<td style="width: 250px"><input type="text"
																			value="<%=searchFor%>" id="searchFor"
																			name="searchFor" class="form-control" maxlength="100"
																			style="width: 200px" /><a
																			style="text-decoration: none;"
																			href="javascript:void();"
																			onclick=" return submitSearchData();">&nbsp;&nbsp;<img
																				src="${pageContext.request.contextPath}/images/search.png"
																				width="31" height="24" border="0"
																				style="vertical-align: middle;" /></a></td>
																		<td>&nbsp;</td>
																	</tr>
																</table>
															</form>
															<br />
															<p>
																<table width="882" style="table-layout: fixed;"
																	border="1" cellspacing="0" cellpadding="0"
																	id="userApprovalTable">


																	<tr class="bg1" style="width: 100%">
																		<td class="ipm_n" style="width: 16%">GIIN</td>
																		<td style="width: 13%" class="ipm_n">Nombre</td>
																		<td style="width: 18%" class="ipm_n">Correo Electr�nico</td>
																		<td style="width: 13%" class="ipm_n">ID de empleado</td>
																		<td style="width: 11%" class="ipm_n">Fecha de Registro
																		</td>
																		<td style="width: 9%" class="ipm_n">Fecha modificada</td>
																		<td style="width: 7%" class="ipm_n">Tipo de usuario</td>
																		<td style="width: 7%" class="ipm_n">Estado</td>
																		<td style="width: 5%" class="ipm_n">Seleccionar</td>
																	</tr>



																</table>
																<br />
																<form enctype="multipart/form-data"
																	action="${pageContext.request.contextPath}/UserApproval?option=update"
																	name="AdminUserEditUrl" method="post">
																	<input type="hidden" id="user_LockedStatus"
																		name="user_LockedStatus" /> <input type="hidden"
																		id="user_adminLockStatus" name="user_adminLockStatus" />
																	<%
																		if (null == searchFor || searchFor == "") {
																																		searchFor = "Blank";
																																	}
																	%>
																	<input type="hidden" id="searchFor_hidden"
																		value="<%=searchFor%>" name="searchFor_hidden" /> <input
																		type="hidden" value="<%=searchIn%>"
																		id="searchIn_hidden" name="searchIn_hidden" /> <input
																		name="file1_admin" id="file1_admin" type="file"
																		style="display: none;"
																		onchange="return checkFileFormat(this);" /> <input
																		name="file2_admin" id="file2_admin" type="file"
																		style="display: none;"
																		onchange="return checkFileFormat(this);" /> <input
																		type="hidden" id="user_currentStatus"
																		name="user_currentStatus" /> <input type="hidden"
																		id="userType_toUpdate" name="userType_toUpdate" /> <input
																		type="hidden" id="user_ComplianceType"
																		name="user_ComplianceType" />
																	<table width="860" border="0" cellspacing="0"
																		cellpadding="0">
																		<tr>
																			<td width="9"><img
																				src="${pageContext.request.contextPath}/images/g-t-l.png"
																				width="9" height="29" /></td>
																			<td
																				background="${pageContext.request.contextPath}/images/g-t-m.png"
																				class="bgtext1">Financial Institution Details</td>
																			<td width="9"><img
																				src="${pageContext.request.contextPath}/images/g-t-r.png"
																				width="9" height="29" /></td>
																		</tr>
																		<tr>
																			<td
																				background="${pageContext.request.contextPath}/images/g.png"
																				style="background-repeat: repeat-y;"></td>
																			<td><table width="840" border="0"
																					cellspacing="0" cellpadding="0" align="center">
																					<tr>
																						<td><table width="840" border="0"
																								cellspacing="0" cellpadding="0">

																								<!--  <tr>
    <td colspan="5" class="bgtext2">&nbsp;
      </td>
    </tr> -->

																								<tr>

																									<td colspan="5" id="FierrorMsg">&nbsp;</td>
																								</tr>
																								<tr>
																									<td>&nbsp;</td>
																									<td colspan="4">&nbsp;</td>
																								</tr>
																								<tr>
																									<td width="100">GIIN/IN<span
																										style="color: #F00;">*</span>
																									</td>
																									<!-- <td><input type="text"
																										name="GIIN_adminApproval"
																										id="GIIN_adminApproval"
																										style="width: 200px; padding: 0px 2px 2px 5px;"
																										class="form-control" /></td> -->

																									<td valign="top">
																										<div id="adminGiin_div">
																											<input type="hidden" name="GIIN_InType"
																												id="GIIN_InType" value="GIIN" /> <input
																												type="hidden" name="GIIN_adminApproval"
																												id="GIIN_adminApproval" /> <input
																												type="text" name="admin_text_giin1"
																												id="admin_text_giin1" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplhanumeric");'
																												onpaste='return validateInput(this.id);' />
																											<input type="text" name="admin_text_giin2"
																												id="admin_text_giin2" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplhanumeric");'
																												onpaste='return validateInput(this.id);' /><input
																												type="text" name="admin_text_giin3"
																												id="admin_text_giin3" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplhanumeric");'
																												onpaste='return validateInput(this.id);' /><input
																												type="text" name="admin_text_giin4"
																												id="admin_text_giin4" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplhanumeric");'
																												onpaste='return validateInput(this.id);' /><input
																												type="text" name="admin_text_giin5"
																												id="admin_text_giin5" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplhanumeric");'
																												onpaste='return validateInput(this.id);' /><input
																												type="text" name="admin_text_giin6"
																												id="admin_text_giin6" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplhanumeric");'
																												onpaste='return validateInput(this.id);' />.<input
																												type="text" name="admin_text_giin7"
																												id="admin_text_giin7" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplhanumeric");'
																												onpaste='return validateInput(this.id);' /><input
																												type="text" name="admin_text_giin8"
																												id="admin_text_giin8" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplhanumeric");'
																												onpaste='return validateInput(this.id);' /><input
																												type="text" name="admin_text_giin9"
																												id="admin_text_giin9" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplhanumeric");'
																												onpaste='return validateInput(this.id);' /><input
																												type="text" name="admin_text_giin10"
																												id="admin_text_giin10" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplhanumeric");'
																												onpaste='return validateInput(this.id);' /><input
																												type="text" name="admin_text_giin11"
																												id="admin_text_giin11" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplhanumeric");'
																												onpaste='return validateInput(this.id);' />.<input
																												type="text" name="admin_text_giin12"
																												id="admin_text_giin12" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplha");'
																												onpaste='return validateInput(this.id);' /><input
																												type="text" name="admin_text_giin13"
																												id="admin_text_giin13" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"aplha");'
																												onpaste='return validateInput(this.id);' />.<input
																												type="text" name="admin_text_giin14"
																												id="admin_text_giin14" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"numeric");'
																												onpaste='return validateInput(this.id);' /><input
																												type="text" name="admin_text_giin15"
																												id="admin_text_giin15" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"numeric");'
																												onpaste='return validateInput(this.id);' /><input
																												type="text" name="admin_text_giin16"
																												id="admin_text_giin16" maxlength="1"
																												style="width: 20px; text-align: center;"
																												class="form-control"
																												onkeyup="autoSetFocus(event,this,'giin')"
																												onKeypress='return validateKey(this.id,"numeric");'
																												onpaste='return validateInput(this.id);' />
																										</div>
																										<div id="adminIn_div" style="display: none;">
																											<input type="text" name="fiIN_adminApproval"
																												onblur="return checkFormatAdminPage(this)"
																												id="fiIN_adminApproval" maxlength="100"
																												class="form-control"
																												style="width: 175px; padding: 0px 2px 2px 5px;" />

																											&nbsp;&nbsp;
																											<!-- <input type="text"
																									name="fi_inType" id="fi_inType" class="form-control"
																									style="width: 150px; padding: 0px 2px 2px 5px; text-decoration: none;" /> -->
																											<span style="vertical-align: top;"> IN
																												Type</span><span
																												style="color: #F00; vertical-align: top;">*</span>&nbsp;<select
																												name="fi_inType" id="fi_inType"
																												class="form-control"
																												style="width: 150px; padding: 0px 2px 2px 5px;">

																												<option value="GIIN">GIIN</option>
																												<option value="TIN">TIN</option>
																												<option value="CRN">CRN</option>
																												<option value="EIN">EIN</option>
																											</select>
																										</div>
																									</td>



																									<td>&nbsp;</td>
																									<td valign="top" width="100">Nombre<span
																										style="color: #F00;">*</span>:
																									</td>
																									<td valign="top"><input type="text"
																										name="fiName_adminApproval"
																										onblur="return checkFormatAdminPage(this)"
																										id="fiName_adminApproval" maxlength="100"
																										class="form-control"
																										style="width: 200px; padding: 0px 2px 2px 5px;" /></td>
																								</tr>
																								<tr>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																								</tr>
																								<tr>
																									<td rowspan="3" valign="top">Direcci�n<span
																										style="color: #F00;">*</span>:
																									</td>
																									<td rowspan="3" valign="top"><textarea
																											id="fiAddress_adminApproval"
																											name="fiAddress_adminApproval"
																											onblur="return checkFormatAdminPage(this)"
																											maxlength="2000"
																											style="width: 400px; height: 68px; padding: 0px 2px 2px 5px;"
																											class="form-control"></textarea></td>
																									<td>&nbsp;</td>
																									<td valign="top">Nacionalidad<span
																										style="color: #F00;">*</span>:
																									</td>
																									<td valign="top">
																										<!-- <select name="fiCountry_adminApproval" id="fiCountry_adminApproval" class="form-control" style="width:200px; padding:0px 2px 2px 5px;">
		<option value="">----Select-----</option>
        <option value="LC - <%=com.newgen.bean.GlobalVariables.projectName%> ">LC - <%=com.newgen.bean.GlobalVariables.projectName%> </option>
																										<%--</select> --> <input type="text" name="fiCountry_adminApproval"
																										id="fiCountry_adminApproval"
																										class="form-control" maxlength="100"
																										value="<%=com.newgen.bean.GlobalVariables.countryOnForm%>"
																										style="width: 200px; padding: 0px 2px 2px 5px; background-color: #ffffb3"
																										; readonly /> --%>
																										<!-- Change for All by Shubham on 28072016 'added drop down instead of textbox'starts -->
																										 <select name="fiCountry_adminApproval" 
																										id="fiCountry_adminApproval" class="form-control"
																										style="width: 200px; padding: 0px 2px 2px 5px;">
																									</select> 
																									<!-- Change for All by Shubham on 28072016 end -->
																									</td>
																								</tr>
																								<tr>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																								</tr>
																								<tr>
																									<td>&nbsp;</td>
																									<td valign="top">Correo Electr�nico<span
																										style="color: #F00;">*</span>:
																									</td>
																									<td valign="top"><input type="text"
																										name="fiEmailID_adminApproval"
																										id="fiEmailID_adminApproval"
																										onblur="return checkFormatAdminPage(this);"
																										class="form-control" maxlength="100"
																										style="background: white url(${pageContext.request.contextPath}/images/mail2.png) no-repeat center right;padding:0px 2px 2px 5px; height:25px; width:200px; padding:0px 2px 2px 5px;" /></td>
																								</tr>

																								<tr>
																									<td colspan="5" height="5"></td>
																								</tr>

																							</table></td>
																					</tr>
																				</table></td>
																			<td
																				background="${pageContext.request.contextPath}/images/g1.png"
																				style="background-repeat: repeat-y;"></td>
																		</tr>
																		<tr>
																			<td><img
																				src="${pageContext.request.contextPath}/images/c-b-l.png"
																				width="9" height="5" /></td>
																			<td
																				style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																			<td><img
																				src="${pageContext.request.contextPath}/images/c-b-r.png"
																				width="9" height="5" /></td>
																		</tr>
																	</table>
																	<br />
																	<table width="860" border="0" cellspacing="0"
																		cellpadding="0">
																		<tr>
																			<td width="9"><img
																				src="${pageContext.request.contextPath}/images/g-t-l.png"
																				width="9" height="29" /></td>
																			<td
																				background="${pageContext.request.contextPath}/images/g-t-m.png"
																				class="bgtext1">User Details</td>
																			<td width="9"><img
																				src="${pageContext.request.contextPath}/images/g-t-r.png"
																				width="9" height="29" /></td>
																		</tr>
																		<tr>
																			<td
																				background="${pageContext.request.contextPath}/images/g.png"
																				style="background-repeat: repeat-y;"></td>
																			<td><table width="840" border="0"
																					cellspacing="0" cellpadding="0" align="center">
																					<tr>
																						<td><table width="840" border="0"
																								cellspacing="0" cellpadding="0">

																								<tr>

																									<td colspan="5" id="UsererrorMsg">&nbsp;</td>
																								</tr>
																								<!-- <tr>
																									<td colspan="5" class="bgtext2">&nbsp;</td>
																								</tr> -->

																								<tr>
																									<td valign="top" width="100">Correo Electr�nico<span
																										style="color: #F00;">*</span></td>
																									<td><input type="text"
																										name="userEmailID_adminApproval"
																										id="userEmailID_adminApproval"
																										class="form-control" maxlength="100"
																										style="background: #ffffb3 url(${pageContext.request.contextPath}/images/mail2.png) no-repeat center right;padding:0px 2px 2px 5px; height:25px; width:200px; padding:0px 2px 2px 5px;" /></td>
																									<td>&nbsp;</td>
																									<td valign="top" width="100">Nombre<span
																										style="color: #F00;">*</span>:
																									</td>
																									<td valign="top"><input type="text"
																										name="userName_adminApproval"
																										id="userName_adminApproval"
																										onblur="return checkFormatAdminPage(this)"
																										class="form-control" maxlength="100"
																										style="width: 200px; padding: 0px 2px 2px 5px;" "/></td>
																								</tr>
																								<tr>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																								</tr>
																								<tr>
																									<td rowspan="3" valign="top">Direcci�n<span
																										style="color: #F00;">*</span>:
																									</td>
																									<td rowspan="3" valign="top"><textarea
																											name="userAddress_adminApproval"
																											id="userAddress_adminApproval"
																											onblur="return checkFormatAdminPage(this)"
																											maxlength="2000"
																											style="width: 400px; height: 68px; padding: 0px 2px 2px 5px;"
																											class="form-control"></textarea></td>
																									<td>&nbsp;</td>
																									<td valign="top">Nacionalidad<span
																										style="color: #F00;">*</span>:
																									</td>
																									<td valign="top">
																										<!-- <select name="userCountry_adminApproval" id="userCountry_adminApproval" class="form-control" style="width:200px; padding:0px 2px 2px 5px;">
    	<option value="">---Select----</option>
    	<option value="LC - <%=com.newgen.bean.GlobalVariables.projectName%> ">LC - <%=com.newgen.bean.GlobalVariables.projectName%> </option>
    </select> -->                                                                                      <%--<input type="text" name="userCountry_adminApproval"
																										id="userCountry_adminApproval"
																										class="form-control" maxlength="100"
																										value="<%=com.newgen.bean.GlobalVariables.countryOnForm%>"
																										style="width: 200px; padding: 0px 2px 2px 5px; background-color: #ffffb3"

																										readonly /> --%>
																										<!-- Change for All by Shubham on 20072016 'added drop down instead of textbox'starts -->
																										 <select name="userCountry_adminApproval" 
																										id="userCountry_adminApproval" class="form-control"
																										style="width: 200px; padding: 0px 2px 2px 5px;">
																									</select> 
																									<!-- Change for All by Shubham on 20072016 end -->
																									</td>
																								</tr>
																								<tr>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																								</tr>
																								<tr>
																									<td>&nbsp;</td>
																									<td valign="top">Tel�fono<span
																										style="color: #F00;">*</span>:
																									</td>
																									<td valign="top"><input type="text"
																										name="userPhone_adminApproval"
																										id="userPhone_adminApproval"
																										onblur="return checkFormatAdminPage(this);"
																										class="form-control" maxlength="13"
																										style="width: 200px; padding: 0px 2px 2px 5px;" /></td>
																								</tr>
																								<tr>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																								</tr>
																								<tr>
																									<td valign="top">TIN<span
																										style="color: #F00;">*</span></td>
																									<td><input type="text"
																										name="userEmployee_adminApproval"
																										id="userEmployee_adminApproval"
																										class="form-control" maxlength="100"
																										onblur="return checkFormatAdminPage(this)"
																										style="width: 200px; padding: 0px 2px 2px 5px;" /></td>
																									<td>&nbsp;</td>
																									<td valign="top">Position<span
																										style="color: #F00;">*</span>:
																									</td>
																									<td valign="top"><input type="text"
																										name="userDesignation_adminApproval"
																										id="userDesignation_adminApproval"
																										onblur="return checkFormatAdminPage(this)"
																										class="form-control" maxlength="100"
																										style="width: 200px; padding: 0px 2px 2px 5px;" /></td>
																								</tr>
																								<tr>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																								</tr>
																								<tr>
																									<td valign="top">Sexo<span
																										style="color: #F00;">*</span></td>


																									<td><select
																										name="userGender_adminApproval"
																										id="userGender_adminApproval"
																										class="form-control"
																										style="width: 210px; padding: 0px 2px 2px 5px;">
																											<option value="Male">Male</option>
																											<option value="Female">Female</option>

																									</select></td>
																									<td>&nbsp;</td>
																									<td valign="top">Fecha de Nacimiento<span
																										style="color: #F00;">*</span>:
																									</td>
																									<!-- <td valign="top">Position<span
																										style="color: #F00;">*</span>:
																									</td> -->

																									<td><input type="text"
																										name="userDOB_adminApproval"
																										id="userDOB_adminApproval"
																										class="form-control" readonly="readonly"
																										style="padding: 0px 2px 2px 5px; width: 200px;"
																										onKeypress='return validateKey(this.id,"aplhanumeric");'
																										onpaste='return validateInput(this.id);' /></td>
																								</tr>
																								<tr>
																									<td colspan="5" height="5"></td>
																								</tr>

																							</table></td>
																					</tr>
																				</table></td>
																			<td
																				background="${pageContext.request.contextPath}/images/g1.png"
																				style="background-repeat: repeat-y;"></td>
																		</tr>
																		<tr>
																			<td><img
																				src="${pageContext.request.contextPath}/images/c-b-l.png"
																				width="9" height="5" /></td>
																			<td
																				style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																			<td><img
																				src="${pageContext.request.contextPath}/images/c-b-r.png"
																				width="9" height="5" /></td>
																		</tr>
																	</table>
																	<br />
																	<table width="860" border="0" cellspacing="0"
																		cellpadding="0">
																		<tr>
																			<td width="9"><img
																				src="${pageContext.request.contextPath}/images/g-t-l.png"
																				width="9" height="29" /></td>
																			<td
																				background="${pageContext.request.contextPath}/images/g-t-m.png"
																				class="bgtext1">Documentaci�n de Soporte </td>
																			<td width="9"><img
																				src="${pageContext.request.contextPath}/images/g-t-r.png"
																				width="9" height="29" /></td>
																		</tr>
																		<tr>
																			<td
																				background="${pageContext.request.contextPath}/images/g.png"
																				style="background-repeat: repeat-y;"></td>
																			<td><table width="840" border="0"
																					cellspacing="0" cellpadding="0" align="center">
																					<tr>
																						<td><table width="840" border="0"
																								cellspacing="0" cellpadding="0">
																								<tr>
																									<td colspan="4" id="errorMsgDocFormat">&nbsp;</td>
																								</tr>
																								<tr>
																									<td colspan="4" class="bgtext2"></td>
																								</tr>


																								<tr>
																									<td valign="top" width="110">Documento 1<span
																										style="color: #F00;">*(Max. Size  <%=com.newgen.bean.GlobalVariables.supportiveDoc1MaXSize%>MB)</span></td>
																									<td width="100"><select
																										name="Document1_adminApproval"
																										id="Document1_adminApproval"
																										class="form-control"
																										style="width: 200px; padding: 0px 2px 2px 5px;">
																											<option value="Authorization Letter">Autorizaci�n Carta</option>
																									</select> <!-- <input type="text" name="Document1_adminApproval" 	id="Document1_adminApproval"
																										class="form-control"
																										style="width: 200px; padding: 0px 2px 2px 5px;"
																										readonly /> --> &nbsp;</td>
																									<td style="padding-left: 30PX;" width="100"><a
																										onclick='replaceDocument("Document1")'
																										href="javascript:void();" name="docs1_replace"
																										id="docs1_replace" style="color: #015f7a;"><span
																											style="color: #015f7a;">Reemplazar</span></a></td>
																									<td width="468">
																									<a href="#"
																										name="docs1_adminApproval"
																										id="docs1_adminApproval"
																										style="color: #015f7a;"><span
																											style="color: #015f7a;">Haga Clic aqu� para descargar el documento</span></a></td>

																								</tr>
																								<tr>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>
																									<td>&nbsp;</td>

																								</tr>
																								<tr>
																									<td valign="top" width="110">Documento 2<span
																										style="color: #F00;">*(Max. Size  <%=com.newgen.bean.GlobalVariables.supportiveDoc2MaXSize%>MB)</span></td>
																									<td><select name="Document2_adminApproval"
																										id="Document2_adminApproval"
																										class="form-control"
																										style="width: 200px; padding: 0px 2px 2px 5px;">

																											<option value="Passport">Pasaporte</option>
																											<option value="Driver License">Conductor Licencia</option>
																											<option value="Voter ID">ID de votante</option>
																											<option value="SSN">SSN</option>
																									</select> <!-- <input type="text" name="Document2_adminApproval"
																										id="Document2_adminApproval"
																										class="form-control"
																										style="width: 200px; padding: 0px 2px 2px 5px;"
																										readonly /> --> &nbsp;</td>
																									<td style="padding-left: 30PX;" width="100"><a
																										onclick='replaceDocument("Document2")'
																										href="javascript:void();" name="docs2_replace"
																										id="docs2_replace" style="color: #015f7a;"><span
																											style="color: #015f7a;">Reemplazar</span></a></td>

																									<td width="468"><a href="#"
																										id="docs2_adminApproval"
																										name="docs2_adminApproval"
																										style="color: #015f7a;"><span
																											style="color: #015f7a;">Haga Clic aqu� para descargar el documento</span></a></td>
																								</tr>
																								<tr>
																									<td colspan="4" height="5"></td>
																								</tr>

																							</table></td>
																					</tr>
																				</table></td>
																			<td
																				background="${pageContext.request.contextPath}/images/g1.png"
																				style="background-repeat: repeat-y;"></td>
																		</tr>
																		<tr>
																			<td><img
																				src="${pageContext.request.contextPath}/images/c-b-l.png"
																				width="9" height="5" /></td>
																			<td
																				style="background-image:url(${pageContext.request.contextPath}/images/g2.png)"></td>
																			<td><img
																				src="${pageContext.request.contextPath}/images/c-b-r.png"
																				width="9" height="5" /></td>
																		</tr>
																	</table>
																	<br />
																	<table width="840" border="0" cellspacing="0"
																		cellpadding="0" align="center">
																		<tr>
																			<td colspan="5" id="commentError">&nbsp;</td>
																		</tr>
																		<tr>
																			<!-- Select below to change the status of the user -->
																			<td width="125" valign="top" colspan="2">Estado  de Usuario</td>
																			<td colspan="2" valign="top"><select
																				name="status_adminApproval"
																				id="status_adminApproval" class="form-control"
																				style="width: 200px; padding: 0px 2px 2px 5px;">
																					<!-- <option value="Blocked">Blocked</option> -->
																					<option value="Active">Activo</option>
																					<option value="Block">Inactivo</option>
																					<option value="Delete">Borrar</option>
																			</select></td>
																			<!-- <td colspan="2">&nbsp;</td> -->

																			<td valign="top" colspan="2" align="left"><input
																				type="checkbox" id="user_locked" name="user_locked"
																				onclick="checkUserType(this);" /> Usuario bloqueado
																				&nbsp;</td>
																		</tr>
																		<tr>
																			<td valign="top">&nbsp;</td>
																			<td colspan="3">&nbsp;</td>
																			<td>&nbsp;</td>
																		</tr>
																		<tr>
																			<td valign="top" colspan="2">Comentarios:</td>
																			<td colspan="3"><textarea
																					name="comments_adminApproval"
																					id="comments_adminApproval"
																					style="width: 500px; height: 68px; padding: 0px 2px 2px 5px;"
																					class="form-control"></textarea></td>
																			<!-- <td>&nbsp;</td> -->
																		</tr>
																		<tr>
																			<td valign="top">&nbsp;</td>
																			<td>&nbsp;</td>
																			<td>&nbsp;</td>
																			<td width="145" valign="top">&nbsp;</td>
																			<td>&nbsp;</td>
																		</tr>


																		<tr>
																			<td colspan="5" align="center"><a
																				href="javascript:void();"
																				onclick=" return submit();"><img
																					src="${pageContext.request.contextPath}/images/submit.png"
																					width="65" height="24" border="0"></a></td>
																		</tr>
																		<tr>
																			<td colspan="5" align="center">&nbsp;</td>
																		</tr>
																		<tr>
																			<td colspan="5" align="center">&nbsp;</td>
																		</tr>

																	</table>
																</form>
															</p>

														</div>
													</div></td>
												<td width="30"></td>
											</tr>
										</table></td>
								</tr>
								<tr>
									<td height="40">&nbsp;</td>
								</tr>
								<tr>
									<td align="center"><img
										src="${pageContext.request.contextPath}/images/line.png"
										width="921" height="14" /></td>
								</tr>

								<%--  <tr>
									<td class="hedr">For Govt. of <%=com.newgen.bean.GlobalVariables.projectName%>
										Powered by Newgen Software Inc.
									</td>
								</tr>  --%>

								<tr>
									<td>&nbsp;</td>
								</tr>
							</table></td>
					</tr>

				</table></td>
			<td background="${pageContext.request.contextPath}/images/bg-g-r.gif"
				height="1" width="19"></td>
		</tr>
	</table>
	<!-- ADDED BY SHIPRA -->
	<script type="text/javascript">
		var menu1 = new menu.dd("menu1");
		menu1.init("menu", "menuhover");
		var reportMenu = new menu.dd("reportMenu");
		reportMenu.init("reportTab", "menuhover");
	</script><!-- end -->
</body>
</html>
