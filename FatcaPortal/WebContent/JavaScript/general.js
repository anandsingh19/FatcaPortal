function trim(myString)     
{     
    return myString.replace(/^\s+|\s+$/g, '');    
}

function CalculateTotalInvoiceAmount(){

	var InvoiceBaseAmount = '0.0';
	var ServiceTax = '0.0';
	var EducationCess = '0.0';
	var HEducationCess = '0.0';
	var VAT = '0.0';
	var AdditionalVatValue = '0.0';
	var AnyOtherTax = '0.0';
	var Freight = '0.0';
	var Discount = '0.0';
	var PenaltyDeduction = '0.0';
	var TotalInvoiceAmount='0.0';
	
	if (null != document.getElementById("InvoiceBaseAmount") && document.getElementById("InvoiceBaseAmount").value != ''){
		InvoiceBaseAmount = document.getElementById("InvoiceBaseAmount").value;
	} 
	if (null != document.getElementById("ServiceTax") && document.getElementById("ServiceTax").value != ''){
		ServiceTax = document.getElementById("ServiceTax").value;
	} 
	if (null != document.getElementById("EducationCess") && document.getElementById("EducationCess").value != ''){
		EducationCess = document.getElementById("EducationCess").value;
	} 
	if (null != document.getElementById("HigherEducationCess") && document.getElementById("HigherEducationCess").value != ''){
		HEducationCess = document.getElementById("HigherEducationCess").value;
	} 
	if (null != document.getElementById("Vat") && document.getElementById("Vat").value != ''){
		VAT = document.getElementById("Vat").value;
	} 
	if (null != document.getElementById("AdditionalVatValue") && document.getElementById("AdditionalVatValue").value != ''){
		AdditionalVatValue = document.getElementById("AdditionalVatValue").value;
	} 
	if (null != document.getElementById("AnyOtherTax") && document.getElementById("AnyOtherTax").value != ''){
		AnyOtherTax = document.getElementById("AnyOtherTax").value;
	} 
	if (null != document.getElementById("Freight") && document.getElementById("Freight").value != ''){
		Freight = document.getElementById("Freight").value;
	} 
	if (null != document.getElementById("Discount") && document.getElementById("Discount").value != ''){
		Discount = document.getElementById("Discount").value;
	} 
	if (null != document.getElementById("PenaltyDeduction") && document.getElementById("PenaltyDeduction").value != ''){
		PenaltyDeduction = document.getElementById("PenaltyDeduction").value;
	} 
	
	TotalInvoiceAmount = ((parseFloat(InvoiceBaseAmount) + parseFloat(ServiceTax) + parseFloat(EducationCess)
			+ parseFloat(HEducationCess) + parseFloat(VAT) + parseFloat(AdditionalVatValue) + parseFloat(AnyOtherTax) + parseFloat(Freight))
			- (parseFloat(Discount) + parseFloat(PenaltyDeduction)));
		TotalInvoiceAmount = parseFloat(TotalInvoiceAmount.toFixed(3));
		document.getElementById("TotalInvoiceAmount").value = TotalInvoiceAmount;//Math.round(TotalInvoiceAmount);
	
}

function myWindow(){
	var InvoiceNumberInSystemcrt = document.getElementById("InvoiceNumberInSystem");
	var CompanyCode = document.getElementById("CompanyCode");
	var InvoiceNumberInSystem = document.getElementById("InvoiceNumberInSystem");
	var DocumentTypes = document.getElementById("DocumentType").value;
	if(CompanyCode==null|| trim(CompanyCode.options[CompanyCode.selectedIndex].text)==""){
		alert("First enter Company Code");
		CompanyCode.focus();
	}
	else if(InvoiceNumberInSystem.value==null|| trim(InvoiceNumberInSystem.value)==""){
		alert("First enter Invoice Number");
		InvoiceNumberInSystem.focus();
	}
	else{
		if(!document.getElementById('InvoiceNumberInSystem').readOnly){
			alert("Invoice number cannot be edited once documents are attached.");
		}
		var win = window.open("JSP/Attachment.jsp?InvoiceNumber="+InvoiceNumberInSystem.value+"&DocumentTypes="+DocumentTypes+"&DocumentSize='${requestScope.DocumentSize}'","dataitem","Height=300px,Width=520px, scrollbars=yes,dependent=yes,menubar=no,toolbar=no,status=no,modal=yes,alwaysRaised=yes, Left=280, top=300");
		win.onunload =onun; 

	    function onun() {
	        if(win.location != "about:blank") // This is so that the function 
	                                          // doesn't do anything when the 
	                                          // window is first opened.
	        {// alert("closed");}
	    	}
		}	
}
}

function ClearFields()
{	
	if(null!=document.getElementById("RequestFor")){
		document.getElementById("RequestFor").value='PO';
	}
	if(null!=document.getElementById("AttentionTo")){
		document.getElementById("AttentionTo").value='';
	}
	
	if(null!=document.getElementById("InvoiceNumberInSystem")){
		document.getElementById("InvoiceNumberInSystem").value='';
		document.getElementById("InvoiceNumberInSystem").readOnly =false;
	}
	
	if(null!=document.getElementById("InvoiceDate")){
		document.getElementById("InvoiceDate").value='';
	}
	
	if(null!=document.getElementById("BillingCurrency")){
		document.getElementById("BillingCurrency").value='INR';
	}
	
	if(null!=document.getElementById("InvoiceBaseAmount")){
		document.getElementById("InvoiceBaseAmount").value='';
	}
	if(null!=document.getElementById("ServiceTax")){
		document.getElementById("ServiceTax").value='';
	}
	
	if(null!=document.getElementById("EducationCess")){
		document.getElementById("EducationCess").value='';
	}
	document.getElementById("EducationCess").value='';
	if(null!=document.getElementById("HigherEducationCess")){
		document.getElementById("HigherEducationCess").value='';
	}
	
	if(null!=document.getElementById("Vat")){
		document.getElementById("Vat").value='';
	}
	
	if(null!=document.getElementById("AdditionalVatValue")){
		document.getElementById("AdditionalVatValue").value='';
	}
	
	if(null!=document.getElementById("AnyOtherTax")){
		document.getElementById("AnyOtherTax").value='';
	}
	
	if(null!=document.getElementById("Freight")){
		document.getElementById("Freight").value='';
	}
	
	if(null!=document.getElementById("TotalInvoiceAmount")){
		document.getElementById("TotalInvoiceAmount").value='';
	}
	
	if(null!=document.getElementById("Discount")){
		document.getElementById("Discount").value='';
	}
	
	if(null!=document.getElementById("PenaltyDeduction")){
		document.getElementById("PenaltyDeduction").value='';
	}
	
	if(null!=document.getElementById("VoucherNarration")){
		document.getElementById("VoucherNarration").value='';
	}
	
	if(null!=document.getElementById("FromDate")){
		document.getElementById("FromDate").value='';
	}
	
	if(null!=document.getElementById("ToDate")){
		document.getElementById("ToDate").value='';
	}
	
	if(null!=document.getElementById("PoNo")){
		document.getElementById("PoNo").value='';
	}
	
	if(null!=document.getElementById("AnyOtherTaxType")){
		document.getElementById("AnyOtherTaxType").value='';
	}
	
		
}
function SetEditable(){
	
	fieldEnable("RequestFor");
	fieldEnable("AttentionTo");
	fieldEnable("InvoiceNumberInSystem");
	fieldEnable("InvoiceDate");
	fieldEnable("BillingCurrency");
	fieldEnable("InvoiceBaseAmount");
	fieldEnable("ServiceTax");
	fieldEnable("EducationCess");
	fieldEnable("HigherEducationCess");
	fieldEnable("Vat");
	fieldEnable("AdditionalVatValue");
	fieldEnable("AnyOtherTax");
	fieldEnable("Freight");
	fieldEnable("Discount");
	fieldEnable("PenaltyDeduction");
	fieldEnable("TotalInvoiceAmount");
	fieldEnable("VoucherNarration");
	fieldEnable("FromDate");
	fieldEnable("ToDate");
	fieldEnable("PoNo");
	fieldEnable("AnyOtherTaxType");
	calanderDisable('NonCalander3');
	calanderEnable('calander3');
	calanderDisable('NonCalander19');
	calanderEnable('calander19');
	calanderDisable('NonCalander20');
	calanderEnable('calander20');
}

function SetDisable(){
	fieldDisable("RequestFor");
	fieldDisable("AttentionTo");
	fieldDisable("InvoiceNumberInSystem");
	fieldDisable("InvoiceDate");
	fieldDisable("BillingCurrency");
	fieldDisable("InvoiceBaseAmount");
	fieldDisable("ServiceTax");
	fieldDisable("EducationCess");
	fieldDisable("HigherEducationCess");
	fieldDisable("Vat");
	fieldDisable("AdditionalVatValue");
	fieldDisable("AnyOtherTax");
	fieldDisable("Freight");
	fieldDisable("Discount");
	fieldDisable("PenaltyDeduction");
	fieldDisable("TotalInvoiceAmount");
	fieldDisable("VoucherNarration");
	fieldDisable("FromDate");
	fieldDisable("ToDate");
	fieldDisable("PoNo");
	fieldDisable("AnyOtherTaxType");
	calanderDisable('calander3');
	calanderEnable('NonCalander3');
	calanderDisable('calander19');
	calanderEnable('NonCalander19');
	calanderDisable('calander20');
	calanderEnable('NonCalander20');
}

function validation(){
	var CompanyCode = document.getElementById("CompanyCode");
	var VendorCode = document.getElementById("VendorCode");
	var CompanyName = document.getElementById("CompanyName");
	var VendorName = document.getElementById("VendorName");
	var InvoiceNumberInSystem = document.getElementById("InvoiceNumberInSystem");
	var InvoiceDate = document.getElementById("InvoiceDate");
	var InvoiceBaseAmount = document.getElementById("InvoiceBaseAmount");
	var RequestFor = document.getElementById("RequestFor");
	var TotalInvoiceAmount = document.getElementById("TotalInvoiceAmount");
	var FromDate = document.getElementById("FromDate");
	var ToDate = document.getElementById("ToDate");
	var PoNo = document.getElementById("PoNo");
	var fileadded =document.getElementById("addedDocumends").value;
	
	if(CompanyCode.value==null||trim(CompanyCode.options[CompanyCode.selectedIndex].text)==""){
		alert("Company Code must be entered.");
		CompanyCode.focus();
		return false;
		}
	else if(VendorCode.value==null||trim(VendorCode.value)==""){
		alert("Vendor Code must be entered.");
		VendorCode.focus();
		return false;
		}
	else if(CompanyName.value==null||trim(CompanyName.value)==""){
		alert("Company Name must be entered.");
		CompanyName.focus();
		return false;
		}
	else if(VendorName.value==null||trim(VendorName.value)==""){
		alert("Vendor Name must be entered.");
		VendorName.focus();
		return false;
		}
	else if(trim(RequestFor.value)=='PO' && (PoNo.value==null||trim(PoNo.value)==""))
		{
			alert("PO No must be entered if Request For is PO.");
			PoNo.focus();
			return false;
		}
	else if(null != document.getElementById("mandatoryFeildList") && trim(document.getElementById("mandatoryFeildList").value) != "" ){
		var mandatoryfeilds = document.getElementById("mandatoryFeildList").value;
		var mandatoryFeildsList = mandatoryfeilds.split(",");
		for(var i=0;i<mandatoryFeildsList.length-1;i++){
			var feildsdetails = mandatoryFeildsList[i].split("=");
			var feildname = feildsdetails[0];
			var feildDisplayName = feildsdetails[1];
			if(!feildMandatory(feildname, feildDisplayName)){
				return false;
				break;
			}
			
		}
	}
	if((trim(fileadded)=="" || fileadded==null)||(trim(fileadded)!="" && fileadded.indexOf("Invoice Document") == -1)){
		alert("Invoice Document must be attached");
		return false;
		}
	else if(ToDate.value!=null&&trim(ToDate.value)!=""&&trim(FromDate.value)==""){
		alert("From Date must be entered if To Date is entered.");
		FromDate.focus();
		return false;
		}
	else{
			if(trim(FromDate.value)!=""){
				if(!feildMandatory("ToDate","To Date")){
					return false;
					}
				else{
					var FromDatearr = FromDate.value.split("/");
				    var ToDatearr = ToDate.value.split("/");
					    if(FromDatearr[2]>ToDatearr[2]){
					      alert("From Date cannot be greater than To Date");
					      return false;
					      }
					    else if(FromDatearr[2] == ToDatearr[2] && FromDatearr[1]>ToDatearr[1]){
					      alert("From Date cannot be greater than To Date");
					    return false;
					     }
					    else if(FromDatearr[2] == ToDatearr[2] && FromDatearr[1] == ToDatearr[1] && FromDatearr[0]>ToDatearr[0]){
					      alert("From Date cannot be greater than To Date");
					      return false;
					     }	
					}					
				}
			return true;
			}
	
}

function feildMandatory(feildName, feildDisplayName){
	if(document.getElementById(feildName).value==null||trim(document.getElementById(feildName).value)==""){
		alert(feildDisplayName+" must be entered.");
		document.getElementById(feildName).focus();
		return false;
		}
	else{
		return true;
		}
}
function numberCheck(feild,key){
	var keycode = (key.which) ? key.which : key.keyCode;
	var feild = document.getElementById(feild);
	if(feild.value.length==0 && keycode==46 ){
		return false;
		}
	if (keycode == 08 || keycode == 127 || keycode==09){
		return true;
		}
	if (!(keycode==08 || keycode==46 || keycode == 127)&&(keycode < 48 || keycode > 57)){
			return false;
		}
	else{
			if (feild.value.length <17 && feild.value.length!=13 ){
				if(feild.value.indexOf('.')>=0 && keycode==46){
					return false;
					}
				else{
					if(feild.value.indexOf('.')>=0){
						var valuesplit = feild.value.split('.');
						if(valuesplit[1].length<3){
							return true;
							}
						else{
							return false;
							}
						}
					else{
						return true;
						}
					return true;
					}
			}
			else if(feild.value.length==13){
				if(feild.value.indexOf('.')<0 && keycode==46){
					return true;
					}
				else if(feild.value.indexOf('.')>=0 && keycode!=46){
					var valuesplit = feild.value.split('.');
					if(valuesplit[1].length<3){
						return true;
						}
					else{
						return false;
						}
				}
				else{
					return false;
					}
				}
			else{
				return false;
			}
		}
}
function setReadonlyFalse(){
	fieldReadWrite("AttentionTo");
	fieldReadWrite("RequestFor");
	fieldReadWrite("InvoiceNumberInSystem");
	fieldReadWrite("BillingCurrency");
	fieldReadWrite("InvoiceBaseAmount");
	fieldReadWrite("ServiceTax");
	fieldReadWrite("EducationCess");
	fieldReadWrite("HigherEducationCess");
	fieldReadWrite("Vat");
	fieldReadWrite("AdditionalVatValue");
	fieldReadWrite("AnyOtherTax");
	fieldReadWrite("Freight");
	fieldReadWrite("TotalInvoiceAmount");
	fieldReadWrite("Discount");
	fieldReadWrite("PenaltyDeduction");
	fieldReadWrite("VoucherNarration");
	fieldReadWrite("PoNo");
	fieldReadWrite("AnyOtherTaxType");
		
	calanderDisable('calander3');
	calanderEnable('NonCalander3');
	calanderDisable('calander19');
	calanderEnable('NonCalander19');
	calanderDisable('calander20');
	calanderEnable('NonCalander20');
}

function back(){
	window.history.back();
}

function setDateEditable(){
	calanderDisable('NonCalander3');
	calanderEnable('calander3');
	calanderDisable('NonCalander19');
	calanderEnable('calander19');
	calanderDisable('NonCalander20');
	calanderEnable('calander20');
}
function ResetTable(){
	if( null != document.getElementById('InvoiceNumberInSystem') && document.getElementById('InvoiceNumberInSystem').readOnly){
		var name = document.getElementById('InvoiceNumberInSystem').value+",R";
		xmlhttpPost(name);
		element = document.getElementById('divfiles');
		element.parentNode.removeChild(element);
	}
	ClearFields();
	
}


function checkInvoice(key){
	var keycode = (key.which) ? key.which : key.keyCode;
	if(null != document.getElementById('InvoiceNumberInSystem') && document.getElementById('InvoiceNumberInSystem').readOnly && keycode == 08){
		return false;
		}
	
}
function CheckCompanyCodeandVendorCode(){
			
	if(document.getElementById("CompanyCode").value==null||trim(document.getElementById("CompanyCode").options[document.getElementById("CompanyCode").selectedIndex].text)==''){
		SetDisable();
		//alert("Please Enter Company Code");
		}
	else if(document.getElementById("VendorCode").value==null||trim(document.getElementById("VendorCode").value)==''){
		SetDisable();
		//alert("Please Enter Vendor Code");
	}
	else{
			SetEditable();
	}
	
}


function keycheck(id, event){
	
	if(event.keyCode==27||event.keyCode==122){
		return false;
	}
	if ((event.keyCode==08)&&(event.srcElement.type!='text'&& event.srcElement.type!='textarea'&& event.srcElement.type!='password'))
	{
		return false;
	}
	else if((event.srcElement.readOnly==true)){
		return false;
	}
}

function disableFieldsOnCheck(fieldToBeCompared,valueToBeCompared, fieldToBeDisabled ){
	if (fieldToBeCompared == null || document.getElementById(fieldToBeCompared).value == ''){
		document.getElementById(fieldToBeDisabled).value='';
		document.getElementById(fieldToBeDisabled).disabled=false;
		return false;
	}
	else{
		if (trim(document.getElementById(fieldToBeCompared).value) == valueToBeCompared){
			document.getElementById(fieldToBeDisabled).value='';
			document.getElementById(fieldToBeDisabled).disabled=true;
				return true;
		}
		else{
			//document.getElementById(fieldToBeDisabled).value='';
			document.getElementById(fieldToBeDisabled).disabled=false;
			return false;
		}
	}
}
function ShowCompanyData(){
	var CompanyCode =  document.getElementById("CompanyCode");
	var id= CompanyCode.options[CompanyCode.selectedIndex].id;
	var CmpData = id.split('##');
	if(CmpData.length>=0 && CmpData[0]!=null){
		document.getElementById("CompanyName").value = CmpData[0];
		}
	if(CmpData.length>=1 && CmpData[1]!=null){
		document.getElementById("CompanyAddress").value = CmpData[1];
		}
	
}
function customOnBlur(id, event){
	
	if(id=="InvoiceBaseAmount"){
		CalculateTotalInvoiceAmount();
	}
	if(id=="ServiceTax"){
		CalculateTotalInvoiceAmount();
	}
	if(id=="EducationCess"){
		CalculateTotalInvoiceAmount();
	}
	if(id=="HigherEducationCess"){
		CalculateTotalInvoiceAmount();
	}
	if(id=="Vat"){
		CalculateTotalInvoiceAmount();
	}
	if(id=="AdditionalVatValue"){
		CalculateTotalInvoiceAmount();
	}
	if(id=="AnyOtherTax"){
		CalculateTotalInvoiceAmount();
	}
	if(id=="Freight"){
		CalculateTotalInvoiceAmount();
	}
	if(id=="Discount"){
		CalculateTotalInvoiceAmount();
	}
	if(id=="PenaltyDeduction"){
		CalculateTotalInvoiceAmount();
	}
	if(id=="VendorCode"){
		CheckCompanyCodeandVendorCode();
	}
	if(id=="RequestFor"){
		disableFieldsOnCheck(id, 'Non PO', 'PoNo')
	}
	//Code changes on 2013-04-24 by Abhishek S
	if(id=="InvoiceNumberInSystem"){
		checkInvoiceNumber(id,event);
	}
	//Code changed end here - 2013-04-24
}
function customOnClick(id, event){
	if(id=="Reset"){
		ResetTable();
	}
	if(id=="Attachments"){
		myWindow();
	}
	if(id=="hrefBack"){
		back();
	}
}
function customOnKeyPress(id, event){
	if(id=="InvoiceBaseAmount"){
		return numberCheck(id, event);
	}
	else if(id=="ServiceTax"){
		return numberCheck(id, event);
	}
	else if(id=="EducationCess"){
		return numberCheck(id, event);
	}
	else if(id=="HigherEducationCess"){
		return numberCheck(id, event);
	}
	else if(id=="Vat"){
		return numberCheck(id, event);
	}
	else if(id=="AdditionalVatValue"){
		return numberCheck(id, event);
	}
	else if(id=="AnyOtherTax"){
		return numberCheck(id, event);
	}
	else if(id=="Freight"){
		return numberCheck(id, event);
	}
	else if(id=="Discount"){
		return numberCheck(id, event);
	}
	else if(id=="PenaltyDeduction"){
		return numberCheck(id, event);
	}
	else if(id=="TotalInvoiceAmount"){
		return numberCheck(id, event);
	}
	else if(id=="RequestFor"&& event.keyCode==08){
		 return false;
	}
	else if(id=="VoucherNarration" && document.getElementById("VoucherNarration").value.length > 999){
		return false;
	}
}
function customOnSubmit(id, event){
	if(id=="InvoiceDetails"){
		return validation();
	}
	
}
function customOnKeydown(id, event){
	if(id=="InvoiceformBody"){
		return keycheck(id, event);
	}
	if(id="loginpage"){
		return keycheck(id, event);
	}
}
function customOnChange(id, event){
	if(id=="CompanyCode"){
		ShowCompanyData();
	}
	
}
function fieldDisable(id){
	if(null != document.getElementById(id)){
		document.getElementById(id).disabled=true;
		if(id != "RequestFor" && id != "BillingCurrency")
		document.getElementById(id).value='';
	}
}
function fieldEnable(id){
	if(null != document.getElementById(id)){
		document.getElementById(id).disabled=false;
		}
}
function fieldReadWrite(id){
	if(null != document.getElementById(id)){
		document.getElementById(id).readOnly="readOnly";
		}
}
function calanderEnable(id){
	if(null != document.getElementById(id)){
		document.getElementById(id).style.display='';
		}
}
function calanderDisable(id){
	if(null != document.getElementById(id)){
		document.getElementById(id).style.display='none';
		}
}

function PasswordValidation(id){
	if(id.value != ''){
		var passwordRegEx1 = /[a-z]/g;
		var passwordRegEx2 = /[A-Z]/g;
		var passwordRegEx3 = /[0-9]/g;
		if(id.value.length < 8){
			 alert("Please enter a valid Password.Password should be atleast of 8 characters.Password should contain atleast 1 numeral, 1 lower case letter and 1 upper case letter");
	         id.focus(true);
		}else{
			var matching = passwordRegEx1.test(id.value);
	    	if(!matching){
	    		alert("Please enter a valid Password.Password should be atleast of 8 characters.Password should contain atleast 1 numeral, 1 lower case letter and 1 upper case letter");
	    	 	id.focus(true);
	 		}else{
	    		matching = passwordRegEx2.test(id.value);
	    		if(!matching){
	        	alert("Please enter a valid Password.Password should be atleast of 8 characters.Password should contain atleast 1 numeral, 1 lower case letter and 1 upper case letter");
	         	id.focus(true);
	    		}else{
	    			matching = passwordRegEx3.test(id.value);
		    		if(!matching){
		         	alert("Please enter a valid Password.Password should be atleast of 8 characters.Password should contain atleast 1 numeral, 1 lower case letter and 1 upper case letter");
		         	id.focus(true);
		    		}
	    		}
	    	}
		}
	}
}

function verifyEmail(id){

    var status = true;     
	if(trim(id.value)!=''){
    var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

         if (id.value.search(emailRegEx) == -1) {

              alert("Please enter a valid email address.");
              status = false;
             id.focus(true);
         }    
	}
         return status;	
}

function phoneCheck(feild,key){

	var keycode = (key.which) ? key.which : key.keyCode;
	if(keycode == 08 || keycode == 127 || (keycode >= 48 && keycode <= 57)){
		return true;
	}else{
		return false;
	}
}