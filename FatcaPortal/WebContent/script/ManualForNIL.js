function chkForNilReport(isNilReport) {

	try {
		var doc = document;
		if (isNilReport == "Y") {

			// make a servlet call to delete all saved data
			doc.getElementById("nilReportChk").checked = true;
			doc.getElementById("isNilReport").value = "Y";
			doc.getElementById("sustantial_Owner_Div").style.display = "none";
			doc.getElementById("acctHolder_div").style.display = "none";
			doc.getElementById("Part3_tr").style.display = "none";
			/*
			 * } else { doc.getElementById(obj.id).checked = false;
			 * doc.getElementById("isNilReport").value = "N"; }
			 */
		}


	} catch (e) {
		alert(e);
	}

}

function addGridData_NIL(gridType) {

	var doc = document;
	var valusObj = [];
	var fields = undefined;
	var errormsgId = "accErrorMsg";
	var length = 0;
	var value = undefined;
	var tableId = undefined;

	try {

		doc.getElementById(errormsgId).innerHTML = "&nbsp;";
		
		if (gridType == "accountInformation") {

			tableId = "manual_Data_Grid_Table";
			fields = [ "filerName_manual", "filerAddres_manual",
			           "filerCountry_manual", "giin", "isSponsoredEntity",
			           "sponsoredEntityName", "sponEntityAddress",
			           "sponEntityCountry", "sponEntitygiin", "isIntermediary",
			           "interEntityName", "interEntityAddress",
			           "interEntityCountry", "interEntityGiin", "isNilReport",
			           "filerCategory_manual","sponEntityFilerCategory" ];

			//changes done by shubham for filer category in nil report on 12 06 2017

		}
		if (!isValidateManual_NIL(tableId)) {
			return false;
		}
		if (fields != undefined) {
			length = fields.length;

			var i;
			for (i = 0; i < length; i++) {

				if (gridType == "accountInformation"
					&& (fields[i] == "isSponsoredEntity" || fields[i] == "isIntermediary")) {
					value = getRadioValue(fields[i]);
				} else {
					value = doc.getElementById(fields[i]).value;
				}
				value = (value == undefined ? "" : value);
				valusObj.push(value);
				// str=str+value+"@|";
			}

			if (valusObj.length > 0 && valusObj != undefined)

				addRows_NIL(tableId, valusObj);

		}
		return true;

	} catch (e) {
		alert('agregar datos de cuadr�cula--> ' + e);
	}
}

function addRows_NIL(tableId, value) {

	var doc = document;
	var cellValue = value;
	var cellLength = 0;
	var row = "";
	var cell = "";
	var tableObj = doc.getElementById(tableId);
	var rowCount = tableObj.rows.length;
	var classes = undefined;
	var width = undefined;
	var columns = undefined;
	var ids = undefined;
	var radiobtnGrpName = undefined;
	var tableName = undefined;
	var key = undefined;
	var indexOfValue = undefined;
	var totalCoundId = undefined;
	var parentKey = undefined;
	try {
		refreshColor(tableId);

		if (tableId == "manual_Data_Grid_Table") {
			totalCoundId = "totalAccountCount";
			key = "radio_manual_Data_Grid_Table_" + cellValue[3] + "_"
			+ cellValue[8] + "_" + cellValue[13];

			if (accountInformationObj[key]) {
				alert("No puede agregar cuentas duplicadas.");
				doc.getElementById(key + "_ID").style.backgroundColor = 'red';
				return false;
			}

			columns = [ "FilerGIIN", "SponsorGIIN", "IntermediaryGIIN",
			            "CustomerName", "CustomerTIN", "Account Number", "radio" ];
			cellLength = columns.length;
			indexOfValue = [ 3, 8, 13 ];// indexes for values to be shown in the

			fields = [ "filerName_manual", "filerAddres_manual",
			           "filerCountry_manual", "giin", "isSponsoredEntity",
			           "sponsoredEntityName", "sponEntityAddress",
			           "sponEntityCountry", "sponEntitygiin", "isIntermediary",
			           "interEntityName", "interEntityAddress",
			           "interEntityCountry", "interEntityGiin",
			           "filerCategory_manual","sponEntityFilerCategory" ];
			//changes done by shubham for filer category in nil report on 12 06 2017
			accountInformationObj[key] = new AccountInformationTable(
					cellValue[0], cellValue[1], cellValue[2], cellValue[3],
					cellValue[4], cellValue[5], cellValue[6], cellValue[7],
					cellValue[8], cellValue[9], cellValue[10], cellValue[11],
					cellValue[12], cellValue[13], "", "", "", "", "", "", "",
					"", "", "", "", "", "", "", "", "A", "", "", "", "", "",
					"", "", "", "", "", "", "", cellValue[14],cellValue[15],cellValue[16]);
			//changes done by shubham for cellValue[15],cellValue[16] in nil report on 12 06 2017
			radiobtnGrpName = "accountInformationGroupRadio";
			tableName = "manual_Data_Grid_Table";
			/* bug id 11116* changed class name */
			classes = "ipm_n2";/* "ipm_n"; */
			width = "50";
			// deleteTables("ownerList_table");

		}
		row = tableObj.insertRow();
		// var rowNum = rowCount - 1;
		row.id = key + "_ID";
		var i;
		for (i = 0; i < cellLength; i++) {
			cell = row.insertCell(i);
			cell.className = classes;
			cell.style.width = width;
			if (columns[i] == "radio")
				cell.innerHTML = '<input type="radio" name="' + radiobtnGrpName
				+ '"  id="' + key
				+ '" onclick="return getOwnerInforamtion_NIL('
				+ tableName + ',this);">';
			else if (i == "3" || i == "4" || i == "5") {
				cell.innerHTML = "&nbsp;";
			} else {
				cell.innerHTML = cellValue[indexOfValue[i]];
			}

		}

		reset_NIL(tableId);
		if (totalCoundId != undefined)
			doc.getElementById(totalCoundId).innerHTML = "The total no of record is :"
				+ getTableCount(tableName);

	} catch (ex) {

		alert('a�adir filas NIL-->' + ex);
	}
}

function reset_NIL(source) {
	var doc = document;
	var resetField = undefined;
	var len = 0;
	var resetGIIN = undefined;
	var lengths = 0;
	if (source == "manual_Data_Grid_Table") {
		resetField = [ "sponsoredEntityName", "sponEntityAddress",
		               "sponEntitygiin", "interEntityName", "interEntityAddress",
		               "interEntityGiin", "finance_ActNumber", "finance_balance",
		               "finance_Interest", "finance_Dividend", "finance_Redemption",
		               "finance_Others", "IndividualOREntityName",
		               "IndividualOREntityAddress", "IndividualOREntityTin",
		               "sponEntityCountry", "interEntityCountry", "finance_Country",
		               "IndividualOREntityCountry", "AccHolderCIF", "accDOB" ];
		resetGIIN = [ "text_giin", "text_giin_inter" ];
		lengths = resetGIIN.length;

	}
	if (resetField != undefined)
		len = resetField.length;
	try {
		var i;
		for (i = 0; i < len; i++) {
			doc.getElementById(resetField[i]).value = "";

		}

		if (lengths > 0) {
			var k = 0;
			for (k; k < lengths; k++) {
				var j = 0;
				var id = undefined;
				var suffix = 0;
				for (j; j < 16; j++) {
					suffix = suffix + 1;
					id = resetGIIN[k] + suffix;
					doc.getElementById(id).value = "";

				}
			}
		}

	} catch (e) {
		alert(e);
	}
}

function saveManualGrid_NIL(event) {

	var doc = document;
	var action = undefined;
	var param = "";
	try {
		//alert("");
		var totalRecord = getTableCount("manual_Data_Grid_Table");
		//alert("totalRecord " + totalRecord);
		var reportingYear = doc.getElementById("reportingYear").value;
		var submissionType = doc.getElementById("submissionType").value;
		var url = appPath + "/SubmitManualData";
		//changes made by Alok on 24-11-2016 Start
		// ADD SO To ARRAY LIST OF Substantial ARRYA
		for ( var key in ownerTableObj) {
			var object = ownerTableObj[key];
			action = object.action.toUpperCase();
			if (action == "M" || action == "A")
				substantailOwnerArray.push(object);

		}
		// END

		// ADD ACCOUNT INFORMATION
		action = undefined;
		for ( var key in accountInformationObj) {
			var object = accountInformationObj[key];
			action = object.action.toUpperCase();
			if (action == "M" || action == "A")
				accountInformationArray.push(object);
		}

		//changes made by Alok  on 24-11-2016 End
		// END
		/*
		 * if (event != "submit") { if (accountInformationArray.length == 0 &&
		 * substantailOwnerArray.length == 0) { alert("Kindly add atleast one
		 * filer information"); return false; } }
		 */

		param = "event=" + event + "&submissionType=" + submissionType
		+ "&reportingYear=" + reportingYear + "&ownerJson="
		+ encodeURIComponent(JSON.stringify(substantailOwnerArray))
		+ "&manualJson="
		+ encodeURIComponent(JSON.stringify(accountInformationArray));
		/*
		 * param = "event=" + event + "&submissionType=" + submissionType +
		 * "&reportingYear=" + reportingYear + "&ownerJson=" +
		 * encodeURIComponent(JSON.stringify(substantailOwnerArray)) +
		 * "&manualJson=" +
		 * encodeURIComponent(JSON.stringify(accountInformationArray));
		 */
		var response = ajaxCallManualLongData(url, param);
		/*
		 * if (response == "success") { alert("The data has been submitted
		 * successfully"); // location.reload(); // refresh(); } else
		 */
		if (response == "sessionExpired") {
			//alert("Session has been Expired");
			forwardJSP("SessionExpired");
		} else if (response == "Exception") {
			forwardJSP("ExceptionType");
			alert("Intente despu�s de alg�n tiempo o p�ngase en contacto con el administrador del sistema.");

		}

	} catch (e) {
		alert('guardar los datos de la cuadr�cula manual NIL--> ' + e);
	}
	return true;
}
function isValidateManual_NIL(src) {

	var mandatoryFields = undefined;
	// var mandatoryLabels = undefined;
	var errorMsg = undefined;
	var len = 0;
	var doc = document;

	try {
		if (src == "manual_Data_Grid_Table") {

			mandatoryFields = [ "isSponsoredEntity", "isIntermediary" ];
			len = mandatoryFields.length;
			errorMsg = "filerGiinErrormsg";
			var i = 0;
			for (i = 0; i < len; i++) {
				if (getValuesOfradios(mandatoryFields[i]) == "yes") {
					var fieldList;
					var labels;
					var length = 0;
					if (mandatoryFields[i] == "isSponsoredEntity") {
						fieldList = [ "sponsoredEntityName",
						              "sponEntityAddress", "sponEntityCountry",
						              "sponEntitygiin", 
						              "sponEntityFilerCategory" ];
						labels = [ "Sponser Name", "Sponser Address",
						           "Sponser Country", "Sponser GIIN",
						           "Sponsor Filer Category"];
						//changes done by shubham for sponsor filer category in nil report on 12 06 2017
						errorMsg = "filerGiinErrormsg";

					} else if (mandatoryFields[i] == "isIntermediary") {
						fieldList = [ "interEntityName", "interEntityAddress",
						              "interEntityCountry", "interEntityGiin" ]
						labels = [ "Intermidiary Name", "Intermidiary Address",
						           "Intermidiary Country", "Intermidiary GIIN" ];
						errorMsg = "filerGiinErrormsg";
					}
					var j = 0;
					length = fieldList.length;
					doc.getElementById(errorMsg).innerHTML = "&nbsp;";
					for (j = 0; j < length; j++) {

						if (doc.getElementById(fieldList[j]).value == null
								|| doc.getElementById(fieldList[j]).value == undefined
								|| doc.getElementById(fieldList[j]).value == ""
									|| doc.getElementById(fieldList[j]).value == "undefined") {

							doc.getElementById(errorMsg).innerHTML = "The Field "
								+ labels[j] + " cannot be blank";
							doc.getElementById(errorMsg).className = "errorMsg";
							doc.getElementById(fieldList[j]).focus();

							return false;

						}

					}

				}
			}

			/*
			 * KD:- ---> AS THERE IS NO FIELD TO CHECK IF RADIO BUTTON IS
			 * SELECTED
			 */
			/*
			 * else {
			 * 
			 * doc.getElementById(errorMsg).innerHTML = "&nbsp;"; if
			 * (doc.getElementById(mandatoryFields[i]).value == null ||
			 * doc.getElementById(mandatoryFields[i]).value == undefined ||
			 * doc.getElementById(mandatoryFields[i]).value == "" ||
			 * doc.getElementById(mandatoryFields[i]).value == "undefined") {
			 * doc.getElementById(errorMsg).innerHTML = "The Field " +
			 * mandatoryLabels[i] + " cannot be blank"; //
			 * doc.getElementById(errorMsg).focus();
			 * doc.getElementById(mandatoryFields[i]).focus();
			 * doc.getElementById(errorMsg).className = "errorMsg"; return
			 * false; } }
			 */
			/*
			 * KD:- <--- AS THERE IS NO FIELD TO CHECK IF RADIO BUTTON IS
			 * SELECTED
			 */
			// }
			// IndividualOREntityTin,"Account Holder TIN"
		}
		// common For Entity Type and Account

	} catch (e) {
		alert(e);
	}
	return true;
}

function getOwnerInforamtion_NIL(tableObj, radioObj) {

	var doc = document;
	var fields = undefined;
	var jsonArrayCols = undefined;
	var colsLength = undefined;
	var key = radioObj.id;
	// alert("key"+key);
	var jsonArrayColsOptionvalue = undefined;
	var fieldsOptionTag = undefined;
	var jsonArrayColsOptionLen = 0;
	var jsonOptionValue = undefined;
	try {

		refreshColor(tableObj.id);
		if (tableObj.id == "manual_Data_Grid_Table") {

			fields = [ "sponsoredEntityName", "sponEntityAddress",
			           "sponEntitygiin", "interEntityName", "interEntityAddress",
			           "interEntityGiin" ];
			jsonArrayCols = [ "spName", "spAddress", "spGIIN", "interName",
			                  "InterAddress", "interGIIN" ];
			fieldsOptionTag = [ "sponEntityCountry", "interEntityCountry" ];
			jsonArrayColsOptionvalue = [ "spCountryCode", "interCountryCode" ];

		}
		if (jsonArrayCols != undefined)
			colsLength = jsonArrayCols.length;
		if (jsonArrayColsOptionvalue != undefined)
			jsonArrayColsOptionLen = jsonArrayColsOptionvalue.length;
		var values = undefined;
		for ( var i = 0; i < colsLength; i++) {

			values = accountInformationObj[key][jsonArrayCols[i]];

			// alert("cols:>"+jsonArrayCols[i]+"<values"+values);

			if (tableObj.id == "manual_Data_Grid_Table") {
				if (fields[i] == "sponEntitygiin"
					|| fields[i] == "interEntityGiin") {

					if (fields[i] == "sponEntitygiin") {
						if (accountInformationObj[key]["isSponsoredEntity"] == "no") {
							doc.getElementById("sponsoredEntity_no").checked = true;
							displayModules("isSponsoredEntity");
							$('.spon').hide();

							// displayModules('isSponsoredEntity');

							// doc.getElementById("sponsoredEntity_no").click();
						} else if (accountInformationObj[key]["isSponsoredEntity"] == "yes") {
							doc.getElementById("sponsoredEntity_yes").checked = true;
							// alert("sp"+doc.getElementById("sponEntitygiin").value);
							displayModules("isSponsoredEntity");
							$('.spon').show();

						} else {
							// defualt
						}
					} else if (fields[i] == "interEntityGiin") {
						if (accountInformationObj[key]["isIntermediary"] == "yes") {
							doc.getElementById("isIntermediary_yes").checked = true;
							displayModules("isIntermediary");
							$('.inter').show();
							// displayModules('isIntermediary');
						} else if (accountInformationObj[key]["isIntermediary"] == "no") {
							doc.getElementById("intermediary_no").checked = true;
							displayModules("isIntermediary");
							$('.inter').hide();
							// displayModules('isIntermediary');

						} else {
							// defualt
						}
					}
					// alert("values:"+values +"field"+fields[i]);

					setGIIN(values, fields[i]);
				}

				/*
				 * else if (fields[i] == "isIndividualOREntity") { //
				 * alert(accountInformationObj[key]["accHolderType"] ) if
				 * (accountInformationObj[key]["accHolderType"] != null &&
				 * accountInformationObj[key]["accHolderType"] != undefined)
				 * Check_AccountHolder_Type(accountInformationObj[key]["accHolderType"]); //
				 * enable added so addSORowsOnclickAccountGrid(key); }
				 */
				else {
					doc.getElementById(fields[i]).value = values;
				}

			}

			/*
			 * else if (tableObj.id == "ownerList_table") { //
			 * alert("Field:"+fields[i]+"value"+values);
			 * doc.getElementById(fields[i]).value = values; }
			 */
			else {
				// default
			}
			/*
			 * if (tableObj.id == "ownerList_table" &&
			 * ownerTableObj[key]["submittedFlag"] == "Y") {
			 * doc.getElementById("OwnerTin_manual").readOnly = true;
			 * doc.getElementById("finance_ActNumber").readOnly = true; } else
			 * if (tableObj.id == "manual_Data_Grid_Table" &&
			 * accountInformationObj[key]["submittedFlag"] == "Y") {
			 * doc.getElementById("finance_ActNumber").readOnly = true; } else {
			 * doc.getElementById("OwnerTin_manual").readOnly = false;
			 * doc.getElementById("finance_ActNumber").readOnly = false; }
			 */

		}
		/*
		 * for ( var i = 0; i < jsonArrayColsOptionLen; i++) { //
		 * alert("id:"+fieldsOptionTag[i]+"value:"+ownerTableObj[key][jsonArrayColsOptionvalue[i]]);
		 * jsonOptionValue = (tableObj.id == "ownerList_table" ?
		 * ownerTableObj[key][jsonArrayColsOptionvalue[i]] :
		 * accountInformationObj[key][jsonArrayColsOptionvalue[i]]); //
		 * alert(fieldsOptionTag[i],jsonOptionValue)
		 * selectDropDown(fieldsOptionTag[i], jsonOptionValue);// param }
		 */

	} catch (e) {

		alert(e);
	}

}

/* for delete */
function deleteSO_NIL(parentKey, submittedFlag) {
	try {

		var doc = document;
		for ( var a in ownerTableObj) {
			var key = a;
			var object = ownerTableObj[key];
			if (object.parentKey == parentKey) {

				// doc.getElementById(key + "_ID").style.backgroundColor =
				// 'red';

				if (submittedFlag != "" && submittedFlag != undefined)
					ownerTableObj[key]["action"] = "D";
				else {
					delete ownerTableObj[key];
				}

			}
		}

		if (submittedFlag == "" || submittedFlag == undefined)
			deleteTables("ownerList_table");// Refresh the tables if the
		// transaction
		// has not persist
	} catch (e) {
		alert(e);
	}

}
function deleteRow_NIL(tableName, radioOptionName) {
	// alert("inside deleteRow_NIL");

	var doc = document;
	var totalCoundId = "totalAccountCount";

	try {

		if (!deleteFromJSONObject_NIL(tableName, radioOptionName)) {
			alert("No se seleccion� ninguna fila.");
			return false;
		}

		if (totalCoundId != undefined) {
			doc.getElementById(totalCoundId).innerHTML = "The total no of record is :"
				+ getTableCount(tableName);
		}

	} catch (e) {
		alert("excepci�n:" + e);
	}

}
function deleteFromJSONObject_NIL(tableName, radioOptionName) {
	var doc = document;
	var response = undefined;
	var obj = doc.getElementById(tableName);
	response = selectUserListRow_NIL(tableName, radioOptionName);
	if (response == "-1")
		return false;
	var selectedId = $('input[name="' + radioOptionName + '"]:checked').attr(
	'id');
	// alert("selectedId--> " + selectedId);
	doc.getElementById(selectedId + "_ID").style.backgroundColor = 'red';
	var submittedFlag = undefined;
	if (tableName == "manual_Data_Grid_Table") {

		if (accountInformationObj[selectedId] != undefined) {

			submittedFlag = accountInformationObj[selectedId]["submittedFlag"];

			if (submittedFlag !== "" && submittedFlag != undefined)// Means
				// data has
				// been
				// saved in
				// DB
			{
				accountInformationObj[selectedId]["action"] = 'D';
				// deleteSO(selectedId, submittedFlag);
			} else {
				// deleteSO(selectedId, submittedFlag);
				delete accountInformationObj[selectedId];

			}

			// deleteSO(selectedId,submittedFlag);

			/*
			 * if (accountInformationObj[selectedId]["submittedFlag"] != "Y") { //
			 * If any account group is delete then so data would also delete
			 * deleteSO(selectedId); delete accountInformationObj[selectedId];
			 * alert("Deleted Successfully"); } else { alert("You cannot delete
			 * the submitted Data.Only you can modify the data"); return false; }
			 */

		}
	} else {

	}

	if (submittedFlag != "" && submittedFlag != undefined)
		alert("Para eliminar las filas, confirme el cambio o haga clic en guardar o enviar el bot�n");
	else {
		if (response !== -1) {
			obj.deleteRow(response + 1);
			alert("Borrado exitosamente");
		}
	}

	return true;
}
/* for delete */

/* for modify */
function modifiedRows_NIL(tableName, radioOptionName) {
	var selectedId = undefined;
	// alert("inside modifiedRows_NIL");
	try {
		refreshColor(tableName);
		var response = selectUserListRow_NIL(tableName, radioOptionName);
		if (response > -1) {
			selectedId = $('input[name="' + radioOptionName + '"]:checked')
			.attr('id');
		} else {
			alert("No se seleccion� ninguna fila.");
			return false;
		}
		if (selectedId != undefined) {

			if (!isValidateManual_NIL(tableName)) {
				return false;
			}

			modifyValues_NIL(tableName, selectedId);

		}

	} catch (e) {
		alert(e);
	}
	return true;
}

function modifyValues_NIL(tableName, selectedRow) {

	// alert("inside modifyValues_NIL");

	var doc = document;
	var selectedOptionObj = doc.getElementById(selectedRow);

	var selectedrowID = selectedRow;// selectedRow.substring(6,
	// selectedRow.length);
	// alert(selectedRow);
	var row = doc.getElementById(selectedRow + "_ID");
	var cell = row.cells;
	var fields = undefined;
	var length = 0;
	var cellValue = [];

	var currentOptionkeyOrId = undefined;

	var key = undefined;

	// alert(tableName+"-->"+selectedRow);
	// Always keep non-display cell in last columns
	// Always maintain the order of the cell value which you need to display to
	// make maintaince easy.
	if (tableName == 'manual_Data_Grid_Table') {

		fields = [ "filerName_manual", "filerAddres_manual",
		           "filerCountry_manual", "giin", "isSponsoredEntity",
		           "sponsoredEntityName", "sponEntityAddress",
		           "sponEntityCountry", "sponEntitygiin", "isIntermediary",
		           "interEntityName", "interEntityAddress", "interEntityCountry",
		           "interEntityGiin", "isNilReport" ,
		           "filerCategory_manual","sponEntityFilerCategory"];
		//changes done by shubham for filerCategory_manual","sponEntityFilerCategory" in nil report on 12 06 2017
	}
	if (fields != undefined)
		length = fields.length;
	try {
		if (length > 0) {
			var i;
			for (i = 0; i < length; i++) {

				if (tableName == "manual_Data_Grid_Table"
					&& (fields[i] == "isSponsoredEntity" || fields[i] == "isIntermediary")) {
					value = getRadioValue(fields[i]);
				} else {
					value = doc.getElementById(fields[i]).value;
				}
				value = (value == undefined ? "" : value);
				cellValue.push(value);
			}
			if (tableName == "manual_Data_Grid_Table") {

				key = "radio_manual_Data_Grid_Table_" + cellValue[3] + "_"
				+ cellValue[8] + "_" + cellValue[13];

				if (selectedRow != key && accountInformationObj[key]) {
					alert("No se puede modificar la Entidad duplicada.");
					doc.getElementById(key + "_ID").style.backgroundColor = 'red';
					return false;
				}

				// deleteFromJSONObject(tableName, selectedRow);
				var corrDocrefID = accountInformationObj[selectedRow]["corrDocrefID"];
				var docrefid = accountInformationObj[selectedRow]["docrefid"];
				var mesgRefID = accountInformationObj[selectedRow]["mesgRefID"];
				var corrMesggRefID = accountInformationObj[selectedRow]["corrMesggRefID"];
				var submittedFlag = accountInformationObj[selectedRow]["submittedFlag"];
				var rowId = accountInformationObj[selectedRow]["rowId"];
				var spDocRefID = accountInformationObj[selectedRow]["spDocRefID"];
				var spCorrDocRefID = accountInformationObj[selectedRow]["spCorrDocRefID"];
				var interDocRefID = accountInformationObj[selectedRow]["interDocRefID"];
				var interCorrDocRefID = accountInformationObj[selectedRow]["interCorrDocRefID"];
				var filerDocRefID = accountInformationObj[selectedRow]["filerDocRefID"];
				var filerCorrDocRefID = accountInformationObj[selectedRow]["filerCorrDocRefID"];
				delete accountInformationObj[selectedRow];

				

				var cellIndexValue = [ 3, 8, 13 ];
				var j;
				var length = cellIndexValue.length;
				for (j = 0; j < length; j++) {
					cell[j].innerHTML = cellValue[cellIndexValue[j]];
				}

				accountInformationObj[key] = new AccountInformationTable(
						cellValue[0], cellValue[1], cellValue[2], cellValue[3],
						cellValue[4], cellValue[5], cellValue[6], cellValue[7],
						cellValue[8], cellValue[9], cellValue[10],
						cellValue[11], cellValue[12], cellValue[13], "", "",
						"", "", "", "", "", "", "", "", "", "", "",
						corrDocrefID, docrefid, "M", submittedFlag, rowId,
						mesgRefID, corrMesggRefID, "", "", spDocRefID,
						spCorrDocRefID, interDocRefID, interCorrDocRefID,
						filerDocRefID, filerCorrDocRefID, cellValue[14],cellValue[15]
						,cellValue[16]);
				//changes done by shubham for cellValue[15],cellValue[16] in nil report on 12 06 2017
				
				selectedOptionObj.setAttribute("id", key);
				row.setAttribute("id", key + "_ID");

				// START UPDATE THE SO OBJECT
				/*
				 * parameters
				 * 
				 * accountNo,accName,accTin,entityType,orignalParentkey,oldParentKey
				 */
				// Check if it is entity type and have some entity
				// alert(cellValue[21]
				// +"len"+doc.getElementById("ownerList_table").rows.length);
				/*
				 * if (cellValue[21] == "Entity" &&
				 * doc.getElementById("ownerList_table").rows.length > 1)
				 * updateTheSOTables(cellValue[14], cellValue[22],
				 * cellValue[25], cellValue[26], key, selectedRow,
				 * cellValue[27], cellValue[28]);
				 */
				// END
			} else {
				// no parameter to check default
			}

			// END NEW CODE

		}
		alert("Los datos han sido modificados con �xito");
	} catch (e) {
		alert(e);
	}
}

/* for modify */

/* for save */
function saveManualGridData_NIL(event) {
	// alert("event--> " + event);
	// return false;
	var doc = document;
	var action = undefined;
	var param = "";
	try {
		var reportingYear = doc.getElementById("reportingYear").value;
		var submissionType = doc.getElementById("submissionType").value;
		var url = appPath + "/SubmitManualData";
		// ADD SO To ARRAY LIST OF Substantial ARRYA
		for ( var key in ownerTableObj) {
			var object = ownerTableObj[key];
			action = object.action.toUpperCase();
			if (action == "M" || action == "A" || action == "D")
				substantailOwnerArray.push(object);

		}
		// END
		// ADD ACCOUNT INFORMATION
		action = undefined;
		for ( var key in accountInformationObj) {
			var object = accountInformationObj[key];
			// alert(object.action);
			action = object.action.toUpperCase();
			if (action == "M" || action == "A" || action == "D")
				accountInformationArray.push(object);

		}
		// END

		if (event != "submit") {
			if (accountInformationArray.length == 0
					&& substantailOwnerArray.length == 0) {
				alert("Por favor, agregue al menos un archivo de informaci�n");
				return false;
			}
		}

		// alert("FinalSo:"+JSON.stringify(substantailOwnerArray));
		// alert("AccountInformationArray:"+JSON.stringify(accountInformationArray));

		/* var filerGIIN=doc.getElementById("giin").value; */
		param = "event=" + event + "&submissionType=" + submissionType
		+ "&reportingYear=" + reportingYear + "&ownerJson="
		+ encodeURIComponent(JSON.stringify(substantailOwnerArray))
		+ "&manualJson="
		+ encodeURIComponent(JSON.stringify(accountInformationArray));
		/*
		 * param = "event=" + event + "&submissionType=" + submissionType +
		 * "&reportingYear=" + reportingYear +
		 * "&ownerJson=NILReport&manualJson=" +
		 * encodeURIComponent(JSON.stringify(accountInformationArray))
		 */
		// alert("url"+url);
		var response = ajaxCallManualLongData(url, param);
		if (response == "success") {
			alert("Los datos han sido guardados / enviados correctamente");
			location.reload();
			// refresh();
		} else if (response == "sessionExpired") {
			//alert("Session has been Expired");
			forwardJSP("SessionExpired");
		} else if (response == "Exception") {
			forwardJSP("ExceptionType");
			alert("Intente despu�s de alg�n tiempo o p�ngase en contacto con el administrador del sistema.");

		}

	} catch (e) {
		alert('guardar los datos de la cuadr�cula manual--> ' + e);
	}
	return true;
}
/* fro save */

function removefromJSONObject_NIL(table, radioGrpName) {
	// alert("inside removefromJSONObject_NIL ");
	var dataRemoved = false;
	var doc = document;
	var submittedFlag;
	try {
		var tableId = doc.getElementById(table);
		var radio = doc.getElementsByName(radioGrpName);

		if (tableId.rows.length > 1) {

			var i;
			for (i = 0; i < radio.length; i++) {
				if (accountInformationObj[radio[i].id] != undefined) {
					submittedFlag = accountInformationObj[radio[i].id]["submittedFlag"];
					if (submittedFlag !== "" && submittedFlag != undefined)// Means
						// data has
						// been
						// saved in
						// DB
					{
						accountInformationObj[radio[i].id]["action"] = 'D';
						deleteSO_NIL(radio[i].id, submittedFlag);
					} else {
						deleteSO_NIL(radio[i].id, submittedFlag);
						delete accountInformationObj[radio[i].id];
					}
				}
			}
			if (saveManualGrid_NIL("save")) {
				dataRemoved = true;
			}
		}

		else {
			// alert("No row selected ");
			// alert("No row to delete ");
			dataRemoved = true;
		}

		return dataRemoved;
	} catch (e) {
		alert(e);
	}
}

function deleteTables_NIL(table) {
	var deletedFromtable = false;
	try {
		var doc = document;
		var elmtTable = doc.getElementById(table);
		var length = elmtTable.rows.length;
		var x;
		var totalCoundId = undefined;

		if (table == "manual_Data_Grid_Table") {
			radioGrpName = "accountInformationGroupRadio";
			totalCoundId = "totalAccountCount";
		} else {
			radioGrpName = "ownerListtableRadio";
			totalCoundId = "totalSoCount";
		}
		if (removefromJSONObject_NIL(table, radioGrpName)) {
			for (x = length - 1; x > 0; x--) {
				elmtTable.deleteRow(x);
			}
			deletedFromtable = true;
		}
		if (table != "userApprovalTable")
			if (totalCoundId != undefined)
				doc.getElementById(totalCoundId).innerHTML = "The total no of record is :"
					+ getTableCount(table);
		return deletedFromtable;
	} catch (e) {
		alert("eliminar tablas NIL-->  " + e);
	}
}

function selectUserListRow_NIL(tableNameId, radioGrpName) {

	var tableId;
	var chkflag = false;
	var doc = document;
	// alert("nametb:"+tableNameId+"radio"+radioGrpName);
	var index = -1;
	tableId = doc.getElementById(tableNameId);
	var radio = doc.getElementsByName(radioGrpName);
	try {
		if (tableId.rows.length > 1) {

			var i;
			for (i = 0; i < radio.length; i++) {
				if (radio[i].checked) {
					chkflag = true;
					index = i;
					break;
				}
			}
			if (chkflag) {
				return index;
			} else {
				alert("Seleccione al menos una opci�n");
				return index;
			}

		} else {
			// alert("No row selected.");
			return index;
		}
		return index;
	} catch (e) {
		alert(e);
	}

}