var ownerTableObj = [];
var accountInformationObj = [];
var substantailOwnerArray = [];
var accountInformationArray = [];

/*
 * function accountEntityRelation(parentKey,childKey){ this.childKey=childKey; }
 */

function ownerTable(rowID, filerGIIN, accountNumber, accName, accTin,
		entityType, entityName, entityAddress, entityCountryCode, entityTin,
		corrDocrefID, docrefid, action, submittedFlag, parentKey, accHolderCIF,
		accDOB) {
	// Entity+filer GIIN
	this.rowID = rowID;
	this.filerGIIN = filerGIIN;
	this.accountNumber = accountNumber;
	this.accName = accName;
	this.accTin = accTin;
	this.entityType = entityType;
	// SO
	this.entityName = entityName;
	this.entityAddress = entityAddress;
	this.entityCountryCode = entityCountryCode;
	this.entityTin = entityTin;
	// Below fields are custom purpose
	this.corrDocrefID = corrDocrefID;
	this.docrefid = docrefid;
	this.action = action;
	this.submittedFlag = submittedFlag;
	this.parentKey = parentKey;
	this.accHolderCIF = accHolderCIF;
	this.accDOB = accDOB;

}

function AccountInformationTable(fiName, fiAddress, fiCountryCode, fiGIIn,
		isSponsoredEntity, spName, spAddress, spCountryCode, spGIIN,
		isIntermediary, interName, InterAddress, interCountryCode, interGIIN,
		accountNumber, accCurrencyCode, accBalance, accIntrest, accDividend,
		accGrossRedem, accOthers, accHolderType, accName, accAddress,
		accCountryCode, accTin, accEntityType, corrDocrefID, docrefid, action,
		submittedFlag, rowId, mesgRefID, corrMesgRefID, accHolderCIF, accDOB,
		spDocRefID, spCorrDocRefID, interDocRefID, interCorrDocRefID,
		filerDocRefID, filerCorrDocRefID, isNilReport,filerCategory_manual,
		sponEntityFilerCategory) {
	//filerCategory_manual added for filer category by shubham on 09 06 2017

	this.fiName = fiName;
	this.fiAddress = fiAddress;
	this.fiCountryCode = fiCountryCode;
	this.fiGIIn = fiGIIn;
	this.isSponsoredEntity = isSponsoredEntity;
	this.spName = spName;
	this.spAddress = spAddress;
	this.spCountryCode = spCountryCode;
	this.spGIIN = spGIIN;
	this.isIntermediary = isIntermediary;
	this.interName = interName;
	this.InterAddress = InterAddress;
	this.interCountryCode = interCountryCode;
	this.interGIIN = interGIIN;
	this.accountNumber = accountNumber;
	this.accCurrencyCode = accCurrencyCode;
	this.accBalance = accBalance;
	this.accIntrest = accIntrest;
	this.accDividend = accDividend;
	this.accGrossRedem = accGrossRedem;
	this.accOthers = accOthers;
	this.accHolderType = accHolderType;
	this.accName = accName;
	this.accAddress = accAddress;
	this.accCountryCode = accCountryCode;
	this.accTin = accTin;
	this.accEntityType = accEntityType;
	this.corrDocrefID = corrDocrefID;
	this.docrefid = docrefid;
	this.action = action;
	/* this.rowid = rowid; */
	this.submittedFlag = submittedFlag;
	this.rowId = rowId;
	this.mesgRefID = mesgRefID;
	this.corrMesgRefID = corrMesgRefID;
	// Added on 04.01.2016 by arbind to handle sponser change/Modification
	this.accHolderCIF = accHolderCIF;
	this.accDOB = accDOB;
	this.spDocRefID = spDocRefID;
	this.spCorrDocRefID = spCorrDocRefID;
	this.interDocRefID = interDocRefID;
	this.interCorrDocRefID = interCorrDocRefID;
	this.filerDocRefID = filerDocRefID;
	this.filerCorrDocRefID = filerCorrDocRefID;
	this.isNilReport = isNilReport;
	//filerCategory_manual added for filer category by shubham on 09 06 2017
	this.filerCategory_manual = filerCategory_manual;
	this.sponEntityFilerCategory = sponEntityFilerCategory;
}

function getSubmissionType(obj, event, src, compliance) {
	// alert(src);
	var doc = document;
	var url = "";
	var param = "";
	var response = "";
	var dropdownObjID = "";
	var errorMsgID = "";
	var reportingYear = "";
	// alert(obj.value);
	try {
		if (obj.value != 'blank') {

			if (src == "Manual") {
				dropdownObjID = "Manual_Submission_Type";
				errorMsgID = "errorMsgManualScreen";
				param = "event=" + event + "&UserEmail=" + src
						+ "&reportingYear=" + obj.value + "&ComplianceType="
						+ compliance + "&testActual=Actual";
			} else if (src == "Xml") {
				dropdownObjID = "new_update";
				errorMsgID = "UploadMsg";
				reportingYear = doc.getElementById("reportingYear").value;

				if (reportingYear == "" || reportingYear == "--Select--") {
					doc.getElementById(errorMsgID).innerHTML = "Seleccione primero el a�o del informe.";
					doc.getElementById("submissionType").value = "";
					return false;
				} else {
					doc.getElementById(errorMsgID).innerHTML = "&nbsp;";
					param = "event=" + event + "&UserEmail=" + src
							+ "&reportingYear=" + reportingYear
							+ "&ComplianceType=" + compliance + "&testActual="
							+ obj.value;
				}
			}

			url = appPath + "/SubmitManualData";
			// src --> either from manual or xml

			var response = ajaxCall(url, param);
			// alert(response);
			if (response == "sessionExpired") {
				forwardJSP("SessionExpired");
				return true;
			} else if (response == "Exception") {
				forwardJSP("ExceptionType");
				return true;
			} else {
				// doc.getElementById("Manual_Submission_Type").value =
				// response;

				/**/

				doc.getElementById(dropdownObjID).value = response;
				/*
				 * var dropDownObj = doc.getElementById(dropdownObjID); for (
				 * var i = 0; i < dropDownObj.length; i++) { if
				 * (dropDownObj[i].value == response) { dropDownObj[i].selected =
				 * true; } else { dropDownObj[i].disabled = true; } }
				 */
				/**/
				// doc.getElementById("Manual_Submission_Type").readOnly = true;
				// doc.getElementById("Manual_Submission_Type").disable = true;
				doc.getElementById(dropdownObjID).className += " "
						+ "readOnlyClass";
				doc.getElementById(errorMsgID).innerHTML = "&nbsp;";
				return true;
			}

		} else {
			resetManualSelectionScreen(src);
			return true;
		}

	} catch (e) {
		alert(e);
	}
}

function resetManualSelectionScreen(src) {

	var doc = document;
	var fieldIDAry = "";
	if (src == "Manual") {
		fieldIDAry = [ "Manual_Reporting_Year", "Manual_Submission_Type" ];
	} else {

		fieldIDAry = [ "reportingYear", "new_update" ];

	}
	var len = "";
	try {
		len = fieldIDAry.length;
		for ( var i = 0; i < len; i++) {
			doc.getElementById(fieldIDAry[i]).value = "blank";
			doc.getElementById(fieldIDAry[i]).disabled = false;
			doc.getElementById(fieldIDAry[i]).className = "form-control";
		}
	} catch (e) {
		alert(e);
	}
}

function getManualForm() {
	var doc = document;
	try {

		if (doc.getElementById("Manual_Reporting_Year").value != "blank"
				&& doc.getElementById("Manual_Reporting_Year").value != "") {
			manualsubmitform();
			return true;
		} else {
			doc.getElementById("errorMsgManualScreen").innerHTML = "Seleccione el a�o de informe";
			doc.getElementById("errorMsgManualScreen").className = "errorMsg";
			return false;
		}

	} catch (e) {
		alert(e);
	}
}

function manualsubmitform() {
	var doc = document;
	doc.manualselection.submit();
}

function replaceDocument(docType) {
	var doc = document;
	try {
		if (docType == "Document1") {
			doc.getElementById("file1_admin").click();
		} else if (docType == "Document2") {
			doc.getElementById("file2_admin").click();
		}

	} catch (e) {
		alert(e);
	}

}

function mandateAdminApprovalPage() {
	// alert("mandateAdminApprovalPage");
	var selectID = [ "fiName_adminApproval", "fiAddress_adminApproval",
			"fiEmailID_adminApproval", "userName_adminApproval",
			"userAddress_adminApproval", "userPhone_adminApproval",
			"userEmployee_adminApproval", "userDesignation_adminApproval" ];
	var label = [ "FI Name", "FI Address", "Fi Email", "User Name",
			"User Address", "User Phone", "User Employee", "User Designation" ];
	var doc = document;
	var len = selectID.length;
	var isValidate = true;
	// alert("inside");
	try {
		for ( var i = 0; i < len; i++) {
			// alert("value--> "+doc.getElementById(selectID[i]).value);
			if (doc.getElementById(selectID[i]).value == null
					|| doc.getElementById(selectID[i]).value == undefined
					|| doc.getElementById(selectID[i]).value == ""
					|| doc.getElementById(selectID[i]).value == "undefined") {
				// alert("The " + label[i] + " cannot be blank");

				if (selectID[i].indexOf("fi") != -1) {
					// UsererrorMsg
					doc.getElementById("FierrorMsg").innerHTML = "El "
							+ label[i] + " no puede estar en blanco";
					doc.getElementById("FierrorMsg").className = " errorMsg";
					doc.getElementById("UsererrorMsg").innerHTML = "&nbsp;";
					doc.getElementById("commentError").innerHTML = "&nbsp;";

					doc.getElementById("FierrorMsg").focus();
				} else

				if (selectID[i].indexOf("user") != -1) {
					// UsererrorMsg
					doc.getElementById("UsererrorMsg").innerHTML = "El "
							+ label[i] + " no puede estar en blanco";
					doc.getElementById("UsererrorMsg").className = " errorMsg";
					doc.getElementById("FierrorMsg").innerHTML = "&nbsp;";
					doc.getElementById("commentError").innerHTML = "&nbsp;";
					doc.getElementById("UsererrorMsg").focus();
				}

				// doc.getElementById(selectID[i]).focus();
				isValidate = false;

				break;
			} else {
				doc.getElementById("FierrorMsg").innerHTML = "&nbsp;";
				doc.getElementById("UsererrorMsg").innerHTML = "&nbsp;";

			}

		}

		return isValidate;

	} catch (ex) {

		alert("mandato P�gina de aprobaci�n de administrador--> " + ex);
	}

}

function checkFormatAdminPage(Data) {
	// alert("checkFormatAdminPage");
	var labelMsg = "";
	var value = Data.value;
	var filter = "";
	var errorMsgId = "";
	// var errorMsg = "";
	var doc = document;
	// alert(Data.id);
	// var isflag = undefined;
	if (Data.id == "fiEmailID_adminApproval"
			|| Data.id == "userEmailID_adminApproval") {
		filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		labelMsg = "Identificaci�n de correo electr�nico";
		if (Data.id == "userEmailID_adminApproval") {
			errorMsgId = "UsererrorMsg";

		} else if (Data.id == "fiEmailID_adminApproval") {
			errorMsgId = "FierrorMsg";
		}

	} else if (Data.id == "userPhone_adminApproval") {
		// Format Support
		// 123-345-3456 and (078)789-8908
		// XXX-XXX-XXXX
		// XXX.XXX.XXXX
		// XXX XXX XXXX
		/*
		 * (123) 456-7890 123-456-7890 123.456.7890 1234567890 +31636363634
		 * 075-63546725
		 */
		filter = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
		labelMsg = "N�mero de tel�fono";
		errorMsgId = "UsererrorMsg";

	} else {

		if (value != null && value != undefined && value != ""
				&& value != "undefined") {

			doc.getElementById("UsererrorMsg").innerHTML = "&nbsp;";
			doc.getElementById("FierrorMsg").innerHTML = "&nbsp;";
		}
	}
	doc.getElementById(errorMsgId).innerHTML = "&nbsp;";
	if (!filter.test(value)) {
		doc.getElementById(errorMsgId).innerHTML = "Proporcione una "
			+ labelMsg+" v�lido";
		doc.getElementById(errorMsgId).className = "errorMsg";
		Data.focus();
		return false;
	} else {
		return true;
	}
}

function checkOTPExist(src) {

	var doc = document;
	var url = "";
	var param = "";
	var email = "";
	var event = "";
	var otp = "";
	try {
		if (src == "registrationOTP") {
			email = doc.getElementById("emailForPIN").value;
			event = "CheckOTPExist";
			otp = "test";
		}
		url = appPath + "/Registration";
		param = "email=" + email + "&otp=" + otp + "&source=" + src + "&event="
				+ event;
		if (ajaxCall(url, param) == "success") {
			doc.getElementById("pin").readOnly = false;
		} else {
			doc.getElementById("pin").readOnly = true;
		}

	} catch (e) {
		alert("compruebe que OTP existe--> " + e);
	}
}

/* CRS Script 29012015 */
function checkComplianceType(obj) {
	var doc = document;
	try {
		var isComplianceChecked = doc.getElementById(obj.id).checked;
		if (obj.id == "fatcaReportCheck") {
			if (isComplianceChecked) {
				/* 1 for checked 0 for unchecked */
				doc.getElementById("fatcaCompliance").value = "1";
			} else {
				doc.getElementById("fatcaCompliance").value = "0";
			}

		} else if (obj.id == "crsReportCheck") {
			if (isComplianceChecked) {
				doc.getElementById("crsCompliance").value = "1";
			} else {
				doc.getElementById("crsCompliance").value = "0";
			}
		}

	} catch (e) {
		alert(e);
	}

}

function isComplianceSelected() {
	var doc = document;

	try {
		var fatcaCompliance = doc.getElementById("fatcaCompliance").value;
		var crsCompliance = doc.getElementById("crsCompliance").value;
		if (fatcaCompliance == "0" && crsCompliance == "0")
			return false;
		/*
		 * if (fatcaCompliance == "0") return false;
		 */
		else
			return true;
	} catch (e) {
		alert(e);
	}

}

function checkMaxFiAdmin() {
	// alert("inside checkMaxFiAdmin");
	var flag = false;
	var doc = document;
	var url = "";
	var param = "";
	var option = "ChkMaxFiAdmin";
	var giin = "";
	// var otp = "";
	try {
		if (doc.getElementById("fatcaCompliance").value == "1") {
			giin = doc.getElementById("giin").value;
		} else {
			giin = doc.getElementById("giin1").value;
		}
		url = appPath + "/AccountCreation";
		param = "option=" + option + "&giin=" + giin;
		var response = ajaxCall(url, param);
		// alert(response);
		if (response.toUpperCase() == "TRUE") {
			flag = true;
		} else {
			flag = false;
		}

	} catch (e) {
		alert("compruebe que OTP existe--> " + e);
	}
	return flag;
}

function isLastFiAdmin(giin) {
	// alert(giin);
	var flag = false;
	var url = "";
	var param = "";
	var option = "ChkIsLastFiAdmin";
	try {
		url = appPath + "/UserApproval";
		param = "option=" + option + "&giin=" + giin;
		var response = ajaxCall(url, param);

		if (response.toUpperCase() == "TRUE") {
			flag = true;
		} else {
			flag = false;
		}
	} catch (e) {
		alert("es el �ltimo administrador de Fi--> " + e);
	}
	return flag;

}
// *********************************************************************************
// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
// Group : Application Projects1 (US)
// Project : [Fatca IRD]
// Date Written : 08/06/2016
// Date Modified :
// Author : Khushdil Kaushik
// Description : To display and hide a particular div
// ***********************************************************************************/
function showMyDIV(src) {

	var doc = document;
	var divIDs = [ "customReport_div", "userReport_div", "userForGIIN_div",
			"yearlyReportStatus_div" ];
	var length = divIDs.length;
	try {
		for ( var i = 0; i < length; i++) {
			if (divIDs[i] == src) {
				doc.getElementById(divIDs[i]).style.display = "block";
			} else {

				doc.getElementById(divIDs[i]).style.display = "none";
			}
		}
		return true;
	} catch (e) {
		alert("excepci�n en mostrar mi DIV :: " + e);
	}
}

// *********************************************************************************
// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
// Group : Application Projects1 (US)
// Project : [Fatca IRD]
// Date Written : 13/06/2016
// Date Modified :
// Author : Khushdil Kaushik
// Description : To vaildate form fileds and to submit the form
// ***********************************************************************************/

function submitCustomeReportData(id) {

	var doc = document;
	var formFilds;
	var formFiledsLabel;
	var len;
	try {

		if (id == 'customReport') {
			document.customReportForm.submit();
			return true;
		} else if (id == 'customReport_giin') {
			formFilds = [ "fromDate", "toDate" ];
			formFiledsLabel = [ "From Date", "To Date" ];
			len = formFilds.length;
			for ( var i = 0; i < len; i++) {
				if (doc.getElementById(formFilds[i]).value == null
						|| doc.getElementById(formFilds[i]).value == undefined
						|| doc.getElementById(formFilds[i]).value == ""
						|| doc.getElementById(formFilds[i]).value == "undefined") {

					doc.getElementById("errormsg_giinReport").innerHTML = "El "
							+ formFiledsLabel[i] + " no puede estar en blanco";
					doc.getElementById("errormsg_giinReport").className = " errorMsg";
					doc.getElementById("errormsg_yearlyReport").innerHTML = "&nbsp;";
					return false;
				}
			}

			document.customReportForm_giin.submit();
			doc.getElementById("errormsg_yearlyReport").innerHTML = "&nbsp;";
			doc.getElementById("errormsg_giinReport").innerHTML = "&nbsp;";
			return true;

		} else if (id == 'customReport_yearly') {

			formFilds = [ "fromYear_custom_report", "toYear_custom_report",
					"giinForYearlyReport" ];
			formFiledsLabel = [ "From Date", "To Date", "GIIN" ];
			len = formFilds.length;
			//giin made non mandatory by alok on 23 11 2016 
			for ( var i = 0; i < len-1; i++) {
				if (doc.getElementById(formFilds[i]).value == null
						|| doc.getElementById(formFilds[i]).value == undefined
						|| doc.getElementById(formFilds[i]).value == ""
						|| doc.getElementById(formFilds[i]).value == "undefined"
						|| doc.getElementById(formFilds[i]).value == "Blank") {

					doc.getElementById("errormsg_yearlyReport").innerHTML = "El "
							+ formFiledsLabel[i] + " no puede estar en blanco";
					doc.getElementById("errormsg_yearlyReport").className = " errorMsg";
					doc.getElementById("errormsg_giinReport").innerHTML = "&nbsp;";
					return false;
				}
			}

			document.customReportForm_yearly.submit();
			doc.getElementById("errormsg_yearlyReport").innerHTML = "&nbsp;";
			doc.getElementById("errormsg_giinReport").innerHTML = "&nbsp;";
			return true;

		}

	} catch (e) {
		alert("Excepci�n --> " + e);
	}
}

// *********************************************************************************
// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
// Group : Application Projects1 (US)
// Project : [CRS Fatca Barbados]
// Date Written : 28/07/2016
// Date Modified :
// Author : Khushdil Kaushik
// Description : to reset the fields except reporting year on xml upload page
// intermediary
// ***********************************************************************************/

function resetFields(src) {
	// alert(src);
	var doc = document;
	var fieldIDAry = "";
	if (src == "Manual") {
		fieldIDAry = [ "Manual_Reporting_Year", "Manual_Submission_Type" ];
	} else {
		fieldIDAry = [ "submissionType", "new_update" ];

	}
	var len = "";
	try {
		len = fieldIDAry.length;
		for ( var i = 0; i < len; i++) {
			doc.getElementById(fieldIDAry[i]).value = "";
			doc.getElementById(fieldIDAry[i]).disabled = false;
			doc.getElementById(fieldIDAry[i]).className = "form-control";
		}
		doc.getElementById("UploadMsg").innerHTML = "&nbsp;";
		return true;
	} catch (e) {
		alert(e);
	}
}