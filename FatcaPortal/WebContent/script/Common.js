/*------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : Common Js function
 File Name                                                   : Common.js
 Author                                                      : Khushdil Kaushik
 Date (DD/MM/YYYY)                                           : 19/10/2015
 Description                                                 : common js function for validations
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    			Change Description
 10232				 18/11/2015		Khushdil Kaushik		added a new function - getMasterDropDown(),and change in sentOTP function
 10234				 20/11/2015		Khushdil Kaushik		added a new function -resetFIDetails(),and change in getGIINDetails function
 11420               27/04/2016     Khushdil Kaushik         alert message changed
 11430               27/04/2016     Khushdil Kaushik          Changed user status if locked set to locked instead of active or blocked 
 11418               27/04/2016     Khushdil Kaushik          Changed message for incorrect pin
 11438               27/04/2016     Khushdil Kaushik         alert removed
 11482               17/05/2016     Khushdil Kaushik         alert removed
 11486               23/05/2016     Khushdil Kaushik         tab index added and removed based on condition
 11592               27/06/2016     Khushdil Kaushik        remained field added for mandatory alert
 11603               27/06/2016     Khushdil Kaushik        message corrected
 ------------------------------------------------------------------------------------------------------*/

var appPath = getContextPathWithDomain();
var fiUserInformation;// Global variables
var validFileFormat = "PDF";
var SEARCHFOR = undefined;
var SEARCHIN = undefined;
function ltrimString(str) {
	for ( var k = 0; k < str.length && isWhitespace(str.charAt(k)); k++)
		;
	return str.substring(k, str.length);
}
function rtrimString(str) {
	for ( var j = str.length - 1; j >= 0 && isWhitespace(str.charAt(j)); j--)
		;
	return str.substring(0, j + 1);
}
function trimString(str) {
	return ltrimString(rtrimString(str));
}
function isWhitespace(charToCheck) {
	var whitespaceChars = " \t\n\r\f";
	return (whitespaceChars.indexOf(charToCheck) != -1);
}

function checkFormat(Data) {
	/*bug id 11438*/
	// alert(Data.id);
	/*bug id 11438*/
	// var email = document.getElementById('txtEmail');
	// alert("inside test");
	var labelMsg = "";
	var value = Data.value;
	var filter = "";
	var errorMsgId = "";
	var errorMsg = "";
	var doc = document;
	// alert(Data);
	// alert(Data.id);
	// Resolved By Arbind on 11/10/2015 For BUG ID 10228,10229,102300;
	var isflag = undefined;

	// alert(isflag);
	if (Data.id == "emailForPIN" || Data.id == "fi_email"
			|| Data.id == "emailForPIN_ForgotPassword"
			|| Data.id == "user_email") {
		filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		labelMsg = "Identificaci�n de correo electr�nico";
		if (Data.id == "emailForPIN_ForgotPassword") {
			errorMsgId = "errorMsgSendPin_ForgotPassword";
			isflag = "isConformEmailTrueforgotPass";
		} else if (Data.id == "fi_email") {
			errorMsgId = "fi_errormsg";
		} else {
			errorMsgId = "errorMsgSendPin";
			isflag = "isConformEmailTrue";
		}

	}
	if (Data.id == "user_phone" || Data.id == "fi_Phone") {
		// Format Support
		// 123-345-3456 and (078)789-8908
		// XXX-XXX-XXXX
		// XXX.XXX.XXXX
		// XXX XXX XXXX
		/*
		 * (123) 456-7890 123-456-7890 123.456.7890 1234567890 +31636363634
		 * 075-63546725
		 */
		filter = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
		//filter = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
		labelMsg = "N�mero de tel�fono";
		if (Data.id == "fi_Phone") {
			errorMsgId = "fi_errormsg";
		} else {
			errorMsgId = "user_errormsg";
		}

	}

	doc.getElementById(errorMsgId).innerHTML = "&nbsp;";
	if (!filter.test(value)) {
		doc.getElementById(errorMsgId).innerHTML = "Proporcione una "
				+ labelMsg+" v�lido";
		doc.getElementById(errorMsgId).className = "errorMsg";

		Data.focus();
		// Resolved By Arbind on 11/10/2015 For BUG ID 10228,10229,102300;
		if (isflag != undefined) {
			doc.getElementById(isflag).value = "false";
		}
		return false;
	} else {
		// Resolved By Arbind on 11/10/2015 For BUG ID 10228,10229,102300;
		if (isflag != undefined) {
			doc.getElementById(isflag).value = "true";
		}

		/* added by KD 20012016 */
		if (Data.id == "emailForPIN") {
			checkOTPExist('registrationOTP');
		}
		/* added by KD 20012016 */
		return true;
	}
}
function sendOTP(source, event) {

	var doc = document;
	var url = undefined;
	var param = undefined;
	var response = undefined;
	var email = "";
	var otp = "";
	var servletmapping = undefined;
	var errorID = undefined;
	var confirmMailID = undefined;
	var emailFieldId = undefined;
	var pinFieldId = undefined;
	try {
		if (source == "registrationOTP") {

			if (doc.getElementById("isConformEmailTrue").value != "true") {
				doc.getElementById("errorMsgSendPin").innerHTML = "Por favor, ingrese la ID de correo electr�nico.";
				doc.getElementById("errorMsgSendPin").className = " errorMsg";
				return false;
			}
			errorID = "errorMsgSendPin";
			confirmMailID = "isConformEmailTrue";
			emailFieldId = "emailForPIN";
			pinFieldId = "pin";
			if (event == "generateOTP") {

				if (!checkCaptcha('', '', 'Registration'))
					return false;
				email = doc.getElementById("emailForPIN").value;
				otp = "";

			} else if (event == "authenticateOTP") {
				email = doc.getElementById("emailForPIN").value;
				/*
				 * checkING if compliance type selected or not (function written
				 * in GlobalVariable.js)
				 */
				if (!isComplianceSelected()) {
					doc.getElementById(errorID).innerHTML = SelectCompliance;
					doc.getElementById(errorID).className = " errorMsg";
					return false;
				}
				if (doc.getElementById("crsCompliance").value == "1"
						&& doc.getElementById("fatcaCompliance").value == "0") {
					doc.getElementById("inDiv").style.display = "block";
					doc.getElementById("giinDiv").style.display = "none";

				}

				otp = doc.getElementById("pin").value;
				if (otp == "" || otp == null || otp == undefined) {
					doc.getElementById(errorID).innerHTML = EnterPin;
					doc.getElementById(errorID).className = " errorMsg";
					doc.getElementById(pinFieldId).readOnly = false;

					return false;
				}

			}
			// alert('registrationOTP222');
			servletmapping = "Registration";

			// to check captcha
		}
		if (source == "forgotpasswordOTP") {

			servletmapping = "ForgotPassword";
			errorID = "errorMsgSendPin_ForgotPassword";
			confirmMailID = "isConformEmailTrueforgotPass";
			emailFieldId = "emailForPIN_ForgotPassword";
			pinFieldId = "pin_ForgotPassword";
			if (doc.getElementById("isConformEmailTrueforgotPass").value != "true") {

				doc.getElementById(errorID).innerHTML = "Por favor, ingrese la ID de correo electr�nico.";
				doc.getElementById(errorID).className = " errorMsg";
				return false;
			}

			if (event == "generateOTP") {
				email = doc.getElementById("emailForPIN_ForgotPassword").value;
				otp = "";

			} else if (event == "authenticateOTP") {

				email = doc.getElementById("emailForPIN_ForgotPassword").value;
				otp = doc.getElementById("pin_ForgotPassword").value;
				if (otp == "" || otp == null || otp == undefined) {
					doc.getElementById(errorID).innerHTML = "Por favor, escriba pin.";
					doc.getElementById(errorID).className = " errorMsg";
					return false;
				}
			}

		}

		url = appPath + "/" + servletmapping;

		param = "email=" + email + "&otp=" + otp + "&source=" + source
				+ "&event=" + event;
		response = ajaxCall(url, param);

		if (response != "" || response != null || response != undefined) {
			if (source == "registrationOTP" || source == "forgotpasswordOTP") {
				if (response == "fail" && event == "generateOTP") {
					// doc.getElementById(emailFieldId).value = "";
					// doc.getElementById(confirmMailID).value = "false";
					doc.getElementById(pinFieldId).readOnly = true;
					doc.getElementById(errorID).innerHTML = (source == "registrationOTP" ? "Este ID de correo electr�nico ya se ha registrado en el sistema."
							: "No est� registrado en el sistema.");
					doc.getElementById(errorID).className = " errorMsg";

					if (source == "registrationOTP") {
						doc.getElementById("refreshCaptch2").click();
					}
					return false;
				} else if (response == "success" && event == "generateOTP") {
					doc.getElementById(errorID).innerHTML = "El pin se ha enviado a tu ID de correo electr�nico.";
					doc.getElementById(errorID).className = "successMsg";
					doc.getElementById(pinFieldId).readOnly = false;
					return true;
				}

				if (response == "fail" && event == "authenticateOTP") {

					// doc.getElementById(errorID).innerHTML = "&nbsp;";
					doc.getElementById(pinFieldId).value = "";
					/* bug id 11418 starts */
					doc.getElementById(errorID).innerHTML = "El pin enterado es incorrecto o expirado. Regenerar o introducir el pin correcto.";
					/* bug id 11418 end */
					doc.getElementById(errorID).className = " errorMsg";
					return false;
				} else if (response == "success" && event == "authenticateOTP") {

					if (source == "registrationOTP") {
						doc.getElementById("user_email").value = doc
								.getElementById("emailForPIN").value;
						$("#reg-tab1").hide();
						$("#reg-tab2").show("show");
						$('#reg-tab2').click(function() {
							return true;
						}).click();
						setmaximumLength("userRegistration");
						/* Change for ALL -By Kd on 20072016 starts */
						getMasterDropDown("UserRegistration","Country");
						/*Change for ALL - By Kd on 20072016 end*/

					} else {

						// Forgot password
						doc.getElementById("form_ForgotPassword").style.display = "block";
						doc.getElementById(errorID).innerHTML = "&nbsp;";
					}
				}
			}
		} else {

			doc.getElementById(errorID).innerHTML = "Hay alg�n problema en el sistema. Intente despu�s de alg�n tiempo";
			doc.getElementById(errorID).className = " errorMsg";
		}
	} catch (e) {

		alert(e.message);
	}

}

function ajaxCall(url, param) {
	var responses = "";
	try {

		var xhttp = getACTObj();
		/*
		 * if (window.XMLHttpRequest) { // code for modern browsers xhttp = new
		 * XMLHttpRequest(); } else { // code for IE6, IE5 xhttp = new
		 * ActiveXObject("Microsoft.XMLHTTP"); }
		 */
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				responses = xhttp.responseText;
				// alert(responses);

			}
		};
		xhttp.open("POST", url, false);
		xhttp.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		xhttp.send(param);
		// alert("done "+responses);
		return responses;
	} catch (ex) {
		alert("Hay alg�n problema por favor intente despu�s de alg�n tiempo o p�ngase en contacto con el administrador del sistema.");
		return responses;

	}

}
function getContextPathWithDomain() {
	var ctx = window.location.pathname.substring(0, window.location.pathname
			.indexOf("/", 2));
	var URL = window.location.protocol + "//" + window.location.host + "" + ctx;
	return URL;
}
function loadEvent() {

	$("#reg-tab2").hide();
	
}
function checkMandatory() {
	// alert("check")
	var doc = document;

	var isValidate = true;
	var selectID = "";
	var label = "";
	/*bug id 11592*/
	if (doc.getElementById("fatcaCompliance").value == "1") {
		selectID = [ "giin", "fi_name", "fi_address", "fi_country", "fi_email",
				"user_email", "user_name", "user_address", "user_country",
				"user_phone", "user_empid", "user_designation", "user_gender",
				"userDOB", "document1", "file1", "document2", "file2",
				"fi_password_Reg", "fi_confirm_password2_Reg" ];
		label = [ "GIIN", "Name", "Address", "Country", "Email ID",
				"Email ID", "Name", "Address", "Nationality", "Phone", "TIN",
				"Position", "Gender", "Date of Birth", "Document 1", "File 1",
				"Document 2", "File 2", "Password", "Confirm Password" ];
	} else {
		selectID = [ "giin1", "fi_name", "fi_address", "fi_country",
				"fi_email", "user_email", "user_name", "user_address",
				"user_country", "user_phone", "user_empid", "user_designation",
				"user_gender", "userDOB", "document1", "file1", "document2",
				"file2", "fi_password_Reg", "fi_confirm_password2_Reg" ];
		label = [ "GIIN", "Name", "Address", "Country", "Email ID",
				"Email ID", "Name", "Address", "Nationality", "Phone", "TIN",
				"Position", "Gender", "Date of Birth", "Document 1", "File 1",
				"Document 2", "File 2", "Password", "Confirm Password" ];
	}
	/*bug id 11592*/
	// alert(selectID);
	var len = selectID.length;
	// alert("inside");
	try {
		for ( var i = 0; i < len; i++) {
			// alert("id:"+selectID[i]);
			// alert("value:"+doc.getElementById(selectID[i]).value);
			// alert(selectID[i]);
			if (doc.getElementById(selectID[i]).value == null
					|| doc.getElementById(selectID[i]).value == undefined
					|| doc.getElementById(selectID[i]).value == ""
					|| doc.getElementById(selectID[i]).value == "undefined") {
				// alert("The " + label[i] + " cannot be blank");
				if (selectID[i].indexOf("user_") != -1) {
					doc.getElementById("user_errormsg").innerHTML = "El "
							+ label[i] + " no puede estar en blanco";
					doc.getElementById("user_errormsg").className = " errorMsg";
					doc.getElementById("fi_errormsg").innerHTML = "&nbsp;";
				} else if (selectID[i] == "document1" || selectID[i] == "file1"
						|| selectID[i] == "document2" || selectID[i] == "file2") {

					doc.getElementById("doc_errormsg").innerHTML = "El "
							+ label[i] + " no puede estar en blanco";
					doc.getElementById("doc_errormsg").className = " errorMsg";
					doc.getElementById("user_errormsg").innerHTML = "&nbsp;";
				} else if (selectID[i] == "fi_password_Reg"
						|| selectID[i] == "fi_confirm_password2_Reg") {

					doc.getElementById("errorPassword").innerHTML = "El "
							+ label[i] + " no puede estar en blanco";
					doc.getElementById("errorPassword").className = " errorMsg";
					doc.getElementById("doc_errormsg").innerHTML = "&nbsp;";

				} else {
					doc.getElementById("fi_errormsg").innerHTML = "The "
							+ label[i] + " no puede estar en blanco";
					doc.getElementById("fi_errormsg").className = " errorMsg";
				}
				isValidate = false;
				if (selectID[i] != "giin")
					doc.getElementById(selectID[i]).focus();

				break;

			}

		}

		return isValidate;

	} catch (ex) {

		alert("cheque obligatorio--> " + ex);
	}

}

function validate() {
	// alert("check man")
	try {

		if (!checkMandatory()) {

			return false;
		}
		return true;
	} catch (ex) {

		alert("validar--> " + ex);
	}

}

function isEmpty(obj) {
	if (obj) {
		if (obj.value) {
			if (typeof obj.value != "undefined") {
				if (obj.value != null) {
					try {
						if (obj.value.fulltrim() != "") {
							return false;
						}
					} catch (ex) {
						if (trim(obj.value) != "") {
							return false;
						}
					}
				}
			}
		}
	}
	return true;
}
// ---------------------------------------------------------------
if (typeof String.prototype.endsWith != 'function') {
	String.prototype.endsWith = function(str) {
		return this.slice(-str.length) == str;
	};
}
if (typeof String.prototype.startsWith != 'function') {
	String.prototype.startsWith = function(str) {
		return this.slice(0, str.length) == str;
	};
}
if (typeof String.prototype.contains != 'function') {
	String.prototype.contains = function(str) {
		return this.indexOf(str) != -1;
	};
}

if (typeof String.prototype.equals != 'function') {
	String.prototype.equals = function(str) {
		return this == str;
	};
}

if (typeof String.prototype.equalsIgnoreCase != 'function') {
	String.prototype.equalsIgnoreCase = function(str) {
		return this.toLowerCase() == str.toLowerCase();
	};
}

if (typeof String.prototype.ltrim != 'function') {
	String.prototype.ltrim = function() {
		return this.replace(/^\s+/, '');
	};
}

if (typeof String.prototype.rtrim != 'function') {
	String.prototype.rtrim = function() {
		return this.replace(/\s+$/, '');
	};
}

if (typeof String.prototype.trim != 'function') {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, '');
	};
}

if (typeof String.prototype.fulltrim != 'function') {
	String.prototype.fulltrim = function() {
		return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, '').replace(/\s+/g,
				' ');
	};
}

if (typeof String.prototype.padLeft != 'function') {
	Number.prototype.padLeft = function(base, chr) {
		var len = (String(base || 10).length - String(this).length) + 1;
		return len > 0 ? new Array(len).join(chr || '0') + this : this;
	};
}

if (typeof Date.prototype.mmddyyyy != 'function') {
	Date.prototype.mmddyyyy = function() {
		var yyyy = this.getFullYear().padLeft().toString();
		var mm = (this.getMonth() + 1).padLeft().toString(); // getMonth() is
		// zero-based
		var dd = this.getDate().padLeft().toString();
		return mm + '/' + dd + '/' + yyyy;
	};
}
function getGIINDetails() {
	//alert();

	var doc = document;
	var giin = "";
	// alert("crs" + doc.getElementById("crsCompliance").value);
	// alert("fatca" + doc.getElementById("fatcaCompliance").value);
	if (doc.getElementById("fatcaCompliance").value == "1") {
		giin = doc.getElementById("giin").value;
	} else {
		giin = doc.getElementById("giin1").value;
	}
	// alert(giin);
	/*
	 * var giinFieldID = [ "text1", "text2", "text3", "text4", "text5", "text6",
	 * "text7", "text8", "text9", "text10", "text11", "text12", "text13",
	 * "text14", "text15", "text16" ];
	 */
	var fieldID = [ "fi_name", "fi_address", "fi_Phone", "fi_email" ];
	var JSONObjColumnname = [ "name", "address", "phone", "emailID" ];

	var url = "";
	var param = "";
	var response = "";
	doc.getElementById("fi_errormsg").innerHTML = "&nbsp;";
	if (giin != null && giin != undefined && giin != "") {
		url = appPath + "/AccountCreation";
		param = "option=searchgiin" + "&giin=" + giin;
		try {

			response = ajaxCall(url, param);
			// alert(response);
			if (response != undefined && response != null
					&& response.toUpperCase() != "NULL" && response != "") {

				var obj = JSON.parse(response);
				var len = fieldID.length;

				/* BUG 10234 */
				if (obj["name"] != "GIINNotExist") {
					for ( var i = 0; i < len; i++) {

						if (obj[JSONObjColumnname[i]] != ""
								&& obj[JSONObjColumnname[i]] != "GIINNotExist") {
/*bug id 11486 */
							if (i == 3) {
								doc.getElementById(fieldID[i]).setAttribute(
										"tabindex", "-1");
								doc.getElementById(fieldID[i]).value = obj[JSONObjColumnname[i]];
								doc.getElementById(fieldID[i]).readOnly = true;
								doc.getElementById(fieldID[i]).style.background = "#ffffb3 url('../images/mail2.png') no-repeat center right;padding:0px 2px 2px 5px; height:25px; width:200px; padding:0px 2px 2px 5px;";
							} else {
								doc.getElementById(fieldID[i]).setAttribute(
										"tabindex", "-1");
								doc.getElementById(fieldID[i]).value = obj[JSONObjColumnname[i]];
								doc.getElementById(fieldID[i]).readOnly = true;
								doc.getElementById(fieldID[i]).className += " "
										+ "readOnlyClass";
							}
							/*bug id 11486 */

						} else {
							doc.getElementById(fieldID[i]).removeAttribute(
									"tabindex");
							doc.getElementById(fieldID[i]).readOnly = false;
							doc.getElementById(fieldID[i]).className = "form-control";
							if (i == 3) {
								doc.getElementById(fieldID[i]).removeAttribute(
										"tabindex");
								doc.getElementById(fieldID[i]).readOnly = false;
								doc.getElementById(fieldID[i]).style.background = "white url('../images/mail2.png') no-repeat center right;padding:0px 2px 2px 5px; height:25px; width:200px; padding:0px 2px 2px 5px;";
							}
						}

					}

					if (doc.getElementById("fatcaCompliance").value == "1") {
						// make readonly all giin fields
						makeGIINFieldsReadOnly("registration");
					} else {

						doc.getElementById("giin1").readOnly = true;
						doc.getElementById("giin1").className += " "
								+ "readOnlyClass";

					}

				} else {
					// alert(doc.getElementById("fatcaCompliance").value);
					if (doc.getElementById("fatcaCompliance").value == "1") {
						// blank all giin fields
						blankGIINFields("registration");
						doc.getElementById("giin").value = "";
					} else {
						// giin1 is for in in case of crs
						doc.getElementById("giin1").value = "";
					}

					doc.getElementById("fi_errormsg").innerHTML = NoRecordFound;
					doc.getElementById("fi_errormsg").className = " errorMsg";
				}

				/* BUG 10234 */
			} else {
				// alert();
				if (doc.getElementById("fatcaCompliance").value == "1") {
					// blank all giin fields
					blankGIINFields("registration");
					doc.getElementById("giin").value = "";
				} else {
					// giin1 is for in in case of crs
					doc.getElementById("giin1").value = "";
				}

				makeFIreadonly("nonEdiatable");
				doc.getElementById("fi_errormsg").innerHTML = NoRecordFound;
				doc.getElementById("fi_errormsg").className = " errorMsg";
			}
			// alert("name:"+obj["name"]);
		} catch (ex) {
			alert(ex);
		}
	} else {

		doc.getElementById("fi_errormsg").innerHTML = GIINCanNotBlank;
		doc.getElementById("fi_errormsg").className = " errorMsg";
	}

}

function disableBackButton() {

	window.history.forward();
	function noBack() {
		window.history.forward();
	}
}

/*
 * function getUserApprovalList(event) {
 * 
 * var response; var doc = document; var obj; var len; var tableObj =
 * doc.getElementById("userApprovalTable"); var url = appPath + "/UserApproval";
 * var param = "option=" + event;
 * 
 * try { if (tableObj.rows.length > 1) deleteTables("userApprovalTable"); if
 * (event == "getPageCount") { if (ajaxCall(url, param) == "success") { //
 * response=getUserApprovalList('current'); param = "option=current"; response =
 * ajaxCall(url, param); if (response != undefined && response.toUpperCase() !=
 * "NULL" && response != "" && tableObj != undefined) { fiUserInformation =
 * JSON.parse(response); addRefferalRows(tableObj); } } }
 * 
 * else if (event == "next" || event == "prev") { param = "option=" + event;
 * response = ajaxCall(url, param); if (response != undefined &&
 * response.toUpperCase() != "NULL" && response != "" && tableObj != undefined) {
 * fiUserInformation = JSON.parse(response); addRefferalRows(tableObj); } } }
 * catch (e) { alert("Error:" + e.message); } }
 */

function getUserApprovalList(event, searchFor, searchIn) {

	var response;
	var doc = document;
	var obj;
	var len;
	var tableObj = doc.getElementById("userApprovalTable");
	var url = appPath + "/UserApproval";
	var param = "option=" + event + "&searchFor=" + searchFor + "&searchIn="
			+ searchIn;
	// alert("searchFor in common.js--> " + searchFor);
	// alert("SEARCHFOR in common.js--> " + SEARCHFOR);
	SEARCHFOR = searchFor;
	SEARCHIN = searchIn;
	// alert("SEARCHFOR after updatein common.js--> " + SEARCHFOR);
	try {
		if (tableObj.rows.length > 1)
			deleteTables("userApprovalTable");
		if (event == "getPageCount") {
			if (ajaxCall(url, param) == "success") {
				// response=getUserApprovalList('current');
				param = "option=current&searchFor=" + searchFor + "&searchIn="
						+ searchIn;
				response = ajaxCall(url, param);
				// alert("response--> " + response);
				if (response != undefined && response.toUpperCase() != "NULL"
						&& response != "" && tableObj != undefined) {
					fiUserInformation = JSON.parse(response);
					addRefferalRows(tableObj);
				}
			}
		}

		else if (event == "next" || event == "prev") {
			param = "option=" + event + "&searchFor=" + searchFor
					+ "&searchIn=" + searchIn;
			response = ajaxCall(url, param);
			if (response != undefined && response.toUpperCase() != "NULL"
					&& response != "" && tableObj != undefined) {
				fiUserInformation = JSON.parse(response);
				addRefferalRows(tableObj);

			}

		}

	} catch (e) {
		alert("Error:" + e.message);
	}

}

function addRefferalRows(tableId) {

	var objects = fiUserInformation;
	var column = [ "giin", "userName", "userEmailId", "userEmployeeID",
			"userRegisteredOn", "userModifiedOn", "operation", "flag", "userID" ];// These
	// are
	// the
	// json
	// key
	// name
	var row = "";
	var cell = "";
	var flag = "00";

	try {

		if (objects != undefined) {
			var len = objects.length;
			for (a in objects) {

				/* if (a == 0) { */
				// a=0;
				if (a < len - 1) {
					row = tableId.insertRow();
					row.id = "userInformation_row_" + a;
					for ( var i = 0; i < 9; i++) {
						// alert("cols:"+column[i]+"<value>"+objects[a][column[i]]);

						var value = ((objects[a][column[i]] == undefined
								|| objects[a][column[i]].toUpperCase() == "NULL" || objects[a][column[i]] == "") ? "&nbsp;"
								: objects[a][column[i]]);
						cell = row.insertCell(i);
						cell.className = "ipm_n2";

						if (i == 8)// Insert radio button at the end of the
						// column
						// if there is any structure change please
						// change it accordingly
						{
							cell.style.width = "50";
							cell.innerHTML = "<input type='radio' name='status' id='"
									+ value
									+ "' onclick='getUserInforamtion(this)'/>";
						} else {
							/*
							 * value = (column[i] == "flag" && value == 1 ?
							 * "Active" : column[i] == "flag" && value == 0 ?
							 * "InActive" : value); cell.innerHTML = value;
							 */
							/* bug id 11430 starts */
							value = (column[i] == "flag"
									&& objects[a]["isUserLocked"] == "Y" ? "Locked"
									: column[i] == "flag" && value == 1 ? "Active"
											: column[i] == "flag" && value == 0 ? "Inactive"
													: value);
							/* bug id 11430 end */
							cell.innerHTML = value;
						}
					}
				} else
					flag = objects[len - 1];

			}
		}
		if (flag != "00") {
			row = tableId.insertRow();
			cell = row.insertCell(0);
			cell.colSpan = "7";
			if (flag == "01") {
				cell.innerHTML = " prev | <a id='nextLinkAdmin' style='color:blue' href=\"javascript: void();\"  onclick=\"getUserApprovalList('next','"
						+ SEARCHFOR + "','" + SEARCHIN + "');\">next</a>";
			} else if (flag == "10")
				cell.innerHTML = " <a id='prevLinkAdmin' style='color:blue'  href=\"javascript: void();\" onclick=\"getUserApprovalList('prev','"
						+ SEARCHFOR + "','" + SEARCHIN + "');\">prev</a>| next";

			else if (flag == "11")
				cell.innerHTML = " <a id='prevLinkAdmin' style='color:blue'  href=\"javascript: void();\" onclick=\"getUserApprovalList('prev','"
						+ SEARCHFOR
						+ "','"
						+ SEARCHIN
						+ "');\">prev</a>| <a id='nextLinkAdmin' style='color:blue' href=\"javascript: void();\"  onclick=\"getUserApprovalList('next','"
						+ SEARCHFOR + "','" + SEARCHIN + "');\">next</a>";
			// cell.innerHTML = "<a id='prevLinkAdmin' href=\"javascript:
			// void();\" onclick=\"getUserApprovalList('prev');\">prev</a>|<a
			// id='nextLinkAdmin' href=\"javascript: void();\"
			// onclick=\"getUserApprovalList('next');\">next</a>";
		}

	} catch (ex) {

		alert(ex);
	}
}
function getUserInforamtion(obj) {

	var userInformationkey = obj.id;
	var doc = document;
	var fieldIDArray;
	var complianceType = fiUserInformation[userInformationkey]["complianceType"];
	/*
	 * alert("compilanceType--> " +
	 * fiUserInformation[userInformationkey]["complianceType"]);
	 */
	if (complianceType.toUpperCase() == "CRS") {
		doc.getElementById("user_ComplianceType").value = "CRS";
		doc.getElementById("adminIn_div").style.display = "block";
		doc.getElementById("adminGiin_div").style.display = "none";
		fieldIDArray = [ "fiIN_adminApproval", "fiName_adminApproval",
				"fiAddress_adminApproval", "fiCountry_adminApproval",
				"fiEmailID_adminApproval", "userEmailID_adminApproval",
				"userName_adminApproval", "userAddress_adminApproval",
				"userCountry_adminApproval", "userPhone_adminApproval",
				"userEmployee_adminApproval", "userDesignation_adminApproval",
				"Document1_adminApproval", "Document2_adminApproval",
				"docs1_adminApproval", "docs2_adminApproval",
				"userType_toUpdate", "user_LockedStatus", "fi_inType",
				"userDOB_adminApproval", "userGender_adminApproval" ];
	} else {
		doc.getElementById("user_ComplianceType").value = "FATCA";// may be
		// fatca or
		// both
		doc.getElementById("adminIn_div").style.display = "none";
		doc.getElementById("adminGiin_div").style.display = "block";
		fieldIDArray = [ "GIIN_adminApproval", "fiName_adminApproval",
				"fiAddress_adminApproval", "fiCountry_adminApproval",
				"fiEmailID_adminApproval", "userEmailID_adminApproval",
				"userName_adminApproval", "userAddress_adminApproval",
				"userCountry_adminApproval", "userPhone_adminApproval",
				"userEmployee_adminApproval", "userDesignation_adminApproval",
				"Document1_adminApproval", "Document2_adminApproval",
				"docs1_adminApproval", "docs2_adminApproval",
				"userType_toUpdate", "user_LockedStatus", "GIIN_InType",
				"userDOB_adminApproval", "userGender_adminApproval" ];

	}
	var jsonKeyArray = [ "giin", "fiName", "fiAddress", "fiCountry", "fiEmail",
			"userEmailId", "userName", "userAddress", "userCountry",
			"userPhone", "userEmployeeID", "userDesignation", "userDocType1",
			"userDocType2", "userDocType1File1", "userDocType2File2",
			"operation", "isUserLocked", "inType", "userDOB", "userGender" ];
	var len = fieldIDArray.length;
	var value = "";
	//
	doc.getElementById("user_currentStatus").value = fiUserInformation[userInformationkey]["flag"] == 1 ? "Active"
			: "Blocked";

	try {
		if (userInformationkey != undefined
				&& userInformationkey.toUpperCase() != "NULL"
				&& userInformationkey != "") {
			for ( var i = 0; i < len; i++) {

				value = (fiUserInformation[userInformationkey][jsonKeyArray[i]] == undefined
						|| fiUserInformation[userInformationkey][jsonKeyArray[i]]
								.toUpperCase() == "NULL" ? ""
						: fiUserInformation[userInformationkey][jsonKeyArray[i]]);
				// For Drop Down Field
				/*
				 * if (fieldIDArray[i] == "fiCountry_adminApproval" ||
				 * fieldIDArray[i] == "userCountry_adminApproval" ||
				 * fieldIDArray[i] == "Document1_adminApproval" ||
				 * fieldIDArray[i] == "Document2_adminApproval") {
				 * 
				 * selectDropDown(fieldIDArray[i], value);// param //
				 * fieldName,value } else
				 */
				if (fieldIDArray[i] == "GIIN_adminApproval") {
					setGIIN(value, "GIIN_adminApproval");
				} else if (fieldIDArray[i] == "docs1_adminApproval"
						|| fieldIDArray[i] == "docs2_adminApproval") {

					// alert(fiUserInformation[userInformationkey]["docType_Location"]);
					// alert("l:"+appPath+"//Download?path="+fiUserInformation[userInformationkey]["docType_Location"]+"/"+value);
					doc.getElementById(fieldIDArray[i]).href = appPath
							+ "/Download?path="
							+ fiUserInformation[userInformationkey]["uploadFileLocation"]
							+ "/" + value;
				}
/*Change for all by KD on 20072016 'added userCountry_adminApproval,fiCountry_adminApproval in else if check' starts*/
				else if (fieldIDArray[i] == "Document2_adminApproval"
						|| fieldIDArray[i] == "fi_inType" || fieldIDArray[i] == "userCountry_adminApproval" || fieldIDArray[i] == "fiCountry_adminApproval") {
					// fieldIDArray[i], value
					// var doc = document;
					var selectObj = doc.getElementById(fieldIDArray[i]);
					var length = selectObj.options.length;
					var matchValue = (value == undefined || value == "null"
							|| value == "NULL" ? "" : value);

					for ( var j = 0; j < length; j++) {
						if (trimString(selectObj.options[j].value) == trimString(matchValue)) {
							selectObj.options[j].selected = true;
							break;
						} else {
							selectObj.options[0].selected = true;
						}
					}
				}
				
				/**/
			/*	else if (fieldIDArray[i] == "userCountry_adminApproval") {
				
				var selectObj = doc.getElementById(fieldIDArray[i]);
				var length = selectObj.options.length;
				var matchValue = (value == undefined || value == "null"
						|| value == "NULL" ? "" : value);

				for ( var j = 0; j < length; j++) {
					if (trimString(selectObj.options[j].value) == trimString(matchValue)) {
						selectObj.options[j].selected = true;
						break;
					} else {
						selectObj.options[0].selected = true;
					}
				}
			}*/

				else {
					doc.getElementById(fieldIDArray[i]).value = value;
				}
				if (doc.getElementById("user_LockedStatus").value.toUpperCase() == "Y") {
					doc.getElementById("user_locked").checked = true;
					doc.getElementById("user_adminLockStatus").value = "Y";
				} else {
					doc.getElementById("user_locked").checked = false;
					doc.getElementById("user_adminLockStatus").value = "N";
				}

			}

			isreadOnly("adminAprroval", complianceType);

		}
	} catch (ex) {
		alert(ex);
	}

}
function selectDropDown(fieldName, value) {

	var doc = document;
	var selectObj = doc.getElementById(fieldName);
	var len = selectObj.options.length;
	var matchValue = (value == undefined || value == "null" || value == "NULL" ? ""
			: value);

	for ( var i = 0; i < len; i++) {
		// alert(trimString(selectObj.options[i].value)
		// +"k-->"+trimString(value));
		if (trimString(selectObj.options[i].value) == trimString(matchValue)) {
			selectObj.options[i].selected = true;
			// return;
			break;
		} else {
			selectObj.options[0].selected = true;

		}
	}
	selectObj.style.disabled = true;
}
function isreadOnly(page, complianceType) {

	var fieldIDArray = "";
	var len = 0;
	var doc = document;
/*Change for All by KD on 20072016 'remove usercountry_adminapproval' - starts*/
	if (page == "adminAprroval") {
		if (complianceType.toUpperCase() == "CRS") {
			fieldIDArray = [ "fiIN_adminApproval", "fiCountry_adminApproval",
					"userEmailID_adminApproval" ];
		} else {
			fieldIDArray = [ "GIIN_adminApproval", "fiCountry_adminApproval",
					"userEmailID_adminApproval" ];
		}
		/*Change for All by KD on 20072016 'remove usercountry_adminapproval' - starts*/
	}
	len = fieldIDArray.length;
	for ( var i = 0; i < len; i++) {

		if (fieldIDArray[i] == "GIIN_adminApproval") {
			var j = 0;
			for (j = 1; j <= 16; j++) {

				doc.getElementById("admin_text_giin" + j).readOnly = true;
				doc.getElementById("admin_text_giin" + j).className += " "
						+ "readOnlyClass";
			}
		} else {

			doc.getElementById(fieldIDArray[i]).readOnly = true;
			doc.getElementById(fieldIDArray[i]).className += " "
					+ "readOnlyClass";
		}

	}

}

function getTransactionBatch(pageNo, totalCount, complianceType) {
	// alert("getTransactionBatch");
	// alert(complianceType);
	try {
		var doc = document;
		var fieldID = [ "pageNo", "XMLArrayList" ];
		// alert("PageNo : "+pageNo);
		// alert("totalCount : "+totalCount);
		var url = "";
		var param = "";
		var response = "";
		var reporting_year = doc.getElementById("reporting_year").value;
		var tableObj = doc.getElementById("userApprovalTable");
		url = appPath + "/TransactionBatch";
		param = "pageNo=" + pageNo + "&totalCount=" + totalCount
				+ "&complianceType=" + complianceType + "&reportingYear="
				+ reporting_year;
		// alert("url="+url);
		response = ajaxCall(url, param);
	
		if (response == "sessionExpired") {
			//alert("Session has been Expired");
			forwardJSP("SessionExpired");
		} else {
			var obj = JSON.parse(response);
			if (obj["Exception"] != null) {
				alert("Excepci�n : " + obj["Exception"]);
				forwardJSP("ExceptionType");
			} else {
				// alert("pageNo= "+obj["pageNo"]);
				// alert("XMLArrayList "+obj["ROWS"]);
				addTransactionBatchRows(response, totalCount, complianceType);

			}
		}

	} catch (ex) {
		alert(ex);
	}
}

function addTransactionBatchRows(response, totalCount, complianceType) {
var doc = document;
	var rowObjs = JSON.parse(response);

	var pageNo = rowObjs["pageNo"];
	var nextFlag = rowObjs["nextFlag"];
	var prevPage = parseInt(pageNo) - 1;
	var nextPage = parseInt(pageNo) + 1;
	var tableString = "<table width='750' border='1' cellspacing='0' cellpadding='0'>"
			+ "<tr class='bg1'>"
			+ "<td class='ipm_n'>Report Name</td>"
			+ "<td class='ipm_n'>Submitted by</td>"
			+ "<td class='ipm_n'>Type</td>"
			+ "<td class='ipm_n'>Stage</td>"
			+ "<td class='ipm_n'>Status (?)</td>"
			+ "<td class='ipm_n'>Date &amp; Time</td>" + "</tr>";
	// alert("tableString : "+tableString);

	for ( var row in rowObjs["ROWS"]) {
		// alert(row);
		// alert(rowObjs["ROWS"][row]["FileUpload_By"]);
		tableString = tableString
				+ "<tr>"
				+ "<td class='ipm_n2'>"
				+ (rowObjs["ROWS"][row]["d_Filename"] == undefined
						|| rowObjs["ROWS"][row]["d_Filename"] == "null" ? "&nbsp"
						: rowObjs["ROWS"][row]["d_Filename"])
				+ "</td>"
				+ "<td class='ipm_n2'>"
				+ (rowObjs["ROWS"][row]["FileUpload_By"] == undefined
						|| rowObjs["ROWS"][row]["FileUpload_By"] == "null" ? "&nbsp"
						: rowObjs["ROWS"][row]["FileUpload_By"])
				+ "</td>"
				+ "<td class='ipm_n2'>"
				+ (rowObjs["ROWS"][row]["File_Type"] == undefined
						|| rowObjs["ROWS"][row]["File_Type"] == "null" ? "&nbsp"
						: rowObjs["ROWS"][row]["File_Type"])
				+ "</td>"
				+ "<td class='ipm_n2'>"
				+ (rowObjs["ROWS"][row]["File_Stage"] == undefined
						|| rowObjs["ROWS"][row]["File_Stage"] == "null" ? "&nbsp"
						: rowObjs["ROWS"][row]["File_Stage"]) + "</td>";

		if (rowObjs["ROWS"][row]["File_Status"] != null
				&& rowObjs["ROWS"][row]["File_Status"].toUpperCase().indexOf(
						"FAIL") != -1) {
			tableString = tableString + "<td class='ipm_n2'><a href='"
					+ appPath + "/Download?path="
					+ rowObjs["ROWS"][row]["LogDownload_Path"]
					+ "' style='color:#015f7a;'>"
					+ "<span style='color:#015f7a;'>"
					+ rowObjs["ROWS"][row]["File_Status"] + "</span></a></td>";
		} else {

			tableString = tableString
					+ "<td class='ipm_n2'>"
					+ (rowObjs["ROWS"][row]["File_Status"] == undefined
							|| rowObjs["ROWS"][row]["File_Status"] == "null" ? "&nbsp"
							: rowObjs["ROWS"][row]["File_Status"]) + "</td>";
		}
		tableString = tableString
				+ "<td class='ipm_n2'>"
				+ (rowObjs["ROWS"][row]["FileUpload_Date"] == undefined
						|| rowObjs["ROWS"][row]["FileUpload_Date"] == "null" ? "&nbsp"
						: rowObjs["ROWS"][row]["FileUpload_Date"]) + "</td>";
	}
	tableString = tableString + "<tr>" + "<td colspan=5>&nbsp;</td>"
			+ "<td align='right'>";
	if (prevPage >= 0) {
		tableString = tableString
				+ "<a href=\"javascript:void();\" onclick=\"getTransactionBatch('"
				+ prevPage + "','" + totalCount + "','" + complianceType
				+ "')\">  Prev </a>";
	} else {
		tableString = tableString + "Prev | ";
	}

	if (nextPage != null && nextFlag == "TRUE") {
		tableString = tableString
				+ "<a href=\"javascript:void();\" onclick=\"getTransactionBatch('"
				+ nextPage + "','" + totalCount + "','" + complianceType
				+ "');\"> | Next</a>";
	} else {
		tableString = tableString + " | Next";
	}
	tableString = tableString + "</td>";
	+"</tr>" + "</table>";
	// alert("after loop");
	// alert("tableString"+tableString);
	doc.getElementById("transactionBatchTable").innerHTML = tableString;

}

function resetCaptcha(captchaOnPage) {

	if (captchaOnPage == "Registration") {
		document.getElementById('imgCaptcha2').src = appPath + '/Captchaimage?'
				+ Math.random();
		document.getElementById('txtInput2').value = "";

	} else {

		document.getElementById('imgCaptcha').src = 'Captchaimage?'
				+ Math.random();
		document.getElementById('txtInput').value = "";
	}
}

function strTrim(x) {
	return x.replace(/^\s+|\s+$/gm, '');
}

function checkCaptcha(url, sParams, checkCaptchaFrom) {
	var value = "";
	var doc = document;
	try {
		if (checkCaptchaFrom == "Login") {
			if (!encryptPassword("login")) {
				return false;
			}
		}

		if (checkCaptchaFrom == "Login") {
			value = doc.getElementById('txtInput').value;
			if (strTrim(value) == "") {
				doc.getElementById("loginErrorMsg").innerHTML = "Por favor, ingrese la Captcha";
				doc.getElementById("loginErrorMsg").className = "errorMsg";
				doc.getElementById('txtInput').value = "";
				return false;
			}
		} else {
			// alert('inside else');
			value = doc.getElementById('txtInput2').value;
			// alert(value);
			if (strTrim(value) == "") {
				// alert("when value is null");
				doc.getElementById("errorMsgSendPin").innerHTML = "Por favor, ingrese la Captcha";
				doc.getElementById("errorMsgSendPin").className = "errorMsg";
				doc.getElementById('txtInput2').value = "";
				return false;
			}
		}
		url = appPath + "/JSP/CheckCaptcha.jsp";
		param = "code=" + value;

		response = trimString(ajaxCall(url, param));

		if (response != null && response != undefined && response != '') {
			if (response == "FAIL") {

				if (checkCaptchaFrom == "Login") {
					doc.getElementById("loginErrorMsg").innerHTML = "Introducido captcha no coincidente";
					doc.getElementById("loginErrorMsg").className = "errorMsg";
					doc.getElementById("password").value = "";
					doc.getElementById("refreshCaptch").click();

					return false;
				} else {
					doc.getElementById("errorMsgSendPin").innerHTML = "Introducido captcha no coincidente";
					doc.getElementById("errorMsgSendPin").className = "errorMsg";
					doc.getElementById("refreshCaptch2").click();
					return false;
				}
			} else {
				if (checkCaptchaFrom == "Login") {
					var username = doc.getElementById("userName").value;
					/*bug id 11482*/
					//alert(username);
					/*bug id 11482*/
					url = appPath + "/LoginServlet";
					param = "code=b_userLoggedin&userName=" + username;
					var response = trimString(ajaxCall(url, param));
					if (response.toUpperCase() == "TRUE") {
						if (confirm("El usuario ya ha iniciado sesi�n. Desea continuar ?")) {
							logoutUser("user");
							loginsubmitform();
							return true;
						} else {
							window.location.href = appPath;
							return false;
						}
					} else {
						loginsubmitform();
						return true;
					}
				} else {
					return true;
				}
			}

			/*
			 * var url = "CheckCaptcha.jsp"; var sParams = "code=" + value;
			 */

			// alert("sParams--> " + sParams);
			/*
			 * var retval = "-1"; var req = getACTObj(); req.onreadystatechange =
			 * processRequest; req.open("POST", url, false);
			 * req.setRequestHeader('Content-Type',
			 * 'application/x-www-form-urlencoded'); req.send(sParams);
			 * //alert(req.status); function processRequest() { //
			 * alert(req.readyState); if (req.readyState == 4) {
			 * //alert(req.status); if (req.status == 200) {
			 * alert(strTrim(req.responseText)); } else { retval = '-1'; } } }
			 * function parseMessages() { retval = strTrim(req.responseText); }
			 * //alert(retval); return retval;
			 */
		}
	} catch (e) {
		alert(e);
	}
}

function getACTObj() {
	if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	}
	var a = [ "Microsoft.XMLHTTP", "MSXML2.XMLHTTP.6.0", "MSXML2.XMLHTTP.5.0",
			"MSXML2.XMLHTTP.4.0", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP" ];
	for ( var c = 0; c < a.length; c++) {
		try {
			return new ActiveXObject(a[c]);
		} catch (b) {
		}
	}
	return null;
}

function loginsubmitform() {
	var doc = document;
	doc.login.submit();
}
function isConfirmPassword(source) {
	var doc = document;

	// alert(doc.getElementById("fi_password").value);
	// alert(doc.getElementById("fi_confirm_password2").value);
	var password1 = "";
	var password2 = "";
	var errormsgId = "";
	var isConfirm = true;
	var hiddenISConfirm = "";
	errors = [];
	try {
		if (source == "forgotapssword") {
			password1 = "newpassword_forgotpassword";
			password2 = "confirmpassword_forgotpassword";
			errormsgId = "errorMsg_forgotPassword";
			hiddenISConfirm = "isConformPassword_ForgotPassword";
		} else if (source == "myEditInformation") {

			password1 = "fi_password";
			password2 = "fi_confirm_password2";
			errormsgId = "fi_password_errorMsg";
			hiddenISConfirm = "isConformPassword";
		} else if (source == "registrationPage") {

			// default
			password1 = "fi_password_Reg";
			password2 = "fi_confirm_password2_Reg";
			errormsgId = "errorPassword";
			hiddenISConfirm = "isConformPassword_Reg";
		}
		if (checkPasswordFormat(password1)) {
			if (doc.getElementById(password1).value != ""
					&& doc.getElementById(password2).value) {
				doc.getElementById(errormsgId).innerHTML = "&nbsp;";
				if (doc.getElementById(password1).value != doc
						.getElementById(password2).value) {
					doc.getElementById(errormsgId).innerHTML = "La contrase�a de confirmaci�n no coincide.";
					doc.getElementById(errormsgId).className = "errorMsg";
					isConfirm = false;
				}
				doc.getElementById(hiddenISConfirm).value = isConfirm;

			} else {

				doc.getElementById(errormsgId).innerHTML = "Por favor Ingrese la contrase�a nuevamente";
				doc.getElementById(errormsgId).className = "errorMsg";
			}
		} else {
			doc.getElementById(errormsgId).innerHTML = "Su contrase�a debe tener al menos 8 caracteres y debe contener 1 n�mero y 1 car�cter especial";
			doc.getElementById(errormsgId).className = "errorMsg";
		}

	} catch (ex) {

		alert(ex);
	}
	return isConfirm;
}
function validateKey(id, type) {

	// alert("id:"+id +"type:"+type);
	try {
		var keyCode = event.which | event.keyCode;
		var regularExpression = undefined;
		if (keyCode == 13)
			return true;
		var key = String.fromCharCode(keyCode);

		if (type == "aplha")
			regularExpression = "[a-zA-Z ��������]";
		else if (type == "password")
			regularExpression = "[a-z A-Z0-9.#$*@!��������]";
		// regularExpression = "/^(?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{7,}$/";
		else if (type == "numeric")
			regularExpression = "[0-9]";
		else if (type == "aplhanumeric")
			regularExpression = "[a-zA-Z0-9. ��������]";
		else if (type == "address")
			//regularExpression = "[a-zA-Z0-9 .,:;#@\'()|/-_]"; 
			/*
																 * bug id
																 * --10231
																 */
			//regularExpression = "[a-zA-Z0-9 .,:;#@\'()|/*-_]"; 
			/* Change for ALL -By Kd on 21072016 'changed regular expression for ( and - ' starts */
			regularExpression = "[a-zA-Z0-9 .,:;#@\'()-|/_��������]";
		/* Change for ALL -By Kd on 21072016 - end */
		else if (type == "float")
			regularExpression = "[0-9.]";
		else if (type == "money")
			regularExpression = /\d{1,2}$/;
		else
			regularExpression = "[a-zA-Z0-9 .,:;!#@?\"'+=$%{}()|/*-_��������]";
		if (key == '<' || key == '>' || key == '?' || key == '=')
			return false;
		
		if (id == "user_phone"  ) {
			if (key != key.match("[0-9.-]")) {
				return false;
			}
		} else {
			// "[a-zA-Z0-9 .,:;!#@&?\"'+=$%{}()|/*-_]"
			// alert(regularExpression);
			// alert(key)
			// alert(key.match(regularExpression));
			// alert(key +"match"+key.match(regularExpression));
			if (key != key.match(regularExpression))
				return false;
		}
		return true;
	} catch (e) {
		alert("excepci�n:" + e);
	}
}

function validateInput(obj, evt, act) {
	if (obj.className == "inputreadOnly")
		return false;

	switch (act) {
	case "alpha":
		re = /[^a-zA-Z��������]+/gi;
		break;
	case "numeric":
		re = /[^0-9]+/gi;
		break;
	case "numericdate":
		re = /[^0-9\/]+/gi;
		break;
	case "float":
		re = /[^0-9.-]+/gi;
		break;
	case "alphanumeric":
		re = /[^a-zA-Z0-9��������]+/gi;
		break;
	case "alphanumeric1":
		re = /[^0-9]+/gi;
		break;
	case "alphanumeric2":
		re = /[^a-z��������0-9.,+#-\/ ]+/gi;
		break;
	case "alphanumeric3":
		re = /[^a-z0-9-��������]+/gi;
		break;
	case "alphanumeric4":
		re = /[^a-z0-9-,��������]+/gi;
		break;
	case "alphanumeric5":
		re = /[^0-9-, ]+/gi;
		break;
	case "alphacomments":
		re = /[^a-zA-Z ��������]+/gi;
		break;
	case "Alpha-Numeric":
		re = /[^a-zA-Z0-9-��������]+/gi;
		break;
	case "address":
		re = /[^a-z0-9-.,#|\/ ��������]+/i;
		break;
	case "city":
		re = /[^a-z0-9-., ��������]+/i;
		break;
	case "Decimal":
		re = /[^0-9.]+/gi;
		break;
	case "alphabetic and special":
		re = "/[^a-zA-Z0-9. ,@*%;:()^\'\"+=_ /-��������]+/gi";
		break;
	case "Alpha-Numeric and -":
		re = /[^a-zA-Z0-9-��������]+/gi;
		break;
	case "time":
		re = /[^0-9:]/gi;
		break;
	case "EMAIL":
		re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+([,]{0,1})*$/;
	}
	try {
		var data = window.clipboarddData==null?evt.clipboardData.getData("Text"):window.clipboarddData.getData("Text");
		// data = data.replace(/[^a-zA-Z0-9]/gi, '');

		if (act == "ALL") {
			data = data.replace(/[>]+/gi, '');
			data = data.replace(/[<]+/gi, '');
			data = trimString(data);
			data = data.substr(0, obj.getAttribute("MaxLength")
					- (obj.value.length));
		} /*
			 * else if (act == "EMAIL") { data = validateEmail(data); } else if
			 * (obj.id == "PCT") { return false; }
			 */else {
			/*
			 * For Bug:
			 * PCTM_UT_409,PCTM_UT_336,PCTM_UT_266,PCTM_UT_194,PCTM_UT_119,PCTM_UT_045--Resolved
			 * By Sandeep Poonia
			 */
			data = data.replace(re, '');
		}
		if (data == null)
			return false;
		data = '' + data + '';
		if(window.clipboardData==null)
			evt.clipboardData.setData("Text", data);
		else
			window.clipboardData.setData("Text", data);
		/*
		 * var sinkFieldArray =
		 * ("CMP_FNAME,CMP_LNAME,CMP_SUMMARY,ADDRESSLINE1,ADDRESSLINE2,STATE,CITY,ZIPCODE").split(",");
		 * for (var i = 0; i < sinkFieldArray.length; i++) { var obj1 =
		 * document.getElementById("Letter_" + sinkFieldArray[i]); var obj2 =
		 * document.getElementById(sinkFieldArray[i]); if (obj1 && obj2) { if
		 * (obj.id.toUpperCase() == obj1.id.toUpperCase()) { obj2.value = data; }
		 * else if (obj.id.toUpperCase() == obj2.id.toUpperCase()) { obj1.value =
		 * data; } } }
		 */
		return true;
	} catch (ex) {
		alert("No se pueden pegar datos desde el portapapeles");
	}
	return false;
}

function selectUserListRow(tableNameId, radioGrpName) {

	var tableId;
	var chkflag = false;
	var doc = document;
	// alert("nametb:"+tableNameId+"radio"+radioGrpName);
	var index = -1;
	tableId = doc.getElementById(tableNameId);
	var radio = doc.getElementsByName(radioGrpName);
	try {
		if (tableId.rows.length > 1) {

			var i;
			for (i = 0; i < radio.length; i++) {
				if (radio[i].checked) {
					chkflag = true;
					index = i;
					break;
				}
			}
			if (chkflag) {
				return index;
			} else {
				alert("Seleccione al menos una opci�n");
				return index;
			}

		} else {
			alert("No hay filas para seleccionar");
			return index;
		}
		return index;
	} catch (e) {
		alert(e);
	}

}
function removeReadOnly(sourcePage) {
	var columns;
	var len = 0;
	var doc = document;
	if (sourcePage == "userAdminApproval")
		columns = [ "GIIN_adminApproval", "fiName_adminApproval",
				"fiEmailID_adminApproval", "fiCountry_adminApproval",
				"fiAddress_adminApproval", "userEmailID_adminApproval",
				"userName_adminApproval", "userEmployee_adminApproval",
				"userAddress_adminApproval", "userDesignation_adminApproval",
				"userCountry_adminApproval", "userPhone_adminApproval" ];

	if (columns != undefined) {
		len = columns.length;
	}

	try {
		if (len > 0) {
			var i;
			for (i = len - 1; i >= 0; i--) {
				// alert(i);
				// alert(columns[i]);
				doc.getElementById(columns[i]).readOnly = false;

			}
		}
	} catch (ex) {

		alert("Excepci�n:" + ex);
	}

}

function makeReadOnly(sourcePage) {
	// alert("readonly");
	var columns;
	var len = 0;
	var doc = document;
	if (sourcePage == "userAdminApproval")
		columns = [ "GIIN_adminApproval", "fiName_adminApproval",
				"fiEmailID_adminApproval", "fiCountry_adminApproval",
				"fiAddress_adminApproval", "userEmailID_adminApproval",
				"userName_adminApproval", "userEmployee_adminApproval",
				"userAddress_adminApproval", "userDesignation_adminApproval",
				"userCountry_adminApproval", "userPhone_adminApproval" ];
	else if (sourcePage.toUpperCase() == "FATCA"
			|| sourcePage.toUpperCase() == "BOTH")
		columns = [ "fi_giin", "fi_name", "fi_email", "fi_country",
				"fi_address", "user_email", "user_name", "user_empid",
				"user_designation", "user_phone", "user_country",
				"user_address", "userDOB", "user_gender" ];
	else if (sourcePage.toUpperCase() == "CRS")
		columns = [ "fi_in", "fi_inType", "fi_name", "fi_email", "fi_country",
				"fi_address", "user_email", "user_name", "user_empid",
				"user_designation", "user_phone", "user_country",
				"user_address", "userDOB", "user_gender" ];

	if (columns != undefined) {
		len = columns.length;
	}

	try {
		if (len > 0) {
			var i;
			for (i = len - 1; i >= 0; i--) {
				// alert(i);
				// alert(columns[i]);

				if (columns[i] == "fi_giin") {
					var j = 0;
					for (j = 1; j <= 16; j++) {

						doc.getElementById("edit_text_giin" + j).readOnly = true;
						doc.getElementById("edit_text_giin" + j).className += " "
								+ "readOnlyClass";
					}
				} else {

					doc.getElementById(columns[i]).readOnly = true;
					doc.getElementById(columns[i]).className += " "
							+ "readOnlyClass";
				}
			}
		}
	} catch (ex) {

		alert("Excepci�n:" + ex);
	}

}

function checkPasswordFromDB() {
	// alert("checkPasswordFromDB");
	var doc = document;
	var Password = encrptPassword1("Old_fi_password");
	// alert('Password--> ' + Password);
	var url = appPath + "/MyInfoEdit";
	var param = "event=validate" + "&oldPassWord=" + Password;
	// alert("url"+url);
	var response = trimString(ajaxCall(url, param));
	// alert(response);
	if (response != null && response != undefined && response != "") {
		if (response.indexOf("success") != -1) {

			doc.getElementById("errorAuthPassword").innerHTML = "Contrase�a correctamente autenticada";
			doc.getElementById("adminUserEdit").style.display = "";
			doc.getElementById("errorAuthPassword").className = "successMsg";
			doc.getElementById("fi_password").required = true;
			doc.getElementById("fi_confirm_password2").required = true;
			return true;
		} else if (response.indexOf("expire") != -1) {
			forwardJSP("SessionExpired");
		} else {
			doc.getElementById("errorAuthPassword").innerHTML = "Contrase�a incorrecta ingresada";
			doc.getElementById("errorAuthPassword").className = "errorMsg";
			doc.getElementById("adminUserEdit").style.display = "none";
			doc.getElementById("Old_fi_password").value = "";
			return false;
		}

	} else {
		doc.getElementById("errorAuthPassword").innerHTML = "Algunos Problemas en la Vreificaci�n de Contrase�as";
		doc.getElementById("errorAuthPassword").className = "errorMsg";
		doc.getElementById("adminUserEdit").style.display = "none";
		doc.getElementById("Old_fi_password").value = "";
		return false;
	}

}

function mandatoryCheck(flag) {
	// alert("mandatoryCheck");
	var selectID = "";
	var label = "";
	var doc = document;
	var len = 0;
	var isValidate = true;
	// alert("flag : "+ flag);
	var errorMsgDiv = "";
	if (flag == 'MyInfoEdit') {
		selectID = [ "Old_fi_password", "fi_password", "fi_confirm_password2" ];
		label = [ "Old Password", "New Password", "Confirm Password" ];
		// errorMsgDiv="errorAuthPassword";

	}
	if (flag == 'XMLUpload') {
		/*
		 * selectID = [ "reportingYear", "submissionType", "new_update" ]; label = [
		 * "Reporting Year", "Submission Type", "submission or an update" ];
		 */
		selectID = [ "reportingYear","submissionType", "new_update" ];
		label = [ "Reporting Year","Submission Type", "submission - new or an update" ];
		// errorMsgDiv="errorAuthPassword";

	}
	var len = selectID.length;
	// alert("len : "+ len);
	if (len != 0) {

		// alert("inside");
		try {
			var i;
			for (i = 0; i < len; i++) {
				// alert("id:"+selectID[i]);
				// alert("value:"+doc.getElementById(selectID[i]).value);
				// alert("selectID[i] : "+selectID[i]);
				if (doc.getElementById(selectID[i]).value == null
						|| doc.getElementById(selectID[i]).value == undefined
						|| doc.getElementById(selectID[i]).value == ""
						|| doc.getElementById(selectID[i]).value == "undefined"
						|| doc.getElementById(selectID[i]).value == "--Select--") {
					var columns = "";
					if (flag == 'MyInfoEdit')
						columns = selectID[i] + "_errorMsg";
					else if (flag == 'XMLUpload')
						columns = "UploadMsg";

					// alert("columns101 : "+columns);

					doc.getElementById(columns).innerHTML = "El " + label[i]
							+ " no puede estar en blanco";
					// alert("columns102 : "+columns);
					doc.getElementById(columns).className = "errorMsg";

					isValidate = false;
					doc.getElementById(selectID[i]).focus();
					break;

				} else if (flag == 'MyInfoEdit') {

					doc.getElementById(selectID[i] + "_errorMsg").innerHTML = "&nbsp;";

					isValidate = true;
				}

			}
			// alert("isValidate : " + isValidate);
			return isValidate;

		} catch (ex) {
			return false;
			alert(ex);
		}
	}

}

/*
 * function mandatoryCheck(flag) { // alert("mandatoryCheck"); var selectID =
 * ""; var label = ""; var doc = document; var len = 0; var isValidate = true; //
 * alert("flag : "+ flag); var errorMsgDiv = ""; if (flag == 'MyInfoEdit') {
 * selectID = [ "fi_password", "fi_confirm_password2" ]; label = [ "New
 * Password", "Confirm Password" ]; // errorMsgDiv="errorAuthPassword"; } if
 * (flag == 'XMLUpload') { selectID = [ "reportingYear", "new_update" ]; label = [
 * "Reporting Year", "submission or an update" ]; //
 * errorMsgDiv="errorAuthPassword"; } var len = selectID.length; // alert("len : "+
 * len); if (len != 0) { // alert("inside"); try { var i; for (i = 0; i < len;
 * i++) { // alert("id:"+selectID[i]); //
 * alert("value:"+doc.getElementById(selectID[i]).value); // alert("selectID[i] :
 * "+selectID[i]); if (doc.getElementById(selectID[i]).value == null ||
 * doc.getElementById(selectID[i]).value == undefined ||
 * doc.getElementById(selectID[i]).value == "" ||
 * doc.getElementById(selectID[i]).value == "undefined" ||
 * doc.getElementById(selectID[i]).value == "--Select--") { var columns = ""; if
 * (flag == 'MyInfoEdit') columns = selectID[i] + "_errorMsg"; else if (flag ==
 * 'XMLUpload') columns = "UploadMsg"; // alert("columns101 : "+columns);
 * 
 * doc.getElementById(columns).innerHTML = "The " + label[i] + " cannot be
 * blank"; // alert("columns102 : "+columns);
 * doc.getElementById(columns).className = "errorMsg"; isValidate = false;
 * doc.getElementById(selectID[i]).focus(); break; } } // alert("isValidate :
 * "+isValidate); return isValidate; } catch (ex) { return false; alert(ex); } } }
 */

function validate2(flag) {
	// alert("101");
	try {
		// alert("101");
		if (!mandatoryCheck(flag)) {
			// alert("validate2 : "+validate2);
			return false;
		}
		return true;
	} catch (ex) {

		alert(ex);
	}

}

/* bug 10232 */
// Type:Column name
/*
 * function getMasterDropDown(src, type) {
 * 
 * var doc = document; var url = ""; var param = ""; var response = ""; var
 * comboID = undefined;
 * 
 * if (src == "UserRegistration" && type == "Country") { comboID = [
 * "fi_country", "user_country" ]; } else if (src == "ManualRegistration" &&
 * type == "Country") {
 * 
 * comboID = [ "filerCountry_manual", "sponEntityCountry", "interEntityCountry",
 * "IndividualOREntityCountry", "OwnerCountry_manual" ]; } else if (src ==
 * "ManualRegistration" && type == "Currency") { comboID = [ "finance_Country" ]; }
 * else if (src == "ManualRegistration" && type == "EntityType") { comboID = [
 * "ActHolderEntityType_manual" ]; }
 * 
 * url = appPath + "/AccountCreation"; param = "option=getMaster" +
 * "&columnName=" + type;
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * if (src == "UserRegistration" && type == "Country") { try { var j = 0; var
 * len = comboID.length; for (j; j < len; j++) {
 * doc.getElementById(comboID[j]).value = ajaxCall( url, param);
 * doc.getElementById(comboID[j]).disabled = true;
 * doc.getElementById(comboID[j]).className = "readOnlyClass"; } } catch (ex) {
 * 
 * alert(ex); } }
 * 
 * try { var j = 0; var len = comboID.length; for (j; j < len; j++) { var
 * objCombo = doc.getElementById(comboID[j]); objCombo.options.length = 0;
 * response = ajaxCall(url, param); // alert(response); if (response != null &&
 * response.toUpperCase() != "NULL" && response != "") { var ourRowArr =
 * trimString(response).split("|"); objCombo.options.length = 0; optn =
 * document.createElement("option"); optn.text = "------Select------";
 * optn.value = ""; objCombo.options.add(optn); var i = 0; var rowLength = 0;
 * rowLength = ourRowArr.length; for ( var i = 0; i < rowLength; i++) {
 * 
 * optn = document.createElement("option"); optn.text = ourRowArr[i];
 * 
 * if (ourRowArr[i].indexOf("-") != -1) { optn.value = ourRowArr[i].substring(0,
 * ourRowArr[i] .indexOf("-")); } else { optn.value = ourRowArr[i]; } //
 * optn.value = ourRowArr[i]; objCombo.options.add(optn); } } } } catch (ex) {
 * 
 * alert(ex); } }
 */

function getMasterDropDown(src, type) {

	var doc = document;
	var url = "";
	var param = "";
	var response = "";
	var comboID = undefined;

	if (src == "UserRegistration" && type == "Country") {
		comboID = [ "user_country" ];

	} else if (src == "ManualRegistration" && type == "Country") {

		comboID = [ "filerCountry_manual", "sponEntityCountry",
				"interEntityCountry", "IndividualOREntityCountry",
				"OwnerCountry_manual" ];

	} else if (src == "ManualRegistration" && type == "Currency") {
		comboID = [ "finance_Country" ];
	} else if (src == "ManualRegistration" && type == "EntityType") {
		comboID = [ "ActHolderEntityType_manual" ];
	}
	/* Change for ALL -By Kd on 20072016 'code added'starts */
	else if (src == "AdminApproval" && type == "Country") {
		comboID = [ "userCountry_adminApproval","fiCountry_adminApproval" ];
	}
	//changes by shubham for filer category drop down on 09 06 2017
	else if(src == "ManualRegistration" && type == "FILER_CATEGORY")
		comboID = [ "filerCategory_manual" ];
	else if(src == "ManualRegistration" && type == "FILCATSPON")
		comboID = [ "sponEntityFilerCategory" ];
	
	
	/* Change for ALL -By Kd on 20072016 end */
	url = appPath + "/AccountCreation";
	param = "option=getMaster" + "&columnName=" + type;
	/*
	 * 
	 * 
	 * 
	 * 
	 */
	/*
	 * if (src == "UserRegistration" && type == "Country") { try { var j = 0;
	 * var len = comboID.length; for (j; j < len; j++) {
	 * doc.getElementById(comboID[j]).value = ajaxCall( url, param);
	 * doc.getElementById(comboID[j]).disabled = true;
	 * doc.getElementById(comboID[j]).className = "readOnlyClass"; } } catch
	 * (ex) {
	 * 
	 * alert(ex); } }
	 */
	try {
		var j = 0;
		var len = comboID.length;
		for (j; j < len; j++) {
			var objCombo = doc.getElementById(comboID[j]);
			objCombo.options.length = 0;
			response = ajaxCall(url, param);
			// alert(response);
			if (response != null && response.toUpperCase() != "NULL"
					&& response != "") {
				var ourRowArr = trimString(response).split("|");
				objCombo.options.length = 0;
				optn = document.createElement("option");
				optn.text = "------Select------";
				optn.value = "";
				objCombo.options.add(optn);
				var i = 0;
				var rowLength = 0;
				rowLength = ourRowArr.length;
				for ( var i = 0; i < rowLength; i++) {

					optn = document.createElement("option");
					optn.text = ourRowArr[i];
					/* added by KD 11032016 */
					if (ourRowArr[i].indexOf("-") != -1) {
						optn.value = trimString(ourRowArr[i].substring(0,
								ourRowArr[i].indexOf("-")));
					} else {
						optn.value = trimString(ourRowArr[i]);
					}
					/* added by KD 11032016 */
					// optn.value = ourRowArr[i];
					objCombo.options.add(optn);

				}
			}

		}

	} catch (ex) {

		alert(ex);
	}

}
/* bug 10232 */

function callEditServlet() {
	var response = undefined;
	try {
		url = appPath + "/MyInfoEdit";
		param = "callFrom=ajax";
		response = ajaxCall(url, param);
		return true;
	} catch (e) {
		alert(e);
	}
}

/* BUG 10234 */

function resetFIDetails() {
	var doc = document;
	try {
		if (doc.getElementById("fatcaCompliance").value == "1") {
			var giinFieldID = [ "text1", "text2", "text3", "text4", "text5",
					"text6", "text7", "text8", "text9", "text10", "text11",
					"text12", "text13", "text14", "text15", "text16",
					"fi_name", "fi_address", "fi_email", "fi_Phone" ];
			var len = giinFieldID.length;

			for ( var i = 0; i < len; i++) {
				doc.getElementById(giinFieldID[i]).value = "";

				if (i < 16) {
					doc.getElementById(giinFieldID[i]).readOnly = false;
					doc.getElementById(giinFieldID[i]).className = "form-control";
				} else {
					if (i == 18) {
						doc.getElementById(giinFieldID[i]).style.background = "#ffffb3 url('../images/mail2.png') no-repeat center right;padding:0px 2px 2px 5px; height:25px; width:200px; padding:0px 2px 2px 5px;";
						doc.getElementById(giinFieldID[i]).readOnly = true;
					} else {
						doc.getElementById(giinFieldID[i]).readOnly = true;
						doc.getElementById(giinFieldID[i]).className += " "
								+ "readOnlyClass";
					}
				}

			}
			doc.getElementById("giin").value = "";
		} else {
			var giinFieldID = [ "fi_name", "fi_address", "fi_email", "fi_Phone" ];
			var len = giinFieldID.length;
			for ( var i = 0; i < len; i++) {
				doc.getElementById(giinFieldID[i]).value = "";
				if (i == 2) {
					doc.getElementById(giinFieldID[i]).style.background = "#ffffb3 url('../images/mail2.png') no-repeat center right;padding:0px 2px 2px 5px; height:25px; width:200px; padding:0px 2px 2px 5px;";
					doc.getElementById(giinFieldID[i]).readOnly = true;
				} else {
					doc.getElementById(giinFieldID[i]).readOnly = true;
					doc.getElementById(giinFieldID[i]).className += " "
							+ "readOnlyClass";
				}
			}
			doc.getElementById("giin1").readOnly = false;
			doc.getElementById("giin1").value = "";
			doc.getElementById("giin1").className = "form-control";

		}
		return true;

	} catch (e) {
		alert(e);
	}

}
/*
 * function executeServlet(src) {
 * 
 * var doc = document; var url = undefined; var param = undefined; var id =
 * undefined; var emailID = undefined var password = undefined;
 * 
 * try {
 * 
 * 
 * if (src == "captchaImage") {
 * 
 * url = appPath + "/Captchaimage"; param=""; }
 * 
 * if (src == "userEdit") { password = doc.getElementById("fi_password").value;
 * url = appPath + "/MyInfoEdit"; param = "event=update" + "&newPassWord=" +
 * password; } if (src == "ForgotPassword") { password =
 * doc.getElementById("newpassword_forgotpassword").value; emailID =
 * doc.getElementById("emailForPIN_ForgotPassword").value; url = appPath +
 * "/MyInfoEdit";
 * 
 * param = "event=updateForgotPassword" + "&newPassWord=" + password +
 * "&emailID=" + emailID; } if (src == "admin-approval") {
 * 
 * var emailID = doc.getElementById("userEmailID_adminApproval").value; var
 * status = doc.getElementById("status_adminApproval").value; var comments =
 * doc.getElementById("comments_adminApproval").value; var userName =
 * doc.getElementById("userName_adminApproval").value; var userType_toUpdate =
 * doc.getElementById("userType_toUpdate").value; var editUserGIIN =
 * doc.getElementById("GIIN_adminApproval").value; var userLockedStatus =
 * doc.getElementById("user_LockedStatus").value;
 * 
 * url = appPath + "/UserApproval"; param = "option=update" + "&comments=" +
 * comments + "&emailID=" + emailID + "&status=" + status + "&fiUserName=" +
 * userName + "&userTypetoUpdate=" + userType_toUpdate + "&editUserGIIN=" +
 * editUserGIIN + "&isUserLocked=" + userLockedStatus; }
 * 
 * var response = trimString(ajaxCall(url, param));
 * 
 * if (response == "sucess") {
 * 
 * if (src == "userEdit") { alert("Your information has been updated.");
 * window.location.href = appPath + "/JSP/home.jsp"; } else if (src ==
 * "admin-approval") { alert("Your information has been updated.");
 * getUserApprovalList('getPageCount'); blankAllFields(); //
 * window.location.reload(); } else {
 * 
 * reset("ForgotPassword"); alert("Your password has been updated.");
 * doc.getElementById("form_ForgotPassword").style.display = "none";
 * doc.getElementById("errorMsgSendPin_ForgotPassword").innerHTML = "&nbsp;"; } }
 * else { alert("There is some system error please try after some time or
 * contact system administrator"); return false; } } catch (e) {
 * 
 * alert(e); } }
 */

function executeServlet(src) {

	var doc = document;
	var url = undefined;
	var param = undefined;
	var id = undefined;
	var emailID = undefined;
	var password = undefined;
	var oldPassword = undefined;

	try {

		/*
		 * if (src == "captchaImage") {
		 * 
		 * url = appPath + "/Captchaimage"; param=""; }
		 */
		if (src == "userEdit") {
			password = doc.getElementById("fi_password").value;
			oldPassword = doc.getElementById("Old_fi_password").value;

			url = appPath + "/MyInfoEdit";
			param = "event=update" + "&newPassWord=" + password
					+ "&oldPassword=" + oldPassword;

		}
		if (src == "ForgotPassword") {
			password = doc.getElementById("newpassword_forgotpassword").value;
			emailID = doc.getElementById("emailForPIN_ForgotPassword").value;
			url = appPath + "/MyInfoEdit";

			param = "event=updateForgotPassword" + "&newPassWord=" + password
					+ "&emailID=" + emailID;
		}
		if (src == "admin-approval") {

			var emailID = doc.getElementById("userEmailID_adminApproval").value;
			var status = doc.getElementById("status_adminApproval").value;
			var comments = doc.getElementById("comments_adminApproval").value;
			var userName = doc.getElementById("userName_adminApproval").value;
			var userType_toUpdate = doc.getElementById("userType_toUpdate").value;
			var editUserGIIN = doc.getElementById("GIIN_adminApproval").value;
			var userLockedStatus = doc.getElementById("user_LockedStatus").value;

			url = appPath + "/UserApproval";
			param = "option=update" + "&comments=" + comments + "&emailID="
					+ emailID + "&status=" + status + "&fiUserName=" + userName
					+ "&userTypetoUpdate=" + userType_toUpdate
					+ "&editUserGIIN=" + editUserGIIN + "&isUserLocked="
					+ userLockedStatus;
		}

		var response = trimString(ajaxCall(url, param));

		if (response == "sucess") {

			if (src == "userEdit") {
				alert("Su informaci�n ha sido actualizada.");
				window.location.href = appPath + "/JSP/home.jsp";
			} else if (src == "admin-approval") {
				alert("Su informaci�n ha sido actualizada.");
				getUserApprovalList('getPageCount', SEARCHFOR, SEARCHIN);
				// getUserApprovalList('getPageCount');
				blankAllFields();
				// window.location.reload();
			} else {

				reset("ForgotPassword");
				alert("Tu contrase�a ha sido actualizada.");
				doc.getElementById("form_ForgotPassword").style.display = "none";
				doc.getElementById("errorMsgSendPin_ForgotPassword").innerHTML = "&nbsp;";

			}
		} else if (response.toUpperCase() == "OLDFAIL") {
			doc.getElementById("Old_fi_password").value = "";
			doc.getElementById("fi_password").value = "";
			doc.getElementById("fi_confirm_password2").value = "";
			alert("Contrase�a antigua no coincidente.Por favor, introduzca la contrase�a antigua correcta");

			return false;
		} else {
			alert("Hay alg�n error del sistema intente despu�s de alg�n tiempo o contacte con el administrador del sistema");
			return false;
		}
	} catch (e) {

		alert("Excepci�n :: ejecutar Servlet:: " + e);

	}

}

/* BUG 10234 */

function encrptPassword1(id) {
	var doc = document;
	var hashedPwd = "";
	try {
		var uiPwd = doc.getElementById(id).value;
		hashedPwd = CryptoJS.MD5(uiPwd);
	} catch (e) {
		alert("excepci�n:" + e);
	}
	return hashedPwd;
}
//Changes made by Alok on 24-11-2016 Start
function checkFileFormat(obj) {
	var doc = document;
	var file = "";
	var id = obj.id;
	var fileInput1="";
	var fileInput2="";
	var file11="";
	var file22="";
	var filesize1=0;
	var filesize2=0;
	try {
		file = doc.getElementById(id).value;
		if (id == "file1") {
			doc.getElementById("file1").click();
			
			 fileInput1= doc.getElementById("file1");		   
			  file11 = fileInput1.files[0];
             
             filesize1= (file11.size)/(1024*1024) ;
  
	  
	   
	   

			    }
			   
		 if (id == "file2") {
			doc.getElementById("file2").click();
			
			
			 fileInput2 =  doc.getElementById("file2");
			   
			    
             file22 = fileInput2.files[0];
            
             filesize2= (file22.size)/(1024*1024) ;
                
         
		 }
		 
		 
		 var url = appPath + "/AccountCreation";

//var url1= appPath + "/UserApproval";
	/*param = "option=update" + "&comments=" + comments + "&emailID="
	+ emailID + "&status=" + status + "&fiUserName=" + userName
	+ "&userTypetoUpdate=" + userType_toUpdate
	+ "&editUserGIIN=" + editUserGIIN + "&isUserLocked="
	+ userLockedStatus;*/
		var param = "fileName=" + file + "&option=checkFileFormat" + "&filesize="+filesize1 + "&filesize21="+filesize2 ;
		//changes made by Alok on 24-11-2016 End
		var response = trimString(ajaxCall(url, param));
		if (response != null && response != undefined && response != "") {
			if (id.indexOf("admin") != -1) {
				if (response != "True") {
					validFileFormat = response.substring(
							response.indexOf("@") + 3, response.length);

					doc.getElementById("errorMsgDocFormat").innerHTML = "Por favor seleccione  "
							+ validFileFormat + " archivo. ";
					doc.getElementById("errorMsgDocFormat").className = "errorMsg";
					// doc.getElementById("Submit_button").style.display =
					// "none";
					doc.getElementById(id).value.innerHTML = "";
					return false;

				} else {
					doc.getElementById("errorMsgDocFormat").innerHTML = "";
					// doc.getElementById("Submit_button").style.display =
					// "block";
					return true;
				}
			} else {
				if (response != "True") {
					// as there are @@@ in the string
					if(response.includes("PDF"))
					validFileFormat = response.substring(
							response.indexOf("@") + 3, response.length);
					
					
					else
						validFileFormat = response.substring(
								response.indexOf("@") + 3, response.length-4);

					doc.getElementById("doc_errormsg").innerHTML = "Por favor seleccione  "
							+ validFileFormat + " archivo. ";
					doc.getElementById("doc_errormsg").className = "errorMsg";
					doc.getElementById("Submit_button").style.display = "none";
					doc.getElementById(id).value.innerHTML = "";
					return false;

				} else {
					doc.getElementById("doc_errormsg").innerHTML = "";
					doc.getElementById("Submit_button").style.display = "block";
					return true;
				}
			}
		}

	} catch (e) {
		alert(e);
	}
}
function validateFileType() {

	var doc = document;
	var fieldID = [ "file1", "file2" ];
	var fieldLabel = [ "Document1", "Document2" ];
	var len = fieldID.length;
	var file = "";
	var fileExt = "";
	var filevalidated = false;
	try {
		for ( var i = 0; i < len; i++) {
			file = doc.getElementById(fieldID[i]).value;
			if (file != null && file != undefined && file != "") {

				fileExt = file
						.substring(file.lastIndexOf(".") + 1, file.length);

				if (validFileFormat != null && validFileFormat != undefined) {
					if (fileExt.toUpperCase() != validFileFormat.toUpperCase()) {
						doc.getElementById("doc_errormsg").innerHTML = "Por favor seleccione el archivo  "
								+ validFileFormat
								+ " para  "
								+ fieldLabel[i];
						doc.getElementById("doc_errormsg").className = "errorMsg";
						return false;
					} else {
						{

							filevalidated = true;
						}

					}
				}

			}

		}

	} catch (e) {
		alert(e);
	}
	return filevalidated;

}

function validateConfirmPassword1(source) {
	// alert("inside validateConfirmPassword1");
	var doc = document;
	var password1 = "";
	var password2 = "";
	var errormsgId = "";
	var isConfirm = true;
	try {
		if (source == "Registration") {
			password1 = "fi_password_Reg";
			password2 = "fi_confirm_password2_Reg";
			errormsgId = "errorPassword";
		}
		if (checkPasswordFormat(password1)) {
			if (doc.getElementById(password1).value != doc
					.getElementById(password2).value) {
				doc.getElementById(errormsgId).innerHTML = "La contrase�a de confirmaci�n no coincide.";
				doc.getElementById(errormsgId).className = "errorMsg";
				isConfirm = false;
			} else
				isConfirm = true;

		} else {
			doc.getElementById(errormsgId).innerHTML = "Su contrase�a debe tener al menos 8 caracteres y debe contener 1 n�mero y 1 car�cter especial";
			doc.getElementById(errormsgId).className = "errorMsg";
			isConfirm = false;
		}
		return isConfirm;
	} catch (e) {
		alert(e);
	}
}

/*
 * function validateConfirmPassword1(source) {
 * 
 * var doc = document; var password1 = ""; var password2 = ""; var errormsgId =
 * ""; try { if (source == "Registration") { password1 = "fi_password_Reg";
 * password2 = "fi_confirm_password2_Reg"; errormsgId = "errorPassword"; }
 * 
 * if (doc.getElementById(password1).value != doc
 * .getElementById(password2).value) { doc.getElementById(errormsgId).innerHTML =
 * "The confirm password does not match.";
 * doc.getElementById(errormsgId).className = "errorMsg"; return false; } else
 * return true; } catch (e) { alert(e); } }
 */

function autoSetFocus(event, element, output) {

	var ch = element.id;
	// alert(ch);
	// var last=ch.slice(-2);
	var last = ch.match(/\d+$/)[0];
	// alert(last);
	var first = ch.replace(last, "");
	// alert(first);
	setValue(first, output);
	last++;
	if (((event.keyCode >= 48) && (event.keyCode <= 57))// 0-9
			|| ((event.keyCode >= 65) && (event.keyCode <= 97))// a-z
			|| ((event.keyCode >= 96) && (event.keyCode <= 105))// numpad0 -
	// numpad9
	) {
		/*
		 * if(last<10) document.getElementById(first+"0"+last).select(); else
		 */
		document.getElementById(first + last).select();
	}
}

function setValue(first, output) {
	var flag = 1;
	var i;
	var elem;
	for (i = 1; i <= 16; i++) {
		/*
		 * if(i<10) elem=first+"0"+i; else
		 */
		elem = first + i;

		if (document.getElementById(elem).value == "") {
			flag = 0;
			document.getElementById(output).value = "";
			break;
		}
	}
	if (flag == 1) {
		var n = "";
		for (i = 1; i <= 16; i++) {
			/*
			 * if(i<10) elem=first+"0"+i; else
			 */
			elem = first + i;
			n = n + document.getElementById(elem).value;
			if (i == 6 || i == 11 || i == 13)
				n = n + '.';
		}
		document.getElementById(output).value = n;
	}
}

function checkUserType(obj) {
	var doc = document;
	try {
		var usertype = doc.getElementById(obj.id).checked;

		if (obj.id == "user_locked") {
			// alert('usertype-->' + usertype);
			if (usertype) {
				// doc.getElementById("user_LockedStatus").value = "Y";
				doc.getElementById("user_adminLockStatus").value = "Y";
			} else {
				// doc.getElementById("user_LockedStatus").value = "N";
				doc.getElementById("user_adminLockStatus").value = "N";
			}
			// alert(doc.getElementById("user_LockedStatus").value);
		} else {

			if (usertype) {
				doc.getElementById("fiUserType").value = "FI Admin";
			} else {
				doc.getElementById("fiUserType").value = "FI User";
			}
		}
		// return true;

	} catch (e) {
		alert(e);
	}
}

function checkPasswordFormat(pwd) {
	var regularExp = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;
	// var regularExp = /^(?=.*\d)(?=.*[A-Z])(?=.*[!#$%&?$/;
	var pass = document.getElementById(pwd).value;
	if (regularExp.test(pass))
		return true;
	else
		return false;

}

function blankAllFields() {
	var doc = document;
	var fieldIDArray = [ "GIIN_adminApproval", "fiName_adminApproval",
			"fiAddress_adminApproval", "fiCountry_adminApproval",
			"fiEmailID_adminApproval", "userEmailID_adminApproval",
			"userName_adminApproval", "userAddress_adminApproval",
			"userCountry_adminApproval", "userPhone_adminApproval",
			"userEmployee_adminApproval", "userDesignation_adminApproval",
			"Document1_adminApproval", "Document2_adminApproval",
			"docs1_adminApproval", "docs2_adminApproval", "userType_toUpdate",
			"comments_adminApproval" ];
	var len = fieldIDArray.length;
	try {
		for ( var i = 0; i < len; i++) {
			if (fieldIDArray[i] == "docs1_adminApproval"
					|| fieldIDArray[i] == "docs2_adminApproval") {
				doc.getElementById(fieldIDArray[i]).href = "#";
			} else {
				doc.getElementById(fieldIDArray[i]).value = "";
			}

		}

	} catch (e) {
		alert(e);
	}

}

function fun_AllowOnlyAmountAndDot(txt) {

	var doc = document;

	if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode == 46) {

		var txtbx = doc.getElementById(txt);
		var cursorPoint = getCursorPoint(txtbx);
		var amount = doc.getElementById(txt).value;
		var present = 0;
		var count = 0;

		if (amount.indexOf(".", present) || amount.indexOf(".", present + 1))
			;
		{
			// alert('0');

		}
		do {
			present = amount.indexOf(".", present);
			if (present != -1) {
				count++;
				present++;
			}
		} while (present != -1);
		if (present == -1 && amount.length == 0 && event.keyCode == 46) {
			event.keyCode = 0;

			// alert("Wrong position of decimal point not allowed !!");
			return false;
		}

		if (count >= 1 && event.keyCode == 46) {

			event.keyCode = 0;
			// alert("Only one decimal point is allowed !!");
			return false;
		}
		if (count == 1) {
			var lastdigits = amount.substring(amount.indexOf(".") + 1,
					amount.length);
			if (lastdigits.length >= 2 && cursorPoint > amount.indexOf(".")) {
				// alert("Two decimal places only allowed");
				event.keyCode = 0;
				return false;
			}
		}
		return true;
	} else {
		event.keyCode = 0;
		// alert("Only Numbers with dot allowed !!");
		return false;
	}

}

function getCursorPoint(el) {
	if (el.selectionStart) {
		return el.selectionStart;
	} else if (document.selection) {
		el.focus();

		var r = document.selection.createRange();
		if (r == null) {
			return 0;
		}

		var re = el.createTextRange(), rc = re.duplicate();
		re.moveToBookmark(r.getBookmark());
		rc.setEndPoint('EndToStart', re);

		return rc.text.length;
	}
	return 0;
}

function logoutUser(source) {
	var url = "";
	var param = "";
	var updateStatus = "";
	try {
		if (source != "user2") {
			url = appPath + "/LogoutServlet";
			param = "code=" + source;
			updateStatus = trimString(ajaxCall(url, param));
			// alert();
			if (updateStatus.toUpperCase() == "SUCCESS") {
				if (source == "jsp" || source == "sessionExpire") {
					window.location.href = appPath + "/JSP/logout.jsp";
				}
				return true;
			} else {
				return false;
			}
		} else if (source == "user2") {
			window.location.href = appPath + "/JSP/logout.jsp";
			return true;
		}

	} catch (e) {
		alert(e);
	}

}

function validateGiinField(id, type) {
	var doc = document;
	var regularExpression = "";
	try {
		var keyCode = event.which | event.keyCode;
		var key = String.fromCharCode(keyCode);
		if (type == "aplha") {
			regularExpression = "[a-zA-Z]";
			if (key != key.match(regularExpression)) {
				/* bug id 11420 starts */
				alert("Ingrese un car�cter v�lido.");
				/* bug id 11420 end */
				return false;
			} else {
				return true;
			}
		}

		else if (type == "numeric") {
			regularExpression = "[0-9]";
			if (key != key.match(regularExpression)) {
				alert("Ingrese un car�cter v�lido.");
				return false;
			} else {
				return true;
			}
		} else if (type == "aplhanumeric") {
			regularExpression = "[a-zA-Z0-9]";
			if (key != key.match(regularExpression)) {
				alert("Ingrese un car�cter v�lido.");
				return false;
			} else {
				return true;
			}
		}

	} catch (e) {
		alert(e);
	}
}
function autoSetFocus2(event, element, output) {
	var ch = element.id;
	var last = ch.match(/\d+$/)[0];
	var first = ch.replace(last, "");
	try {
		if (last < 16)
			last++;
		if (((event.keyCode >= 48) && (event.keyCode <= 57))// 0-9
				|| ((event.keyCode >= 65) && (event.keyCode <= 97))// a-z
				|| ((event.keyCode >= 96) && (event.keyCode <= 105))// numpad0 -
		// numpad9
		) {
			document.getElementById(first + last).select();
		}

		setValue(first, output);
	} catch (e) {
		alert(e);
	}
}

function forwardJSP(type) {
	if (type == "SessionExpired")
		window.location.href = appPath + "/JSP/SessionExpired.jsp";
	else if (type == "ExceptionType")
		window.location.href = appPath + "/JSP/ExceptionPage.jsp";
}

function makeFIreadonly(src) {

	var doc = document;
	var fieldIDArray = [ "fi_name", "fi_address", "fi_email", "fi_Phone" ];
	var len = fieldIDArray.length;
	try {
		if (src == "nonEdiatable") {
			for ( var i = 0; i < len; i++) {
				doc.getElementById(fieldIDArray[i]).setAttribute("tabindex",
						"-1");
				doc.getElementById(fieldIDArray[i]).readOnly = true;
				doc.getElementById(fieldIDArray[i]).className += " "
						+ "readOnlyClass";
				if (i == 2) {
					doc.getElementById(fieldIDArray[i]).setAttribute(
							"tabindex", "-1");
					doc.getElementById(fieldIDArray[i]).readOnly = true;
					doc.getElementById(fieldIDArray[i]).style.background = "#ffffb3 url('../images/mail2.png') no-repeat center right;padding:0px 2px 2px 5px; height:25px; width:200px; padding:0px 2px 2px 5px;";
				}
			}
		} else if (src == "editable") {
			for ( var i = 0; i < len; i++) {
				doc.getElementById(fieldIDArray[i]).removeAttribute("tabindex");
				doc.getElementById(fieldIDArray[i]).readOnly = false;
				doc.getElementById(fieldIDArray[i]).className = "form-control";
				if (i == 2) {
					doc.getElementById(fieldIDArray[i]).removeAttribute(
							"tabindex");
					doc.getElementById(fieldIDArray[i]).readOnly = false;
					doc.getElementById(fieldIDArray[i]).style.background = "white url('../images/mail2.png') no-repeat center right;padding:0px 2px 2px 5px; height:25px; width:200px; padding:0px 2px 2px 5px;";
				}
			}
		}

	} catch (e) {
		alert(e);
	}
}
function blankGIINFields(src) {
	var doc = document;
	var giinFieldID = "";
	var len = 0;
	if (src == "registration") {
		giinFieldID = [ "text1", "text2", "text3", "text4", "text5", "text6",
				"text7", "text8", "text9", "text10", "text11", "text12",
				"text13", "text14", "text15", "text16" ];

	} else if (src == "Sponsor") {
		giinFieldID = [ "text_giin1", "text_giin2", "text_giin3", "text_giin4",
				"text_giin5", "text_giin6", "text_giin7", "text_giin8",
				"text_giin9", "text_giin10", "text_giin11", "text_giin12",
				"text_giin13", "text_giin14", "text_giin15", "text_giin16" ];

	} else if (src == "Intermediary") {
		giinFieldID = [ "text_giin_inter1", "text_giin_inter2",
				"text_giin_inter3", "text_giin_inter4", "text_giin_inter5",
				"text_giin_inter6", "text_giin_inter7", "text_giin_inter8",
				"text_giin_inter9", "text_giin_inter10", "text_giin_inter11",
				"text_giin_inter12", "text_giin_inter13", "text_giin_inter14",
				"text_giin_inter15", "text_giin_inter16" ];

	}
	if (giinFieldID != undefined)
		len = giinFieldID.length;
	try {

		for ( var i = 0; i < len; i++) {
			doc.getElementById(giinFieldID[i]).value = "";
		}
	} catch (e) {
		alert(e);
	}
}

function makeGIINFieldsReadOnly(src) {
	var doc = document;
	var giinFieldID = "";
	var len = 0;
	if (src == "registration") {
		giinFieldID = [ "text1", "text2", "text3", "text4", "text5", "text6",
				"text7", "text8", "text9", "text10", "text11", "text12",
				"text13", "text14", "text15", "text16" ];

	} else if (src == "Sponsor") {
		giinFieldID = [ "text_giin1", "text_giin2", "text_giin3", "text_giin4",
				"text_giin5", "text_giin6", "text_giin7", "text_giin8",
				"text_giin9", "text_giin10", "text_giin11", "text_giin12",
				"text_giin13", "text_giin14", "text_giin15", "text_giin16" ];

	} else if (src == "Intermediary") {
		giinFieldID = [ "text_giin_inter1", "text_giin_inter2",
				"text_giin_inter3", "text_giin_inter4", "text_giin_inter5",
				"text_giin_inter6", "text_giin_inter7", "text_giin_inter8",
				"text_giin_inter9", "text_giin_inter10", "text_giin_inter11",
				"text_giin_inter12", "text_giin_inter13", "text_giin_inter14",
				"text_giin_inter15", "text_giin_inter16" ];

	}
	if (giinFieldID != undefined)
		len = giinFieldID.length;
	try {

		for ( var i = 0; i < len; i++) {
			doc.getElementById(giinFieldID[i]).readOnly = true;
			doc.getElementById(giinFieldID[i]).className += " "
					+ "readOnlyClass";

		}
	} catch (e) {
		alert(e);
	}
}
/*
 * function getFAQ(event) {
 * 
 * var response; var doc = document; var faqArray; var tableObj =
 * doc.getElementById("userApprovalTable"); var url = appPath + "/FAQServlet";
 * var param = "callFor=" + event;
 * 
 * try { response = ajaxCall(url, param); if (response != undefined &&
 * response.toUpperCase() != "NULL" && response != "" && tableObj != undefined) {
 * faqArray = JSON.parse(response); } } catch (e) { alert("Error:" + e.message); } }
 */

function CheckForgotMandatoryField() {
	var selectID = "";
	var label = "";
	var doc = document;
	var len = 0;
	var isValidate = true;
	var errorMsgDiv = "errorMsg_forgotPassword";
	selectID = [ "newpassword_forgotpassword", "confirmpassword_forgotpassword" ];
	label = [ "New Password", "Confirm Password" ];
	var len = selectID.length;
	if (len != 0) {
		try {
			var i;
			for (i = 0; i < len; i++) {
				if (doc.getElementById(selectID[i]).value == null
						|| doc.getElementById(selectID[i]).value == undefined
						|| doc.getElementById(selectID[i]).value == ""
						|| doc.getElementById(selectID[i]).value == "undefined"
						|| doc.getElementById(selectID[i]).value == "--Select--") {
					doc.getElementById(errorMsgDiv).innerHTML = "El "
							+ label[i] + " no puede estar en blanco";
					doc.getElementById(errorMsgDiv).className = "errorMsg";
					isValidate = false;
					doc.getElementById(selectID[i]).focus();
					break;
				} else {

					doc.getElementById(errorMsgDiv).innerHTML = "&nbsp;";
					isValidate = true;
				}

			}

			return isValidate;

		} catch (ex) {
			return false;
			alert(ex);
		}
	}

}

function confirmPassword_forgotPassword() {
	var doc = document;
	var password1 = "";
	var password2 = "";
	var errormsgId = "";
	var isConfirm = true;
	errors = [];
	try {
		password1 = "newpassword_forgotpassword";
		password2 = "confirmpassword_forgotpassword";
		errormsgId = "errorMsg_forgotPassword";
		if (checkPasswordFormat(password1)) {
			if (doc.getElementById(password1).value != ""
					&& doc.getElementById(password2).value) {
				doc.getElementById(errormsgId).innerHTML = "&nbsp;";
				if (doc.getElementById(password1).value != doc
						.getElementById(password2).value) {
					doc.getElementById(errormsgId).innerHTML = "La contrase�a de confirmaci�n no coincide.";
					doc.getElementById(errormsgId).className = "errorMsg";
					isConfirm = false;
				} else {
					isConfirm = true;
				}
			} else {
				doc.getElementById(errormsgId).innerHTML = "Vuelve a introducir la contrase�a";
				doc.getElementById(errormsgId).className = "errorMsg";
				isConfirm = false;
			}
		} else {
			doc.getElementById(errormsgId).innerHTML = "Su contrase�a debe tener al menos 8 caracteres y debe contener 1 n�mero y 1 car�cter especial";
			doc.getElementById(errormsgId).className = "errorMsg";
			isConfirm = false;
		}
		return isConfirm;
	} catch (ex) {
		alert(ex);
	}
}
function confirmRegistrationPassword() {
	var doc = document;
	var password1 = "";
	var password2 = "";
	var errormsgId = "";
	var isConfirm = true;

	errors = [];
	try {
		password1 = "fi_password_Reg";
		password2 = "fi_confirm_password2_Reg";
		errormsgId = "errorPassword";

		if (checkPasswordFormat(password1)) {
			if (doc.getElementById(password1).value != ""
					&& doc.getElementById(password2).value) {
				doc.getElementById(errormsgId).innerHTML = "&nbsp;";
				if (doc.getElementById(password1).value != doc
						.getElementById(password2).value) {
					doc.getElementById(errormsgId).innerHTML = "La contrase�a de confirmaci�n no coincide.";
					doc.getElementById(errormsgId).className = "errorMsg";
					isConfirm = false;
				} else {
					isConfirm = true;
				}

			} else {

				doc.getElementById(errormsgId).innerHTML = "Vuelve a introducir la contrase�a";
				doc.getElementById(errormsgId).className = "errorMsg";
				isConfirm = false;
			}
		} else {
			doc.getElementById(errormsgId).innerHTML = "Su contrase�a debe tener al menos 8 caracteres y debe contener 1 n�mero y 1 car�cter especial";
			doc.getElementById(errormsgId).className = "errorMsg";
			isConfirm = false;
		}

	} catch (ex) {

		alert(ex);
	}
	return isConfirm;
}
function confirmPassword_editInfo() {
	var doc = document;
	var password1 = "";
	var password2 = "";
	var errormsgId = "";
	var isConfirm = true;
	errors = [];
	try {

		password1 = "fi_password";
		password2 = "fi_confirm_password2";
		errormsgId = "fi_password_errorMsg";
		hiddenISConfirm = "isConformPassword";
		if (checkPasswordFormat(password1)) {
			if (doc.getElementById(password1).value != ""
					&& doc.getElementById(password2).value) {
				doc.getElementById(errormsgId).innerHTML = "&nbsp;";
				if (doc.getElementById(password1).value != doc
						.getElementById(password2).value) {
					doc.getElementById(errormsgId).innerHTML = "La contrase�a de confirmaci�n no coincide.";
					doc.getElementById(errormsgId).className = "errorMsg";
					isConfirm = false;
				} else {
					isConfirm = true;
				}

			} else {

				doc.getElementById(errormsgId).innerHTML = "Vuelve a introducir la contrase�a";
				doc.getElementById(errormsgId).className = "errorMsg";
				isConfirm = false;
			}
		} else {
			doc.getElementById(errormsgId).innerHTML = "Su contrase�a debe tener al menos 8 caracteres y debe contener 1 n�mero y 1 car�cter especial";
			doc.getElementById(errormsgId).className = "errorMsg";
			isConfirm = false;
		}
		return isConfirm;
	} catch (ex) {

		alert(ex);
	}

}