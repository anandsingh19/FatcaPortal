/*<!-- ------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : Manual Form
 File Name                                                   : Manual
 Author                                                      : Arbind
 Date (DD/MM/YYYY)                                           : 22/12/2015
 Description                                                 : Manual XML generation
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    			Change Description
 11116				 08/02/2016		Khushdil Kaushik		Fixed Table Layout
 10955				 10/02/2016		Khushdil Kaushik		Removed duplicate json object	
 11366				 30/03/2016		Khushdil Kaushik		change key for grid row
 11369				 30/03/2016		Khushdil Kaushik		change key for grid row
 11377				 01/04/2016		Khushdil Kaushik		check if parent key is there before deleting so data
 11598,11600		 27/06/2016		Khushdil Kaushik		new code written
 11604				 27/06/2016		Khushdil Kaushik		radio button checked property set to false 
 11606				 27/06/2016		Khushdil Kaushik		all data field reset
 ------------------------------------------------------------------------------------------------------
 */

/*begin:- added by KD 26022016*/
var ISNILREPORT = "N";
/* end:- added by KD 26022016 */
var ACT_GRID_ROW_KEY = undefined;
var SO_GRID_ROW_KEY = undefined;
function Check_AccountHolder_Type(accontHolderType) {
	var doc = document;
	var len = $(".owner_radio").length;
	try {

		doc.getElementById("isIndividualOREntityErrorMsg").innerHTML = "";
		if (accontHolderType == "Individual") {
			/* bug 11604 */
			for ( var i = 0; i < len; i++) {
				$(".owner_radio")[i].checked = false;
			}
			/* bug 11604 */

			doc.getElementById("Part3_tr").style.display = "none";
			doc.getElementById("entityType_tr").style.display = "none";
			doc.getElementById("substantial_table_tr").style.display = "none";
			doc.getElementById("Entity_radio").checked = false;
			doc.getElementById("individual_radio").checked = true;// this add
			// when you
			// checked
			// radio
			// button on
			// grid

			doc.getElementById("IndividualOREntityName").value = "";
			doc.getElementById("IndividualOREntityAddress").value = "";
			doc.getElementById("IndividualOREntityTin").value = "";
			doc.getElementById("IndividualOREntityCountry").value = "";
			/*bog id 11606*/
			doc.getElementById("accDOB").value = "";
			doc.getElementById("AccHolderCIF").value = "";
			/*bog id 11606*/
			// make the read Only Field of SO in case of
			doc.getElementById("OwnerName_manual").value = "";
			doc.getElementById("OwnerAddress_manual").value = "";
			doc.getElementById("OwnerTin_manual").value = "";
			doc.getElementById("OwnerCountry_manual").value = "";
			doc.getElementById("ActHolderEntityType_manual").value = "";
			doc.getElementById("OwnerName_manual").readOnly = true;
			doc.getElementById("OwnerAddress_manual").readOnly = true;
			doc.getElementById("OwnerTin_manual").readOnly = true;
			doc.getElementById("OwnerCountry_manual").disabled = true;
			doc.getElementById("ActHolderEntityType_manual").disabled = true;

			// END

			// REFRESH
			doc.getElementById("IndividualOREntityName").readOnly = false;
			doc.getElementById("IndividualOREntityAddress").readOnly = false;
			doc.getElementById("IndividualOREntityTin").readOnly = false;
			doc.getElementById("IndividualOREntityCountry").disabled = false;

		} else if (accontHolderType == "Entity") {
			/* bug 11604 */
			for ( var i = 0; i < len; i++) {
				$(".owner_radio")[i].checked = false;
			}
			/* bug 11604 */
			doc.getElementById("Part3_tr").style.display = "block";
			doc.getElementById("entityType_tr").style.display = "";
			doc.getElementById("substantial_table_tr").style.display = "block";
			doc.getElementById("individual_radio").checked = false;
			doc.getElementById("Entity_radio").checked = true;// this add when
			// you
			// checked radio
			// button on grid

			// START INDIVIDUAL
			doc.getElementById("IndividualOREntityName").value = "";
			doc.getElementById("IndividualOREntityAddress").value = "";
			doc.getElementById("IndividualOREntityTin").value = "";
			doc.getElementById("IndividualOREntityCountry").value = "";
			/*bog id 11606*/
			doc.getElementById("accDOB").value = "";
			doc.getElementById("AccHolderCIF").value = "";
			/*bog id 11606*/

			// END INDIVIDUAL
			// REFRESH THE SO
			doc.getElementById("OwnerName_manual").readOnly = false;
			doc.getElementById("OwnerAddress_manual").readOnly = false;
			doc.getElementById("OwnerTin_manual").readOnly = false;
			doc.getElementById("OwnerCountry_manual").disabled = false;
			doc.getElementById("ActHolderEntityType_manual").disabled = false;

		}
	} catch (e) {
		alert(e);
	}

}

function addGridData(gridType) {

	var doc = document;
	var valusObj = [];
	var fields = undefined;
	var errormsgId = "accErrorMsg";
	var length = 0;
	var value = undefined;
	var tableId = undefined;
	var uniqueIdentifier = undefined;
	var matchIndexCell = undefined;
	var label = undefined;
	var	url = appPath + "/AccountCreation";
	var param = "option=activeSession";
	

	try {

		var response = ajaxCallManualLongData(url, param);/* begin:- added by KD 24022016 */
		var isNilReport = doc.getElementById("isNilReport").value;
		if (isNilReport == "Y") {
			addGridData_NIL(gridType);// function written in manualForNIL.js
		} else {
			/* end:- added by KD 24022016 */

			doc.getElementById(errormsgId).innerHTML = "&nbsp;";
			doc.getElementById("isIndividualOREntityErrorMsg").innerHTML = "&nbsp;";
			doc.getElementById("errorMsgEntitySo").innerHTML = "&nbsp;";

			if (gridType == "SubstantialOwner") {
				tableId = "ownerList_table";
				fields = [ "giin", "finance_ActNumber",
						"IndividualOREntityName", "IndividualOREntityTin",
						"ActHolderEntityType_manual", "OwnerName_manual",
						"OwnerAddress_manual", "OwnerCountry_manual",
						"OwnerTin_manual", "AccHolderCIF", "accDOB" ];
				// End SO
			} else if (gridType == "accountInformation") {

				tableId = "manual_Data_Grid_Table";
				fields = [ "filerName_manual", "filerAddres_manual",
						"filerCountry_manual", "giin", "isSponsoredEntity",
						"sponsoredEntityName", "sponEntityAddress",
						"sponEntityCountry", "sponEntitygiin",
						"isIntermediary", "interEntityName",
						"interEntityAddress", "interEntityCountry",
						"interEntityGiin", "finance_ActNumber",
						"finance_Country", "finance_balance",
						"finance_Interest", "finance_Dividend",
						"finance_Redemption", "finance_Others",
						"isIndividualOREntity", "IndividualOREntityName",
						"IndividualOREntityAddress",
						"IndividualOREntityCountry", "IndividualOREntityTin",
						"ActHolderEntityType_manual", "AccHolderCIF", "accDOB",
						"isNilReport","filerCategory_manual","sponEntityFilerCategory"
						 ];// isNilReport added by KD
							//Filer category added by shubham on 09 06 2017
			}
			if (!isValidateManual(tableId)) {
				return false;
			}
			if (fields != undefined) {
				length = fields.length;

				var i;
				for (i = 0; i < length; i++) {

					// The below code is just to get the value of radio option
					if (gridType == "accountInformation"
							&& (fields[i] == "isSponsoredEntity"
									|| fields[i] == "isIntermediary" || fields[i] == "isIndividualOREntity")) {
						value = getRadioValue(fields[i]);
					} else {

						value = doc.getElementById(fields[i]).value;
					}
					value = (value == undefined ? "" : value);

					valusObj.push(value);// Push all the values in array
					// str=str+value+"@|";
				}

				if (valusObj.length > 0 && valusObj != undefined)

					addRows(tableId, valusObj);

			}
			return true;
		}
	} catch (e) {
		alert('agregar datos de cuadr�cula--> ' + e);
	}
}

function addRows(tableId, value) {

	var doc = document;
	var cellValue = value;
	var cellLength = 0;
	var row = "";
	var cell = "";
	var tableObj = doc.getElementById(tableId);
	var rowCount = tableObj.rows.length;
	var classes = undefined;
	var width = undefined;
	var columns = undefined;
	var ids = undefined;
	var radiobtnGrpName = undefined;
	var tableName = undefined;
	var key = undefined;
	var indexOfValue = undefined;
	var totalCoundId = undefined;
	var parentKey = undefined;

	try {
		refreshColor(tableId);

		if (tableId == "ownerList_table") {

			fields = [ "giin", "finance_ActNumber", "IndividualOREntityName",
					"IndividualOREntityTin", "ActHolderEntityType_manual",
					"OwnerName_manual", "OwnerAddress_manual",
					"OwnerCountry_manual", "OwnerTin_manual", "AccHolderCIF",
					"accDOB" ];

			// Key is the combination of the filer giin,account
			// no,acctin.ownertin
			// GIIN+ACCNO+TIN_CIF_DOB_SOTIN
			key = "radio_ownerList_table_" + cellValue[0] + "_" + cellValue[1]
					+ "_" + cellValue[3] + "_" + cellValue[8] + "_"
					+ cellValue[9] + "_" + cellValue[10];// child

			if (ownerTableObj[key]) {
				alert("No puede agregar entidades duplicadas.");

				doc.getElementById(key + "_ID").style.backgroundColor = 'red';
				return false;
			}
			// key
			// is
			// entity
			// key

			parentKey = "radio_manual_Data_Grid_Table_" + cellValue[0] + "_"
					+ cellValue[1] + "_" + cellValue[3] + "_" + cellValue[9]
					+ "_" + cellValue[10];// here
			// parent
			// key
			// is
			// Filer
			// GIIN+account
			// no+account
			// Tin
			// to
			// identify
			// its
			// parent
			// account
			columns = [ "AccountNumber", "EntityName", "OwnerName", "OwnerTin",
					"radio" ];
			indexOfValue = [ 1, 2, 5, 8 ];
			cellLength = columns.length;

			ownerTableObj[key] = new ownerTable("", cellValue[0], cellValue[1],
					cellValue[2], cellValue[3], cellValue[4], cellValue[5],
					cellValue[6], cellValue[7], cellValue[8], "", "", "A", "",
					parentKey, cellValue[9], cellValue[10]);

			radiobtnGrpName = "ownerListtableRadio";
			tableName = "ownerList_table";
			/* bug id 11116* changed class name */
			classes = "ipm_n2";
			width = "50";
			totalCoundId = "totalSoCount";

		} else if (tableId == "manual_Data_Grid_Table") {

			// For reference
			/*
			 * fields = [ "filerName_manual", "filerAddres_manual",
			 * "filerCountry_manual", "giin", "isSponsoredEntity",
			 * "sponsoredEntityName", "sponEntityAddress", "sponEntityCountry",
			 * "sponEntitygiin", "isIntermediary", "interEntityName",
			 * "interEntityAddress", "interEntityCountry", "interEntityGiin",
			 * "finance_ActNumber", "finance_Country", "finance_balance",
			 * "finance_Interest", "finance_Dividend", "finance_Redemption",
			 * "finance_Others", "isIndividualOREntity",
			 * "IndividualOREntityName", "IndividualOREntityAddress",
			 * "IndividualOREntityCountry",
			 * "IndividualOREntityTin","AccHolderCIF","accDOB"];
			 */

			totalCoundId = "totalAccountCount";

			// Key is the combination of giin+account+tin
			key = "radio_manual_Data_Grid_Table_" + cellValue[3] + "_"
					+ cellValue[14] + "_" + cellValue[25] + "_" + cellValue[27]
					+ "_" + cellValue[28];

			if (accountInformationObj[key]) {
				alert("No puede agregar cuentas duplicadas.");
				doc.getElementById(key + "_ID").style.backgroundColor = 'red';
				return false;
			}

			columns = [ "FilerGIIN", "SponsorGIIN", "IntermediaryGIIN",
					"CustomerName", "CustomerTIN", "Account Number", "radio" ];
			cellLength = columns.length;
			indexOfValue = [ 3, 8, 13, 22, 25, 14 ];
			accountInformationObj[key] = new AccountInformationTable(
					cellValue[0], cellValue[1], cellValue[2], cellValue[3],
					cellValue[4], cellValue[5], cellValue[6], cellValue[7],
					cellValue[8], cellValue[9], cellValue[10], cellValue[11],
					cellValue[12], cellValue[13], cellValue[14], cellValue[15],
					cellValue[16], cellValue[17], cellValue[18], cellValue[19],
					cellValue[20], cellValue[21], cellValue[22], cellValue[23],
					cellValue[24], cellValue[25], cellValue[26], "", "", "A",
					"", "", "", "", cellValue[27], cellValue[28], "", "", "",
					"", "", "", cellValue[29],cellValue[30],cellValue[31]);
			//changes done by shubham for add rows cellValue[30],cellValue[31]
			radiobtnGrpName = "accountInformationGroupRadio";
			tableName = "manual_Data_Grid_Table";
			/* bug id 11116* changed class name */
			classes = "ipm_n2";/* "ipm_n"; */
			width = "50";
			/**/
			if (getTableCount("ownerList_table") > 0
					&& ACT_GRID_ROW_KEY != undefined) {

				addSOData(cellValue[14], cellValue[22], cellValue[25],
						cellValue[26], key, ACT_GRID_ROW_KEY, cellValue[27],
						cellValue[28]);
				// addGridData("SubstantialOwner");
			}
			/**/
			deleteTables("ownerList_table");

		}
		row = tableObj.insertRow();
		row.id = key + "_ID";

		var i;
		for (i = 0; i < cellLength; i++) {
			cell = row.insertCell(i);
			cell.className = classes;
			cell.style.width = width;
			if (columns[i] == "radio")
				cell.innerHTML = '<input type="radio" class="radio_checked" name="'
						+ radiobtnGrpName
						+ '"  id="'
						+ key
						+ '" onclick="return getOwnerInforamtion('
						+ tableName
						+ ',this);">';
			else {
				// alert();
				cell.innerHTML = '<a data-toggle="tooltip" title="'
						+ cellValue[indexOfValue[i]] + '">'
						+ cellValue[indexOfValue[i]] + '</a>';

			}

		}

		reset(tableId);
		if (totalCoundId != undefined)
			doc.getElementById(totalCoundId).innerHTML = "El n�mero total de registros :"
					+ getTableCount(tableName);
		if (tableId == "manual_Data_Grid_Table") {
			makeReadonlySPON_InterFields();
		}

	} catch (ex) {

		alert('a�adir filas-->' + ex);
	}
}
/*
 * This function load the auto populate the values from json object to the
 * presentation layer on selection of radio button
 */

function getOwnerInforamtion(tableObj, radioObj) {

	var doc = document;
	var fields = undefined;
	var jsonArrayCols = undefined;
	var colsLength = undefined;
	var key = radioObj.id;

	var jsonArrayColsOptionvalue = undefined;
	var fieldsOptionTag = undefined;
	var jsonArrayColsOptionLen = 0;
	var jsonOptionValue = undefined;
	try {

		refreshColor(tableObj.id);
		if (tableObj.id == "ownerList_table") {

			// below for only text field;
			// Note Do For option tag also

			fields = [ "OwnerName_manual", "OwnerAddress_manual",
					"OwnerTin_manual" ];
			jsonArrayCols = [ "entityName", "entityAddress", "entityTin" ];
			fieldsOptionTag = [ "OwnerCountry_manual" ];
			jsonArrayColsOptionvalue = [ "entityCountryCode" ];
			jsonArrayColsOptionLen = jsonArrayColsOptionvalue.length;
			SO_GRID_ROW_KEY = radioObj.id;
		} else if (tableObj.id == "manual_Data_Grid_Table") {

			fields = [ "sponsoredEntityName", "sponEntityAddress",
					"sponEntitygiin", "interEntityName", "interEntityAddress",
					"interEntityGiin", "finance_ActNumber", "finance_balance",
					"finance_Interest", "finance_Dividend",
					"finance_Redemption", "finance_Others",
					"isIndividualOREntity", "IndividualOREntityName",
					"IndividualOREntityAddress", "IndividualOREntityTin",
					"AccHolderCIF", "accDOB" ];
			jsonArrayCols = [ "spName", "spAddress", "spGIIN", "interName",
					"InterAddress", "interGIIN", "accountNumber", "accBalance",
					"accIntrest", "accDividend", "accGrossRedem", "accOthers",
					"accHolderType", "accName", "accAddress", "accTin",
					"accHolderCIF", "accDOB" ];
			fieldsOptionTag = [ "sponEntityCountry", "interEntityCountry",
					"finance_Country", "IndividualOREntityCountry",
					"ActHolderEntityType_manual","filerCategory_manual","sponEntityFilerCategory" ];
			jsonArrayColsOptionvalue = [ "spCountryCode", "interCountryCode",
					"accCurrencyCode", "accCountryCode", "accEntityType",
					"filerCategory_manual","sponEntityFilerCategory" ];
			//changes done by shubham for manual form load and values populate on 12 06 2017 for filer category
			/**/
			ACT_GRID_ROW_KEY = radioObj.id;

			/**/

		}
		if (jsonArrayCols != undefined)
			colsLength = jsonArrayCols.length;
		if (jsonArrayColsOptionvalue != undefined)
			jsonArrayColsOptionLen = jsonArrayColsOptionvalue.length;
		var values = undefined;
		for ( var i = 0; i < colsLength; i++) {

			values = (tableObj.id == "ownerList_table" ? ownerTableObj[key][jsonArrayCols[i]]
					: accountInformationObj[key][jsonArrayCols[i]]);

			if (tableObj.id == "manual_Data_Grid_Table") {
				if (fields[i] == "sponEntitygiin"
						|| fields[i] == "interEntityGiin") {

					if (fields[i] == "sponEntitygiin") {
						if (accountInformationObj[key]["isSponsoredEntity"] == "no") {
							doc.getElementById("sponsoredEntity_no").checked = true;
							displayModules("isSponsoredEntity");
							$('.spon').hide();

							// displayModules('isSponsoredEntity');

							// doc.getElementById("sponsoredEntity_no").click();
						} else if (accountInformationObj[key]["isSponsoredEntity"] == "yes") {
							doc.getElementById("sponsoredEntity_yes").checked = true;
							displayModules("isSponsoredEntity");
							$('.spon').show();

						} else {
							// defualt
						}
					} else if (fields[i] == "interEntityGiin") {
						if (accountInformationObj[key]["isIntermediary"] == "yes") {
							doc.getElementById("isIntermediary_yes").checked = true;
							displayModules("isIntermediary");
							$('.inter').show();
							// displayModules('isIntermediary');
						} else if (accountInformationObj[key]["isIntermediary"] == "no") {
							doc.getElementById("intermediary_no").checked = true;
							displayModules("isIntermediary");
							$('.inter').hide();
							// displayModules('isIntermediary');

						} else {
							// defualt
						}
					}

					setGIIN(values, fields[i]);
				} else if (fields[i] == "isIndividualOREntity") {

					// alert(accountInformationObj[key]["accHolderType"] )
					if (accountInformationObj[key]["accHolderType"] != null
							&& accountInformationObj[key]["accHolderType"] != undefined)
						Check_AccountHolder_Type(accountInformationObj[key]["accHolderType"]);
					// enable added so
					addSORowsOnclickAccountGrid(key);
				} else {
					doc.getElementById(fields[i]).value = values;
				}

				reset("ownerList_table");
			} else if (tableObj.id == "ownerList_table") {
				doc.getElementById(fields[i]).value = values;
			} else {
				// default
			}

			if (tableObj.id == "ownerList_table"
					&& ownerTableObj[key]["submittedFlag"] == "Y") {
				doc.getElementById("OwnerTin_manual").readOnly = true;
				doc.getElementById("finance_ActNumber").readOnly = true;
			} else if (tableObj.id == "manual_Data_Grid_Table"
					&& accountInformationObj[key]["submittedFlag"] == "Y") {
				doc.getElementById("finance_ActNumber").readOnly = true;
			} else {
				doc.getElementById("OwnerTin_manual").readOnly = false;
				doc.getElementById("finance_ActNumber").readOnly = false;
			}

		}
		for ( var i = 0; i < jsonArrayColsOptionLen; i++) {
			jsonOptionValue = (tableObj.id == "ownerList_table" ? ownerTableObj[key][jsonArrayColsOptionvalue[i]]
					: accountInformationObj[key][jsonArrayColsOptionvalue[i]]);
			selectDropDown(fieldsOptionTag[i], jsonOptionValue);// param
		}

	} catch (e) {

		alert(e);
	}

}

function saveManualGridData(event) {
	var doc = document;
	var action = undefined;

	var param = "";

	try {
		/* begin:- added by KD 25022016 */
		var totalRecord = getTableCount("manual_Data_Grid_Table");
		var isNilReport = doc.getElementById("isNilReport").value;
		if (isNilReport == "Y") {
			saveManualGridData_NIL(event);// function written
			// in
			// manualForNIL.js
		} else {
			/* end:- added by KD 25022016 */

			var reportingYear = doc.getElementById("reportingYear").value;
			var submissionType = doc.getElementById("submissionType").value;
			var url = appPath + "/SubmitManualData";

			// ADD SO To ARRAY LIST OF Substantial ARRYA
			for ( var key in ownerTableObj) {
				var object = ownerTableObj[key];
				action = object.action.toUpperCase();

				if (action == "M" || action == "A" || action == "D")
					substantailOwnerArray.push(object);

			}
			// END
			// ADD ACCOUNT INFORMATION
			action = undefined;
			for ( var key in accountInformationObj) {
				var object = accountInformationObj[key];
				action = object.action.toUpperCase();

				if (action == "M" || action == "A" || action == "D")
					accountInformationArray.push(object);

			}
			// END
			/* begin commented by KD on 29022015 */
			// if (event != "submit") {
			/* end commented by KD on 29022015 */

			if (totalRecord == 0) {
				if (accountInformationArray.length == 0
						&& substantailOwnerArray.length == 0) {
					alert("Por favor, agregue al menos un archivo de informaci�n.");
					return false;
				}
			}
			// }
			param = "event="
					+ event
					+ "&submissionType="
					+ submissionType
					+ "&reportingYear="
					+ reportingYear
					+ "&ownerJson="
					+ encodeURIComponent(JSON.stringify(substantailOwnerArray))
					+ "&manualJson="
					+ encodeURIComponent(JSON
							.stringify(accountInformationArray));

			var response = ajaxCallManualLongData(url, param);
			if (response == "success") {
				alert("Los datos han sido guardados / enviados correctamente.");
				location.reload();
				// refresh();
			} else if (response == "sessionExpired") {
				forwardJSP("SessionExpired");
			} else if (response == "Exception") {
				forwardJSP("ExceptionType");
				alert("Intente despu�s de alg�n tiempo o p�ngase en contacto con el administrador del sistema.");

			}
		}
	} catch (e) {
		alert('guardar los datos de la cuadr�cula manual--> ' + e);
	}
	return true;
}

function blank_all_Giin(src, hidden) {
	var doc;
	try {
		doc = document;

		for ( var i = 1; i < 17; i++) {

			doc.getElementById(src + i).value = "";
			doc.getElementById(hidden).value = "";
		}
	} catch (e) {
		alert('blanco--> ' + e);
	}
}

function reset(source) {
	var doc = document;
	var resetField = undefined;
	var len = 0;
	var resetGIIN = undefined;
	var lengths = 0;
	if (source == "ownerList_table") {
		resetField = [ "OwnerName_manual", "OwnerAddress_manual",
				"OwnerCountry_manual", "OwnerTin_manual" ];

	} else if (source == "manual_Data_Grid_Table") {
		resetField = [ "sponsoredEntityName", "sponEntityAddress",
				"sponEntitygiin", "interEntityName", "interEntityAddress",
				"interEntityGiin", "finance_ActNumber", "finance_balance",
				"finance_Interest", "finance_Dividend", "finance_Redemption",
				"finance_Others", "IndividualOREntityName",
				"IndividualOREntityAddress", "IndividualOREntityTin",
				"sponEntityCountry", "interEntityCountry", "finance_Country",
				"IndividualOREntityCountry", "AccHolderCIF", "accDOB" ];
		resetGIIN = [ "text_giin", "text_giin_inter" ];
		lengths = resetGIIN.length;

	} else if (source == "ForgotPassword") {
		resetField = [ "newpassword_forgotpassword",
				"confirmpassword_forgotpassword", "emailForPIN_ForgotPassword",
				"pin_ForgotPassword" ];
	}
	if (resetField != undefined)
		len = resetField.length;
	try {
		var i;
		for (i = 0; i < len; i++) {
			doc.getElementById(resetField[i]).value = "";

		}

		if (lengths > 0) {
			var k = 0;
			for (k; k < lengths; k++) {
				var j = 0;
				var id = undefined;
				var suffix = 0;
				for (j; j < 16; j++) {
					suffix = suffix + 1;
					id = resetGIIN[k] + suffix;
					doc.getElementById(id).value = "";

				}
			}
		}

	} catch (e) {
		alert(e);
	}
}

function isduplicateByCell(Giin, CustomerTIN, AccountNO, SOTIN, tableId) {

	var doc = document;
	var oTable = doc.getElementById(tableId);
	var i;
	var rowLength = oTable.rows.length;
	var flag = false;
	if (rowLength > 1) {
		// refreshColor(tableId);
		for (i = 1; i < rowLength; i++) {
			var oCells = oTable.rows.item(i).cells;
			if (tableId == "manual_Data_Grid_Table") {
				if (Giin.toUpperCase() == oCells[0].firstChild.data
						.toUpperCase()
						&& CustomerTIN.toUpperCase() == oCells[4].firstChild.data
								.toUpperCase()
						&& AccountNO.toUpperCase() == oCells[5].firstChild.data
								.toUpperCase()) {
					oTable.rows[i].style.backgroundColor = 'red';
					flag = true;
					break;
				}

			}
			/*
			 * else if (tableId == "manual_Data_Grid_Table") { }
			 */
		}
	}
	return flag;
}

function refreshColor(tableId) {
	var doc = document;

	var oTable = doc.getElementById(tableId);
	var rowLength = oTable.rows.length;
	var i;
	for (i = 1; i < rowLength; i++) {
		oTable.rows[i].style.backgroundColor = 'white';
	}
}

function isValidateManual(src) {

	var mandatoryFields = undefined;
	var mandatoryLabels = undefined;
	var errorMsg = undefined;
	var len = 0;
	var doc = document;

	try {
		if (src == "ownerList_table") {
			mandatoryFields = [ "finance_ActNumber", "OwnerName_manual",
					"OwnerAddress_manual", "OwnerCountry_manual",
					"OwnerTin_manual", "ActHolderEntityType_manual" ];
			mandatoryLabels = [ "Account Number", "Owner Name",
					"Owner Address", "Owner Country", "Tin of Owner",
					"Enitity Type" ];
			erromsgId = "errorMsgEntitySo";
			len = mandatoryFields.length;
			var j = 0;
			for (j = 0; j < len; j++) {

				if (mandatoryFields[j] == "finance_ActNumber") {
					erromsgId = "accErrorMsg";
				} else {
					erromsgId = "errorMsgEntitySo";
				}

				if (doc.getElementById(mandatoryFields[j]).value == null
						|| doc.getElementById(mandatoryFields[j]).value == undefined
						|| doc.getElementById(mandatoryFields[j]).value == ""
						|| doc.getElementById(mandatoryFields[j]).value == "undefined") {

					doc.getElementById(erromsgId).innerHTML = "El campo "
							+ mandatoryLabels[j] + " no puede estar en blanco";
					doc.getElementById(erromsgId).className = "errorMsg";
					doc.getElementById(mandatoryFields[j]).focus();
					doc.getElementById(mandatoryFields[j]).blur();
					return false;

				}

			}

		} else if (src == "manual_Data_Grid_Table") {

			mandatoryFields = [ "filerName_manual", "filerAddres_manual",
					"filerCountry_manual", "giin","filerCategory_manual", "isSponsoredEntity",
					"isIntermediary", "finance_ActNumber", "finance_Country",
					"finance_balance", "IndividualOREntityName",
					"IndividualOREntityAddress", "IndividualOREntityCountry",
					"isIndividualOREntity" ];
			//added by shubham for mandatory check of filer category on 09 06 2017
			len = mandatoryFields.length;
			errorMsg = "filerGiinErrormsg";
			mandatoryLabels = [ "Filer Name", "Filer Address", "Filer Country",
					"Filer GIIN","Filer Category", "NA", "NA", "Account Number",
					"Currency Code", "Account Balance", "Account Holder Name",
					"Account Holder Address", "Account Holder Country" ];
			var i = 0;
			for (i = 0; i < len; i++) {

				if (mandatoryFields[i] == "finance_ActNumber"
						|| mandatoryFields[i] == "finance_Country"
						|| mandatoryFields[i] == "finance_balance") {
					errorMsg = "accErrorMsg";
				} else if (mandatoryFields[i] == "IndividualOREntityName"
						|| mandatoryFields[i] == "IndividualOREntityAddress"
						|| mandatoryFields[i] == "IndividualOREntityCountry"
						|| mandatoryFields[i] == "IndividualOREntityTin") {
					errorMsg = "isIndividualOREntityErrorMsg";
				}

				if (mandatoryFields[i] == "isSponsoredEntity"
						|| mandatoryFields[i] == "isIntermediary"
						|| mandatoryFields[i] == "isIndividualOREntity") {
					// For Sponser Entity

					// value = getValuesOfradios(mandatoryFields[i]);

					if (getValuesOfradios(mandatoryFields[i]) == "yes"
							|| getValuesOfradios(mandatoryFields[i]) == "Entity") {
						var fieldList;
						var labels;
						var length = 0;
						var ActHolderEntityType;
						var count;
						if (mandatoryFields[i] == "isSponsoredEntity") {
							fieldList = [ "sponsoredEntityName",
									"sponEntityAddress", "sponEntityCountry",
									"sponEntitygiin","sponEntityFilerCategory" ];
							labels = [ "Sponser Name", "Sponser Address",
									"Sponser Country", "Sponser GIIN",
									"Sponsor Filer Category" ];
							//added by shubham on 09 06 2017 for  Sponsor Filer Category
							errorMsg = "filerGiinErrormsg";

						} else if (mandatoryFields[i] == "isIntermediary") {
							fieldList = [ "interEntityName",
									"interEntityAddress", "interEntityCountry",
									"interEntityGiin" ]
							labels = [ "Intermidiary Name",
									"Intermidiary Address",
									"Intermidiary Country", "Intermidiary GIIN" ];
							errorMsg = "filerGiinErrormsg";

						} else if (mandatoryFields[i] == "isIndividualOREntity") {

							ActHolderEntityType = doc
									.getElementById("ActHolderEntityType_manual").value;

							// getTableCount("ownerList_table"));
							count = getTableCount("ownerList_table");
							if ((ActHolderEntityType == "FATCA101" || ActHolderEntityType == "FATCA102")
									&& count == 0) {
								fieldList = [ "finance_ActNumber",
										"OwnerName_manual",
										"OwnerAddress_manual",
										"OwnerCountry_manual",
										"OwnerTin_manual",
										"ActHolderEntityType_manual" ];
								labels = [ "Account Number", "Owner Name",
										"Owner Address", "Owner Country",
										"Tin of Owner", "Enitity Type" ];
								errorMsg = "errorMsgEntitySo";
							} else {
								fieldList = [ "ActHolderEntityType_manual" ];
								labels = [ "Entity Type" ];
								errorMsg = "isIndividualOREntityErrorMsg";
							}

						}
						var j = 0;
						length = fieldList.length;
						doc.getElementById(errorMsg).innerHTML = "&nbsp;";
						for (j = 0; j < length; j++) {

							if (doc.getElementById(fieldList[j]).value == null
									|| doc.getElementById(fieldList[j]).value == undefined
									|| doc.getElementById(fieldList[j]).value == ""
									|| doc.getElementById(fieldList[j]).value == "undefined") {

								doc.getElementById(errorMsg).innerHTML = "El campo "
										+ labels[j] + " no puede estar en blanco";
								doc.getElementById(errorMsg).className = "errorMsg";
								doc.getElementById(fieldList[j]).focus();
								doc.getElementById(fieldList[j]).blur();
								return false;

							}

						}
						if (count == 0
								&& mandatoryFields[i] == "isIndividualOREntity"
								&& (ActHolderEntityType == "FATCA101" || ActHolderEntityType == "FATCA102")) { //
							// isValidateManual("ownerList_table");
							addGridData("SubstantialOwner");
						}

					}
				} else {

					doc.getElementById(errorMsg).innerHTML = "&nbsp;";
					if (doc.getElementById(mandatoryFields[i]).value == null
							|| doc.getElementById(mandatoryFields[i]).value == undefined
							|| doc.getElementById(mandatoryFields[i]).value == ""
							|| doc.getElementById(mandatoryFields[i]).value == "undefined") {
						doc.getElementById(errorMsg).innerHTML = "El campo "
								+ mandatoryLabels[i] + " no puede estar en blanco";
						// doc.getElementById(errorMsg).focus();
						doc.getElementById(errorMsg).className = "errorMsg";
						doc.getElementById(mandatoryFields[i]).focus();
						doc.getElementById(mandatoryFields[i]).blur();
						return false;

					}

				}

			}

			// IndividualOREntityTin,"Account Holder TIN"

		}
		// common For Entity Type and Account

		if (doc.getElementById("IndividualOREntityTin").value == undefined
				|| doc.getElementById("IndividualOREntityTin").value == ""
				|| doc.getElementById("IndividualOREntityTin").value
						.toUpperCase() == "NULL") {

			if (doc.getElementById("accDOB").value == undefined
					|| doc.getElementById("accDOB").value == ""
					|| doc.getElementById("accDOB").value.toUpperCase() == "NULL")

			{
				doc.getElementById("isIndividualOREntityErrorMsg").innerHTML = "Proporcionar TIN o ambos CIF y DOB.";
				doc.getElementById("accDOB").focus();
				doc.getElementById("isIndividualOREntityErrorMsg").className = "errorMsg";
				return false;

			} else if (doc.getElementById("AccHolderCIF").value == undefined
					|| doc.getElementById("AccHolderCIF").value == ""
					|| doc.getElementById("AccHolderCIF").value.toUpperCase() == "NULL") {
				doc.getElementById("isIndividualOREntityErrorMsg").innerHTML = "Proporcionar TIN o ambos CIF y DOB.";
				doc.getElementById("isIndividualOREntityErrorMsg").className = "errorMsg";
				doc.getElementById("AccHolderCIF").focus();
				return false;

			}

		}

	} catch (e) {
		alert(e);
	}
	return true;
}
function modifiedRows(tableName, radioOptionName) {
	var selectedId = undefined;

	try {

		/* begin:- added by KD 24022016 */
		var doc = document;
		var isNilReport = doc.getElementById("isNilReport").value;
		if (isNilReport == "Y") {
			modifiedRows_NIL(tableName, radioOptionName);// function written
			// in
			// manualForNIL.js
		} else {
			/* end:- added by KD 24022016 */

			refreshColor(tableName);
			var response = selectUserListRow(tableName, radioOptionName);
			if (response > -1) {
				selectedId = $('input[name="' + radioOptionName + '"]:checked')
						.attr('id');

			} else {
				return false;
			}
			if (selectedId != undefined) {

				if (!isValidateManual(tableName)) {
					return false;
				}

				modifyValues(tableName, selectedId);

			} else {

			}

		}
	} catch (e) {
		alert(e);
	}
	return true;
}

function modifyValues(tableName, selectedRow) {

	var doc = document;
	var selectedOptionObj = doc.getElementById(selectedRow);

	var selectedrowID = selectedRow;// selectedRow.substring(6,selectedRow.length);
	var row = doc.getElementById(selectedRow + "_ID");
	var cell = row.cells;
	var fields = undefined;
	var length = 0;
	var cellValue = [];
	// var prevKeyOrId = selectedRow;
	var currentOptionkeyOrId = undefined;
	// var currentRowkeyOrId = undefined;
	var key = undefined;
	// Always keep non-display cell in last columns
	// Always maintain the order of the cell value which you need to
	// display to
	// make maintaince easy.
	if (tableName == 'ownerList_table') {
		fields = [ "OwnerName_manual", "OwnerAddress_manual",
				"OwnerCountry_manual", "OwnerTin_manual" ];
	} else if (tableName == 'manual_Data_Grid_Table') {

		fields = [ "filerName_manual", "filerAddres_manual",
				"filerCountry_manual", "giin", "isSponsoredEntity",
				"sponsoredEntityName", "sponEntityAddress",
				"sponEntityCountry", "sponEntitygiin", "isIntermediary",
				"interEntityName", "interEntityAddress", "interEntityCountry",
				"interEntityGiin", "finance_ActNumber", "finance_Country",
				"finance_balance", "finance_Interest", "finance_Dividend",
				"finance_Redemption", "finance_Others", "isIndividualOREntity",
				"IndividualOREntityName", "IndividualOREntityAddress",
				"IndividualOREntityCountry", "IndividualOREntityTin",
				"ActHolderEntityType_manual", "AccHolderCIF", "accDOB",
				"isNilReport","filerCategory_manual","sponEntityFilerCategory" ];
		
		//changes made by shubham for save changes on 12 06 2017 to display all data
	}
	if (fields != undefined)
		length = fields.length;
	try {
		if (length > 0) {
			var i;
			for (i = 0; i < length; i++) {

				if (tableName == "manual_Data_Grid_Table"
						&& (fields[i] == "isSponsoredEntity"
								|| fields[i] == "isIntermediary" || fields[i] == "isIndividualOREntity")) {
					value = getRadioValue(fields[i]);
				} else {
					value = doc.getElementById(fields[i]).value;
				}

				// value = doc.getElementById(fields[i]).value;
				value = (value == undefined ? "" : value);
				cellValue.push(value);

			}
			if (tableName == "ownerList_table") {

				var filerGIIN = ownerTableObj[selectedRow]["filerGIIN"];
				var accountNumber = ownerTableObj[selectedRow]["accountNumber"];
				var accHolderCIF = ownerTableObj[selectedRow]["accHolderCIF"];
				var accDOB = ownerTableObj[selectedRow]["accDOB"];
				var accName = ownerTableObj[selectedRow]["accName"];
				var accTin = ownerTableObj[selectedRow]["accTin"];
				var entityType = ownerTableObj[selectedRow]["entityType"];
				var cordocRefID = ownerTableObj[selectedRow]["corrDocrefID"];
				var docRefID = ownerTableObj[selectedRow]["docrefid"];
				// var submittedFlag =
				// ownerTableObj[selectedRow]["submittedFlag"];
				var rowId = ownerTableObj[selectedRow]["rowID"];
				// Current key is the combination of filer
				// giin,account,tin,owner tin

				/**/

				var newAct = doc.getElementById("finance_ActNumber").value;
				var newActTin = doc.getElementById("IndividualOREntityTin").value;
				var newActHolderCif = doc.getElementById("AccHolderCIF").value;
				var newActDob = doc.getElementById("accDOB").value;
				var newActName = doc.getElementById("IndividualOREntityName").value;
				var newEntityType = doc
						.getElementById("ActHolderEntityType_manual").value;

				var newKeyOrId = "radio_ownerList_table_" + filerGIIN + "_"
						+ newAct + "_" + newActTin + "_" + cellValue[3] + "_"
						+ newActHolderCif + "_" + newActDob;

				if (newKeyOrId != SO_GRID_ROW_KEY) {

					if (selectedRow != newKeyOrId && ownerTableObj[newKeyOrId]) {
						alert("No se puede modificar la Entidad duplicada.");

						doc.getElementById(newKeyOrId + "_ID").style.backgroundColor = 'red';
						return false;
					}
					cell[0].innerHTML = newAct;
					cell[1].innerHTML = newActName;
					cell[2].innerHTML = cellValue[0];
					cell[3].innerHTML = cellValue[3];
					parentKey = "radio_manual_Data_Grid_Table_" + filerGIIN
							+ "_" + newAct + "_" + newActTin + "_"
							+ newActHolderCif + "_" + newActDob;
					// delete ownerTableObj[selectedRow];
					ownerTableObj[newKeyOrId] = new ownerTable(rowId,
							filerGIIN, newAct, newActName, newActTin,
							newEntityType, cellValue[0], cellValue[1],
							cellValue[2], cellValue[3], cordocRefID, docRefID,
							"M", "save", parentKey, newActHolderCif, newActDob);
					// END // Set the New row ID Of the selected modified rows
					// selectedOptionObj.setAttribute("id", newKeyOrId);
					// row.setAttribute("id", newKeyOrId + "_ID");
				}

				else {

					currentOptionkeyOrId = "radio_ownerList_table_" + filerGIIN
							+ "_" + accountNumber + "_" + accTin + "_"
							+ cellValue[3] + "_" + accHolderCIF + "_" + accDOB;
					// currentOptionkeyOrId);
					// Current key is the combination of filer
					// giin,account,account
					// tin,owner tine

					if (selectedRow != currentOptionkeyOrId
							&& ownerTableObj[currentOptionkeyOrId]) {
						alert("No se puede modificar la Entidad duplicada.");
						doc.getElementById(currentOptionkeyOrId + "_ID").style.backgroundColor = 'red';
						return false;
					}

					cell[0].innerHTML = accountNumber;
					cell[1].innerHTML = accName;
					cell[2].innerHTML = cellValue[0];
					cell[3].innerHTML = cellValue[3];

					parentKey = "radio_manual_Data_Grid_Table_" + filerGIIN
							+ "_" + accountNumber + "_" + accTin + "_"
							+ accHolderCIF + "_" + accDOB;// here
					// parent
					// key
					// is
					// Filer
					// GIIN+account
					// no+account
					// Tin
					// to
					// identify
					// its
					// parent
					// account
					// START Modify data into json Object
					// FIRST DELETE
					// THEN ADD

					// alert("New Key:"+currentOptionkeyOrId)
					delete ownerTableObj[selectedRow];
					ownerTableObj[currentOptionkeyOrId] = new ownerTable(rowId,
							filerGIIN, accountNumber, accName, accTin,
							entityType, cellValue[0], cellValue[1],
							cellValue[2], cellValue[3], cordocRefID, docRefID,
							"M", "save", parentKey, accHolderCIF, accDOB);

					// END
					// Set the New row ID Of the selected modified rows
					selectedOptionObj.setAttribute("id", currentOptionkeyOrId);
					row.setAttribute("id", currentOptionkeyOrId + "_ID");

				}
				reset("ownerList_table");
				// END

			} else if (tableName == "manual_Data_Grid_Table") {

				key = "radio_manual_Data_Grid_Table_" + cellValue[3] + "_"
						+ cellValue[14] + "_" + cellValue[25] + "_"
						+ cellValue[27] + "_" + cellValue[28];
				if (selectedRow != key && accountInformationObj[key]) {
					alert("No se puede modificar la Entidad duplicada.");
					doc.getElementById(key + "_ID").style.backgroundColor = 'red';
					return false;
				}

				// deleteFromJSONObject(tableName, selectedRow);
				var corrDocrefID = accountInformationObj[selectedRow]["corrDocrefID"];
				var docrefid = accountInformationObj[selectedRow]["docrefid"];
				var mesgRefID = accountInformationObj[selectedRow]["mesgRefID"];
				var corrMesggRefID = accountInformationObj[selectedRow]["corrMesggRefID"];
				var submittedFlag = accountInformationObj[selectedRow]["submittedFlag"];
				var rowId = accountInformationObj[selectedRow]["rowId"];
				var spDocRefID = accountInformationObj[selectedRow]["spDocRefID"];
				var spCorrDocRefID = accountInformationObj[selectedRow]["spCorrDocRefID"];
				var interDocRefID = accountInformationObj[selectedRow]["interDocRefID"];
				var interCorrDocRefID = accountInformationObj[selectedRow]["interCorrDocRefID"];
				var filerDocRefID = accountInformationObj[selectedRow]["filerDocRefID"];
				var filerCorrDocRefID = accountInformationObj[selectedRow]["filerCorrDocRefID"];
				// var isNilReport =
				// accountInformationObj[selectedRow]["isNilReport"];
				delete accountInformationObj[selectedRow];

				var cellIndexValue = [ 3, 8, 13, 22, 25, 14 ];
				var j;
				var length = cellIndexValue.length;
				for (j = 0; j < length; j++) {
					cell[j].innerHTML = cellValue[cellIndexValue[j]];
				}

				accountInformationObj[key] = new AccountInformationTable(
						cellValue[0], cellValue[1], cellValue[2], cellValue[3],
						cellValue[4], cellValue[5], cellValue[6], cellValue[7],
						cellValue[8], cellValue[9], cellValue[10],
						cellValue[11], cellValue[12], cellValue[13],
						cellValue[14], cellValue[15], cellValue[16],
						cellValue[17], cellValue[18], cellValue[19],
						cellValue[20], cellValue[21], cellValue[22],
						cellValue[23], cellValue[24], cellValue[25],
						cellValue[26], corrDocrefID, docrefid, "M",
						submittedFlag, rowId, mesgRefID, corrMesggRefID,
						cellValue[27], cellValue[28], spDocRefID,
						spCorrDocRefID, interDocRefID, interCorrDocRefID,
						filerDocRefID, filerCorrDocRefID, cellValue[29],
						cellValue[30],cellValue[31]);
				
		//changes done by shubham for filer category in save changes on 12 06 2017 cellValue[30],cellValue[31]
				// Set The New ID
				// alert("M:"+accountInformationObj[key]["rowId"]);
				selectedOptionObj.setAttribute("id", key);
				row.setAttribute("id", key + "_ID");

				// START UPDATE THE SO OBJECT
				/*
				 * parameters
				 * 
				 * accountNo,accName,accTin,entityType,orignalParentkey,oldParentKey
				 */
				// Check if it is entity type and have some entity
				// +"len"+doc.getElementById("ownerList_table").rows.length);
				// doc.getElementById("ownerList_table").rows.length);
				if (cellValue[21] == "Entity"
						&& doc.getElementById("ownerList_table").rows.length > 1)
					updateTheSOTables(cellValue[14], cellValue[22],
							cellValue[25], cellValue[26], key, selectedRow,
							cellValue[27], cellValue[28]);
				// END

			} else {
				// no parameter to check default
			}

			// END NEW CODE

		}
		alert("Los datos han sido modificados con �xito");
	} catch (e) {
		alert("modificar filas 1 " + e);
	}

}

function deleteRow(tableName, radioOptionName) {
	// /ownerListtableRadio

	var doc = document;
	var entityType = undefined;
	try {
		/* begin:- added by KD 24022016 */
		var isNilReport = doc.getElementById("isNilReport").value;
		if (isNilReport == "Y") {
			deleteRow_NIL(tableName, radioOptionName);// function written in
			// manualForNIL.js
		} else {
			entityType = doc.getElementById("ActHolderEntityType_manual").value;
			if (entityType == "FATCA101" || entityType == "FATCA102") {
				if (tableName == "ownerList_table") {
					var totalRecord = 0;
					// deletedRecord set equal to 1 because we already
					// have clicked delete button.
					var deletedRecord = 1;
					for ( var a in ownerTableObj) {
						var key = a;
						totalRecord = totalRecord + 1;
						var object = ownerTableObj[key]; //
						if (object.action == "D") {
							deletedRecord = deletedRecord + 1;
						}

					}
					if (totalRecord == deletedRecord) {
						alert("No puede eliminar todos los registros de Propietario sustancial para este tipo de entidad.");
						return false;
					}
				}
			}
			/* end:- added by KD 24022016 */

			var totalCoundId = "totalAccountCount";

			if (!deleteFromJSONObject(tableName, radioOptionName)) {
				return false;
			}

			if (totalCoundId != undefined) {
				doc.getElementById(totalCoundId).innerHTML = "El n�mero total de registros :"
						+ getTableCount(tableName);
			}
			// }
		}
	} catch (e) {
		alert("excepci�n:" + e);
	}

}
function deleteFromJSONObject(tableName, radioOptionName) {
	var doc = document;
	var response = undefined;
	var obj = doc.getElementById(tableName);
	response = selectUserListRow(tableName, radioOptionName);
	if (response == "-1")
		return false;
	var selectedId = $('input[name="' + radioOptionName + '"]:checked').attr(
			'id');
	// alert(selectedId);
	doc.getElementById(selectedId + "_ID").style.backgroundColor = 'red';
	var submittedFlag = undefined;
	if (tableName == "ownerList_table") {
		if (ownerTableObj[selectedId] != undefined) {

			submittedFlag = ownerTableObj[selectedId]["submittedFlag"];
			if (submittedFlag !== "" && submittedFlag != undefined)// Means
				// data has
				// been
				// saved in
				// DB
				ownerTableObj[selectedId]["action"] = 'D';
			else {
				delete ownerTableObj[selectedId];
			}

		}
	} else if (tableName == "manual_Data_Grid_Table") {

		if (accountInformationObj[selectedId] != undefined) {

			submittedFlag = accountInformationObj[selectedId]["submittedFlag"];

			if (submittedFlag !== "" && submittedFlag != undefined)// Means
			// data has
			// been
			// saved in
			// DB
			{
				accountInformationObj[selectedId]["action"] = 'D';
				deleteSO(selectedId, submittedFlag);
			} else {
				deleteSO(selectedId, submittedFlag);
				delete accountInformationObj[selectedId];

			}

			// deleteSO(selectedId,submittedFlag);

		}
	} else {

	}

	if (submittedFlag != "" && submittedFlag != undefined)
		alert("Para eliminar las filas, confirme el cambio o haga clic en guardar o enviar el bot�n.");
	else {
		if (response !== -1) {
			obj.deleteRow(response + 1);
			alert("Borrado exitosamente");
		}
	}

	return true;
}

function getRadioValue(fields) {

	var value = undefined;
	value = $("input[name='" + fields + "']:checked").val();
	return value;
}

function getDataOnLoad() {

	var doc = document;
	var reportingYear = doc.getElementById("reportingYear").value;
	var url = appPath + "/SubmitManualData?event=load&reportingYear="
			+ reportingYear;
	var response = undefined;
	// var ids = undefined;
	// var rowNum = 0;
	var key = undefined;
	var SOTableObj = undefined;
	var jsonCoulmns = undefined;
	var radiobtnGrpName = undefined;
	var tableName = undefined;
	var row = undefined;
	var cell = undefined;
	/* bug id 11116 */
	var classes = "ipm_n2";
	var width = "50";
	var lengthObj = 0;
	var totalcountID = undefined;
	try {
		// First Get The Drop down Combo
		// 2nd Paramete must be the column name of the master table ;
		getMasterDropDown("ManualRegistration", "Country");
		getMasterDropDown("ManualRegistration", "Currency");
		getMasterDropDown("ManualRegistration", "EntityType");
		//changes for filer category master table by shubham on 09 06 2017
		getMasterDropDown("ManualRegistration", "FILER_CATEGORY");
		getMasterDropDown("ManualRegistration", "FILCATSPON");
		
		response = ajaxCallManual(url);
		if (response.length > 0) {
			var Collection = JSON.parse(response);
			if (Collection != undefined) {

				for (b in Collection) {
					var objects = Collection[b];
					// 0th index represent sunbtantial owner details

					if (b == 0) {
						// Substantial Owner
						for (a in objects) {

							// Key is the combination of the filer giin,account
							// no,acctin.ownertin

							key = "radio_ownerList_table_"
									+ objects[a]["filerGIIN"] + "_"
									+ objects[a]["accountNumber"] + "_"
									+ objects[a]["accTin"] + "_"
									+ objects[a]["entityTin"] + "_"
									+ objects[a]["accHolderCIF"] + "_"
									+ objects[a]["accDOB"];
							ownerTableObj[key] = new ownerTable(
									objects[a]["rowId"],
									objects[a]["filerGIIN"],
									objects[a]["accountNumber"],
									objects[a]["accName"],
									objects[a]["accTin"],
									objects[a]["entityType"],
									objects[a]["entityName"],
									objects[a]["entityAddress"],
									objects[a]["entityCountryCode"],
									objects[a]["entityTin"],
									objects[a]["corrDocrefID"],
									objects[a]["docrefid"], "load",
									objects[a]["submittedFlag"],
									objects[a]["parentKey"],
									objects[a]["accHolderCIF"],
									objects[a]["accDOB"]);
							// On Load keep the action load mean already in db
							// if
							// there is any change then it would became M/A

						}

					} else if (b == 1) {
						// Account Information

						row = undefined;
						SOTableObj = undefined;
						rowNum = 0;
						lengthObj = 0;
						SOTableObj = doc
								.getElementById("manual_Data_Grid_Table");
						jsonCoulmns = [ "fiGIIn", "spGIIN", "interGIIN",
								"accName", "accTin", "accountNumber" ];
						radiobtnGrpName = "accountInformationGroupRadio";
						lengthObj = jsonCoulmns.length + 1;
						totalcountID = "totalAccountCount";

						tableName = "manual_Data_Grid_Table";
						for (a in objects) {

							/* begin:- added by KD 26022016 */
							if (objects[a]["isNilReport"] == "Y") {
								ISNILREPORT = "Y";
								/* bug id 11366,11369 start */

								key = "radio_manual_Data_Grid_Table_"
										+ objects[a]["fiGIIn"] + "_"
										+ objects[a]["spGIIN"] + "_"
										+ objects[a]["interGIIN"];
							} else {
								ISNILREPORT = "N";
								key = "radio_manual_Data_Grid_Table_"
										+ objects[a]["fiGIIn"] + "_"
										+ objects[a]["accountNumber"] + "_"
										+ objects[a]["accTin"] + "_"
										+ objects[a]["accHolderCIF"] + "_"
										+ objects[a]["accDOB"];
								/* bug id 11366,11369 end */
							}
							/* end:- added by KD 26022016 */
							accountInformationObj[key] = new AccountInformationTable(
									objects[a]["fiName"],
									objects[a]["fiAddress"],
									objects[a]["fiCountryCode"],
									objects[a]["fiGIIn"],
									objects[a]["isSponsoredEntity"],
									objects[a]["spName"],
									objects[a]["spAddress"],
									objects[a]["spCountryCode"],
									objects[a]["spGIIN"],
									objects[a]["isIntermediary"],
									objects[a]["interName"],
									objects[a]["InterAddress"],
									objects[a]["interCountryCode"],
									objects[a]["interGIIN"],
									objects[a]["accountNumber"],
									objects[a]["accCurrencyCode"],
									objects[a]["accBalance"],
									objects[a]["accIntrest"],
									objects[a]["accDividend"],
									objects[a]["accGrossRedem"],
									objects[a]["accOthers"],
									objects[a]["accHolderType"],
									objects[a]["accName"],
									objects[a]["accAddress"],
									objects[a]["accCountryCode"],
									objects[a]["accTin"],
									objects[a]["accEntityType"],
									objects[a]["corrDocrefID"],
									objects[a]["docrefid"], "load",
									objects[a]["submittedFlag"],
									objects[a]["rowId"],
									objects[a]["mesgRefID"],
									objects[a]["corrMesggRefID"],
									objects[a]["accHolderCIF"],
									objects[a]["accDOB"],
									objects[a]["spDocRefID"],
									objects[a]["spCorrDocRefID"],
									objects[a]["interDocRefID"],
									objects[a]["interCorrDocRefID"],
									objects[a]["filerDocRefID"],
									objects[a]["filerCorrDocRefID"],
									/* begin:- added by KD 26022016 */
									objects[a]["isNilReport"],
									//changes done by shubham on 09 06 2017 for filer category
									objects[a]["filerCategory_manual"],
									objects[a]["sponEntityFilerCategory"]
									

							);

							// On Load keep the submitted sucess mean already in
							// db if there is any change then it would became
							// blank
							row = SOTableObj.insertRow();
							row.id = key + "_ID";
							var i;

							for (i = 0; i < lengthObj; i++) {
								cell = row.insertCell(i);
								cell.className = classes;
								cell.style.width = width;
								// alert(i +"value="+objects[a][jsonCoulmns[i]])
								if (i == lengthObj - 1)
									cell.innerHTML = '<input type="radio" name="'
											+ radiobtnGrpName
											+ '"  id="'
											+ key
											+ '" onclick="return getOwnerInforamtion('
											+ tableName + ',this);">';
								else
									cell.innerHTML = (objects[a][jsonCoulmns[i]] == undefined
											|| objects[a][jsonCoulmns[i]] == "null" ? "&nbsp"
											: objects[a][jsonCoulmns[i]]);

							}
						}

					} else if (b == 2) {
						// Do when It more array of object
						// Its Filer Information its not a array
						var fieldsarray = [ "filerName_manual",
								"filerAddres_manual", "filerCountry_manual",
								"giin" ];
						var jsonatr = [ "name", "address", "country", "gIIN" ];
						var jsonlen = jsonatr.length;
						var value = undefined;
						if (objects != undefined) {
							var i;
							for (i = 0; i < jsonlen; i++) {
								value = (objects[jsonatr[i]] == undefined ? ""
										: objects[jsonatr[i]]);

								if (fieldsarray[i] == "giin") {
									setGIIN(value, "giin");
									var i = 0;
									for (i = 1; i <= 16; i++) {
										// alert(i)
										doc.getElementById("text" + i).readOnly = true;
										doc.getElementById("text" + i).className += " "
												+ "readOnlyClass";
									}
								} else {
									doc.getElementById(fieldsarray[i]).value = value;

									if (fieldsarray[i] == "filerCountry_manual")
										doc.getElementById(fieldsarray[i]).disabled = true;
									else
										doc.getElementById(fieldsarray[i]).readOnly = true;
									doc.getElementById(fieldsarray[i]).className += " "
											+ "readOnlyClass";
								}

							}

						}

					}
					if (tableName != undefined && tableName != "")
						doc.getElementById(totalcountID).innerHTML = "El n�mero total de registros :"
								+ getTableCount(tableName);

				}

			}
		}

		setmaximumLength("manualForm");
		/* begin:- added by KD 26022016 */
		chkForNilReport(ISNILREPORT); // function written in manulaforNil js
		// file
		/* begin:- added by KD 26022016 */

	} catch (e) {
		alert("excepci�n: " + e);
	}

}

function ajaxCallManual(url) {
	var responses = "";
	try {

		var xhttp;
		if (window.XMLHttpRequest) {
			// code for modern browsers
			xhttp = new XMLHttpRequest();
		} else {
			// code for IE6, IE5
			xhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				responses = xhttp.responseText;
				// alert(responses);

			}
		};
		xhttp.open("POST", url, false);
		xhttp.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		xhttp.send(null);
		// alert("done "+responses);
		return responses;
	} catch (ex) {
		alert("Hay alg�n problema por favor intente despu�s de alg�n tiempo o p�ngase en contacto con el administrador del sistema."
				+ ex);
		return responses;

	}

}

function ajaxCallManualLongData(url, parameter) {
	var responses = "";
	try {

		var xhttp;
		if (window.XMLHttpRequest) {
			// code for modern browsers
			xhttp = new XMLHttpRequest();
		} else {
			// code for IE6, IE5
			xhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				responses = xhttp.responseText;
				// alert(responses);

			}
		};
		xhttp.open("POST", url, false);
		xhttp.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		xhttp.send(parameter);
		// alert("done "+responses);
		return responses;
	} catch (ex) {
		alert("Hay alg�n problema por favor intente despu�s de alg�n tiempo o p�ngase en contacto con el administrador del sistema."
				+ ex);
		return responses;

	}

}

function deleteTables(table) {
	var doc = document;
	var elmtTable = doc.getElementById(table);
	var length = elmtTable.rows.length;
	var x;
	var totalCoundId = undefined;
	for (x = length - 1; x > 0; x--) {
		elmtTable.deleteRow(x);
	}

	if (table == "ownerList_table")
		totalCoundId = "totalSoCount";
	else
		totalCoundId = "totalAccountCount";
	if (table != "userApprovalTable")
		if (totalCoundId != undefined)
			doc.getElementById(totalCoundId).innerHTML = "El n�mero total de registros :"
					+ getTableCount(table);
}

function refresh() {

	substantailOwnerArray.length = 0;
	accountInformationArray.length = 0;
	ownerTableObj.length = 0;
	accountInformationObj.length = 0;
	deleteTables("ownerList_table");
	deleteTables("manual_Data_Grid_Table");
	reset("ownerList_table");
	reset("manual_Data_Grid_Table");
	/* after data has been saved into DB refres */
	getDataOnLoad();
}
function getTableCount(tableID) {
	var doc = document;
	var elmtTable = doc.getElementById(tableID);
	var length = elmtTable.rows.length;
	return length - 1;
}
function setGIIN(giin, src) {
	var prefix = undefined;
	var length = giin.length;
	var doc = document;

	if (src == "giin") {
		prefix = "text";
	} else if (src == "sponEntitygiin") {
		prefix = "text_giin";
	} else if (src == "interEntityGiin") {
		prefix = "text_giin_inter";

	} else if (src == "fi_giin") {
		prefix = "edit_text_giin";
	} else if (src == "GIIN_adminApproval") {
		prefix = "admin_text_giin";
	} else {
		giin = undefined;
	}
	var i = 0;
	var id = undefined;
	var suffix = 0;

	// alert(prefix);
	// alert(giin);
	if (giin != undefined) {

		blank_all_Giin(prefix, src);

		for (i; i < length; i++) {

			suffix = suffix + 1;
			id = prefix + suffix;

			var char = (giin.charAt(i) == undefined ? "" : giin.charAt(i));

			if (char != ".") {

				doc.getElementById(id).value = char;
			} else
				suffix = suffix - 1;

			doc.getElementById(src).value = doc.getElementById(src).value
					+ char;

		}
	}

}

function getValuesOfradios(radioGroupName) {

	var doc = document;
	var rates = doc.getElementsByName(radioGroupName);
	var rate_value = undefined;
	var i = 0;
	var len = rates.length;
	for (i = 0; i < len; i++) {
		if (rates[i].checked) {
			rate_value = rates[i].value;

		}
	}

	return rate_value;
}

function displayModules(src) {

	var doc = document;
	doc.getElementById("filerGiinErrormsg").innerHTML = "&nbsp";
	var fieldArrayText = undefined;
	var fieldArrayGiin = undefined;
	var fieldArraySelect = undefined;
	var lenText = 0;
	var lenSelect = 0;
	var doc = document;
	var isSponserIntermediary = undefined;
	var hiddenGiin = undefined;
	var i;
	var j;
	var k;
	if (src == "isSponsoredEntity") {
		fieldArrayText = [ "sponsoredEntityName", "sponEntityAddress",
				"sponEntityCountry" ];
		fieldArrayGiin = "text_giin";
		hiddenGiin = "sponEntitygiin";
		fieldArraySelect = [ "sponEntityCountry" ];
	}
	if (src == "isIntermediary") {
		fieldArrayText = [ "interEntityName", "interEntityAddress",
				"interEntityCountry" ];
		fieldArrayGiin = "text_giin_inter";
		hiddenGiin = "interEntityGiin";
		fieldArraySelect = [ "interEntityCountry" ];
	}

	if (fieldArrayText != undefined)
		lenText = fieldArrayText.length;

	if (fieldArraySelect != undefined)
		lenSelect = fieldArraySelect.length;

	isSponserIntermediary = getValuesOfradios(src);
	doc.getElementById(hiddenGiin).value = "";
	if (isSponserIntermediary != undefined && isSponserIntermediary == "no") {
		for (i = 0; i < lenText; i++) {

			doc.getElementById(fieldArrayText[i]).value = "";
			doc.getElementById(fieldArrayText[i]).readOnly = true;
		}
		for (j = 0; j < lenSelect; j++) {
			// alert(fieldArraySelect[j]);
			doc.getElementById(fieldArraySelect[j]).disabled = true;
		}

		for (k = 1; k <= 16; k++) {
			// alert(fieldArrayGiin+k);
			doc.getElementById(fieldArrayGiin + k).value = "";
			doc.getElementById(fieldArrayGiin + k).readOnly = true;
		}

	} else {

		for (i = 0; i < lenText; i++) {
			// doc.getElementById(fieldArrayText[i]).value = "";
			doc.getElementById(fieldArrayText[i]).readOnly = false;
		}
		for (i = 0; i < lenSelect; i++) {
			doc.getElementById(fieldArraySelect[i]).disabled = false;
		}
		for (i = 1; i <= 16; i++) {
			// doc.getElementById(fieldArrayGiin + i).value = "";
			doc.getElementById(fieldArrayGiin + i).readOnly = false;
		}

	}

}

function setmaximumLength(src) {

	var fields = undefined;
	var maximumlen = undefined;
	var len = 0;
	var doc = document;

	try {
		if (src == "manualForm") {

			fields = [ "sponsoredEntityName", "sponEntityAddress",
					"interEntityName", "interEntityAddress",
					"finance_ActNumber", "finance_balance", "finance_Interest",
					"finance_Dividend", "finance_Redemption", "finance_Others",
					"IndividualOREntityName", "IndividualOREntityAddress",
					"IndividualOREntityTin", "OwnerName_manual",
					"OwnerAddress_manual", "OwnerTin_manual", "AccHolderCIF" ];
			maximumlen = [ "50", "255", "50", "255", "30", "20", "20", "20",
					"20", "20", "50", "255", "9", "50", "255", "9", "255" ];

		}
		//changes done by shubham for tin length fixed to 9 by shubham
		if (src == "userRegistration") {
			fields = [ "user_email", "user_name", "fi_address", "user_phone",
					"user_empid", "user_designation", "fi_password_Reg",
					"fi_confirm_password2_Reg" ];
			maximumlen = [ "50", "50", "255", "16", "50", "50", "50", "50" ];

		}
		if (fields != undefined)
			len = fields.length;
		var i;
		for (i = 0; i < len; i++) {
			// alert(i +" "+fields[i]);

			doc.getElementById(fields[i]).setAttribute("maxlength",
					maximumlen[i]);
		}
	} catch (e) {
		alert("excepci�n" + e.message);
	}
}

function addSORowsOnclickAccountGrid(parentKey) {

	var doc = document;
	var tableObj = doc.getElementById("ownerList_table");
	var row;
	var cell;
	var columns = [ "accountNumber", "accName", "entityName", "entityTin",
			"radio" ];// JSON
	// FIELD
	var len = columns.length;
	var radiobtnGrpName = "ownerListtableRadio";
	var tableName = "ownerList_table";
	deleteTables("ownerList_table");// Refresh the tables
	for ( var a in ownerTableObj) {
		var key = a;
		var object = ownerTableObj[key];
		if (object.parentKey == parentKey) {
			row = tableObj.insertRow();
			row.id = key + "_ID";
			var i;
			for (i = 0; i < len; i++) {
				cell = row.insertCell(i);
				/* bug id 11116 */
				cell.className = "ipm_n2";
				cell.style.width = "50";
				if (columns[i] == "radio")
					cell.innerHTML = '<input type="radio" class="owner_radio" name="'
							+ radiobtnGrpName
							+ '"  id="'
							+ key
							+ '" onclick="return getOwnerInforamtion('
							+ tableName + ',this);">';
				else
					// cell.innerHTML = object[columns[i]];
					cell.innerHTML = '<a data-toggle="tooltip" title="'
							+ object[columns[i]] + '">' + object[columns[i]]
							+ '</a>';

			}

		} else {
			/* bug id 10955,11377 */
			if (!accountInformationObj[object.parentKey]) {
				delete ownerTableObj[key];
				/* bug id 10955,11377 */
			}

		}
	}
	doc.getElementById("totalSoCount").innerHTML = "El n�mero total de registros :"
			+ getTableCount(tableName);

}

function updateTheSOTables(accountNo, accName, accTin, entityType,
		orignalParentkey, oldParentKey, accountHolderCIF, accDOB) {
	var flag = false;
	for ( var a in ownerTableObj) {
		var key = a;
		var object = ownerTableObj[key];
		if (object.parentKey == oldParentKey) {
			var rowID = object["rowID"];
			var filerGIIN = object["filerGIIN"];
			var entityName = object["entityName"];
			var entityAddress = object["entityAddress"];
			var entityCountryCode = object["entityCountryCode"];
			var entityTin = object["entityTin"];
			var corrDocrefID = object["corrDocrefID"];
			var docrefid = object["docrefid"];
			// var action=object[key]["action"];
			var submittedFlag = object["submittedFlag"];

			// Current key is the combination of filer giin,account,tin
			currentOptionkeyOrId = "radio_ownerList_table_" + filerGIIN + "_"
					+ accountNo + "_" + accTin + "_" + entityTin + "_"
					+ accountHolderCIF + "_" + accDOB;
			parentKey = "radio_manual_Data_Grid_Table_" + filerGIIN + "_"
					+ accountNo + "_" + accTin + "_" + accountHolderCIF + "_"
					+ accDOB;
			// here
			// parent
			// key
			// is
			// Filer
			// GIIN+account
			// no+account
			// Tin
			// to
			// identify
			// its
			// parent
			// account
			parentKey = orignalParentkey;
			// START Modify data into json Object
			// FIRST DELETE

			// THEN ADD
			delete ownerTableObj[key];
			ownerTableObj[currentOptionkeyOrId] = new ownerTable(rowID,
					filerGIIN, accountNo, accName, accTin, entityType,
					entityName, entityAddress, entityCountryCode, entityTin,
					corrDocrefID, docrefid, "M", submittedFlag, parentKey,
					accountHolderCIF, accDOB);

			flag = true;
		} else {

		}
		if (flag) {
			modifySORowsOnModifyAct(orignalParentkey);
			// addSORowsOnclickAccountGrid(orignalParentkey);
		}

	}

}

function deleteSO(parentKey, submittedFlag) {
	try {

		var doc = document;
		for ( var a in ownerTableObj) {
			var key = a;
			var object = ownerTableObj[key];
			if (object.parentKey == parentKey) {

				doc.getElementById(key + "_ID").style.backgroundColor = 'red';

				if (submittedFlag != "" && submittedFlag != undefined)
					ownerTableObj[key]["action"] = "D";
				else {
					delete ownerTableObj[key];
				}

			}
		}

		if (submittedFlag == "" || submittedFlag == undefined)
			deleteTables("ownerList_table");// Refresh the tables if the
		// transaction
		// has not persist
	} catch (e) {
		alert(e);
	}

}

function nilReportCheck(obj) {
	var doc = document;
	try {
		var usertype = doc.getElementById(obj.id).checked;
		var isindividualchecked = doc.getElementById("individual_radio").checked;

		if (usertype) {
			if (confirm("No accounts will be reported in NIL report. Are you sure you want to proceed with NIL reporting?")) {
				// make a servlet call to delete all saved data
				// deleteTables_NIL("ownerList_table");
				if (deleteTables_NIL("manual_Data_Grid_Table")) {
					doc.getElementById("isNilReport").value = "Y";
					if (!isindividualchecked) {
						doc.getElementById("Part3_tr").style.display = "none";
					}
					doc.getElementById("sustantial_Owner_Div").style.display = "none";
					doc.getElementById("acctHolder_div").style.display = "none";

				}
			} else {
				doc.getElementById(obj.id).checked = false;
				doc.getElementById("isNilReport").value = "N";
			}

		} else {
			
			//changes made by Alok Start 24-11-2016
			if (confirm("Nil report data will be completely deleted. Are you sure you want to proceed?")) {
				
			if (deleteTables_NIL("manual_Data_Grid_Table")) {
				doc.getElementById("isNilReport").value = "N";
				// alert(isindividualchecked);
				if (!isindividualchecked) {
					// alert("going to set block");
					doc.getElementById("Part3_tr").style.display = "block";
				}
				doc.getElementById("sustantial_Owner_Div").style.display = "block";
				doc.getElementById("acctHolder_div").style.display = "block";
				saveManualGridData_NIL(event);
			}

		}
			
			else
			{
			doc.getElementById(obj.id).checked = true;
			doc.getElementById("isNilReport").value = "Y";
			
			}
			//changes made by Alok End on 24-11-2016
		}
	} catch (e) {
		alert(e);
	}
}

// **********************************************************************************//
// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
// Group : Application Projects1 (US)
// Project : [Fatca IRD]
// Date Written : 03/05/2016
// Date Modified :
// Author : Khushdil Kaushik
// Description : To check whether data contains a decimal or not
// ***********************************************************************************//

function vaidateDecimal(objID) {
	var doc = document;
	var fieldValue = undefined;
	try {
		fieldValue = doc.getElementById(objID).value;
		if (fieldValue == undefined || fieldValue == null || fieldValue == "") {
			fieldValue = "0.00";
		}
		doc.getElementById(objID).value = parseFloat(fieldValue).toFixed(2);
		return true;

	} catch (e) {
		alert(e);
	}
}
// *********************************************************************************
// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
// Group : Application Projects1 (US)
// Project : [Fatca IRD]
// Date Written : 09/05/2016
// Date Modified :
// Author : Khushdil Kaushik
// Description : To modify So rows by modifying account grid row
// ***********************************************************************************/
function modifySORowsOnModifyAct(parentKey) {

	var doc = document;
	var tableObj = doc.getElementById("ownerList_table");
	var row;
	var cell;
	var columns = [ "accountNumber", "accName", "entityName", "entityTin",
			"radio" ];// JSON FIELD
	var len = columns.length;
	var radiobtnGrpName = "ownerListtableRadio";
	var tableName = "ownerList_table";
	deleteTables("ownerList_table");// Refresh the tables
	for ( var a in ownerTableObj) {
		var key = a;
		var object = ownerTableObj[key];
		if (object.parentKey == parentKey) {
			row = tableObj.insertRow();
			row.id = key + "_ID";
			var i;
			for (i = 0; i < len; i++) {
				cell = row.insertCell(i);
				cell.className = "ipm_n2";
				cell.style.width = "50";
				if (columns[i] == "radio")
					cell.innerHTML = '<input type="radio" name="'
							+ radiobtnGrpName + '"  id="' + key
							+ '" onclick="return getOwnerInforamtion('
							+ tableName + ',this);">';
				else
					cell.innerHTML = object[columns[i]];

			}

		}
	}
	// alert("KD2");
	reset("ownerList_table");
	doc.getElementById("totalSoCount").innerHTML = "El n�mero total de registros :"
			+ getTableCount(tableName);
}

// *********************************************************************************
// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
// Group : Application Projects1 (US)
// Project : [Fatca IRD]
// Date Written : 11/05/2016
// Date Modified :
// Author : Khushdil Kaushik
// Description : To add existing so data with new account grid row
// ***********************************************************************************/

function addSOData(accountNo, accName, accTin, entityType, orignalParentkey,
		oldParentKey, accountHolderCIF, accDOB) {
	var parentKey = undefined;
	var currentOptionkeyOrId = undefined;
	var keyToCompare = undefined;

	for ( var a in ownerTableObj) {
		var key = a;
		var object = ownerTableObj[key];

		if (SO_GRID_ROW_KEY == undefined) {
			keyToCompare = oldParentKey;
		} else {
			keyToCompare = orignalParentkey;
		}

		if (object.parentKey == keyToCompare) {

			var rowID = object["rowID"];
			var filerGIIN = object["filerGIIN"];
			var entityName = object["entityName"];
			var entityAddress = object["entityAddress"];
			var entityCountryCode = object["entityCountryCode"];
			var entityTin = object["entityTin"];
			var corrDocrefID = object["corrDocrefID"];
			var docrefid = object["docrefid"];
			var submittedFlag = object["submittedFlag"];

			currentOptionkeyOrId = "radio_ownerList_table_" + filerGIIN + "_"
					+ accountNo + "_" + accTin + "_" + entityTin + "_"
					+ accountHolderCIF + "_" + accDOB;
			parentKey = "radio_manual_Data_Grid_Table_" + filerGIIN + "_"
					+ accountNo + "_" + accTin + "_" + accountHolderCIF + "_"
					+ accDOB;
			parentKey = orignalParentkey;
			ownerTableObj[currentOptionkeyOrId] = new ownerTable("", filerGIIN,
					accountNo, accName, accTin, entityType, entityName,
					entityAddress, entityCountryCode, entityTin, "", "", "A",
					"", parentKey, accountHolderCIF, accDOB);

		}

		else {

			/*
			 * if (object.parentKey == oldParentKey) { var rowID =
			 * object["rowID"]; var filerGIIN = object["filerGIIN"]; var
			 * entityName = object["entityName"]; var entityAddress =
			 * object["entityAddress"]; var entityCountryCode =
			 * object["entityCountryCode"]; var entityTin = object["entityTin"];
			 * var corrDocrefID = object["corrDocrefID"]; var docrefid =
			 * object["docrefid"]; var submittedFlag = object["submittedFlag"];
			 * 
			 * currentOptionkeyOrId = "radio_ownerList_table_" + filerGIIN + "_" +
			 * accountNo + "_" + accTin + "_" + entityTin + "_" +
			 * accountHolderCIF + "_" + accDOB; parentKey =
			 * "radio_manual_Data_Grid_Table_" + filerGIIN + "_" + accountNo +
			 * "_" + accTin + "_" + accountHolderCIF + "_" + accDOB; parentKey =
			 * orignalParentkey; ownerTableObj[currentOptionkeyOrId] = new
			 * ownerTable("", filerGIIN, accountNo, accName, accTin, entityType,
			 * entityName, entityAddress, entityCountryCode, entityTin, "", "",
			 * "A", "", parentKey, accountHolderCIF, accDOB); // } }
			 */
		}

	}
}

/*******************************************************************************
 * bug 11598,11600/
 *  //
 * ********************************************************************************* //
 * NEWGEN SOFTWARE TECHNOLOGIES LIMITED // Group : Application Projects1 (US) //
 * Project : [CRS Fatca Barbados] // Date Written : 24/06/2016 // Date Modified : //
 * Author : Khushdil Kaushik // Description : To vaildate (FSI - filer,Sponsor
 * and Intermediary giin on // manual form //
 ******************************************************************************/

function validateFSI_Giin(objid) {

	var doc = document;
	var filerGiin = "";
	var interGiin = "";
	var sponGiin = "";
	var fieldID = undefined;
	var url = "";
	var param = "";
	var response = "";
	var giin = "";
	var giinID = "";

	filerGiin = (doc.getElementById("giin").value).toUpperCase();
	interGiin = (doc.getElementById("interEntityGiin").value).toUpperCase();
	sponGiin = (doc.getElementById("sponEntitygiin").value).toUpperCase();
	if (objid == "Sponsor" && sponGiin == "") {
		doc.getElementById("filerGiinErrormsg").innerHTML = "Plese entra en Giin Primero.";
		doc.getElementById("filerGiinErrormsg").className = " errorMsg";
		return false;
	} else if (objid == "Intermediary" && interGiin == "") {
		doc.getElementById("filerGiinErrormsg").innerHTML = "Plese entra en Giin Primero.";
		doc.getElementById("filerGiinErrormsg").className = " errorMsg";
		return false;
	}

	if (interGiin != "" || sponGiin != "") {
		if (filerGiin == interGiin || filerGiin == sponGiin
				|| sponGiin == interGiin) {
			doc.getElementById("filerGiinErrormsg").innerHTML = "GIIN no puede ser el mismo para FILER, PATROCINADOR e INTERMEDIARIO.";
			doc.getElementById("filerGiinErrormsg").className = " errorMsg";
			//makeReadonlySPON_InterFields();
			//makeGIINFieldsReadOnly("Sponsor");
			//makeGIINFieldsReadOnly("Intermediary");
			return false;
		} else {
			doc.getElementById("filerGiinErrormsg").innerHTML = "&nbsp;";
			if (objid == "Sponsor") {
				fieldID = [ "sponsoredEntityName", "sponEntityAddress" ];
				giin = sponGiin;
				giinID = "sponEntitygiin";
			} else {
				fieldID = [ "interEntityName", "interEntityAddress" ];
				giin = interGiin;
				giinID = "interEntityGiin";
			}

			if (giin != null && giin != undefined && giin != "") {
				url = appPath + "/AccountCreation";
				param = "option=searchgiin" + "&giin=" + giin;
				try {
					response = ajaxCall(url, param);
					if (response != undefined && response != null
							&& response.toUpperCase() != "NULL"
							&& response != "") {
						var obj = JSON.parse(response);
						var len = fieldID.length;
						if (obj["name"] != "GIINNotExist") {
							for ( var i = 0; i < len; i++) {

								/*
								 * if (obj[JSONObjColumnname[i]] != "" &&
								 * obj[JSONObjColumnname[i]] != "GIINNotExist") {
								 */

								doc.getElementById(fieldID[i]).removeAttribute(
										"tabindex");
								doc.getElementById(fieldID[i]).readOnly = false;
								doc.getElementById(fieldID[i]).className = "form-control";

								/*
								 * } else { doc.getElementById(fieldID[i])
								 * .setAttribute("tabindex", "-1");
								 * doc.getElementById(fieldID[i]).value =
								 * obj[JSONObjColumnname[i]];
								 * doc.getElementById(fieldID[i]).readOnly =
								 * true;
								 * doc.getElementById(fieldID[i]).className += " " +
								 * "readOnlyClass"; }
								 */
							}
							
							// make readonly all giin fields
							//Change for All by KD on 23072016 'code added' strts
							 makeGIINFieldsReadOnly(objid);
								//Change for All by KD on 23072016 -end
							return true;
						} else {
							blankGIINFields(objid);
							doc.getElementById(giinID).value = "";
							doc.getElementById("filerGiinErrormsg").innerHTML = NoRecordFound;
							doc.getElementById("filerGiinErrormsg").className = " errorMsg";
							return false;
						}
					} else {
						blankGIINFields(objid);
						doc.getElementById(giinID).value = "";

						// makeFIreadonly("nonEdiatable");
						doc.getElementById("filerGiinErrormsg").innerHTML = NoRecordFound;
						doc.getElementById("filerGiinErrormsg").className = " errorMsg";
						return false;
					}
				} catch (ex) {
					alert(ex);
				}
			}

		}
	}

}

// *********************************************************************************
// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
// Group : Application Projects1 (US)
// Project : [CRS Fatca Barbados]
// Date Written : 24/06/2016
// Date Modified :
// Author : Khushdil Kaushik
// Description : to make address and name field read only for sponsor and
// intermediary
// ***********************************************************************************/

function makeReadonlySPON_InterFields() {

	var doc = document;
	var fieldID = [ "sponsoredEntityName", "sponEntityAddress",
			"interEntityName", "interEntityAddress" ];
	var len = fieldID.length;
	try {
		for ( var i = 0; i < len; i++)
			doc.getElementById(fieldID[i]).className += " " + "readOnlyClass";
		// doc.getElementById(fieldID[i]).disabled = true;

	} catch (e) {
		alert("hacer s�lo lectura SPON Inter Fields--> " + e);

	}

}
// *********************************************************************************
// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
// Group : Application Projects1 (US)
// Project : [CRS Fatca Barbados]
// Date Written : 24/06/2016
// Date Modified :
// Author : Khushdil Kaushik
// Description : to make address and name field EDIATABLE for sponsor and
// intermediary
// ***********************************************************************************/

function makeSPON_InterFields_editable() {

	var doc = document;
	var fieldID = [ "sponsoredEntityName", "sponEntityAddress",
			"interEntityName", "interEntityAddress" ];
	var len = fieldID.length;
	try {
		for ( var i = 0; i < len; i++)
			doc.getElementById(fieldID[i]).className = "form-control";
		// doc.getElementById(fieldID[i]).disabled = true;

	} catch (e) {
		alert("hacer SPON Inter Fields editable--> " + e);

	}

}
/** bug 11598,11600 */



function enableButtons(){
	
try {
	var doc=document;
	var reportingYear="<%=reportingYear%>";
	var submissionType="<%=submissionType%>";
			/* submissionType="UPDATE"; */

			if (submissionType.toUpperCase() == "UPDATE") {

				$('#FIAdd').hide();
				//Change for All by Shubham on 23072016 'code commented' - starts
				//$('#addEntity').hide();
				//Change for All by Shubham on 23072016 end
				//Change for All by SHUBHAM on 02082016 'code added' - starts
				$('#FIDelete').hide();
				//Change for All by SHUBHAM on 02082016 end

				$('#saveManualData').hide();
				$('#submitManualData').show();
				doc.getElementById("nilReportChk").disabled = true;
				doc.getElementById("sponsoredEntity_yes").disabled = true;
				doc.getElementById("sponsoredEntity_no").disabled = true;
				doc.getElementById("isIntermediary_yes").disabled = true;
				doc.getElementById("intermediary_no").disabled = true;
				//Change for All by Shubham on 28072016 'code added'' starts
				doc.getElementById("individual_radio").disabled = true;
				doc.getElementById("Entity_radio").disabled = true;
				//Change for All by Shubham on 28072016 end
				makeGIINFieldsReadOnly("Sponsor");
				makeGIINFieldsReadOnly("Intermediary");
				makeSPON_InterFields_editable();
			} else if (submissionType.toUpperCase() == "RESPONSE TO IRS REQUEST") {

				$('#FIAdd').hide();
				$('#FIDelete').hide();
				$('#FIModify').show();

				//Change for All by Shubham on 28072016 Start
				//$('#addEntity').hide();
				//Change for All by Shubham on 28072016 end
				$('#modifyEntity').show();
				$('#deleteEntity').hide();
				$('#saveManualData').hide();
				$('#submitManualData').show();
				doc.getElementById("nilReportChk").disabled = true;
				//Change for All by Shubham on 28072016 ' code added' starts
				doc.getElementById("sponsoredEntity_yes").disabled = true;
				doc.getElementById("sponsoredEntity_no").disabled = true;
				doc.getElementById("isIntermediary_yes").disabled = true;
				doc.getElementById("intermediary_no").disabled = true;

				doc.getElementById("individual_radio").disabled = true;
				doc.getElementById("Entity_radio").disabled = true;

				makeGIINFieldsReadOnly("Sponsor");
				makeGIINFieldsReadOnly("Intermediary");
				makeSPON_InterFields_editable();
				//Change for All by Shubham on 28072016 end
			} else {
				//nilReportChk
				doc.getElementById("nilReportChk").disabled = false;
				//Change for All by Shubham on 28072016 ' code added' starts
				doc.getElementById("sponsoredEntity_yes").disabled = false;
				doc.getElementById("sponsoredEntity_no").disabled = false;
				doc.getElementById("isIntermediary_yes").disabled = false;
				doc.getElementById("intermediary_no").disabled = false;

				doc.getElementById("individual_radio").disabled = false;
				doc.getElementById("Entity_radio").disabled = false;
				//Change for All by Shubham on 28072016 end
			}

			doc.getElementById("ActHolderEntityType_manual").disabled = true;

		} catch (e) {
			alert(e);
		}

	}

