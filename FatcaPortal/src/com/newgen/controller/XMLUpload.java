/*------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : upload xml file
 File Name                                                   : XMLUpload
 Author                                                      : Deepak Sharma
 Date (DD/MM/YYYY)                                           : 23/10/2015
 Description                                                 : toupload the xml file
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    Change Description

 ------------------------------------------------------------------------------------------------------*/

package com.newgen.controller;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.newgen.DAO.Custom;
import com.newgen.DAO.XMLUploadDAO;
import com.newgen.DAO.XMLValidation;
import com.newgen.bean.GlobalMessage;
import com.newgen.bean.GlobalVariables;
import com.newgen.factory.ObjectFactory;

public class XMLUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(XMLUpload.class);
	private static final int bytesInMB = 1024 * 1024; // 1MB

	public XMLUpload() {
		super();

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		logger.info("inside XMLUpload:: dopost");

		String inputXMLPath = null;
		String successFilePath = null;
		String failureXMLPath = null;
		File dirpath = null;
		Map<String, String> ReqAttrMap = new HashMap<String, String>();
		Map<String, InputStream> ReqInStreamMap = new HashMap<String, InputStream>();
		String fileName = null;
		/* Change for All By shubham on 28072016 -- starts */
		String testOrActual = null;
		/* Change for All By shubham on 28072016 -- end */


		String userName = null;
		String sFilename = null;
		String fileNameWithoutExt = null;
		String reportingYear = null;
		String submissionType = null;
		String new_update = null;
		/*
		 * FileReader reader = null; FileWriter writer = null;
		 */
		String status = null;
		String message = null;
		String userGIIN = null;
		String fileExtension = null;
		String userEmail = null;
		String xmlType = null;
		int version = 0;
		Custom custom = null;
		XMLUploadDAO xmlUploadDAO = null;
		String timestamp = null;
		String reportingMode = null;
		try {
			HttpSession session = (HttpSession) request.getSession(false);
			if (session != null) {
				userEmail = (String) session.getAttribute("UserEmail");
				userGIIN = (String) session.getAttribute("UserGIIN");
				userName = (String) session.getAttribute("UserName");
				// }

				if (session.getId().equalsIgnoreCase(
						GlobalVariables.loginUserMap.get(userEmail))) {
					Calendar now = Calendar.getInstance(); // Gets the current
					DateFormat dateFormat = new SimpleDateFormat(
							"dd_MM_yy_HHmmss");
					timestamp = dateFormat.format(now.getTime());
					logger.debug("timestamp-->" + timestamp);
					inputXMLPath = GlobalVariables.uploadLocation
							+ File.separator + userGIIN + File.separator
							+ userEmail + File.separator + "InputXML";
					successFilePath = GlobalVariables.uploadLocation
							+ File.separator + userGIIN + File.separator
							+ userEmail + File.separator + "Success";
					failureXMLPath = GlobalVariables.uploadLocation
							+ File.separator + userGIIN + File.separator
							+ userEmail + File.separator + "Failure";

					GlobalVariables.failedXmlLocation = failureXMLPath;
					GlobalVariables.correctXmlLocation = successFilePath;
					logger.debug("GlobalVariables.failedXmlLocation in XMLUpload --> "
							+ GlobalVariables.failedXmlLocation);
					dirpath = new File(inputXMLPath);
					if (!dirpath.exists()) {
						dirpath.mkdirs();
					}
					dirpath = new File(successFilePath);
					if (!dirpath.exists()) {
						dirpath.mkdirs();
					}
					dirpath = new File(failureXMLPath);
					if (!dirpath.exists()) {
						dirpath.mkdirs();
					}
					custom = ObjectFactory.getCustomObject();
					xmlUploadDAO = ObjectFactory.getXMLUploadDaoObject();

					DiskFileItemFactory factory = new DiskFileItemFactory();
					factory.setSizeThreshold(bytesInMB
							* GlobalVariables.memoryThreshold);
					ServletFileUpload upload = new ServletFileUpload(factory);
					upload.setFileSizeMax(bytesInMB
							* GlobalVariables.maxFileSize);
					upload.setSizeMax(bytesInMB
							* GlobalVariables.maxRequestSize);
					List items = upload.parseRequest(request);
					Iterator iter = items.iterator();
					while (iter.hasNext()) {
						FileItem item = (FileItem) iter.next();
						if (item.isFormField()) {
							ReqAttrMap.put(item.getFieldName(),
									item.getString());
						} else {
							ReqAttrMap.put(item.getFieldName(), item.getName());
							ReqInStreamMap.put("filecontent",
									item.getInputStream());
						}
					}
					fileName = ReqAttrMap.get("uploadFile");
					xmlType = ReqAttrMap.get("XMLtype");

					if (fileName != null && !(fileName.length() == 0)) {
						fileExtension = custom.getFileExtention(fileName);
						sFilename = custom.getFileName(fileName);
						fileNameWithoutExt = sFilename.substring(0,
								sFilename.length() - 4);
						reportingYear = ReqAttrMap.get("reportingYear");
						// submissionType = ReqAttrMap.get("submissionType");
						submissionType = "";// not required
						new_update = ReqAttrMap.get("new_update");
						/* check submission mode */
						reportingMode = ReqAttrMap.get("reportingMode");


						/*
						 * Change for All by shubham on 28072016 added code and one
						 * more parameter -- starts
						 */
						GlobalVariables.userAllowedtoUpload = "Do Not Allow";
						// after this in next function "isReportinngModeCorrect"
						// it actual value will be populated from db
						if (custom.isReportinngModeCorrect(
								(String) session.getAttribute("UserGIIN"),
								reportingYear, reportingMode, xmlType,
								new_update)) {
							// xmlType is either fatca or crs
							/*
							 * if (!GlobalVariables.userAllowedtoUpload
							 * .equalsIgnoreCase("Do Not Allow")) {
							 */






							/* Change for All by shubham on 28072016 -- end */
							if (fileExtension != null
									&& fileExtension.equalsIgnoreCase("XML")) {
								sFilename = fileNameWithoutExt + "_"
										+ timestamp + ".xml";
								custom.SaveUploadedFile(inputXMLPath,
										sFilename,
										ReqInStreamMap.get("filecontent"));
								status = XMLValidation.ReadXML(userEmail,
										userGIIN, sFilename, submissionType,
										reportingYear, version,
										"PORTAL VALIDATION", inputXMLPath,
										failureXMLPath, xmlType);
								logger.info("status from read xml is --> "
										+ status);
								if (status.equalsIgnoreCase("Success")) {

									File targetfile = new File(successFilePath
											+ File.separator + sFilename);
									File sourcefile = new File(inputXMLPath
											+ File.separator + sFilename);
									if (targetfile.exists())
										targetfile.delete();
									/*Files.move(sourcefile.toPath(),
											targetfile.toPath());
									*/Files.copy(sourcefile.toPath(),
											targetfile.toPath());
									message = GlobalMessage.UPLOAD_SUCCESS_MSG;
								} else if (status.equalsIgnoreCase("Failure")) {
									File targetfile = new File(failureXMLPath
											+ File.separator + sFilename);
									File sourcefile = new File(inputXMLPath
											+ File.separator + sFilename);

									if (targetfile.exists())
										targetfile.delete();
									/*Files.move(sourcefile.toPath(),
											targetfile.toPath());*/
									Files.copy(sourcefile.toPath(),
											targetfile.toPath());
									message = GlobalMessage.UPLOAD_FAIL_MSG;
								} else if (status
										.equalsIgnoreCase("SchemaFailure")) {

									message = GlobalMessage.SCHEMA_FAIL_MSG;
								}

								request.setAttribute("msg", message);
								if (null != xmlType
										&& xmlType.equalsIgnoreCase("crs")) {

									RequestDispatcher rd = request
											.getRequestDispatcher("JSP/XMLUpload-CRS.jsp");
									rd.forward(request, response);
								} else {
									RequestDispatcher rd = request
											.getRequestDispatcher("JSP/XMLUpload.jsp");
									rd.forward(request, response);
								}

							} else {

								request.setAttribute("msg",
										GlobalMessage.INVALID_FILE_FORMAT);
								if (null != xmlType
										&& xmlType.equalsIgnoreCase("crs")) {

									RequestDispatcher rd = request
											.getRequestDispatcher("JSP/XMLUpload-CRS.jsp");
									rd.forward(request, response);
								} else {
									RequestDispatcher rd = request
											.getRequestDispatcher("JSP/XMLUpload.jsp");
									rd.forward(request, response);
								}
							}













						} else {/*Change for all by KD on 2807216  -- starts*/
							if (GlobalVariables.userAllowedtoUpload
									.equalsIgnoreCase("Do Not Allow")) {
								request.setAttribute("msg",
										GlobalMessage.XML_SUBMISSION_XML_MSG);
							} else {

								request.setAttribute("msg",
										GlobalMessage.REPORT_SUBMISSION_XML_MSG);
							}
							/*Change for all by KD on 2807216  -- end*/


							if (null != xmlType
									&& xmlType.equalsIgnoreCase("crs")) {

								RequestDispatcher rd = request
										.getRequestDispatcher("JSP/XMLUpload-CRS.jsp");
								rd.forward(request, response);
							} else {
								RequestDispatcher rd = request
										.getRequestDispatcher("JSP/XMLUpload.jsp");
								rd.forward(request, response);
							}
							/* kd */}
					} else {
						request.setAttribute("msg",
								GlobalMessage.PLEASE_SELECT_FILE);
						if (null != xmlType && xmlType.equalsIgnoreCase("crs")) {

							RequestDispatcher rd = request
									.getRequestDispatcher("JSP/XMLUpload-CRS.jsp");
							rd.forward(request, response);
						} else {
							RequestDispatcher rd = request
									.getRequestDispatcher("JSP/XMLUpload.jsp");
							rd.forward(request, response);
						}

					}
				} else {
					logger.debug("session expired , session id not matching");
					RequestDispatcher rd = request
							.getRequestDispatcher("JSP/SessionExpired.jsp");
					rd.forward(request, response);

				}
			} else {
				logger.debug("session expired , session is null");
				RequestDispatcher rd = request
						.getRequestDispatcher("index.jsp");
				rd.forward(request, response);
			}

		} catch (Exception e) {
			try {

				logger.info("uploadInputXml :: Exception --> " + e.getMessage());
				request.setAttribute("msg",
						"Some Exception occured at server end, please look into logs for more details");
				e.printStackTrace();
				if (null != xmlType && xmlType.equalsIgnoreCase("crs")) {

					RequestDispatcher rd = request
							.getRequestDispatcher("JSP/XMLUpload-CRS.jsp");
					rd.forward(request, response);
				} else {
					RequestDispatcher rd = request
							.getRequestDispatcher("JSP/XMLUpload.jsp");
					rd.forward(request, response);
				}

			} catch (Exception ex) {
				logger.info("uploadInputXml :: Exception inside exception --> "
						+ ex.getMessage());

			}

		}

	}
}
