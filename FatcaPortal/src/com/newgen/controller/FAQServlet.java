/*------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : FAQ from DB
 File Name                                                   : FAQServlet
 Author                                                      : Khushdil Kaushik
 Date (DD/MM/YYYY)                                           : 08/02/2016
 Description                                                 : servlet to get FAQ from DB
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    Change Description
 
 ------------------------------------------------------------------------------------------------------*/

package com.newgen.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.newgen.DAO.FAQDao;
import com.newgen.bean.FrequentlyAskedQue;
import com.newgen.bean.GlobalMessage;
import com.newgen.factory.ObjectFactory;

/**
 * Servlet implementation class FAQServlet
 */
public class FAQServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(FAQServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FAQServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/*protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String callFor = null;
		FAQDao fAQDaoObj = ObjectFactory.getFaqDaoObject();
		Map<Integer, FrequentlyAskedQue> faqMap = null;
		// response.setContentType("application/json");
		// PrintWriter out = response.getWriter();
		try {

			callFor = request.getParameter("callFor");
			if (null != callFor && callFor.length() > 0
					&& "FAQ".equalsIgnoreCase(callFor)) {
				faqMap = fAQDaoObj.getFAQ();
				request.setAttribute("faq", faqMap);
				request.getRequestDispatcher("JSP/faq_new.jsp").forward(
						request, response);
			}
			// out.print(jsonArray);

		} catch (Exception e) {
			logger.debug("Exception in FAQServlet::doPost:: " + e.getMessage());
		}
	}*/
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String callFor = null;
		FAQDao fAQDaoObj = ObjectFactory.getFaqDaoObject();
		Map<Integer, FrequentlyAskedQue> faqMap = null;
		List<String> contactPageDatalist = null;
		// response.setContentType("application/json");
		// PrintWriter out = response.getWriter();
		try {

			callFor = request.getParameter("callFor");
			if (null != callFor && callFor.length() > 0
					&& "FAQ".equalsIgnoreCase(callFor)) {
				faqMap = fAQDaoObj.getFAQ();
				request.setAttribute("faq", faqMap);
				request.getRequestDispatcher("JSP/faq_new.jsp").forward(
						request, response);
			} else if (null != callFor && callFor.length() > 0
					&& "ContactUS".equalsIgnoreCase(callFor)) {
				contactPageDatalist = fAQDaoObj.getContactData();
				request.setAttribute("contactUS", contactPageDatalist);
				request.getRequestDispatcher("JSP/contact-us.jsp").forward(
						request, response);
			}
			// out.print(jsonArray);

		} catch (Exception e) {
			logger.debug("Exception in FAQServlet::doPost:: " + e.getMessage());
		}
	}
}
