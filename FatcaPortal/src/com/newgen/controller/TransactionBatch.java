/*------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : Transaction History of FI User
 File Name                                                   : TransactionBatch
 Author                                                      : Deepak Sharma
 Date (DD/MM/YYYY)                                           : 16/10/2015
 Description                                                 : fetch data to show on transaction history page( paging)
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    Change Description
 10254				 17/11/2015		Khushdil	chanhes/removed order by field in the query
 ------------------------------------------------------------------------------------------------------*/

package com.newgen.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.newgen.factory.ObjectFactory;
import com.newgen.interfaces.Transactional;

public class TransactionBatch extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(TransactionBatch.class);

	public TransactionBatch() {
		super();

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		logger.info("inside TransactionBatch::doPost");
		String pageNo = request.getParameter("pageNo");
		String totalCount = request.getParameter("totalCount");
		String complianceType = request.getParameter("complianceType");
		String reportingYear = request.getParameter("reportingYear");
		

		PrintWriter out = response.getWriter();

		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", -1);

		Transactional transaction = ObjectFactory
				.getTransactionBatchDAOObject();
		HttpSession session = request.getSession(false);
		JSONObject jObj = null;
		out.flush();
		try {
			if (session != null) {
				jObj = transaction.getBatch(pageNo, totalCount,
						(String) session.getAttribute("UserName"),
						(String) session.getAttribute("UserEmail"),
						(String) session.getAttribute("UserGIIN"),
						complianceType,reportingYear);
				out.print(jObj);
			} else {
				logger.info("Your session has been expired please try after some time");
				out.print("SessionExpired");

			}

		} catch (Exception e) {
			try {
				jObj = new JSONObject();
				jObj.put("Exception", e.getMessage());
				out.print("JSONObject=" + jObj);
			} catch (JSONException e1) {

				logger.info("Exception in Transaction Batchservlet :"
						+ e1.getMessage());
			}

		}
	}

}
