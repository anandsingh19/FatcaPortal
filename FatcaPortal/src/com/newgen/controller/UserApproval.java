/*------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : user approval/rejection for Fi user
 File Name                                                   : UserApproval
 Author                                                      : Khushdil Kaushik
 Date (DD/MM/YYYY)                                           : 16/10/2015
 Description                                                 : to fetch all the new created user list and to update their state form active to block or vice versa
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   	Changed By   			 Change Description
 bug id 11429         27/04/2016      Khushdil Kaushik        updated user_adminLockStatus instead of user already locking status 
 ------------------------------------------------------------------------------------------------------*/

package com.newgen.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.newgen.DAO.Custom;
import com.newgen.bean.FIRegistration;
import com.newgen.bean.GlobalVariables;
import com.newgen.factory.ObjectFactory;
import com.newgen.interfaces.Customable;
import com.newgen.interfaces.UserApprovable;

public class UserApproval extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(UserApproval.class);
	private static final int bytesInMB = 1024 * 1024; // 1MB

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String option = request.getParameter("option");
		// changes made by Alok on 19-12-2016 Start
		
		PrintWriter out = response.getWriter();
		logger.debug("user approval option --> " + option);
		if ("update".equalsIgnoreCase(option)) {
			String fileName = request.getParameter("fileName");
			logger.info("fileName--> " + fileName);
			if (isFileFomatValid1(fileName))
			{
				out.print("True");
			}
			else
			{
				//out.print("False@@@" + GlobalVariables.supportiveDocType);
				//changes made by Alok on 09-12-2016 Start
				//out.print("File Size has exceeded the defined size. Please upload a smaller size file. " );
				
			RequestDispatcher rd = request
						.getRequestDispatcher("JSP/ApprovalFail.jsp");
				rd.include(request, response);		
				
				//changes made by Alok on 09-12-2016 End
				
			}

			
		
			updateUserData(request, response);
		}
		
		// changes made by Alok on 19-12-2016 End
			
			
			else if ("ChkIsLastFiAdmin".equalsIgnoreCase(option)) {
			chkIsLastFiAdmin(request, response);
		}else if ("searchUser".equalsIgnoreCase(option)) {
			searchUser(request, response, option);
		} else {
			getUserList(request, response, option);
		}

	}

	// changes made by Alok on 19-12-2016 Start
	
	public boolean isFileFomatValid1(String fileName)

	{
		boolean validFile = false;
		Custom custom = null;
		custom = ObjectFactory.getCustomObject();
		String fileExtention = null;
		try {
			if (fileName.length() != 0) {
				fileExtention = custom.getFileExtention(fileName);
				if (fileExtention
						.equalsIgnoreCase(GlobalVariables.supportiveDocType))
					validFile = true;

			}

		} catch (Exception e) {
			logger.info("Exception in isFileFomatValid-->  " + e.getMessage());
		}
		return validFile;

	}

// changes made by Alok on 19-12-2016 End
	
	
	
	public void getUserList(HttpServletRequest request,
			HttpServletResponse response, String event) throws IOException {
		String userGiin = null;
		String userType = null;
		HttpSession session = null;
		response.setContentType("application/json");
		session = request.getSession(false);
		userType = (String) session.getAttribute("UserType");
		String userGIIN = (String) session.getAttribute("UserGIIN");
		Map<String, Integer> pageDetails = new HashMap<String, Integer>();
		UserApprovable userApproval = ObjectFactory.getUserApprovalDAOObject();

		PrintWriter out = response.getWriter();
		if (event.equalsIgnoreCase("getPageCount")) {
			session.setAttribute("pageDetails",
					userApproval.getPageCount(userType, userGIIN, "1", event,request.getParameter("searchFor"),
							request.getParameter("searchIn")));
			out.print("success");
		} else {

			pageDetails = (Map) session.getAttribute("pageDetails");
			if (event.equalsIgnoreCase("next")) {
				pageDetails.put("currentPage",
						pageDetails.get("currentPage") + 1);
			} else if (event.equalsIgnoreCase("prev")) {
				pageDetails.put("currentPage",
						pageDetails.get("currentPage") - 1);
			}
			logger.debug("search in-->  "+request.getParameter("searchIn"));
			org.json.JSONArray jsonArray = userApproval.getFI(userType,
					userGIIN, pageDetails.get("currentPage"), event,request.getParameter("searchFor"),
					request.getParameter("searchIn"));

			int currentPage = pageDetails.get("currentPage");
			int totalPage = pageDetails.get("totalCount");
			if (currentPage == 1 && totalPage == 1) {
				jsonArray.put("00");// disable prev and next
			} else if (currentPage == 1 && totalPage > 1)
				jsonArray.put("01");// disable prev and next Enable
			else if (currentPage == totalPage && totalPage > 1) {
				jsonArray.put("10"); // Enable prev and next disable
			} else if (currentPage > 1 && currentPage < totalPage) {
				jsonArray.put("11");// Enable prev and next Enable
			}
			out.print(jsonArray);
		}

	}

	public void updateUserData(HttpServletRequest request,
			HttpServletResponse response) {
		String msg = null;
		int errorUpd = 0;
		String sUserDoc1 = null;
		String sUserDoc2 = null;
		String complianceType = null;

		HttpSession session = request.getSession(false);
		PrintWriter out = null;
		Map<String, String> ReqAttrMap = new HashMap<String, String>();
		Map<String, InputStream> fileInStrmMap = new HashMap<String, InputStream>();
		Customable custom = ObjectFactory.getCustomObject();
		String searchFor = null;
		String searchIn = null;
		try {
			out = response.getWriter();
			if (session != null) {
				complianceType = (String) session
						.getAttribute("ComplianceType");

				if (ServletFileUpload.isMultipartContent(request)) {

					DiskFileItemFactory factory = new DiskFileItemFactory();
					factory.setSizeThreshold(bytesInMB
							* GlobalVariables.memoryThreshold);
					ServletFileUpload upload = new ServletFileUpload(factory);
// changes made by Alok on 21-11-2016 Start
					upload.setFileSizeMax(bytesInMB
							* GlobalVariables.supportiveDoc1MaXSize);
					upload.setSizeMax(bytesInMB
							* GlobalVariables.supportiveDoc1MaXSize);
					upload.setFileSizeMax(bytesInMB
							* GlobalVariables.supportiveDoc2MaXSize);
					upload.setSizeMax(bytesInMB
							* GlobalVariables.supportiveDoc2MaXSize);
					
					// changes made by Alok on 21-11-2016 End
					List items = upload.parseRequest(request);
					Iterator iter = items.iterator();
					while (iter.hasNext()) {
						FileItem item = (FileItem) iter.next();
						if (item.isFormField()) {
							ReqAttrMap.put(item.getFieldName(),
									item.getString());
						} else {
							if (item.getName() != null
									&& (!item.getName().equals(""))) {
								ReqAttrMap.put(item.getFieldName(),
										item.getName());
								fileInStrmMap.put(item.getFieldName(),
										item.getInputStream());
							}
						}
					}
				}

				FIRegistration userToUpdate = new FIRegistration();

				/*
				 * sUserDoc1 = ReqAttrMap.get("Document1_adminApproval");
				 * sUserDoc2 = ReqAttrMap.get("Document2_adminApproval");
				 */
				logger.debug("is locked status--> "
						+ ReqAttrMap.get("user_LockedStatus"));
				logger.debug("is locked status by admin--> "
						+ ReqAttrMap.get("user_adminLockStatus"));
				userToUpdate.setGiin(ReqAttrMap.get("GIIN_adminApproval"));
				userToUpdate.setFiName(ReqAttrMap.get("fiName_adminApproval"));
				userToUpdate.setFiAddress(ReqAttrMap
						.get("fiAddress_adminApproval"));
				userToUpdate.setFiCountry(ReqAttrMap
						.get("fiCountry_adminApproval"));
				userToUpdate.setFiEmail(ReqAttrMap
						.get("fiEmailID_adminApproval"));
				userToUpdate.setUserEmailId(ReqAttrMap
						.get("userEmailID_adminApproval"));
				userToUpdate.setUserName(ReqAttrMap
						.get("userName_adminApproval"));
				userToUpdate.setUserAddress(ReqAttrMap
						.get("userAddress_adminApproval"));
				userToUpdate.setUserPhone(ReqAttrMap
						.get("userPhone_adminApproval"));
				userToUpdate.setUserCountry(ReqAttrMap
						.get("userCountry_adminApproval"));
				userToUpdate.setUserEmployeeID(ReqAttrMap
						.get("userEmployee_adminApproval"));
				userToUpdate.setUserDesignation(ReqAttrMap
						.get("userDesignation_adminApproval"));
				/*bug id 11429 starts*/
				userToUpdate.setIsUserLocked(ReqAttrMap
						.get("user_adminLockStatus"));
				/*
				 * userToUpdate.setIsUserLocked(ReqAttrMap
				 * .get("user_LockedStatus"));
				 */
				
				/*bug id 11429 end*/
				userToUpdate.setComments(ReqAttrMap
						.get("comments_adminApproval"));
				userToUpdate.setFlag(ReqAttrMap.get("status_adminApproval"));
				userToUpdate.setOperation(ReqAttrMap.get("userType_toUpdate"));
				userToUpdate.setUserDocType1(ReqAttrMap
						.get("Document1_adminApproval"));
				userToUpdate.setUserDocType1File1(custom.getFileName(ReqAttrMap
						.get("file1_admin")));
				userToUpdate.setUserDocType2(ReqAttrMap
						.get("Document2_adminApproval"));
				userToUpdate.setUserDocType2File2(custom.getFileName(ReqAttrMap
						.get("file2_admin")));
				if (null != complianceType
						&& "crs".equalsIgnoreCase(complianceType)) {
					userToUpdate.setInType(ReqAttrMap.get("fi_inType"));
				} else {
					userToUpdate.setInType(ReqAttrMap.get("GIIN_InType"));
				}
				
				userToUpdate
				.setUserDOB(ReqAttrMap.get("userDOB_adminApproval"));
		userToUpdate.setUserGender(ReqAttrMap
				.get("userGender_adminApproval"));
				logger.debug("file2_admin--> "
						+ custom.getFileName(ReqAttrMap.get("file2_admin")));
				logger.debug("file1_admin--> "
						+ custom.getFileName(ReqAttrMap.get("file1_admin")));

				/*
				 * logger.debug("file2_admin--> " +
				 * custom.getFileName(ReqAttrMap.get("file2_admin")));
				 * logger.debug("file2_admin--> " +
				 * custom.getFileName(ReqAttrMap.get("file2_admin")));
				 */
				searchFor = ReqAttrMap.get("searchFor_hidden");
				searchIn = ReqAttrMap.get("searchIn_hidden");
				logger.debug("searchFor in update --> " + searchFor);
				logger.debug("searchIn in update --> " + searchIn);
				UserApprovable userapproval = ObjectFactory
						.getUserApprovalDAOObject();
				errorUpd = userapproval.isApprovedUser(userToUpdate,
						ReqAttrMap, fileInStrmMap, session);
				if (errorUpd > 0) {
					// out.print("sucess");
					request.setAttribute("searchFor", searchFor);
					request.setAttribute("searchIn", searchIn);
					logger.debug("success--> " + errorUpd);
					request.getRequestDispatcher("JSP/admin-approval.jsp")
							.forward(request, response);
				} else {
					// out.print("fail");
					logger.debug("fail--> " + errorUpd);
					request.setAttribute("searchFor", searchFor);
					request.setAttribute("searchIn", searchIn);
					request.getRequestDispatcher("JSP/admin-approval.jsp")
							.forward(request, response);
				}
			} else {
				logger.info("Your session has been expired.");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			}

		} catch (Exception e) {
			logger.info("Exception in updateUserData-->  " + e.getMessage());

		}
	}

	public void chkIsLastFiAdmin(HttpServletRequest request,
			HttpServletResponse response) {
		boolean isLastFiadmin = false;
		try {
			HttpSession session = null;
			session = request.getSession(false);
			if (session != null) {
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				UserApprovable userapproval = ObjectFactory
						.getUserApprovalDAOObject();
				isLastFiadmin = userapproval.isLastFIAdmin(request
						.getParameter("giin"));
				if (isLastFiadmin) {
					out.print("True");
				} else {
					out.print("False");
				}
			} else {
				logger.info("Your session has been expired.");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			}
		} catch (Exception e) {
			logger.info("Exception in chkIsLastFiAdmin-->  " + e.getMessage());
		}

	}

	public void searchUser(HttpServletRequest request,
			HttpServletResponse response, String event) {
		String searchFor = null;
		String searchIn = null;
		// HttpSession session = null;
		try {
			searchFor = request.getParameter("searchFor");
			searchIn = request.getParameter("searchIn");
			if (null == searchFor || searchFor == "") {
				searchFor = "Blank";
			}
			// session = request.getSession(false);
			logger.debug("searchFor in searchUser--> " + searchFor);
			logger.debug("searchIn in searchUser--> " + searchIn);
			request.setAttribute("searchFor", searchFor);
			request.setAttribute("searchIn", searchIn);
			// session.setAttribute("searchFor",searchFor);
			// session.setAttribute("searchIn",searchIn);
			request.getRequestDispatcher("JSP/admin-approval.jsp").forward(
					request, response);
		} catch (Exception e) {
			logger.debug("Exception in searchUser--> " + e.getMessage());
		}

	}
	
}
