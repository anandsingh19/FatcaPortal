package com.newgen.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.newgen.DAO.LoginDAO;
import com.newgen.bean.GlobalVariables;

/**
 * Servlet implementation class LogoutServlet
 */
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(LogoutServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LogoutServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String userEmail = null;
		String updateStatus = "success";
		try {
			PrintWriter out = response.getWriter();
			response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Cache-Control", "no-store");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", -1);
			HttpSession session = request.getSession(false);
			LoginDAO login = new LoginDAO();
			if (session != null) {
				logger.debug("when session not null");
				userEmail = (String) session.getAttribute("UserEmail");
			} else {
				logger.debug("when email is being gotten from map");
				userEmail = GlobalVariables.loginUserIPv4Map.get(InetAddress
						.getLocalHost().toString());

			}
			if (null == userEmail) {
				userEmail = GlobalVariables.loginUserIPv4Map.get(InetAddress
						.getLocalHost().toString());
			}
			logger.debug("userEmail in logout servlet--> " + userEmail);
			/* || request.getParameter("code").equalsIgnoreCase("user2") */
			if (request.getParameter("code").equalsIgnoreCase("user")) {
				updateStatus = login.updateUserLoginDetail(userEmail);
			} else if (request.getParameter("code").equalsIgnoreCase(
					"sessionExpire")) {
				updateStatus = login.updateUserLoginDetail(userEmail);
			} else if (session.getId().equalsIgnoreCase(
					GlobalVariables.loginUserMap.get(userEmail))) {
				updateStatus = login.updateUserLoginDetail(userEmail);
			}
			// if (updateStatus.equalsIgnoreCase("success")) {

			/*
			 * session.setAttribute("UserEmail", "");
			 * session.setAttribute("UserName", ""); //
			 * session.setAttribute("loggedUserID", session.getId());
			 * session.setAttribute("UserType", "");
			 * session.setAttribute("UserGIIN", ""); session.invalidate();
			 * session = null;
			 */
			if (GlobalVariables.loginUserMap.containsKey(userEmail)
					&& request.getParameter("code").equalsIgnoreCase("jsp")) {
				GlobalVariables.loginUserMap.remove(userEmail);
			}
			out.print(updateStatus);
			// }

			/* response.sendRedirect(request.getContextPath()); */

		} catch (Exception e) {
			logger.debug("Exception in LogoutServlet--> " + e.getMessage());
			request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(
					request, response);

		}
	}

}
