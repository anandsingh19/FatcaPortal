/*------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : user login
 File Name                                                   : LoginServlet
 Author                                                      : Khushdil Kaushik
 Date (DD/MM/YYYY)                                           : 16/10/2015
 Description                                                 : user authentication check
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    Change Description
 10260				 18/11/2015		Khushdil		user check- if Fi user go to transaction page
 ------------------------------------------------------------------------------------------------------*/

package com.newgen.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.newgen.DAO.LoginDAO;
import com.newgen.bean.GlobalMessage;
import com.newgen.bean.GlobalVariables;
import com.newgen.bean.LoginUser;
import com.newgen.factory.ObjectFactory;

//import com.newgen.util.ClsUtil;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(LoginServlet.class);

	public LoginServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String loginStatus = null;
		// String oldSession = null;
		try {
			LoginDAO login = new LoginDAO();
			String userEmail = request.getParameter("userName");
			PrintWriter out = response.getWriter();
			if ("b_userLoggedin".equalsIgnoreCase(request.getParameter("code"))) {
				if (login.isUserlogedIn(userEmail)) {
					out.print(true);
				} else {
					out.print(false);
				}
			} else {
				logger.info("The server is going to validat login");
				LoginUser user = ObjectFactory.getLoginUserObject();
				HttpSession session = null;
				session = request.getSession(false);
				String Password = request.getParameter("password");
				logger.info("Password--> " + Password);
				if (Password != null) {
					user.setUserEmail(request.getParameter("userName"));
					user.setPassword(Password);
					if (login != null) {
						loginStatus = login.validateUser(user);
						logger.debug("loginStatus-->  " + loginStatus);
						if (loginStatus.equalsIgnoreCase("success")) {
							if (user.getUserFlag().equalsIgnoreCase("1")) {
								if (request.getSession(false) == null) {
									session = request.getSession(true);
								}
								session.setAttribute("UserEmail",
										user.getUserEmail());
								session.setAttribute("UserName",
										user.getUserName());
								session.setAttribute("loggedUserID",
										session.getId());
								session.setAttribute("UserType",
										user.getUserType());
								session.setAttribute("UserGIIN", user.getGiin());
								session.setAttribute("ComplianceType",
										user.getUserComplianceType());
								login.saveLoggeruser(user, session);

								/*
								 * oldSession = GlobalVariables.loginUserMap
								 * .get(user.getUserEmail());
								 * 
								 * if (oldSession != null) {
								 * GlobalVariables.loginUserMap.remove(user
								 * .getUserEmail());
								 * 
								 * }
								 */
								GlobalVariables.loginUserMap.put(
										user.getUserEmail(), session.getId());
								GlobalVariables.loginUserIPv4Map.put(
										InetAddress.getLocalHost().toString(),
										user.getUserEmail());

								/*
								 * if (user.getUserType()
								 * .equalsIgnoreCase("admin") ||
								 * user.getUserType().equalsIgnoreCase(
								 * "FI admin")) {
								 */
								request.setAttribute("searchFor", "Blank");
								request.setAttribute("searchIn", "Blank");

								/*
								 * request.getRequestDispatcher(
								 * "JSP/admin-approval.jsp").forward( request,
								 * response);
								 */
								request.getRequestDispatcher("JSP/home.jsp")
										.forward(request, response);
								/*
								 * } else { ServletContext context = this
								 * .getServletContext(); if
								 * ("crs".equalsIgnoreCase(user
								 * .getUserComplianceType())) {
								 * RequestDispatcher dispatcher = context
								 * .getRequestDispatcher("/ReportStatusCRS");
								 * dispatcher.forward(request, response); } else
								 * {
								 * 
								 * RequestDispatcher dispatcher = context
								 * .getRequestDispatcher("/ReportStatus");
								 * dispatcher.forward(request, response); } }
								 */

							} else {
								request.setAttribute("msg",
										GlobalMessage.IN_ACTIVE_USR_MSG);
								request.getRequestDispatcher("index.jsp")
										.forward(request, response);
							}

						} else {
							request.setAttribute("msg", loginStatus);
							request.getRequestDispatcher("index.jsp").forward(
									request, response);
						}

					}

				} else {
					logger.debug("Not a proper way to access the page");
					request.getRequestDispatcher("index.jsp").forward(request,
							response);
				}
			}
		} catch (Exception e) {
			logger.debug(e.getMessage());
			e.printStackTrace();
			request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(
					request, response);

		}

	}
}
