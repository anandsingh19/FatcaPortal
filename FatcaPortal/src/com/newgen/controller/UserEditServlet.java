package com.newgen.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.newgen.DAO.EditUserDAO;
import com.newgen.bean.FIRegistration;
import com.newgen.bean.GlobalVariables;
import com.newgen.bean.LoginUser;
import com.newgen.factory.ObjectFactory;

/**
 * Servlet implementation class MyInfoEdit
 */
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(UserEditServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserEditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		String userEmail = null;
		EditUserDAO edituser = null;
		FIRegistration fiuser = null;
		if (session != null) {
			String event = request.getParameter("event");
			edituser = ObjectFactory.getEditUserDAOObject();
			userEmail = (String) session.getAttribute("UserEmail");
			PrintWriter out = response.getWriter();
			/*
			 * if (session.getId().equalsIgnoreCase(
			 * GlobalVariables.loginUserMap.get(userEmail))) {
			 */
			if (event.equalsIgnoreCase("get")) {
				fiuser = edituser.getuserDetails(userEmail);
				request.setAttribute("fiUserObject", fiuser);
				request.getRequestDispatcher("JSP/MyInfoEdit.jsp").forward(
						request, response);

			} else if (event.equalsIgnoreCase("update")) {

				String emailID = userEmail;
				String password = request.getParameter("newPassWord");
				String oldPassword = request.getParameter("oldPassword");
				// String hashValuePwd = null;
				// Customable custom = ObjectFactory.getCustomObject();
				// hashValuePwd = custom.getHashValue(oldPassword);
				LoginUser user = ObjectFactory.getLoginUserObject();
				user.setUserEmail(userEmail);
				user.setPassword(oldPassword);
				if (edituser.validateOldPassword(user)) {
					boolean flag = edituser.isEdit(emailID, password);
					if (flag) {
						out.print("sucess");
					} else {
						out.print("fail");
					}
				} else {
					out.print("oldfail");
				}

			} else if (event.equalsIgnoreCase("updateForgotPassword")) {

				String emailID = request.getParameter("emailID");
				String password = request.getParameter("newPassWord");

				boolean flag = edituser.isEdit(emailID, password);
				if (flag) {
					out.print("sucess");
				} else {
					out.print("fail");
				}

			} else if (event.equalsIgnoreCase("validate")) {
				String password = request.getParameter("oldPassWord");
				LoginUser user = ObjectFactory.getLoginUserObject();
				user.setUserEmail(userEmail);
				user.setPassword(password);
				if (edituser.validate(user)) {
					out.print("success");
				} else {
					out.print("fail");
				}

			}
			/*
			 * } else { out.print("expire");
			 * 
			 * logger.debug("session expired , session id not matching");
			 * RequestDispatcher rd = request
			 * .getRequestDispatcher("JSP/SessionExpired.jsp");
			 * rd.forward(request, response);
			 * 
			 * }
			 */

		} else {

			logger.info("Your Session has been expire");
			/*
			 * request.getRequestDispatcher("JSP/SessionExpired.jsp").forward(
			 * request, response);
			 */
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		}

	}
	/*protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		String userEmail = null;
		EditUserDAO edituser = null;
		FIRegistration fiuser = null;
		if (session != null) {
			String event = request.getParameter("event");
			edituser = ObjectFactory.getEditUserDAOObject();
			userEmail = (String) session.getAttribute("UserEmail");
			PrintWriter out = response.getWriter();
			if (session.getId().equalsIgnoreCase(
					GlobalVariables.loginUserMap.get(userEmail))) {
				if (event.equalsIgnoreCase("get")) {
					fiuser = edituser.getuserDetails(userEmail);
					request.setAttribute("fiUserObject", fiuser);
					request.getRequestDispatcher("JSP/MyInfoEdit.jsp").forward(
							request, response);

				} else if (event.equalsIgnoreCase("update")
						|| event.equalsIgnoreCase("updateForgotPassword")) {

					String emailID = (event.equalsIgnoreCase("update") ? userEmail
							: request.getParameter("emailID"));
					String password = request.getParameter("newPassWord");

					boolean flag = edituser.isEdit(emailID, password);
					if (flag) {
						out.print("sucess");
					} else {
						out.print("fail");
					}

				} else if (event.equalsIgnoreCase("validate")) {
					String password = request.getParameter("oldPassWord");
					LoginUser user = ObjectFactory.getLoginUserObject();
					user.setUserEmail(userEmail);
					user.setPassword(password);
					if (edituser.validate(user)) {
						out.print("success");
					} else {
						out.print("fail");
					}

				}
			} else {
				out.print("expire");
				
				 * logger.debug("session expired , session id not matching");
				 * RequestDispatcher rd = request
				 * .getRequestDispatcher("JSP/SessionExpired.jsp");
				 * rd.forward(request, response);
				 
			}

		} else {

			logger.info("Your Session has been expire");
			
			 * request.getRequestDispatcher("JSP/SessionExpired.jsp").forward(
			 * request, response);
			 
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		}

	}*/

}
