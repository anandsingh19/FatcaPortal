/*------------------------------------------------------------------------------------------------------
 -                      NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Saint Lucia ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : Admin Report
 File Name                                                   : CustomReportServlet
 Author                                                      : Khushdil Kaushik
 Date (DD/MM/YYYY)                                           : 09/06/2016
 Description                                                 : user Registration 
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    Change Description
 
 ------------------------------------------------------------------------------------------------------
 */

package com.newgen.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.newgen.bean.GlobalMessage;
import com.newgen.bean.GlobalVariables;
import com.newgen.factory.ObjectFactory;
import com.newgen.interfaces.Customable;

/**
 * Servlet implementation class CustomReportServlet
 */
public class CustomReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(CustomReportServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustomReportServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		logger.debug("inside CustomeReportServlet::  dopost");
		String reportType = null;
		String userType = null;
		String userGIIN = null;
		//String timestamp = null;
		String userEmail = null;

		try {
			HttpSession session = null;
			session = request.getSession(false);
			userType = (String) session.getAttribute("UserType");
			userGIIN = (String) session.getAttribute("UserGIIN");
			userEmail = (String) session.getAttribute("UserEmail");
			reportType = request.getParameter("reportType");
			logger.debug("path--> " + GlobalVariables.customReportPath);
			/*Calendar now = Calendar.getInstance(); // Gets the current
			DateFormat dateFormat = new SimpleDateFormat("dd_MM_yy_HHmmss");
			timestamp = dateFormat.format(now.getTime());*/
			File f = new File(GlobalVariables.customReportPath + File.separator
					+ userEmail);
			if (!f.exists()) {
				f.mkdirs();
			}
			logger.debug("reportType--> " + reportType);
			if (null != reportType && "allUser".equalsIgnoreCase(reportType)) {
				reportForAllUser(request, response, userType, userGIIN,
						GlobalVariables.customReportPath + File.separator
								+ userEmail);
			} else if (null != reportType
					&& "userofGIIN".equalsIgnoreCase(reportType)) {
				reportOfUserForGIIN(request, response, userType, userGIIN,
						GlobalVariables.customReportPath + File.separator
								+ userEmail);
			} else if (null != reportType
					&& "yearlyReport".equalsIgnoreCase(reportType)) {
				yearlyReportStatus(request, response, userType, userGIIN,
						GlobalVariables.customReportPath + File.separator
								+ userEmail);
			}
		} catch (Exception e) {
			logger.debug("Exception in CustomeReportServlet dopost--> "
					+ e.getMessage());
		}
	}

	// *********************************************************************************
	// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
	// Group : Application Projects1 (US)
	// Project : [Fatca Reporting St.Lucia]
	// Date Written : 09/06/2016
	// Date Modified :
	// Author : Khushdil Kaushik
	// Description : To wrapper to generate registered users report
	// ***********************************************************************************/

	public void reportForAllUser(HttpServletRequest request,
			HttpServletResponse response, String userType, String userGIIN,
			String downloadPath) {
		boolean reportGenerated = false;

		String filename = downloadPath + File.separator + "RegisteredUsers.pdf";
		String reportType = "ALL_USER";
		String searchFor = "";
		
		try {
			Customable custom = ObjectFactory.getCustomObject();
			reportGenerated = custom.generateReportFile(filename, userType,
					userGIIN, reportType, searchFor);
			if (reportGenerated) {
				downloadReport(request, response, filename);
			} else {
				request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(
						request, response);
			}

		} catch (Exception e) {
			logger.debug("Exception in CustomeReportServlet reportForAllUser--> "
					+ e.getMessage());
		}
	}

	// *********************************************************************************
	// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
	// Group : Application Projects1 (US)
	// Project : [Fatca Reporting St.Lucia]
	// Date Written : 13/06/2016
	// Date Modified :
	// Author : Khushdil Kaushik
	// Description : To wrapper to generate registered users history report in a
	// date range
	// ***********************************************************************************/

	public void reportOfUserForGIIN(HttpServletRequest request,
			HttpServletResponse response, String userType, String userGIIN,
			String downloadPath) {
		boolean reportGenerated = false;

		String filename = downloadPath + File.separator + "UsersHistory.pdf";
		String searchFor = "";
		String fromDate = null;
		String toDate = null;
		String reportType = "USER_FOR_GIIN";
		try {
			fromDate = request.getParameter("fromDate");
			toDate = request.getParameter("toDate");
			Customable custom = ObjectFactory.getCustomObject();
			reportGenerated = custom.generateUserHistReport(filename, userType,
					userGIIN, reportType, searchFor, fromDate, toDate);
			if (reportGenerated) {
				downloadReport(request, response, filename);

			} else {
				request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(
						request, response);
			}
		} catch (Exception e) {
			logger.debug("Exception in CustomeReportServlet reportOfUserForGIIN--> "
					+ e.getMessage());
		}
	}

	// *********************************************************************************
	// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
	// Group : Application Projects1 (US)
	// Project : [Fatca Reporting St.Lucia]
	// Date Written : 15/06/2016
	// Date Modified :
	// Author : Khushdil Kaushik
	// Description : To wrapper to generate transaction history report for the
	// selected year range
	// ***********************************************************************************/

	public void yearlyReportStatus(HttpServletRequest request,
			HttpServletResponse response, String userType, String userGIIN,
			String downloadPath) {
		boolean reportGenerated = false;

		String filename = downloadPath + File.separator
				+ "TransactionHistory.pdf";
		String searchFor = null;
		String fromDate = null;
		String toDate = null;
		String reportType = "YEARLY_REPORT";
		try {
			searchFor = request.getParameter("giinForYearlyReport");
			fromDate = request.getParameter("fromYear_custom_report");
			toDate = request.getParameter("toYear_custom_report");
			Customable custom = ObjectFactory.getCustomObject();
			reportGenerated = custom
					.generateTransactionHist(filename, userType, userGIIN,
							reportType, searchFor, fromDate, toDate);
			if (reportGenerated) {
				downloadReport(request, response, filename);

			} else {
				request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(
						request, response);
			}
		} catch (Exception e) {
			logger.debug("Exception in CustomeReportServlet yearlyReportStatus--> "
					+ e.getMessage());
		}
	}

	// *********************************************************************************
	// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
	// Group : Application Projects1 (US)
	// Project : [Fatca Reporting St.Lucia]
	// Date Written : 13/06/2016
	// Date Modified :
	// Author : Khushdil Kaushik
	// Description : To download the generated rport
	// ***********************************************************************************/

	public void downloadReport(HttpServletRequest request,
			HttpServletResponse response, String path) throws ServletException,
			IOException {

		File file = null;
		InputStream fis = null;
		ServletOutputStream os = null;

		try {
			file = new File(path);
			String fileName = file.getName();
			if (file.exists()) {
				logger.info("File location on server::"
						+ file.getAbsolutePath());
				ServletContext ctx = getServletContext();
				fis = new FileInputStream(file);
				String mimeType = ctx.getMimeType(file.getAbsolutePath());
				response.setContentType(mimeType != null ? mimeType
						: "application/octet-stream");
				response.setContentLength((int) file.length());
				response.setHeader("Content-Disposition",
						"attachment; filename=\"" + fileName + "\"");
				os = response.getOutputStream();
				byte[] bufferData = new byte[1024];
				int read = 0;
				while ((read = fis.read(bufferData)) != -1) {
					os.write(bufferData, 0, read);
				}
				logger.info("File downloaded at client successfully");
			} else {
				request.setAttribute("msg", GlobalMessage.FILE_NOT_FOUND);
				request.getRequestDispatcher("/JSP/DownloadError.jsp").forward(
						request, response);
			}
		} catch (FileNotFoundException ex) {
			logger.info("Exception in doPost FileNotFoundException of DownloadServlet  "
					+ ex.getMessage());
			request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(
					request, response);
		} catch (Exception ex) {
			logger.info("Exception in doPost Exception of DownloadServlet  "
					+ ex.getMessage());
			request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(
					request, response);

		} finally {

			try {
				if (os != null) {
					os.flush();
					os.close();
				}
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				logger.info("Exception in finally block doPost of DownloadServlet  "
						+ ex.getMessage());
				request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(
						request, response);
			}
		}
	}

}
