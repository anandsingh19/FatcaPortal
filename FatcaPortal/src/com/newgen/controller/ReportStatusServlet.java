/*------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : Transaction History of FI User
 File Name                                                   : ReportStatusServlet
 Author                                                      : Deepak Sharma
 Date (DD/MM/YYYY)                                           : 16/10/2015
 Description                                                 : fetch data to show on transaction history page
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    Change Description
 10254				 17/11/2015		Khushdil	chanhes/removed order by field in the query
 ------------------------------------------------------------------------------------------------------*/

package com.newgen.controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.newgen.DAO.ReportStatusDAO;
import com.newgen.factory.ObjectFactory;

public class ReportStatusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(ReportStatusServlet.class);

	public ReportStatusServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		doPost(request, response);
		
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		logger.info("inside ReportStatus");
		HttpSession session = null;
		String reportingYear = null;
		reportingYear = request.getParameter("reporting_year");
		logger.debug("reporting_year in servlet --> " + reportingYear);
		if (null == reportingYear) {
			Calendar now = Calendar.getInstance(); // Gets the current date and
													// time
			int year = now.get(Calendar.YEAR);
			reportingYear = (year - 1) + "";
		}

		ReportStatusDAO reportStatus = ObjectFactory.getReportStatusDAoObject();
		Map<String, Object> result = reportStatus.getUsertransactionDetails(
				request, response,"fatca",reportingYear);
		
		Boolean flag = (Boolean) result.get("status");
		logger.debug("Return status:" + flag);
		if (flag) {
			session = request.getSession(false);
			session.setAttribute("ReportingYear", reportingYear);
			request.setAttribute("XMLArrayList", result.get("XMLArrayList"));
			request.setAttribute("nextFlag", result.get("nextFlag"));
			request.setAttribute("pageCount", result.get("pageCount"));
			request.setAttribute("totalCount", result.get("totalCount"));
			request.getRequestDispatcher("/JSP/XMLTransactions.jsp").forward(
					request, response);
		} else {
			response.sendRedirect("index.jsp");

		}

	}
}
