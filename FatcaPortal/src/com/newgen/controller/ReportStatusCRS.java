package com.newgen.controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.newgen.DAO.ReportStatusDAO;
import com.newgen.factory.ObjectFactory;

/**
 * Servlet implementation class ReportStatusCRS
 */
public class ReportStatusCRS extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(ReportStatusCRS.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReportStatusCRS() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		logger.info("inside ReportStatus");
		HttpSession session = null;
		String reportingYear = null;
		reportingYear = request.getParameter("reporting_year");
		logger.debug("reporting_year in servlet --> " + reportingYear);
		if (null == reportingYear) {
			Calendar now = Calendar.getInstance(); // Gets the current date and
													// time
			int year = now.get(Calendar.YEAR);
			reportingYear = (year - 1) + "";
		}

		ReportStatusDAO reportStatus = ObjectFactory.getReportStatusDAoObject();
		Map<String, Object> result = reportStatus.getUsertransactionDetails(
				request, response,"crs",reportingYear);
		
		Boolean flag = (Boolean) result.get("status");
		logger.debug("Return status:" + flag);
		if (flag) {
			session = request.getSession(false);
			session.setAttribute("ReportingYear", reportingYear);
			request.setAttribute("XMLArrayList", result.get("XMLArrayList"));
			request.setAttribute("nextFlag", result.get("nextFlag"));
			request.setAttribute("pageCount", result.get("pageCount"));
			request.setAttribute("totalCount", result.get("totalCount"));
			request.getRequestDispatcher("/JSP/XMLTransactions-CRS.jsp").forward(
					request, response);
		} else {
			response.sendRedirect("index.jsp");

		}

	}

}
