package com.newgen.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;

import org.apache.log4j.Logger;

import com.newgen.DAO.Custom;
import com.newgen.bean.GlobalMessage;
import com.newgen.bean.GlobalVariables;
import com.newgen.factory.ObjectFactory;

/**
 * Servlet implementation class ManualServlet
 */
public class ManualServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(ManualServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ManualServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/*
	 * protected void doPost(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException { String reportingYear =
	 * request.getParameter("Manual_Reporting_Year"); String submissionType =
	 * request.getParameter("Manual_Submission_Type"); HttpSession session =
	 * request.getSession(false); String userEmail = null; try{ if (null !=
	 * session) { userEmail = (String) session.getAttribute("UserEmail");
	 * 
	 * if (session.getId().equalsIgnoreCase(
	 * GlobalVariables.loginUserMap.get(userEmail))) {
	 * 
	 * request.setAttribute("reportingYear", reportingYear);
	 * request.setAttribute("submissionType", submissionType);
	 * request.getRequestDispatcher("/JSP/Manual.jsp").forward( request,
	 * response); } else {
	 * logger.debug("session expired , session id not matching");
	 * RequestDispatcher rd = request
	 * .getRequestDispatcher("JSP/SessionExpired.jsp"); rd.forward(request,
	 * response); } } else {
	 * 
	 * logger.info("Your Session has been expire");
	 * request.getRequestDispatcher("index.jsp") .forward(request, response); }
	 * }catch(Exception ex){
	 * logger.info("There is some exception in Manual Servlet"+ex.getMessage());
	 * request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(request,
	 * response); } }
	 */

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String reportingYear = request.getParameter("Manual_Reporting_Year");
		String submissionType = request.getParameter("Manual_Submission_Type");
		String reportingMode = request.getParameter("reportingMode");
		 //Change for All by Shubham on 28072016  'parameter changed XMlType to complianceType 'starts 
		String complianceType = request.getParameter("complianceType");
		 //Change for All by Shubham on 28072016 end 
		//String complianceType = request.getParameter("XMLtype");

		HttpSession session = request.getSession(false);
		String userEmail = null;
		Custom custom = null;
		try {
			if (null != session) {
				userEmail = (String) session.getAttribute("UserEmail");

				if (session.getId().equalsIgnoreCase(
						GlobalVariables.loginUserMap.get(userEmail))) {

					custom = ObjectFactory.getCustomObject();
					/*Change for All by KD on 28072016 added one more parameter as actual -- starts*/
					if (custom.isReportinngModeCorrect(
							(String) session.getAttribute("UserGIIN"),
							reportingYear, reportingMode, complianceType,"Actual")) {

						request.setAttribute("reportingYear", reportingYear);
						request.setAttribute("submissionType", submissionType);
						request.getRequestDispatcher("/JSP/Manual.jsp")
								.forward(request, response);
					} else {
						request.setAttribute("SubmissionModeMsg",
								GlobalMessage.REPORT_SUBMISSION_MANUAL_MSG);
						request.getRequestDispatcher("/JSP/online-form.jsp")
								.forward(request, response);
					}
				} else {
					logger.debug("session expired , session id not matching");
					RequestDispatcher rd = request
							.getRequestDispatcher("JSP/SessionExpired.jsp");
					rd.forward(request, response);
				}
			} else {

				logger.info("Your Session has been expire");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			}
		} catch (Exception ex) {
			logger.info("There is some exception in Manual Servlet"
					+ ex.getMessage());
			request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(
					request, response);
		}
	}

}
