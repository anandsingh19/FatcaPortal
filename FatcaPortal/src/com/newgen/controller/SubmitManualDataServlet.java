package com.newgen.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.newgen.bean.GlobalVariables;
import com.newgen.factory.ObjectFactory;
import com.newgen.interfaces.UploadManualForm;

/**
 * Servlet implementation class SubmitManualData
 */
public class SubmitManualDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger
			.getLogger(SubmitManualDataServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SubmitManualDataServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		/* Change for all by KD on 28072016 starts */
		doPost(request, response);
		/* Change for all by KD on 28072016 end */
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/*
	 * protected void doPost(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException {
	 * 
	 * String event = request.getParameter("event"); UploadManualForm
	 * uploadManulForm = ObjectFactory .getUploadManualDAOObject(); HttpSession
	 * session = request.getSession(false); PrintWriter out =
	 * response.getWriter(); String emailID = null; String reportingYear = null;
	 * String submissionType = null;
	 * 
	 * try{ if (session != null) {
	 * 
	 * 
	 * emailID = (String) session.getAttribute("UserEmail"); reportingYear =
	 * (String) session.getAttribute("reportingYear"); String filerGIIN =
	 * (String) session.getAttribute("UserGIIN");
	 * 
	 * if (session.getId().equalsIgnoreCase(
	 * GlobalVariables.loginUserMap.get(emailID))) { if
	 * (event.equalsIgnoreCase("giinSubmissionType")) {
	 * 
	 * submissionType = uploadManulForm.getGIINSubmissionType( (String)
	 * session.getAttribute("UserGIIN"), reportingYear);
	 * out.print(submissionType);
	 * 
	 * } else if (event.equalsIgnoreCase("load")) {
	 * 
	 * // getData(request,response); reportingYear =
	 * request.getParameter("reportingYear"); org.json.JSONArray output =
	 * uploadManulForm .getManualFIInformation(filerGIIN, emailID,
	 * reportingYear); out.print(output); } else { String substantialOwner =
	 * request.getParameter("ownerJson"); String accountInformation = request
	 * .getParameter("manualJson");
	 * 
	 * reportingYear = request.getParameter("reportingYear"); submissionType =
	 * request.getParameter("submissionType"); if
	 * (uploadManulForm.isSaveManualFIInformation( substantialOwner,
	 * accountInformation, emailID, event, reportingYear, submissionType,
	 * filerGIIN)) {
	 * 
	 * out.print("success"); } else { out.print("fail"); }
	 * 
	 * } } else {
	 * logger.info("Your session has been expired. session id not matching");
	 * out.print("sessionExpired");
	 * 
	 * } } else { out.print("sessionExpired");
	 * logger.info("Your session has been expired"); } }catch(Exception ex){
	 * logger
	 * .info("There is some exception in submitting manual form"+ex.getMessage
	 * ()); out.print("Exception");
	 * 
	 * 
	 * }
	 * 
	 * }
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String event = request.getParameter("event");
		logger.debug("event in SubmitManualDataServlet-->  " + event);
		UploadManualForm uploadManulForm = ObjectFactory
				.getUploadManualDAOObject();
		HttpSession session = request.getSession(false);
		PrintWriter out = response.getWriter();
		String emailID = null;
		String reportingYear = null;
		String submissionType = null; // used for new or update
		String callFrom = null;
		String complianceType = null;
		/*Changes by Kd on 28072016 added code */	
		String testOrAcctual = null;

		try {
			reportingYear = request.getParameter("reportingYear");
			// we have set UserEmail as request from either Manual or Xml in
			// ajax call
			callFrom = request.getParameter("UserEmail");
			complianceType = request.getParameter("ComplianceType");
			/*Changes by Kd on 28072016 added code */
			testOrAcctual = request.getParameter("testActual"); 
			if (null != session) {
				emailID = (String) session.getAttribute("UserEmail");
				String filerGIIN = (String) session.getAttribute("UserGIIN");

				if (session.getId().equalsIgnoreCase(
						GlobalVariables.loginUserMap.get(emailID))) {
					if (event.equalsIgnoreCase("giinSubmissionType")) {
						/*Changes by Kd on 28072016 added one more param */
						submissionType = uploadManulForm.getGIINSubmissionType(
								(String) session.getAttribute("UserGIIN"),
								reportingYear, callFrom, complianceType,testOrAcctual);
						/*Changes by Kd on 28072016 end */
						out.print(submissionType);

					} else if (event.equalsIgnoreCase("load")) {

						// getData(request,response);
						reportingYear = request.getParameter("reportingYear");
						org.json.JSONArray output = uploadManulForm
								.getManualFIInformation(filerGIIN, emailID,
										reportingYear);
						out.print(output);
					} else {

						String substantialOwner = request
								.getParameter("ownerJson");
						String accountInformation = request
								.getParameter("manualJson");

						// reportingYear =
						// request.getParameter("reportingYear");
						submissionType = request.getParameter("submissionType");
						if (uploadManulForm
								.isSaveManualFIInformation(substantialOwner,
										accountInformation, emailID, event,
										reportingYear, submissionType,
										filerGIIN)) {

							out.print("success");
						} else {
							out.print("fail");
						}

					}
				} else {
					logger.info("Your session has been expired. session id not matching");
					out.print("sessionExpired");

				}
			} else {
				out.print("sessionExpired");
				logger.info("Your session has been expired");
			}
		} catch (Exception ex) {
			logger.info("There is some exception in submitting manual form"
					+ ex.getMessage());
			out.print("Exception");

		}

	}

}
