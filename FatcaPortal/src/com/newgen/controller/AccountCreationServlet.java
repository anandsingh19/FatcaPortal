package com.newgen.controller;

//import java.io.BufferedReader;
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileReader;
//import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
//import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.newgen.DAO.Custom;
import com.newgen.DAO.RegistrationDAO;
//import com.lowagie.tools.split_pdf;
import com.newgen.bean.FIRegistration;
import com.newgen.bean.GlobalMessage;
import com.newgen.bean.GlobalVariables;
//import com.newgen.gui.XMLValidation;
import com.newgen.factory.ObjectFactory;
import com.newgen.interfaces.Resgistrable;

//import java.security.MessageDigest;

/**
 * Servlet implementation class AccountCreationServlet
 */
public class AccountCreationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger
			.getLogger(AccountCreationServlet.class);
	private static final int bytesInMB = 1024 * 1024; // 1MB

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AccountCreationServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String option = null;
		PrintWriter out = response.getWriter();
		option = request.getParameter("option");
		Resgistrable registertaion = null;
		FIRegistration fIRegistration = null;
		registertaion = ObjectFactory.getRegistrationDAOObject();
		fIRegistration = ObjectFactory.getFIRegistrationObject();
		logger.info("option:" + option);

		if (option != null && option.length() != 0
				&& option.equalsIgnoreCase("searchgiin")) {
			response.setContentType("application/json");

			JSONObject fiInfo = registertaion.getFilerInformation(request
					.getParameter("giin").trim());
			if (fiInfo != null)
				out.print(fiInfo);

		}

		else if (option != null && option.length() != 0
				&& option.equalsIgnoreCase("getMaster")) {
			String result = registertaion.getMasterDropDown(request
					.getParameter("columnName"));
			if (result != null) {
				out.print(result);
			} else {
				logger.info("There is some Problem in getting master information");
			}

		}

		/**/
		else if (option != null && option.length() != 0
				&& option.equalsIgnoreCase("ChkMaxFiAdmin")) {
			logger.debug("going to getFiAdminCount");
			logger.debug("giin--> " + request.getParameter("giin"));
			String sMaxFiAdmin = registertaion.getFiAdminCount(request
					.getParameter("giin"));
			logger.debug("sMaxFiAdmin--> " + sMaxFiAdmin);
			if (null != sMaxFiAdmin
					&& Integer.parseInt(sMaxFiAdmin) >= GlobalVariables.iMaxFiAdmin) {
				out.print("True");
			} else {
				out.print("False");
			}
		}
		/**/
		else if (option != null && option.length() != 0
				&& option.equalsIgnoreCase("checkFileFormat")) {
			String fileName = request.getParameter("fileName");
		
// changes made by Alok on 21-11-2016 Start
			String fileSize12 =request.getParameter("filesize");
			double fileSize=Double.parseDouble(fileSize12);
			String fileSize22 =request.getParameter("filesize21");
			double fileSize23=Double.parseDouble(fileSize22);
		
			if(fileSize >= 0.0 || fileSize23>=0.0)
			{
				if(fileSize > GlobalVariables.supportiveDoc1MaXSize)
				{
					out.print("False@@@" + GlobalVariables.supportiveDoc1MaXSize +"MB or smaller ");
				}
				
				if(fileSize23 > GlobalVariables.supportiveDoc2MaXSize)
				{
					out.print("False@@@" + GlobalVariables.supportiveDoc2MaXSize +"MB or smaller ");
				}
				
			}
			
			//Changes made by Alok on 21-11-2016 End
			
			logger.info("fileName--> " + fileName);
			if (isFileFomatValid(fileName))
				out.print("True");
			else
				out.print("False@@@" + GlobalVariables.supportiveDocType);

		} else if (isUserSave(fIRegistration, request, response, registertaion)) {
			request.setAttribute("act_creation_msg",
					GlobalMessage.NEW_ACCOUNT_CREATED);
			request.setAttribute("source", "registration");
			request.getRequestDispatcher("JSP/successfull.jsp").forward(
					request, response);
		} else {

			// Kindly write dow error page

			logger.info("There is some error in creating new account");
			request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(
					request, response);

		}

	}

	public boolean isFileFomatValid(String fileName)

	{
		boolean validFile = false;
		Custom custom = null;
		custom = ObjectFactory.getCustomObject();
		String fileExtention = null;
		try {
			if (fileName.length() != 0) {
				fileExtention = custom.getFileExtention(fileName);
				if (fileExtention
						.equalsIgnoreCase(GlobalVariables.supportiveDocType))
					validFile = true;

			}

		} catch (Exception e) {
			logger.info("Exception in isFileFomatValid-->  " + e.getMessage());
		}
		return validFile;

	}

	public boolean isUserSave(FIRegistration fIRegistration,
			HttpServletRequest request, HttpServletResponse response,
			Resgistrable registertaion) throws IOException, ServletException {

		logger.info("inside register User");
		Map<String, String> ReqAttrMap = new HashMap<String, String>();
		Map<String, InputStream> fileInStrmMap = new HashMap<String, InputStream>();
		boolean flag = false;
		String fatcaCompliance = null;
		String crsCompliance = null;
		String complianceType = null;
		/*
		 * String fileTypeExt1 = null; String fileTypeExt2 = null; String
		 * fileType1 = null; String fileType2 = null;
		 */
		try {
			// Custom custom = ObjectFactory.getCustomObject();

			if (ServletFileUpload.isMultipartContent(request)) {

				DiskFileItemFactory factory = new DiskFileItemFactory();
				factory.setSizeThreshold(bytesInMB
						* GlobalVariables.memoryThreshold);
				ServletFileUpload upload = new ServletFileUpload(factory);
// changes made by Alok on 21-11-2016 Start
				upload.setFileSizeMax(bytesInMB
						* GlobalVariables.supportiveDoc1MaXSize);
				upload.setSizeMax(bytesInMB
						* GlobalVariables.supportiveDoc1MaXSize);
				upload.setFileSizeMax(bytesInMB
						* GlobalVariables.supportiveDoc2MaXSize);
				upload.setSizeMax(bytesInMB
						* GlobalVariables.supportiveDoc2MaXSize);
				//Changes made by Alok on 21-11-2016 End
				List items = upload.parseRequest(request);
				Iterator iter = items.iterator();
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();
					if (item.isFormField()) {
						ReqAttrMap.put(item.getFieldName(), item.getString());
					} else {
						ReqAttrMap.put(item.getFieldName(), item.getName());
						fileInStrmMap.put(item.getFieldName(),
								item.getInputStream());
					}
				}
				crsCompliance = ReqAttrMap.get("crsCompliance");
				fatcaCompliance = ReqAttrMap.get("fatcaCompliance");
				if ("0".equalsIgnoreCase(crsCompliance)
						&& "1".equalsIgnoreCase(fatcaCompliance)) {
					complianceType = "FATCA";
					fIRegistration.setGiin(ReqAttrMap.get("giin"));
					fIRegistration.setInType(ReqAttrMap.get("in_type"));
				} else if ("1".equalsIgnoreCase(crsCompliance)
						&& "0".equalsIgnoreCase(fatcaCompliance)) {
					complianceType = "CRS";
					// giin1 and in_type1 for in in case of crs
					fIRegistration.setGiin(ReqAttrMap.get("giin1"));
					fIRegistration.setInType(ReqAttrMap.get("in_type1"));
				} else if ("1".equalsIgnoreCase(crsCompliance)
						&& "1".equalsIgnoreCase(fatcaCompliance)) {
					complianceType = "BOTH";
					fIRegistration.setGiin(ReqAttrMap.get("giin"));
					fIRegistration.setInType(ReqAttrMap.get("in_type"));
				}

				fIRegistration.setFiName(ReqAttrMap.get("fi_name"));
				fIRegistration.setFiEmail(ReqAttrMap.get("fi_email"));
				fIRegistration.setFiCountry(ReqAttrMap.get("fi_country"));
				fIRegistration.setFiAddress(ReqAttrMap.get("fi_address"));
				fIRegistration.setFiPhone(ReqAttrMap.get("fi_Phone"));

				fIRegistration.setUserEmailId(ReqAttrMap.get("user_email"));
				fIRegistration.setUserName(ReqAttrMap.get("user_name"));
				fIRegistration.setUserAddress(ReqAttrMap.get("user_address"));
				fIRegistration.setUserEmployeeID(ReqAttrMap.get("user_empid"));
				fIRegistration.setUserDesignation(ReqAttrMap
						.get("user_designation"));
				fIRegistration.setUserCountry(ReqAttrMap.get("user_country"));
				fIRegistration.setUserPhone(ReqAttrMap.get("user_phone"));
				fIRegistration.setUserGender(ReqAttrMap.get("user_gender"));
				fIRegistration.setUserDOB(ReqAttrMap.get("userDOB"));
				logger.debug("userDOB--> " + ReqAttrMap.get("user_gender"));
				logger.debug("user_gender--> " + ReqAttrMap.get("userDOB"));
				fIRegistration.setUserDocType1(ReqAttrMap.get("document1"));
				fIRegistration.setUserDocType2(ReqAttrMap.get("document2"));
				fIRegistration.setUserDocType1File1(ReqAttrMap.get("file1"));
				fIRegistration.setUserDocType2File2(ReqAttrMap.get("file2"));
				fIRegistration.setUserPassword(ReqAttrMap
						.get("fi_password_Reg"));
				fIRegistration.setOperation(ReqAttrMap.get("fiUserType"));

				fIRegistration.setComplianceType(complianceType);
				if (registertaion.registerUser(fIRegistration, fileInStrmMap)) {
					flag = true;
				}

			}

		} catch (IOException ex) {

			logger.info("AccountCreationServlet:isUserSave:There  is some IO Exception "
					+ ex.getMessage());
		} catch (Exception e) {

			logger.info("AccountCreationServlet:isUserSave:There is some Exception "
					+ e.getMessage());
		}

		return flag;

	}
}
