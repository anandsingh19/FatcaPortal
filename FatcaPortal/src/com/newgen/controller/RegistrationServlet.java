package com.newgen.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.newgen.DAO.GenearateOTPDAO;
import com.newgen.bean.OTP;
import com.newgen.factory.ObjectFactory;

public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(RegistrationServlet.class);

	public RegistrationServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		doPost(request, response);

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		logger.info("Inside doPost of RegistrationServlet");
		PrintWriter out = null;
		out = response.getWriter();
		OTP otp = ObjectFactory.getOTPbject();
		try {

			otp.setEmail(request.getParameter("email"));
			otp.setOtp(request.getParameter("otp"));
			otp.setSourceType(request.getParameter("source"));
			otp.setEvent(request.getParameter("event"));
			GenearateOTPDAO genearateOTP = ObjectFactory
					.getGenearateOTPDAObject();
			if ("CheckOTPExist".equalsIgnoreCase(request.getParameter("event"))) {
				genearateOTP.isOTPExist(otp);
				if (genearateOTP.isOTPExist(otp)) {
					out.print("success");
					logger.info("OTP exist");

				} else {
					out.print("fail");
					logger.info("OTP  not exist");
				}
			} else {
				if (genearateOTP.validate(otp)) {
					out.print("success");
					logger.info("Successfuly generated");

				} else {
					out.print("fail");
					logger.info("fail generated.....");
				}
			}
		} catch (Exception ex) {
			logger.info("There is some exception in Registraion Servlet"
					+ ex.getMessage());
			request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(
					request, response);
		}

	}

}
