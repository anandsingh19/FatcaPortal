package com.newgen.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.newgen.bean.GlobalMessage;
import com.newgen.bean.GlobalVariables;

/**
 * Servlet implementation class DownloadServlet
 */
public class DownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(DownloadServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DownloadServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if ((String) request.getParameter("path") != null) {

			downloadErrorFile(request, response,
					(String) request.getParameter("path"));
		} else {
			response.sendRedirect("index.jsp");
		}

	}

	public void downloadErrorFile(HttpServletRequest request,
			HttpServletResponse response, String path) throws ServletException,IOException {

		File file = null;
		InputStream fis = null;
		ServletOutputStream os = null;

		try {
			file = new File(path);
			String fileName = file.getName();
			if (file.exists()) {

				logger.info("File location on server::"
						+ file.getAbsolutePath());
				ServletContext ctx = getServletContext();
				fis = new FileInputStream(file);
				String mimeType = ctx.getMimeType(file.getAbsolutePath());
				response.setContentType(mimeType != null ? mimeType
						: "application/octet-stream");
				response.setContentLength((int) file.length());
				response.setHeader("Content-Disposition",
						"attachment; filename=\"" + fileName + "\"");

				os = response.getOutputStream();
				byte[] bufferData = new byte[1024];
				int read = 0;
				while ((read = fis.read(bufferData)) != -1) {
					os.write(bufferData, 0, read);
				}

				logger.info("File downloaded at client successfully");
			} else {
				request.setAttribute("msg", GlobalMessage.FILE_NOT_FOUND);
				request.getRequestDispatcher("/JSP/DownloadError.jsp").forward(
						request, response);
				//throw new ServletException("File doesn't exists on server.");
			}
		} catch (FileNotFoundException ex) {
			logger.info("Exception in doPost FileNotFoundException of DownloadServlet  "
					+ ex.getMessage());
			request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(request, response);
		} catch (Exception ex) {
			logger.info("Exception in doPost Exception of DownloadServlet  "
					+ ex.getMessage());
			request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(request, response);

		} finally {

			try {
				if (os != null) {
					os.flush();
					os.close();
				}
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				logger.info("Exception in finally block doPost of DownloadServlet  "
						+ ex.getMessage());
				request.getRequestDispatcher("JSP/ExceptionPage.jsp").forward(request, response);
			}
		}
	}
}
