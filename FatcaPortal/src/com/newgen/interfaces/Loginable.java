package com.newgen.interfaces;

import javax.servlet.http.HttpSession;

import com.newgen.bean.LoginUser;

public interface Loginable {

	String validateUser(LoginUser user);

	String isAuthenticated(LoginUser user);
	String isOldPasswordAuthenticated(LoginUser user);

	void saveLoggeruser(LoginUser user, HttpSession session);

	boolean isUserlogedIn(String userEmail);
	String updateUserLoginDetail(String userEmail);

}
