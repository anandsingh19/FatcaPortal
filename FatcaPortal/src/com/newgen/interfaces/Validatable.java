package com.newgen.interfaces;

import java.io.IOException;

import com.newgen.bean.LoginUser;



public interface Validatable {
	
	boolean validate(Object obj);
	boolean validateOldPassword(LoginUser user);

}
