package com.newgen.interfaces;

import java.sql.Connection;

import org.json.JSONArray;

public interface UploadManualForm {

	JSONArray getManualFIInformation(String filerGIIN, String emailID,
			String reportingYear);

	boolean isSaveManualFIInformation(String substantialOwner,
			String AccountInformation, String emailID, String event,
			String reportingYear, String submissionType, String filerGIIN);

	boolean isDeleteManualFIInformation(String rowID, String source);

	String getGIINSubmissionType(String giin, String reportingYear,
			String callFrom, String complianceType,String testOrActual);

	String checkSpecialChar(String inputdata);

}
