package com.newgen.interfaces;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public interface XMLUploadable {

	void Insertdata(String FileName, String FileType, String FileSize,
			String FileVersion, String FileUploadBy, String FileUploadDate,
			String FileStatus, String FileUploadPath, String LogDownloadPath,
			String ReportingType, String ReportingYear, String stage,String giin,String complianceType)
			throws SQLException, ClassNotFoundException;

	int getFileVersion(String userEmail, String sFilename);
}
