package com.newgen.interfaces;

import java.io.InputStream;
import java.util.Map;

import org.json.simple.JSONObject;

public interface Resgistrable {

	JSONObject getFilerInformation(String giin);

	boolean registerUser(Object obj, Map<String, InputStream> map);

	String getMasterDropDown(String columnName);

	String getFiAdminCount(String giin);
}
