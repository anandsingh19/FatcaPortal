package com.newgen.interfaces;

import org.json.JSONObject;

public interface Transactional {
	JSONObject getBatch(String pageNo, String totalCount, String userName,
			String userEmail, String userGiin, String complianceType,
			String reportingYear);
}
