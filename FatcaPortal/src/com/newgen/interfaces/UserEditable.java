package com.newgen.interfaces;

import com.newgen.bean.FIRegistration;

public interface UserEditable {
	
	FIRegistration getuserDetails(String emailID);
	boolean isEdit(String emailID,String password);
}
