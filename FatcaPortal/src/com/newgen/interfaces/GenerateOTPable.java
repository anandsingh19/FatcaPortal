package com.newgen.interfaces;

import com.newgen.bean.OTP;

public interface GenerateOTPable {
	String generatOTPValue(OTP obj);
	boolean saveOTP(String hashCode,String EmailID,String oTPSource);
	boolean isOTPvalidated(String hashCode, String emailID, String oTPSource);
	boolean isOTPExist( OTP obj);

}
