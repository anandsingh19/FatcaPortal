package com.newgen.interfaces;

import java.io.InputStream;
import java.util.Map;

public interface Customable {
	String getHashValue(String input);

	boolean SaveSupportiveDoc(Object obj,
			Map<String, InputStream> fileInStreamMap);

	boolean SaveUploadedFile(String SaveLocation, String sourceFileName,
			InputStream fileContent);

	String getFileName(String file);

	String getFileExtention(String file);

	boolean saveReplacedDoc(Object obj, Map<String, InputStream> fileInStrmMap,
			Map<String, String> ReqAttrMap);

	public boolean isReportinngModeCorrect(String GIIN, String reportingYear,
			String reportingMode, String complianceType,String new_update);

	public boolean generateReportFile(String file, String userType,
			String userGIIN, String reportType, String searchFor);

	public boolean generateUserHistReport(String file, String userType,
			String userGIIN, String reportType, String searchFor,
			String fromDate, String toDate);

	public boolean generateTransactionHist(String file, String userType,
			String userGIIN, String reportType, String searchFor,
			String fromDate, String toDate);

}
