package com.newgen.interfaces;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public interface ReportableStatus {
	
	
	Map<String, Object> getUsertransactionDetails(HttpServletRequest request,HttpServletResponse response,String complianceType,String reportingYear) throws ServletException;

}
