package com.newgen.interfaces;

import java.util.List;
import java.util.Map;

import com.newgen.bean.FrequentlyAskedQue;

public interface FAQContactAble {
	public Map<Integer, FrequentlyAskedQue> getFAQ();

	public List<String> getContactData();

}
