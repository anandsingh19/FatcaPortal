package com.newgen.interfaces;

import java.io.InputStream;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.newgen.bean.FIRegistration;

public interface UserApprovable {
	public org.json.JSONArray getFI(String userType, String userGIIN,
			int pageNO, String event, String searchFor, String searchIn);

	public int isApproved(String emailID, String comments, String sAdminStatus,
			String sFiUserName, String userTypetoUpdate, String editUserGIIN,
			String isUserLocked, HttpSession session);

	public Map<String, Integer> getPageCount(String userType, String userGIIN,
			String pageNo, String event, String searchFor, String searchIn);

	public int isApprovedUser(FIRegistration userToUpdate,
			Map<String, String> ReqAttrMap,
			Map<String, InputStream> fileInStrmMap, HttpSession session);

	public String getUploadedFileLocation(String userEmail);

	public boolean isLastFIAdmin(String GIIN);
}
