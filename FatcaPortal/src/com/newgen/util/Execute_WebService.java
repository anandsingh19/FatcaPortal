package com.newgen.util;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import com.newgen.webserviceclient.NGWebServiceClient;

public class Execute_WebService {

	private static Logger logger = Logger.getLogger(Execute_WebService.class);

	public ArrayList<String> returnWebService(String SOAP_inxml,
			String EndPointurl) {
		String SOAPResponse_xml = "";
		try {
			//System.out.println("SOAP_inxml-->  " + SOAP_inxml);
			//ClsLogger.CreateLogs("ExecuteWebservice", SOAP_inxml, "D");
			logger.info("SOAP_Request\n"+SOAP_inxml);
			ArrayList<String> outptXMLlst = new ArrayList<String>();
			NGWebServiceClient ngwsclnt = new NGWebServiceClient(true);
			// System.out.println("input xml : " + SOAP_inxml);
			SOAPResponse_xml = ngwsclnt.ExecuteWs(SOAP_inxml, EndPointurl);
			//System.out.println("SOAPResponse_xml--> " + SOAPResponse_xml);
			//ClsLogger.CreateLogs("ExecuteWebservice", SOAPResponse_xml, "D");
			logger.info("SOAP_Response\n"+SOAP_inxml);
			// System.out.println("Output xml :" + SOAPResponse_xml);
			if (SOAPResponse_xml != "") {
				outptXMLlst = getAllTagValues(SOAPResponse_xml, "FieldValue");
				return outptXMLlst;
			}

			return null;
		} catch (Exception e) {
			SOAPResponse_xml = e.getMessage();
		}
		return null;
	}

	public String generatexml(HashMap xmlvalues, String option) {
		String inputXML = "";
		StringBuilder sb = new StringBuilder();
		sb.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:req=\"http://ngprocedureselectq.ops.newgen.com/schema/Ngprocedureselect/request\"><soapenv:Header/><soapenv:Body><req:ProcedureselectReq><Option>"
				+ option + "</Option><Username>");
		sb.append("system</Username><Password>9kbXBdSqUi7LH2BSXYnhfQ==</Password><req:CriteriaArray>");
		Iterator it = xmlvalues.entrySet().iterator();
		while (it.hasNext()) {

			Map.Entry valuepair = (Map.Entry) it.next();
			sb.append("<req:Criteria><FieldName>" + valuepair.getKey()
					+ "</FieldName><FieldValue>" + valuepair.getValue()
					+ "</FieldValue></req:Criteria>");
		}
		sb.append("</req:CriteriaArray><nextSeedValue>?</nextSeedValue></req:ProcedureselectReq></soapenv:Body></soapenv:Envelope>");
		inputXML = sb.toString();
		return inputXML;
	}

	public static String getTagValue(String xml, String tag) {
		Document doc = getDocument(xml);
		NodeList nodeList = doc.getElementsByTagName(tag);
		int length = nodeList.getLength();
		if (length > 0) {
			Node node = nodeList.item(0);
			if (node.getNodeType() == 1) {
				NodeList childNodes = node.getChildNodes();
				String value = "";
				int count = childNodes.getLength();
				int i = 0;
				while (true) {
					Node item = childNodes.item(i);
					if (item.getNodeType() == 3) {
						value = value + item.getNodeValue();
					}
					++i;
					if (i >= count) {
						return value;
					}
				}
			}
			if (node.getNodeType() == 3) {
				return node.getNodeValue();
			}
		}
		return "";
	}

	public static ArrayList<String> getAllTagValues(String xml, String tag) {
		ArrayList sList = new ArrayList();
		Document doc = getDocument(xml);
		NodeList nodeList = doc.getElementsByTagName(tag);
		int length = nodeList.getLength();
		if (length > 0) {
			for (int k = 0; k < length; ++k) {
				Node node = nodeList.item(k);
				if (node.getNodeType() == 1) {
					NodeList childNodes = node.getChildNodes();
					int count = childNodes.getLength();

					if (count == 0) {
						sList.add("NULL");
					}
					for (int i = 0; i < count; ++i) {
						Node item = childNodes.item(i);

						if (item.getNodeType() != 3)
							continue;
						sList.add(item.getNodeValue());
					}
				} else {
					if (node.getNodeType() != 3)
						continue;
					sList.add(node.getNodeValue());
				}
			}
		}
		return sList;
	}

	public static Document getDocument(String xml) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		DocumentBuilder db = null;
		try {
			db = dbf.newDocumentBuilder();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Document doc = null;
		try {
			doc = db.parse(new InputSource(new StringReader(xml)));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return doc;
	}
}