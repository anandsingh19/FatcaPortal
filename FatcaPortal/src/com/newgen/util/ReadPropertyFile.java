/*------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : Read Property file
 File Name                                                   : ReadPropertyFile
 Author                                                      : Aadesh Srivastava
 Date (DD/MM/YYYY)                                           : 30/10/2015
 Description                                                 : to read property file data
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   Changed By    Change Description
 
 ------------------------------------------------------------------------------------------------------*/

package com.newgen.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.newgen.bean.GlobalVariables;

public class ReadPropertyFile {

	private static Logger logger = Logger.getLogger(ReadPropertyFile.class);

	public static void readGeneralInfo() {

		try {

			// System.out.println(" \n Reading  GeneralInfo file ");

			String propertyPath = System.getProperty("user.dir")
					+ File.separator + "GeneralInfo_Vincent.properties";

			Properties properties = new Properties();
			properties.load(new FileInputStream(propertyPath));
			if ( properties.getProperty("PROJECT_NAME") !=null
					&& properties.getProperty("PROJECT_NAME").trim().length() > 0) {
				GlobalVariables.projectName = properties.getProperty(
						"PROJECT_NAME").trim();
			} else {
				logger.info("please define PROJECT_NAME in GeneralInfo property file");
			}
			if ( properties.getProperty("SHARED_LOCATION") !=null
					&& properties.getProperty("SHARED_LOCATION").trim()
							.length() > 0) {
				GlobalVariables.sharedLocation = properties.getProperty(
						"SHARED_LOCATION").trim();
			} else {
				// System.out.println("please define SHARED_LOCATION in GeneralInfo property file");
				logger.info("please define SHARED_LOCATION in GeneralInfo property file");

			}

			if (  properties.getProperty("BATCH_SIZE") !=null
					&& properties.getProperty("BATCH_SIZE").trim().length() > 0) {
				GlobalVariables.batchSize = Integer.parseInt(properties
						.getProperty("BATCH_SIZE").trim());
			} else {
				logger.info("please set/define BATCH_SIZE in GeneralInfo property file");

			}

			if (properties.getProperty("XMLUPLOAD_LOCATION") !=null 
					&& properties.getProperty("XMLUPLOAD_LOCATION").trim()
							.length() > 0) {
				GlobalVariables.uploadLocation = System.getProperty("user.dir")
						+ File.separator
						+ properties.getProperty("XMLUPLOAD_LOCATION").trim();
			} else {
				logger.info("please define XMLUPLOAD_LOCATION in GeneralInfo property file");
			}
			if (properties.getProperty("MEMORY_THRESHOLD") != null
					&& properties.getProperty("MEMORY_THRESHOLD").trim()
							.length() > 0) {
				GlobalVariables.memoryThreshold = Integer.parseInt(properties
						.getProperty("MEMORY_THRESHOLD").trim());
			} else {
				logger.info("please define MEMORY_THRESHOLD in GeneralInfo property file");
			}
			if (properties.getProperty("MAX_FILE_SIZE") != null
					&& properties.getProperty("MAX_FILE_SIZE").trim().length() > 0) {
				GlobalVariables.maxFileSize = Integer.parseInt(properties
						.getProperty("MAX_FILE_SIZE").trim());
			} else {
				logger.info("please define MAX_FILE_SIZE in GeneralInfo property file");
			}
			if (properties.getProperty("MAX_REQUEST_SIZE") != null
					&& properties.getProperty("MAX_REQUEST_SIZE").trim()
							.length() > 0) {
				GlobalVariables.maxRequestSize = Integer.parseInt(properties
						.getProperty("MAX_REQUEST_SIZE").trim());
			} else {
				logger.info("please define MAX_REQUEST_SIZE in GeneralInfo property file");
			}
			if (properties.getProperty("TEMPLATE_XSD_PATH") != null
					&& properties.getProperty("TEMPLATE_XSD_PATH").trim()
							.length() > 0) {
				GlobalVariables.xSDTemplate = properties.getProperty(
						"TEMPLATE_XSD_PATH").trim();
			} else {
				logger.info("please define TEMPLATE_XSD_PATH in GeneralInfo property file");
			}

			/*
			 * if (properties.getProperty("HTML_FILE_LOCATION").trim() != null
			 * && properties.getProperty("HTML_FILE_LOCATION").trim() .length()
			 * > 0) { GlobalVariables.hTMLFileLocation = properties.getProperty(
			 * "HTML_FILE_LOCATION").trim(); } else { logger.info(
			 * "please define HTML_FILE_LOCATION in GeneralInfo property file");
			 * }
			 */

			if (properties.getProperty("WSDL").trim() != null
					&& properties.getProperty("WSDL").trim().length() > 0) {
				GlobalVariables.WSDL_String = properties.getProperty("WSDL")

				.trim();
			} else {
				logger.debug("Please define WSDL URI in properties file!");

			}
			// changes made by Alok on 21-11-2016 Start
			if (properties.getProperty("SUPPORTIVE_DOCUMENT1_MAX_SIZE") != null
					&& properties.getProperty("SUPPORTIVE_DOCUMENT1_MAX_SIZE")
							.trim().length() > 0) {
				GlobalVariables.supportiveDoc1MaXSize = Integer
						.parseInt(properties.getProperty(
								"SUPPORTIVE_DOCUMENT1_MAX_SIZE").trim());
			} else {
				logger.info("please define SUPPORTIVE_DOCUMENT1_MAX_SIZE in GeneralInfo property file");
			}
			
			
			if (properties.getProperty("SUPPORTIVE_DOCUMENT2_MAX_SIZE") != null
					&& properties.getProperty("SUPPORTIVE_DOCUMENT2_MAX_SIZE")
							.trim().length() > 0) {
				GlobalVariables.supportiveDoc2MaXSize = Integer
						.parseInt(properties.getProperty(
								"SUPPORTIVE_DOCUMENT2_MAX_SIZE").trim());
			} else {
				logger.info("please define SUPPORTIVE_DOCUMENT2_MAX_SIZE in GeneralInfo property file");
			}
			//Changes made by Alok on 21-11-2016 End
						if (properties.getProperty("SUPPORTIVE_DOCUMENT_TYPE") != null
					&& properties.getProperty("SUPPORTIVE_DOCUMENT_TYPE")
							.trim().length() > 0) {
				GlobalVariables.supportiveDocType = properties.getProperty(
						"SUPPORTIVE_DOCUMENT_TYPE").trim();
			} else {
				logger.info("please define SUPPORTIVE_DOCUMENT_TYPE in GeneralInfo property file");
			}

			if (properties.getProperty("OTP_TENURE")!= null
					&& properties.getProperty("OTP_TENURE").trim().length() > 0) {
				GlobalVariables.OTPexpiry = Integer.parseInt(properties
						.getProperty("OTP_TENURE").trim());
			} else {
				logger.info("please define OTP_TENURE in GeneralInfo property file");
			}

			/*
			 * if (properties.getProperty("SUCCESS_PATH").trim() != null &&
			 * properties.getProperty("SUCCESS_PATH").trim().length() > 0) {
			 * GlobalVariables.successPath = properties.getProperty(
			 * "SUCCESS_PATH").trim(); } else {
			 * logger.info("please define SUCCESS_PATH in GeneralInfo property file"
			 * ); }
			 */
			if (properties.getProperty("REPORTING_START_YEAR") != null
					&& properties.getProperty("REPORTING_START_YEAR").trim()
							.length() > 0) {
				GlobalVariables.reportingStartYear = Integer
						.parseInt(properties
								.getProperty("REPORTING_START_YEAR").trim());
			} else {
				logger.info("please define REPORTING_START_YEAR in GeneralInfo property file");
			}

			if (properties.getProperty("COUNTRY_ON_FORM") != null
					&& properties.getProperty("COUNTRY_ON_FORM").trim()
							.length() > 0) {
				GlobalVariables.countryOnForm = properties.getProperty(
						"COUNTRY_ON_FORM").trim();
			} else {
				logger.info("please define COUNTRY_ON_FORM in GeneralInfo property file");
			}

			if (properties.getProperty("SUPPORTED_BROWSERS") != null
					&& properties.getProperty("SUPPORTED_BROWSERS").trim()
							.length() > 0) {
				GlobalVariables.supportedBrowsers = properties.getProperty(
						"SUPPORTED_BROWSERS").trim();
			} else {
				logger.info("please define SUPPORTED_BROWSERS in GeneralInfo property file");
			}

			if (properties.getProperty("CRS_XSD_PATH") != null
					&& properties.getProperty("CRS_XSD_PATH").trim().length() > 0) {
				GlobalVariables.crsXSDPath = properties.getProperty(
						"CRS_XSD_PATH").trim();
			} else {
				logger.info("please define CRS_XSD_PATH in GeneralInfo property file");
			}

			if (properties.getProperty("MAX_FI_ADMIN") != null
					&& properties.getProperty("MAX_FI_ADMIN").trim().length() > 0) {
				GlobalVariables.iMaxFiAdmin = Integer.parseInt(properties
						.getProperty("MAX_FI_ADMIN").trim());
			} else {
				logger.info("please define MAX_FI_ADMIN in GeneralInfo property file");
			}
			if (properties.getProperty("HELPLINK1_LABEL") != null
					&& properties.getProperty("HELPLINK1_LABEL").trim()
							.length() > 0) {
				GlobalVariables.helpLink1_label = properties.getProperty(
						"HELPLINK1_LABEL").trim();
			} else {
				logger.info("please define HELPLINK1_LABEL in GeneralInfo property file");
			}
			if (properties.getProperty("HELPLINK2_LABEL") != null
					&& properties.getProperty("HELPLINK2_LABEL").trim()
							.length() > 0) {
				GlobalVariables.helpLink2_label = properties.getProperty(
						"HELPLINK2_LABEL").trim();
			} else {
				logger.info("please define HELPLINK2_LABEL in GeneralInfo property file");
			}
			if (properties.getProperty("HELPLINK3_LABEL") != null
					&& properties.getProperty("HELPLINK3_LABEL").trim()
							.length() > 0) {
				GlobalVariables.helpLink3_label = properties.getProperty(
						"HELPLINK3_LABEL").trim();
			} else {
				logger.info("please define HELPLINK3_LABEL in GeneralInfo property file");
			}
			if (properties.getProperty("HELPLINK1_URL") != null
					&& properties.getProperty("HELPLINK1_URL").trim().length() > 0) {
				GlobalVariables.helpLink1_url = properties.getProperty(
						"HELPLINK1_URL").trim();
			} else {
				logger.info("please define HELPLINK1_URL in GeneralInfo property file");
			}
			if (properties.getProperty("HELPLINK2_URL") != null
					&& properties.getProperty("HELPLINK2_URL").trim().length() > 0) {
				GlobalVariables.helpLink2_url = properties.getProperty(
						"HELPLINK2_URL").trim();
			} else {
				logger.info("please define HELPLINK2_URL in GeneralInfo property file");
			}
			if (properties.getProperty("HELPLINK3_URL") != null
					&& properties.getProperty("HELPLINK3_URL").trim().length() > 0) {
				GlobalVariables.helpLink3_url = properties.getProperty(
						"HELPLINK3_URL").trim();
			} else {
				logger.info("please define HELPLINK3_URL in GeneralInfo property file");
			}

			if (properties.getProperty("CUSTOM_REPORT_PATH") != null
					&& properties.getProperty("CUSTOM_REPORT_PATH").trim()
							.length() > 0) {
				GlobalVariables.customReportPath = properties.getProperty(
						"CUSTOM_REPORT_PATH").trim();
			} else {
				logger.info("please define CUSTOM_REPORT_PATH in GeneralInfo property file");
			}

			/* Change for All by Shubham on 28072016 'code added' - starts */
			if (properties.getProperty("HELPLINK4_LABEL") != null
					&& properties.getProperty("HELPLINK4_LABEL").trim()
							.length() > 0) {
				GlobalVariables.helpLink4_label = properties.getProperty(
						"HELPLINK4_LABEL").trim();
			} else {
				logger.info("please define HELPLINK4_LABEL in GeneralInfo property file");
			}
			if (properties.getProperty("HELPLINK5_LABEL") != null
					&& properties.getProperty("HELPLINK5_LABEL").trim()
							.length() > 0) {
				GlobalVariables.helpLink5_label = properties.getProperty(
						"HELPLINK5_LABEL").trim();
			} else {
				logger.info("please define HELPLINK5_LABEL in GeneralInfo property file");
			}
			
			if (properties.getProperty("HELPLINK4_URL") != null
					&& properties.getProperty("HELPLINK4_URL").trim().length() > 0) {
				GlobalVariables.helpLink4_url = properties.getProperty(
						"HELPLINK4_URL").trim();
			} else {
				logger.info("please define HELPLINK4_URL in GeneralInfo property file");
			}
			if (properties.getProperty("HELPLINK5_URL") != null
					&& properties.getProperty("HELPLINK5_URL").trim().length() > 0) {
				GlobalVariables.helpLink5_url = properties.getProperty(
						"HELPLINK5_URL").trim();
			} else {
				logger.info("please define HELPLINK5_URL in GeneralInfo property file");
			}
/* Change for All by KD on 27072016 'code added' - starts */
			if (properties.getProperty("USERMANUAL_PATH") != null
					&& properties.getProperty("USERMANUAL_PATH").trim().length() > 0) {
				GlobalVariables.userManualPath = properties.getProperty(
						"USERMANUAL_PATH").trim();
			} else {
				logger.info("please define USERMANUAL_PATH in GeneralInfo property file");
			}
			/* Change for All by KD on 27072016 'code added' - starts */
			/* Change for All by Shubham on 28072016 'code added' - ends */

		} catch (Exception e) {

			logger.info("Exception in ReadPropertyFile:: resdGeneralInfo--> "
					+ e.getMessage());

		}
	}
}
