package com.newgen.DAO;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Logger;

import com.newgen.bean.GlobalMessage;
import com.newgen.bean.GlobalVariables;
import com.newgen.bean.LoginUser;
import com.newgen.factory.ObjectFactory;
import com.newgen.interfaces.Customable;
import com.newgen.interfaces.Loginable;
import com.newgen.util.Execute_WebService;

public class LoginDAO implements Loginable {
	private static Logger logger = Logger.getLogger(LoginDAO.class);

	@Override
	public String validateUser(LoginUser user) {
		String loginStatus = null;
		if ((null == user.getUserEmail() || user.getUserEmail().length() == 0)
				&& (user.getPassword() == null || user.getPassword().length() == 0)) {
			logger.info("user and password Does not match.");
		} else {
			loginStatus = isAuthenticated(user);

		}
		return loginStatus;
	}

	@Override
	public boolean isUserlogedIn(String userEmail) {
		boolean b_userLogedin = false;
		String SOAP_inxml = "";
		String option = "";
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		try {
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("UserEmail", userEmail);
			xmlvalues.put("CheckType", "Login");
			option = "ProcedureFATCAIsUserLogedIn";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {

				if (outptXMLlst.get(0) != null
						&& outptXMLlst.get(0).length() != 0
						&& (outptXMLlst.get(0)).equalsIgnoreCase("EXISTS")) {
					logger.debug("user exist or not --> " + outptXMLlst.get(0));
					b_userLogedin = true;
				}
			}

		} catch (Exception e) {
			logger.debug("Exception in isUserlogedIn:: " + e.getMessage());
		}
		return b_userLogedin;
	}

	@Override
	public String isAuthenticated(LoginUser user) {

		String hashValuePwd = null;
		Customable custom = ObjectFactory.getCustomObject();
		hashValuePwd = custom.getHashValue(user.getPassword());
		// boolean flag = false;
		String loginStatus = null;

		String SOAP_inxml = "";
		String option = "";

		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		String systemIP = null;

		logger.info("Authenticating..");
		try {
			systemIP = InetAddress.getLocalHost().toString();
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();

			xmlvalues.put("userEmail", user.getUserEmail());
			xmlvalues.put("password", hashValuePwd);
			xmlvalues.put("userSystemIP", systemIP);
			option = "ProcedureSearchUser";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);

			if (!outptXMLlst.isEmpty()) {
				if (outptXMLlst.get(0) != null
						&& outptXMLlst.get(0).length() != 0
						&& !(outptXMLlst.get(0)
								.equalsIgnoreCase("Not Registered"))) {
					if (outptXMLlst.get(5) != null
							&& outptXMLlst.get(5).length() != 0
							&& !(outptXMLlst.get(5).equalsIgnoreCase("Y"))) {
						if (outptXMLlst.get(4) != null
								&& outptXMLlst.get(4).length() != 0
								&& (outptXMLlst.get(4).equalsIgnoreCase("0"))) {

							loginStatus = "success";
							if (outptXMLlst.get(0) != null
									&& outptXMLlst.get(0).length() != 0)
								user.setUserName(outptXMLlst.get(0));
							else
								user.setUserName("");

							if (outptXMLlst.get(2) != null
									&& outptXMLlst.get(2).length() != 0)
								user.setGiin(outptXMLlst.get(2));
							else
								user.setGiin("");

							if (outptXMLlst.get(1) != null
									&& outptXMLlst.get(1).length() != 0)
								user.setUserType(outptXMLlst.get(1));
							else
								user.setUserType("");

							if (outptXMLlst.get(3) != null
									&& outptXMLlst.get(3).length() != 0)
								user.setUserFlag(outptXMLlst.get(3));
							else
								user.setUserFlag("");
							

							if (outptXMLlst.get(6) != null
									&& outptXMLlst.get(6).length() != 0)
								user.setUserComplianceType(outptXMLlst.get(6));
							else
								user.setUserComplianceType("");
							logger.info("Authenticated...");

						} else {

							if (Integer.parseInt(outptXMLlst.get(4)) == 5) {
								loginStatus = GlobalMessage.ACCOUNT_LOCKED;
							} else {

								loginStatus = GlobalMessage.LOGIN_FAILED
										+ " "
										+ GlobalMessage.LOGIN_ATTEMPT
										+ (5 - Integer.parseInt(outptXMLlst
												.get(4)));
							}
							logger.debug("loginStatus1--> " + loginStatus);
						}

					} else {
						loginStatus = GlobalMessage.USER_LOCKED_MSG;
						logger.debug("loginStatus2--> " + loginStatus);
					}
				} else {
					loginStatus = GlobalMessage.UN_AUTHORISED_USER;
					logger.debug("loginStatus4--> " + loginStatus);
				}
			} else {
				loginStatus = GlobalMessage.LOGIN_FAILED;
				logger.debug("loginStatus3--> " + loginStatus);
			}

		} catch (Exception ex) {
			logger.info("isAuthenticated():Exception:There is some exception please contact your system admin or try after some time"
					+ ex.getMessage());
		}
		// return flag;
		return loginStatus;

	}

	@Override
	public void saveLoggeruser(LoginUser user, HttpSession session) {
		String SOAP_inxml = "";
		String option = "";
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		String systemIP = null;
		try {
			systemIP = InetAddress.getLocalHost().toString();

			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();

			xmlvalues.put("Username", user.getUserEmail());
			logger.debug("in dao session.getId()--> " + session.getId());
			xmlvalues.put("sessionID", session.getId());
			xmlvalues.put("SystemIP", systemIP);
			option = "ProcedureInsertloginusr";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {
				logger.info("Inserted SuccessFully in logger user table...");
			}

		} catch (Exception ex) {
			logger.info("isAuthenticated():Exception:There is some exception please contact your system admin or try after some time"
					+ ex.getMessage());
		}
	}

	@Override
	public String updateUserLoginDetail(String userEmail) {
		String updateStatus = "Failuer";
		String SOAP_inxml = "";
		String option = "";
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		try {

			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("UserEmail", userEmail);
			xmlvalues.put("CheckType", "Invalidate");
			option = "ProcedureFATCAIsUserLogedIn";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {

				if (outptXMLlst.get(0) != null
						&& outptXMLlst.get(0).length() != 0
						&& (outptXMLlst.get(0)).equalsIgnoreCase("Success")) {
					logger.debug("user login details updated "
							+ outptXMLlst.get(0));
					updateStatus = outptXMLlst.get(0);

				} else {
					logger.debug("Error while updating logedin user detail in logout");
				}
			}

		} catch (Exception e) {
			logger.debug("Exception in updateUserLoginDetail::  "
					+ e.getMessage());
		}
		return updateStatus;
	}
	
	@Override
	public String isOldPasswordAuthenticated(LoginUser user) {

		String hashValuePwd = null;
		Customable custom = ObjectFactory.getCustomObject();
		hashValuePwd = custom.getHashValue(user.getPassword());
		// boolean flag = false;
		String loginStatus = null;

		String SOAP_inxml = "";
		String option = "";

		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		String systemIP = null;

		logger.info("Authenticating..");
		try {
			systemIP = InetAddress.getLocalHost().toString();
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();

			xmlvalues.put("userEmail", user.getUserEmail());
			xmlvalues.put("password", hashValuePwd);
			xmlvalues.put("userSystemIP", systemIP);
			option = "ProcedurevalidateOldPassword";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);

			if (!outptXMLlst.isEmpty()) {
				if (outptXMLlst.get(0) != null
						&& outptXMLlst.get(0).length() != 0
						&& (outptXMLlst.get(0).equalsIgnoreCase("success"))) {
					loginStatus = "success";
				} else {
					loginStatus = "fail";
					logger.debug("loginStatus 4 old password--> " + loginStatus);
				}
			} else {
				loginStatus = "fail";
				logger.debug("error in ProcedurevalidateOldPassword");
			}

		} catch (Exception ex) {
			logger.info("isOldPasswordAuthenticated():Exception:--> "
					+ ex.getMessage());
		}
		// return flag;
		return loginStatus;

	}

}
