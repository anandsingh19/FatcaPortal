package com.newgen.DAO;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONArray;

import com.newgen.bean.GlobalVariables;
import com.newgen.interfaces.UploadManualForm;
import com.newgen.util.Execute_WebService;

public class UploadManualDAO implements UploadManualForm {
	private static Logger logger = Logger.getLogger(UploadManualDAO.class);

	@Override
	public JSONArray getManualFIInformation(String filerGIIN, String emailID,
			String reportingYear) {

		org.json.JSONArray sojsonArray = new org.json.JSONArray();
		org.json.JSONArray collectionOfSOACC = new org.json.JSONArray();
		org.json.JSONObject sojSONObject = null;

		String SOAP_inxml = null;
		String option = null;

		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		int n = 0;
		int i = 0;
		try {

			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("filerGIIN", filerGIIN);
			xmlvalues.put("reportingYear", reportingYear);

			option = "ProcedureSelectSOData";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();

			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			n = outptXMLlst.size() / 17;
			i = 0;
			while (n > 0) {
				sojSONObject = new org.json.JSONObject();
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("filerGIIN", outptXMLlst.get(i++));
				else {
					sojSONObject.put("filerGIIN", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accountNumber", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accountNumber", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accName", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accName", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accTin", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accTin", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("entityType", outptXMLlst.get(i++));
				else {
					sojSONObject.put("entityType", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("entityName", outptXMLlst.get(i++));
				else {
					sojSONObject.put("entityName", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("entityAddress", outptXMLlst.get(i++));
				else {
					sojSONObject.put("entityAddress", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("entityCountryCode", outptXMLlst.get(i++));
				else {
					sojSONObject.put("entityCountryCode", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("entityTin", outptXMLlst.get(i++));
				else {
					sojSONObject.put("entityTin", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("corrDocrefID", outptXMLlst.get(i++));
				else {
					sojSONObject.put("corrDocrefID", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("docrefid", outptXMLlst.get(i++));
				else {
					sojSONObject.put("docrefid", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("action", outptXMLlst.get(i++));
				else {
					sojSONObject.put("action", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("submittedFlag", outptXMLlst.get(i++));
				else {
					sojSONObject.put("submittedFlag", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("parentKey", outptXMLlst.get(i++));
				else {
					sojSONObject.put("parentKey", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("rowId", outptXMLlst.get(i++));
				else {
					sojSONObject.put("rowId", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accHolderCIF", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accHolderCIF", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accDOB", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accDOB", "");
					i++;
				}
				// sojSONObject.put("rowid", count);
				sojsonArray.put(sojSONObject);
				n--;
			}

			collectionOfSOACC.put(sojsonArray);

			sojSONObject = null;
			sojsonArray = null;
			sojsonArray = new org.json.JSONArray();

			xmlvalues = new HashMap<String, String>();
			// exe_websrv = new Execute_WebService();

			xmlvalues.put("filerGIIN", filerGIIN);
			xmlvalues.put("reportingYear", reportingYear);

			option = "ProcedureSelectACData";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			n = 0;
			i = 0;
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			logger.info("AC outptXMLlst" + outptXMLlst);
			n = outptXMLlst.size() / 45;
			//changes done by shubham for filer category two fields by
			//shubham on 12 06 2017
			i = 0;
			while (n > 0) {
				sojSONObject = new org.json.JSONObject();

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("fiName", outptXMLlst.get(i++));
				else {
					sojSONObject.put("fiName", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("fiAddress", outptXMLlst.get(i++));
				else {
					sojSONObject.put("fiAddress", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("fiCountryCode", outptXMLlst.get(i++));
				else {
					sojSONObject.put("fiCountryCode", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("fiGIIn", outptXMLlst.get(i++));
				else {
					sojSONObject.put("fiGIIn", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("isSponsoredEntity", outptXMLlst.get(i++));
				else {
					sojSONObject.put("isSponsoredEntity", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("spName", outptXMLlst.get(i++));
				else {
					sojSONObject.put("spName", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("spAddress", outptXMLlst.get(i++));
				else {
					sojSONObject.put("spAddress", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("spCountryCode", outptXMLlst.get(i++));
				else {
					sojSONObject.put("spCountryCode", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("spGIIN", outptXMLlst.get(i++));
				else {
					sojSONObject.put("spGIIN", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("isIntermediary", outptXMLlst.get(i++));
				else {
					sojSONObject.put("isIntermediary", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("interName", outptXMLlst.get(i++));
				else {
					sojSONObject.put("interName", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("InterAddress", outptXMLlst.get(i++));
				else {
					sojSONObject.put("InterAddress", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("interCountryCode", outptXMLlst.get(i++));
				else {
					sojSONObject.put("interCountryCode", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("interGIIN", outptXMLlst.get(i++));
				else {
					sojSONObject.put("interGIIN", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accountNumber", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accountNumber", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accCurrencyCode", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accCurrencyCode", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accBalance", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accBalance", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accIntrest", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accIntrest", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accDividend", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accDividend", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accGrossRedem", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accGrossRedem", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accOthers", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accOthers", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accHolderType", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accHolderType", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accName", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accName", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accAddress", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accAddress", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accCountryCode", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accCountryCode", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accTin", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accTin", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accEntityType", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accEntityType", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("corrDocrefID", outptXMLlst.get(i++));
				else {
					sojSONObject.put("corrDocrefID", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("docrefid", outptXMLlst.get(i++));
				else {
					sojSONObject.put("docrefid", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("action", outptXMLlst.get(i++));
				else {
					sojSONObject.put("action", "");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("submittedFlag", outptXMLlst.get(i++));
				else {
					sojSONObject.put("submittedFlag", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("rowId", outptXMLlst.get(i++));
				else {
					sojSONObject.put("rowId", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("mesgRefID", outptXMLlst.get(i++));
				else {
					sojSONObject.put("mesgRefID", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("corrMesggRefID", outptXMLlst.get(i++));
				else {
					sojSONObject.put("corrMesggRefID", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accHolderCIF", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accHolderCIF", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("accDOB", outptXMLlst.get(i++));
				else {
					sojSONObject.put("accDOB", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("spDocRefID", outptXMLlst.get(i++));
				else {
					sojSONObject.put("spDocRefID", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("spCorrDocRefID", outptXMLlst.get(i++));
				else {
					sojSONObject.put("spCorrDocRefID", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("interDocRefID", outptXMLlst.get(i++));
				else {
					sojSONObject.put("interDocRefID", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("interCorrDocRefID", outptXMLlst.get(i++));
				else {
					sojSONObject.put("interCorrDocRefID", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("filerDocRefID", outptXMLlst.get(i++));
				else {
					sojSONObject.put("filerDocRefID", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("filerCorrDocRefID", outptXMLlst.get(i++));
				else {
					sojSONObject.put("filerCorrDocRefID", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("isNilReport", outptXMLlst.get(i++));
				else {
					sojSONObject.put("isNilReport", "N");
					i++;
				}
				//changes done for filer category field to be reloaded in form by shubham on 12 06 2017 
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("filerCategory_manual", outptXMLlst.get(i++));
				else {
					sojSONObject.put("filerCategory_manual", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("sponEntityFilerCategory", outptXMLlst.get(i++));
				else {
					sojSONObject.put("sponEntityFilerCategory", "");
					i++;
				}
				//changes done for filer category field to be reloaded in form by shubham on 12 06 2017 end
				// sojSONObject.put("rowid", count);
				sojsonArray.put(sojSONObject);
				n--;
			}

			collectionOfSOACC.put(sojsonArray);

			sojSONObject = null;
			xmlvalues = new HashMap<String, String>();
			// exe_websrv = new Execute_WebService();
			xmlvalues.put("emailID", emailID);// here filer would email id
			option = "ProcedureSelectManualDataJoin";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			n = 0;
			n = outptXMLlst.size() / 4;
			i = 0;
			while (n > 0) {
				sojSONObject = new org.json.JSONObject();
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("name", outptXMLlst.get(i++));
				else {
					sojSONObject.put("name", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("address", outptXMLlst.get(i++));
				else {
					sojSONObject.put("address", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("country", outptXMLlst.get(i++));
				else {
					sojSONObject.put("country", "");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					sojSONObject.put("gIIN", outptXMLlst.get(i++));
				else {
					sojSONObject.put("gIIN", "");
					i++;
				}

				n--;
			}

			collectionOfSOACC.put(sojSONObject);

			n = 0;
			i = 0;
		} catch (Exception ex) {
			logger.info("UploadManualDAO:getManualFIInformation:There is some  exception:"
					+ ex.getMessage());
		}
		logger.info("final outpu of the get method:"
				+ collectionOfSOACC.toString());
		return collectionOfSOACC;

	}

	@Override
	public boolean isSaveManualFIInformation(String substantialOwner,
			String AccountInformation, String emailID, String event,
			String reportingYear, String submissionType, String filerGIIN) {
		// TODO Auto-generated method stub

		boolean flag = false;
		org.json.JSONArray jsonArray = null;
		int arrayLen = 0;
		org.json.JSONObject jSONObject = null;
		String docTypeIndic = null;
		String SOAP_inxml = null;
		String option = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		try {

			jsonArray = new org.json.JSONArray(substantialOwner);
			logger.info("SO jsonArray:" + jsonArray.toString());
			arrayLen = jsonArray.length();
			String corrdocrefID = null;
			String docrefID = null;
			String corrMsgRefID = null;
			String msgRefID = null;
			String spCorrDocrefID = null;
			String spDocrefID = null;
			String interCorrDocRefID = null;
			String interDocRefID = null;
			String filerCorrDocRefID = null;
			String filerDocRefID = null;
			String isNILReport = null;
			
			
			// String isNILReport = null;
			//changes done by shubham on 12 06 2017 for saving filer category data 
			String filerCategory_manual=null;
			String sponEntityFilerCategory=null;

			exe_websrv = new Execute_WebService();
			option = "ProcedureInsertManualDataSO";
			for (int i = 0; i < arrayLen; i++) {

				jSONObject = jsonArray.getJSONObject(i);
				xmlvalues = null;
				corrdocrefID = null;
				docrefID = null;
				xmlvalues = new HashMap<String, String>();
				if (isDeleteManualFIInformation(jSONObject.getString("rowID"),
						"SO")) {
					logger.info("Deleted Successfully");
				}

				xmlvalues.put("filerGIIN", jSONObject.getString("filerGIIN"));
				xmlvalues.put("accountNumber",
						jSONObject.getString("accountNumber"));
				xmlvalues.put("accountCIF",
						jSONObject.getString("accHolderCIF"));// We have not got
																// any
																// information
																// from bsa
																// please ask
				xmlvalues.put("accName", jSONObject.getString("accName"));
				xmlvalues.put("accTin", jSONObject.getString("accTin"));
				xmlvalues.put("entityType", jSONObject.getString("entityType"));

				// Account CIF

				xmlvalues.put("entityName", jSONObject.getString("entityName"));
				xmlvalues.put("entityAddress",
						jSONObject.getString("entityAddress"));
				xmlvalues.put("entityCountryCode",
						jSONObject.getString("entityCountryCode"));
				xmlvalues.put("entityTin", jSONObject.getString("entityTin"));
				xmlvalues.put("USOwner_DOB", jSONObject.getString("accDOB"));

				if (jSONObject.getString("action") != null
						&& jSONObject.getString("action").length() != 0
						&& jSONObject.getString("action").equalsIgnoreCase("M")
						|| jSONObject.getString("action") != null
						&& jSONObject.getString("action").length() != 0
						&& jSONObject.getString("action").equalsIgnoreCase("D")) {

					if (jSONObject.getString("action").equalsIgnoreCase("M")
							&& submissionType.equalsIgnoreCase("Update")) {
						docTypeIndic = "FATCA4";
					} else if (jSONObject.getString("action").equalsIgnoreCase(
							"D")
							&& submissionType.equalsIgnoreCase("Update")) {
						docTypeIndic = "FATCA3";
						// This is a void case
					}

					else if (jSONObject.getString("action").equalsIgnoreCase(
							"M")
							&& submissionType
									.equalsIgnoreCase("Response to IRS request")) {
						docTypeIndic = "FATCA2";
					} else if (submissionType.equalsIgnoreCase("NEW")) {
						docTypeIndic = "FATCA1";
					}

					corrdocrefID = jSONObject.getString("docrefid");
					docrefID = jSONObject.getString("docrefid");

				}

				/*
				 * changed by KD on 24062016 startsdocTypeIndic changed from
				 * Fatca4 to FATCA1 in case of updateas xml is not generated yet
				 * in case of manual
				 */
				/*
				 * if (jSONObject.getString("action") != null &&
				 * jSONObject.getString("action").length() != 0 &&
				 * jSONObject.getString("action").equalsIgnoreCase("M") ||
				 * jSONObject.getString("action") != null &&
				 * jSONObject.getString("action").length() != 0 &&
				 * jSONObject.getString("action").equalsIgnoreCase("D")) {
				 * 
				 * if (jSONObject.getString("action").equalsIgnoreCase("M") &&
				 * submissionType.equalsIgnoreCase("Update")) { docTypeIndic =
				 * "FATCA1"; } else if
				 * (jSONObject.getString("action").equalsIgnoreCase( "D") &&
				 * submissionType.equalsIgnoreCase("Update")) { docTypeIndic =
				 * "FATCA1";// This is a void case }
				 * 
				 * else if (jSONObject.getString("action").equalsIgnoreCase(
				 * "M") && submissionType
				 * .equalsIgnoreCase("Response to IRS request")) { docTypeIndic
				 * = "FATCA2"; } else if
				 * (submissionType.equalsIgnoreCase("NEW")) { docTypeIndic =
				 * "FATCA1"; }
				 * 
				 * corrdocrefID = jSONObject.getString("docrefid"); docrefID =
				 * jSONObject.getString("docrefid");
				 * 
				 * }
				 */

				/* changed by KD on 24062016 end */
				if (jSONObject.getString("action") != null
						&& jSONObject.getString("action").length() != 0
						&& jSONObject.getString("action").equalsIgnoreCase("A")) {
					if (submissionType.equalsIgnoreCase("NEW")) {
						docTypeIndic = "FATCA1";
					}

					docrefID = jSONObject.getString("docrefid");
					corrdocrefID = jSONObject.getString("corrDocrefID");
				}

				xmlvalues.put("UpdatedInDB_Flag",
						jSONObject.getString("action"));
				// xmlvalues.put("Submitted_Flag",
				// jSONObject.getString("submittedFlag"));//Y means just save
				// the data it's not update to the main table
				xmlvalues.put("Submitted_Flag", event);// Y means just save the
														// data it's not update
														// to the main table
				xmlvalues.put("DocRefID", docrefID);
				xmlvalues.put("CorrDRefID", corrdocrefID);
				/*
				 * xmlvalues.put("MesgRefID", ""); xmlvalues.put("CorrMRefID",
				 * "");
				 */
				xmlvalues.put("DocTypeIndic", docTypeIndic);
				xmlvalues.put("SubmittedBy", emailID);
				xmlvalues.put("parentKey", jSONObject.getString("parentKey"));
				// xmlvalues.put("accHolderCIF",
				// jSONObject.getString("accHolderCIF"));
				// xmlvalues.put("accDOB", jSONObject.getString("accDOB"));

				SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
				outptXMLlst = new ArrayList<String>();

				outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
						GlobalVariables.WSDL_String);
				// logger.info("The Output of the so insertion:"+outptXMLlst.get(0));

				if (!outptXMLlst.isEmpty()) {
					logger.info(outptXMLlst.get(0));
				}

			}

			jsonArray = new org.json.JSONArray(AccountInformation);
			logger.info("jsonArray:test" + jsonArray.toString());
			arrayLen = jsonArray.length();
			/*
			 * int jsonArrayColumnsNameLen = jsonArrayColumnsName.length; int k
			 * = 0;
			 */
			// String accCorrdocrefID = null;
			/* ArrayList<String> data = new ArrayList<String>(); */
			option = "ProcedureInsertManualDataAC";
			for (int i = 0; i < arrayLen; i++) {
				jSONObject = jsonArray.getJSONObject(i);
				xmlvalues = null;
				xmlvalues = new HashMap<String, String>();
				corrdocrefID = null;
				docrefID = null;
				msgRefID = null;
				corrMsgRefID = null;

				spCorrDocrefID = null;
				spDocrefID = null;
				interCorrDocRefID = null;
				interDocRefID = null;

				// logger.info("<corrdocrefID>"+jSONObject.getString("corrDocrefID"));
				// logger.info("<docrefID>>"+jSONObject.getString("docrefid"));

				if (jSONObject.getString("action") != null
						&& jSONObject.getString("action").length() != 0
						&& jSONObject.getString("action").equalsIgnoreCase("M")
						|| jSONObject.getString("action") != null
						&& jSONObject.getString("action").length() != 0
						&& jSONObject.getString("action").equalsIgnoreCase("D")) {

					if (jSONObject.getString("action").equalsIgnoreCase("M")
							&& submissionType.equalsIgnoreCase("Update")) {
						docTypeIndic = "FATCA4";
					} else if (jSONObject.getString("action").equalsIgnoreCase(
							"D")
							&& submissionType.equalsIgnoreCase("Update")) {
						docTypeIndic = "FATCA3";// This is a void case
					}

					else if (jSONObject.getString("action").equalsIgnoreCase(
							"M")
							&& submissionType
									.equalsIgnoreCase("Response to IRS request")) {
						docTypeIndic = "FATCA2";
					} else if (submissionType.equalsIgnoreCase("NEW")) {
						docTypeIndic = "FATCA1";
					}

					corrdocrefID = jSONObject.getString("docrefid");
					docrefID = jSONObject.getString("docrefid");
					msgRefID = jSONObject.getString("mesgRefID");
					corrMsgRefID = jSONObject.getString("mesgRefID");

					spCorrDocrefID = jSONObject.getString("spDocRefID");
					spDocrefID = jSONObject.getString("spDocRefID");
					interCorrDocRefID = jSONObject.getString("interDocRefID");
					interDocRefID = jSONObject.getString("interDocRefID");
					filerCorrDocRefID = jSONObject.getString("filerDocRefID");
					filerDocRefID = jSONObject.getString("filerDocRefID");
					isNILReport = jSONObject.getString("isNilReport");

				}
				if (jSONObject.getString("action") != null
						&& jSONObject.getString("action").length() != 0
						&& jSONObject.getString("action").equalsIgnoreCase("A")) {
					// docTypeIndic = "FATCA1";

					if (submissionType.equalsIgnoreCase("NEW")) {
						docTypeIndic = "FATCA1";
					}
					docrefID = jSONObject.getString("docrefid");
					corrdocrefID = jSONObject.getString("corrDocrefID");
					msgRefID = jSONObject.getString("mesgRefID");
					corrMsgRefID = jSONObject.getString("corrMesgRefID");

					spDocrefID = jSONObject.getString("spDocRefID");
					spCorrDocrefID = jSONObject.getString("spCorrDocRefID");
					interCorrDocRefID = jSONObject
							.getString("interCorrDocRefID");
					interDocRefID = jSONObject.getString("interDocRefID");
					filerCorrDocRefID = jSONObject
							.getString("filerCorrDocRefID");
					filerDocRefID = jSONObject.getString("filerDocRefID");
					isNILReport = jSONObject.getString("isNilReport");
					//changes done by shubham on 12 06 2017 for filer category data
					filerCategory_manual =jSONObject.getString("filerCategory_manual");
					sponEntityFilerCategory =jSONObject.getString("sponEntityFilerCategory");
				}
				// logger.info("<corrdocrefID>"+corrdocrefID);
				// logger.info("<docrefID>>"+docrefID);
				/*
				 * Order Of Columns
				 * fiName,fiAddress,fiCountryCode,fiGIIn,isSponsoredEntity
				 * ,spName,spAddress,spCountryCode,
				 * spGIIN,isIntermediary,interName
				 * ,InterAddress,interCountryCode,interGIIN,
				 * accountHolderCIF,accountNumber
				 * ,accCurrencyCode,accBalance,accIntrest,accDividend,
				 * accGrossRedem
				 * ,accOthers,accHolderType,accName,accAddress,accCountryCode
				 * ,accTin,
				 * accEntityType,action,submittedFlag,docrefid,corrDocMID
				 * ,docTypeIndic,submittedBy, reportingYear,submissionType
				 */

				logger.info("ACC Selected RowId:"
						+ jSONObject.getString("rowId"));
				if (isDeleteManualFIInformation(jSONObject.getString("rowId"),
						"ACC")) {
					logger.info("Deleted Successfully");
				}

				xmlvalues.put("fiName",
						checkSpecialChar(jSONObject.getString("fiName")));
				xmlvalues.put("fiAddress",
						checkSpecialChar(jSONObject.getString("fiAddress")));
				xmlvalues.put("fiCountryCode",
						jSONObject.getString("fiCountryCode"));
				xmlvalues.put("fiGIIn", jSONObject.getString("fiGIIn"));
				xmlvalues.put("isSponsoredEntity",
						jSONObject.getString("isSponsoredEntity"));
				xmlvalues.put("spName",
						checkSpecialChar(jSONObject.getString("spName")));
				xmlvalues.put("spAddress",
						checkSpecialChar(jSONObject.getString("spAddress")));
				xmlvalues.put("spCountryCode",
						jSONObject.getString("spCountryCode"));
				xmlvalues.put("spGIIN", jSONObject.getString("spGIIN"));
				xmlvalues.put("isIntermediary",
						jSONObject.getString("isIntermediary"));
				xmlvalues.put("interName",
						checkSpecialChar(jSONObject.getString("interName")));
				xmlvalues.put("InterAddress",
						checkSpecialChar(jSONObject.getString("InterAddress")));
				xmlvalues.put("interCountryCode",
						jSONObject.getString("interCountryCode"));
				xmlvalues.put("interGIIN", jSONObject.getString("interGIIN"));
				xmlvalues.put("accountNumber",
						jSONObject.getString("accountNumber"));
				xmlvalues.put("accCurrencyCode",
						jSONObject.getString("accCurrencyCode"));
				xmlvalues.put("accBalance", jSONObject.getString("accBalance"));
				xmlvalues.put("accIntrest", jSONObject.getString("accIntrest"));
				xmlvalues.put("accDividend",
						jSONObject.getString("accDividend"));
				xmlvalues.put("accGrossRedem",
						jSONObject.getString("accGrossRedem"));
				xmlvalues.put("accOthers", jSONObject.getString("accOthers"));
				xmlvalues.put("accHolderType",
						jSONObject.getString("accHolderType"));
				xmlvalues.put("accName", jSONObject.getString("accName"));
				xmlvalues.put("accAddress", jSONObject.getString("accAddress"));
				xmlvalues.put("accCountryCode",
						jSONObject.getString("accCountryCode"));
				xmlvalues.put("accTin", jSONObject.getString("accTin"));
				xmlvalues.put("accEntityType",
						jSONObject.getString("accEntityType"));
				xmlvalues.put("action", jSONObject.getString("action"));
				xmlvalues.put("submittedFlag", event);
				xmlvalues.put("docrefid", docrefID);
				xmlvalues.put("corrDrefID", corrdocrefID);
				xmlvalues.put("docTypeIndic", docTypeIndic);
				xmlvalues.put("submittedBy", emailID);
				xmlvalues.put("reportingYear", reportingYear);// Kindly add
																// dynamically
				xmlvalues.put("submissionType", submissionType);// Kindly add
																// dynamically
				xmlvalues.put("mesgDRefID", msgRefID);
				xmlvalues.put("corrmsgDRefID", corrMsgRefID);

				// Added on 05/01/2015 to handle sonponser or intermediary
				// modification

				xmlvalues.put("accHolderCIF",
						jSONObject.getString("accHolderCIF"));
				xmlvalues.put("accDOB", jSONObject.getString("accDOB"));

				xmlvalues.put("spDocRefID", spDocrefID);
				xmlvalues.put("spCorrDRefID", spCorrDocrefID);
				xmlvalues.put("interDocRefID", interDocRefID);
				xmlvalues.put("interCorrDRefID", interCorrDocRefID);
				xmlvalues.put("filerDRefID", filerDocRefID);
				xmlvalues.put("filerCorrDRefID", filerCorrDocRefID);
				xmlvalues.put("isNilReport", isNILReport);
				
				//changes done by shubham on 12 06 2017 for sponsor and filer category 
				xmlvalues.put("filerCategory_manual",jSONObject.getString("filerCategory_manual"));
				xmlvalues.put("sponEntityFilerCategory", jSONObject.getString("sponEntityFilerCategory"));
				SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
				outptXMLlst = new ArrayList<String>();

				outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
						GlobalVariables.WSDL_String);

				if (!outptXMLlst.isEmpty()) {
					logger.info(outptXMLlst.get(0));
				} else {
					logger.info("There is some error on insert record");
				}

			}

			if (event.equalsIgnoreCase("submit")) {
				option = "ProcedureFATCAProtalManualDataMovement";
				xmlvalues = new HashMap<String, String>();
				xmlvalues.put("Dummy", filerGIIN);
				xmlvalues.put("Source", "Manual");
				//xmlvalues.put("ReportingYear", reportingYear);
				SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
				outptXMLlst = new ArrayList<String>();
				outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
						GlobalVariables.WSDL_String);

				if (!outptXMLlst.isEmpty()) {
					logger.info(outptXMLlst.get(0));
				}
			}

			flag = true;

		} catch (Exception ex) {
			logger.info("UploadManualDAO:isSaveManualFIInformation:There is some exception:"
					+ ex.getMessage());

		}
		return flag;

	}

	@Override
	public boolean isDeleteManualFIInformation(String rowId, String source) {
		boolean flag = false;
		String SOAP_inxml = "";
		String option = "";
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		xmlvalues = new HashMap<String, String>();
		exe_websrv = new Execute_WebService();

		try {
			if (source.equalsIgnoreCase("SO")) {

				xmlvalues.put("Source", "SO");

			} else {
				xmlvalues.put("Source", "AC");
			}

			xmlvalues.put("Docrefid", rowId);// here docRefId Is the Row Id of
												// the table
			option = "ProcedureFATCAProtalManualDelete";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {
				flag = true;
				logger.info(outptXMLlst.get(0));
			}

		} catch (Exception ex) {
			logger.info("isDeleteManualFIInformation:There is some exception in deleting docrefID:"
					+ ex.getMessage());
		}
		return flag;

	}

	// /*Changes by Kd on 28072016 added one more param /
	@Override
	public String getGIINSubmissionType(String giin, String reportingYear,
			String callFrom, String complianceType, String testOrActual) {

		String submissionType = null;
		String SOAP_inxml = "";
		String option = "";
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		xmlvalues = new HashMap<String, String>();
		exe_websrv = new Execute_WebService();

		try {
			xmlvalues.put("Giin", giin);
			xmlvalues.put("ReportingYear", reportingYear);
			xmlvalues.put("ProcCalledFor", "SubmissionType");
			xmlvalues.put("ProcCalledFrom", callFrom);
			xmlvalues.put("ComplianceType", complianceType);
			/* Changes by Kd on 28072016 added one more param --starts */
			xmlvalues.put("TestOrActual", testOrActual);
			/* Changes by Kd on 28072016 ---end */
			option = "ProcedureFATCAManualFormStatus";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {
				submissionType = outptXMLlst.get(0);
				logger.info(outptXMLlst.get(0));
			}

		} catch (Exception e) {
			logger.debug("Exception in getGIINSubmissionType-->  "
					+ e.getMessage());
		}
		return submissionType;
	}

	@Override
	public String checkSpecialChar(String inputdata) {
		String outputData = "";
		try {
			if (inputdata.contains("&")) {
				outputData = inputdata.replaceAll("&", "&amp;");
			} else {
				outputData = inputdata;
			}
		} catch (Exception e) {
			logger.debug("Exception in checkSpecialChar-->  " + e.getMessage());
		}
		return outputData;
	}

}
