package com.newgen.DAO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.newgen.bean.GlobalVariables;

public class FileUploadOperation

{
	private static Logger logger = Logger.getLogger(FileUploadOperation.class);

	public void convertHTML(String FileName) {
		BufferedReader txtfile = null;
		OutputStream htmlfile = null;
		PrintStream printhtml = null;
		String temp = null;
		String txtfiledata = null;
		StringBuilder htmlfileBuilder = null;
		try {
			logger.debug("GlobalVariables.failedXmlLocation in FileUploadOperation --> "
					+ GlobalVariables.failedXmlLocation);
			txtfile = new BufferedReader(new FileReader(
					GlobalVariables.failedXmlLocation + File.separator
							+ FileName + ".txt"));

			htmlfile = new FileOutputStream(new File(
					GlobalVariables.failedXmlLocation + File.separator
							+ FileName + ".html"));
			printhtml = new PrintStream(htmlfile);

			String[] txtbyLine = new String[5000];

			htmlfileBuilder = new StringBuilder();
			String htmlheader = "<html><head>";
			htmlheader += "<title>Equivalent HTML</title>";
			htmlheader += "</head><body>";
			String htmlfooter = "</body></html>";
			int linenum = 0;

			while ((txtfiledata = txtfile.readLine()) != null) {
				txtbyLine[linenum] = txtfiledata;
				linenum++;
			}
			for (int i = 0; i < linenum; i++) {
				if (i == 0) {
					temp = htmlheader + txtbyLine[0];
					txtbyLine[0] = temp;
				}
				if (linenum == i + 1) {
					temp = txtbyLine[i] + htmlfooter;
					txtbyLine[i] = temp;
				}
				printhtml.println(txtbyLine[i]);
			}

		}

		catch (Exception e) {
			logger.info("Exception in TexttoHtml--> " + e.getMessage());
		} finally {
			try {
				if (printhtml != null) {
					printhtml.close();
				}
				if (htmlfile != null) {
					htmlfile.close();
				}
				if (txtfile != null) {
					txtfile.close();
					File txtFile = new File(GlobalVariables.failedXmlLocation
							+ File.separator + FileName + ".txt");
					logger.info("txt file path--> "
							+ GlobalVariables.failedXmlLocation
							+ File.separator + FileName + ".txt");
					if (txtFile.exists()) {
						logger.info("file found");
						txtFile.delete();

					}

				}

			} catch (Exception e2) {
				logger.info("Exception in ConvertHTML--> " + e2.getMessage());
			}
		}
	}

	public static void generateLog(String paramString, String FileName) {

		StringBuffer localStringBuffer1 = new StringBuffer();
		localStringBuffer1.append("\n");
		localStringBuffer1.append(paramString);
		localStringBuffer1.append("\n");
		StringBuffer localStringBuffer2 = null;
		String str1 = "";
		String logPath = "";
		BufferedWriter localBufferedWriter = null;
		GregorianCalendar localGregorianCalendar = new GregorianCalendar();
		String str2 = String.valueOf("" + localGregorianCalendar.get(5)
				+ (localGregorianCalendar.get(2) + 1)
				+ localGregorianCalendar.get(1));
		try {

			logPath = GlobalVariables.failedXmlLocation;
			logger.debug("logger path in generate log --> "+logPath);
			localStringBuffer2 = new StringBuffer();
			localStringBuffer2.append(logPath);
			File localFile = new File(localStringBuffer2.toString());
			if ((localFile == null) || (!(localFile.isDirectory()))) {
				localFile.mkdir();
			}
			localStringBuffer2.append(File.separator);
			localStringBuffer2.append(FileName + ".txt");
			str1 = localStringBuffer2.toString();
			localBufferedWriter = new BufferedWriter(new FileWriter(str1, true));
			localBufferedWriter.write(localStringBuffer1.toString());

		} catch (Exception localException) {
			logger.debug("Exception in generateLog-->  "+localException.getMessage());
			//localException.printStackTrace();
		} finally {

			try {
				localBufferedWriter.close();
				localStringBuffer2 = null;

			} catch (Exception e) {
				logger.info("Exception in generate log--> " + e.getMessage());
			}
		}
	}

}