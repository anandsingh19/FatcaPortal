package com.newgen.DAO;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.newgen.bean.GlobalVariables;
import com.newgen.interfaces.Transactional;
import com.newgen.util.Execute_WebService;

public class TransactionBatchDAO implements Transactional {
	private static Logger logger = Logger.getLogger(TransactionBatchDAO.class);

	@Override
	public JSONObject getBatch(String pageNo, String totalCount,
			String userName, String userEmail, String userGiin,
			String complianceType, String reportingYear) {
		JSONObject jObj = new JSONObject();
		int pageCount = 0;
		String nextFlag = "TRUE";
		String SOAP_inxml = null;
		String option = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;


		if (pageNo == null)
			pageCount = 0;
		else
			pageCount = Integer.parseInt(pageNo);

		try {
			if (userName != null) {

				JSONArray rowArr = new JSONArray();
				xmlvalues = new HashMap<String, String>();
				exe_websrv = new Execute_WebService();
				xmlvalues.put("BatchSize", GlobalVariables.batchSize + "");
				xmlvalues.put("PageCount", pageCount + "");
				xmlvalues.put("UserEmail", userEmail);
				xmlvalues.put("UserGiin", userGiin);
				xmlvalues.put("ComplianceType", complianceType);
				xmlvalues.put("ReportingYear", reportingYear);
				option = "ProcedureTransactionBatch";

				SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
				outptXMLlst = new ArrayList<String>();

				outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
						GlobalVariables.WSDL_String);
				int n = outptXMLlst.size() / 9;
				int i = 0;
				while (n > 0) {
					JSONObject rows = new JSONObject();
					/*
					 * beanObj.setFiStage(outptXMLlst.get(i++));
					 * beanObj.setFiStatus(outptXMLlst.get(i++));
					 */
					// i++;// for rownum in the table not required here
					//rows.put("fi_stage", outptXMLlst.get(i++));
					//rows.put("fi_status", outptXMLlst.get(i++));
					//rows.put("d_Filename", outptXMLlst.get(i++));
					//rows.put("FileUpload_By", outptXMLlst.get(i++));
					//rows.put("File_Type", outptXMLlst.get(i++));
					//rows.put("File_Stage", outptXMLlst.get(i++));
					//rows.put("File_Status", outptXMLlst.get(i++));
					//rows.put("FileUpload_Date", outptXMLlst.get(i++));

					//rows.put("LogDownload_Path", outptXMLlst.get(i++));
                    /*changes for All by Shubham on 28072016  commented below line  - starts*/
					//rows.put("LogDownload_Path", outptXMLlst.get(i++));
					/*changes for All by Shubham on 28072016  commented below line  - starts*/
					
					
					
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						rows.put("fi_stage",outptXMLlst.get(i++));
					else {
						rows.put("fi_stage","");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						rows.put("fi_status",outptXMLlst.get(i++));
					else {
						rows.put("fi_status","");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						rows.put("d_Filename",outptXMLlst.get(i++));
					else {
						rows.put("d_Filename","");
						i++;
					}

					// /beanObj.setFiStage(outptXMLlst.get(i++));
					// beanObj.setFiStatus(outptXMLlst.get(i++));
					// beanObj.setFileName(outptXMLlst.get(i++));
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						rows.put("FileUpload_By",outptXMLlst.get(i++));
					else {
						rows.put("FileUpload_By","");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						rows.put("File_Type",outptXMLlst.get(i++));
					else {
						rows.put("File_Type","");
						i++;
					}
					// beanObj.setUploadedBy(outptXMLlst.get(i++));
					// beanObj.setFiletype(outptXMLlst.get(i++));

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						rows.put("File_Stage",outptXMLlst.get(i++));
					else {
						rows.put("File_Stage","");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						rows.put("File_Status",outptXMLlst.get(i++));
					else {
						rows.put("File_Status","");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						rows.put("FileUpload_Date",outptXMLlst.get(i++));
					else {
						rows.put("FileUpload_Date","");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						rows.put("LogDownload_Path",outptXMLlst.get(i++));
					else {
						rows.put("LogDownload_Path","");
						i++;
					}
					
					rowArr.put(rows);
					n--;
				}
				if (totalCount != null && totalCount.length() != 0) {
					int count = Integer.parseInt(totalCount);

					if ((GlobalVariables.batchSize * (pageCount + 1)) < count) {
						nextFlag = "TRUE";
					} else
						nextFlag = "FALSE";
				}
				jObj.put("nextFlag", nextFlag);
				jObj.put("pageNo", pageNo + "");
				jObj.put("totalCount", totalCount);
				jObj.put("ROWS", rowArr);

			}

		} catch (Exception ex) {
			logger.info("Exception in  exception block of transaction batch."
					+ ex.getMessage());

		}
		return jObj;

	}

}
