package com.newgen.DAO;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.newgen.bean.FIRegistration;
import com.newgen.bean.GlobalVariables;
import com.newgen.factory.ObjectFactory;
import com.newgen.interfaces.Customable;
import com.newgen.interfaces.Resgistrable;
import com.newgen.util.Execute_WebService;

public class RegistrationDAO implements Resgistrable {

	private static Logger logger = Logger.getLogger(RegistrationDAO.class);

	@Override
	public JSONObject getFilerInformation(String giin) {
		// TODO Auto-generated method stub
		Map<String, String> ginnInforamtion = new HashMap<String, String>();
		JSONObject jsonObj = null;
		String SOAP_inxml = null;
		String option = null;
		HashMap<String, String> xmlvalues = null;
		List<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		if (giin != null && giin.length() != 0) {

			try {

				xmlvalues = new HashMap<String, String>();
				exe_websrv = new Execute_WebService();
				xmlvalues.put("giin", giin);
				option = "ProcedureSelectGIINDetails";
				SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
				outptXMLlst = new ArrayList<String>();
				outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
						GlobalVariables.WSDL_String);

				int n = outptXMLlst.size() / 5;
				int i = 0;
				while (n > 0) {
					jsonObj = new JSONObject();
					ginnInforamtion.put("name",
							(outptXMLlst.get(i).equalsIgnoreCase("null") ? ""
									: outptXMLlst.get(i++)));
					ginnInforamtion.put("address",
							(outptXMLlst.get(i).equalsIgnoreCase("null") ? ""
									: outptXMLlst.get(i++)));
					ginnInforamtion.put("country",
							(outptXMLlst.get(i).equalsIgnoreCase("null") ? ""
									: outptXMLlst.get(i++)));
					ginnInforamtion.put("emailID",
							(outptXMLlst.get(i).equalsIgnoreCase("null") ? ""
									: outptXMLlst.get(i++)));
					ginnInforamtion.put("phone",
							(outptXMLlst.get(i).equalsIgnoreCase("null") ? ""
									: outptXMLlst.get(i++)));
					jsonObj.putAll(ginnInforamtion);
					n--;
				}

			} catch (Exception ex) {
				logger.info("RegistrationDAO:getFilerInformation:There is some exception in getGIINDetails function "
						+ ex.getMessage());

			}
		}

		return jsonObj;
	}

	@Override
	public boolean registerUser(Object obj, Map<String, InputStream> inputStream) {
		// TODO Auto-generated method stub
		Customable custom = ObjectFactory.getCustomObject();
		FIRegistration fIRegistration = null;
		boolean flag = false;
		if (obj instanceof FIRegistration) {
			fIRegistration = (FIRegistration) obj;

		}

		String SOAP_inxml = null;
		String option = null;

		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		String fiCountry = null;
		String userCountry = null;

		if (custom.SaveSupportiveDoc(obj, inputStream)) {
			try {

				/* to get country code (user and fi country are same) */
				fiCountry = fIRegistration.getFiCountry();
				userCountry = fIRegistration.getUserCountry();
				/*Change for all by Shubham on 28072016  'commented user country' starts*/

				if (null != fIRegistration.getFiCountry()
						&& fiCountry.contains("-")) {
					fiCountry = fiCountry.substring(0, fiCountry.indexOf("-"));
					//userCountry = userCountry.substring(0,
						//	userCountry.indexOf("-"));
				} else if (null != fIRegistration.getFiCountry()
						&& fiCountry.length() > 3) {
					fiCountry = fiCountry.substring(0, 2);
					//userCountry = userCountry.substring(0, 2);
				} else {
					fiCountry = "";
					//userCountry = "";
				}
				/* to get country code */
				/*Change for all by Shubham on 28072016  'commented user country' ends*/
				xmlvalues = new HashMap<String, String>();
				exe_websrv = new Execute_WebService();

				xmlvalues.put("GIIN", fIRegistration.getGiin());
				xmlvalues.put("FIName", fIRegistration.getFiName());
				xmlvalues.put("FIEmail", fIRegistration.getFiEmail());
				xmlvalues.put("FICountry", fiCountry);
				xmlvalues.put("FIPhone", fIRegistration.getFiPhone());
				xmlvalues.put("FIAddress", fIRegistration.getFiAddress());
				xmlvalues.put("UserEmail", fIRegistration.getUserEmailId());
				xmlvalues.put("UserName", fIRegistration.getUserName());
				xmlvalues.put("UserEmpID", fIRegistration.getUserEmployeeID());
				xmlvalues.put("UserEmpDesignation",
						fIRegistration.getUserDesignation());
				xmlvalues.put("UserEmpCountry", userCountry);
				xmlvalues.put("UserEmpPhone", fIRegistration.getUserPhone());
				xmlvalues.put("UserAddress", fIRegistration.getUserAddress());
				
				xmlvalues.put("UserGender", fIRegistration.getUserGender());
				xmlvalues.put("UserDOB", fIRegistration.getUserDOB());
				
				xmlvalues.put("Document1", fIRegistration.getUserDocType1());
				xmlvalues.put("DocName1", custom.getFileName(fIRegistration
						.getUserDocType1File1()));
				xmlvalues.put("Document2", fIRegistration.getUserDocType2());
				xmlvalues.put("DocName2", custom.getFileName(fIRegistration
						.getUserDocType2File2()));
				xmlvalues.put("Password",
						custom.getHashValue(fIRegistration.getUserPassword()));
				xmlvalues.put("FIUSER", fIRegistration.getOperation());
				xmlvalues.put("FLAG", "0");
				xmlvalues.put("docTypeLocation",
						GlobalVariables.file_DBLocation);
				xmlvalues.put("InType", fIRegistration.getInType());
				xmlvalues.put("ComplianceType",
						fIRegistration.getComplianceType());
				logger.debug(xmlvalues);

				option = "ProcedureRegisterUser";

				SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
				outptXMLlst = new ArrayList<String>();
				outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
						GlobalVariables.WSDL_String);
				if (outptXMLlst.get(0).equalsIgnoreCase("SUCCESS")) {
					flag = true;
				}

			} catch (Exception ex) {
				// TODO: handle exception
				logger.info("RegistrationDAO:registerUser:Exception"
						+ ex.getMessage());
			}
		}

		return flag;
	}

	@Override
	public String getMasterDropDown(String field) {

		logger.info("Inside  getMasterDropDown" + field);
		String sWhere = "";
		String table = null;

		String output = null;
		table = field;

		String SOAP_inxml = "";
		String option = "";

		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;

		table = "NG_FATCA_MASTER_" + table;
		try {

			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();

			xmlvalues.put("Field", field);
			xmlvalues.put("Table", table);
			xmlvalues.put("sWhere", sWhere);

			option = "ProcedureGetMasterDropDownValues";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			int n = outptXMLlst.size();
			int i = 0;
			if (n > 0) {
				output = (outptXMLlst.get(i).equalsIgnoreCase("null") ? ""
						: outptXMLlst.get(i++));
			}

		} catch (Exception ex) {
			logger.info("RegistrationDAO:getMasterDropDown:There is some exception in getGIINDetails function "
					+ ex.getMessage());

		}

		return output;
	}

	@Override
	public String getFiAdminCount(String giin) {
		String fiAdminCount = null;
		try {
			String SOAP_inxml = null;
			String option = null;
			HashMap<String, String> xmlvalues = null;
			ArrayList<String> outptXMLlst = null;
			Execute_WebService exe_websrv = null;
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("GIIN", giin);
			xmlvalues.put("ProcCallingFor", "getFiAdminCount");
			option = "ProcedureFATCAMaxFIadminCount";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {
				fiAdminCount = outptXMLlst.get(0);
				logger.debug("fiAdminCount in getFiAdminCount Dao --> "
						+ fiAdminCount);
			}
		} catch (Exception e) {
			logger.debug("Exception in getFiAdminCount --> " + e.getMessage());
		}
		return fiAdminCount;
	}

}
