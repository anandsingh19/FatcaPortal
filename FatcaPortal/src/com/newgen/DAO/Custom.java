package com.newgen.DAO;

import java.awt.List;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.newgen.bean.FIRegistration;
import com.newgen.bean.GlobalVariables;
import com.newgen.bean.TransactionXMLBean;
import com.newgen.factory.ObjectFactory;
import com.newgen.interfaces.Customable;
import com.newgen.pdf.PDFSample;
import com.newgen.pdf.PDFTableGenerator;
import com.newgen.util.Execute_WebService;

//change by shubham for pdf report

public class Custom implements Customable {

	private static Logger logger = Logger.getLogger(Custom.class);

	SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
	Date date = new Date();

	@Override
	public String getHashValue(String input) {
		// TODO Auto-generated method stub
		String output = null;
		MessageDigest md = null;
		StringBuilder sb = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(input.getBytes());
			byte[] mdbytes = md.digest();
			sb = new StringBuilder();
			for (int i = 0; i < mdbytes.length; i++) {
				sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16)
						.substring(1));
			}

			output = sb.toString();
		} catch (Exception e) {
			// System.out.println("Exception in getHashValue--> " + e);
			logger.info("Exception in hashvalue");
			e.printStackTrace();
		} finally {

			sb = null;
		}
		return output;
	}

	/* bug # -10249,bug # -10250,bug # -10253 changes */
	@Override
	public boolean SaveSupportiveDoc(Object obj,
			Map<String, InputStream> fileInStrmMap) {
		String[] docArray = new String[2];
		String[] docInStreamArray = new String[2];
		String docName = null;

		String dest_path = null;
		String only_docName = null;
		boolean b_docUploaded = false;

		FIRegistration fIRegistration = null;
		if (obj instanceof FIRegistration) {
			fIRegistration = (FIRegistration) obj;

		}

		try {
			docArray[0] = fIRegistration.getUserDocType1File1();
			docArray[1] = fIRegistration.getUserDocType2File2();
			docInStreamArray[0] = "file1";
			docInStreamArray[1] = "file2";
			File location = new File(GlobalVariables.sharedLocation
					+ File.separator + sdf.format(date));
			if (!location.exists()) {
				location.mkdir();
			}
			File giinFolder = new File(location + File.separator
					+ fIRegistration.getGiin() + File.separator
					+ fIRegistration.getUserEmailId());
			if (!giinFolder.exists()) {
				giinFolder.mkdirs();

			}
			dest_path = giinFolder.getAbsolutePath();
			GlobalVariables.file_DBLocation = dest_path;
			for (int i = 0; i < docArray.length; i++) {
				docName = docArray[i];
				if (docName != null && docName.length() > 0) {
					only_docName = getFileName(docName);
					logger.debug("filename in supportive Documents"
							+ only_docName);
					logger.debug("dest_path in supportive Documents"
							+ dest_path);
					/*
					 * only_docName = docName.substring(
					 * docName.lastIndexOf("\\") + 1, docName.length());
					 */
					if (fileInStrmMap.get(docInStreamArray[i]) != null) {

						if (SaveUploadedFile(dest_path, only_docName,
								fileInStrmMap.get(docInStreamArray[i])))
							b_docUploaded = true;
					} else {
						logger.info("File is blank");
					}

				}
			}
		} catch (Exception e) {
			logger.info("Exception in SaveSupportiveDoc --> " + e.getMessage());
		}
		return b_docUploaded;

	}

	/* bug # -10249,bug # -10250,bug # -10253 changes */
	@Override
	public boolean SaveUploadedFile(String SaveLocation, String sourceFileName,
			InputStream fileContent) {
		boolean fileCopied = false;
		File file = null;
		try {
			logger.info("inside SaveUploadedFile");
			file = new File(SaveLocation);
			if (!file.exists()) {
				file.mkdirs();
			}

			File f = new File(SaveLocation + File.separator + sourceFileName);

			if (f.exists())
				f.delete();
			Files.copy(fileContent, f.toPath());
			fileCopied = true;

		} catch (Exception ex) {
			logger.info("Exception ex" + ex.getMessage());

		}
		return fileCopied;

	}

	@Override
	public String getFileName(String file) {
		String only_docName = null;
		try {
			only_docName = file.substring(file.lastIndexOf(File.separator) + 1,
					file.length());

		} catch (Exception e) {
			logger.info("RegistrationDAO:getFileName:Excpetion in getting file name");
		}
		return only_docName;
	}

	@Override
	public String getFileExtention(String file) {
		String fileExtnsn = null;
		try {
			fileExtnsn = file.substring(file.lastIndexOf(".") + 1,
					file.length());

		} catch (Exception e) {
			logger.info("RegistrationDAO:getFileName:Excpetion in getting file name");
		}
		return fileExtnsn;
	}

	@Override
	public boolean saveReplacedDoc(Object obj,
			Map<String, InputStream> fileInStrmMap,
			Map<String, String> ReqAttrMap) {
		String docName = null;
		String dest_path = null;
		String only_docName = null;
		boolean b_docUploaded = false;

		FIRegistration fIRegistration = null;
		if (obj instanceof FIRegistration) {
			fIRegistration = (FIRegistration) obj;

		}
		try {

			/*
			 * File location = new File(GlobalVariables.sharedLocation +
			 * File.separator + sdf.format(date)); if (!location.exists()) {
			 * location.mkdir(); } File giinFolder = new File(location +
			 * File.separator + fIRegistration.getGiin() + File.separator +
			 * fIRegistration.getUserEmailId()); if (!giinFolder.exists()) {
			 * giinFolder.mkdirs();
			 * 
			 * }
			 */// dest_path = giinFolder.getAbsolutePath();
			dest_path = GlobalVariables.file_DBLocation;
			GlobalVariables.file_DBLocation = dest_path;

			// Set<Entry<String, String>> ReqAttMapEntry =
			// ReqAttrMap.entrySet();

			if (!fileInStrmMap.isEmpty()) {

				for (Entry<String, InputStream> entry : fileInStrmMap
						.entrySet()) {
					logger.debug(entry.getKey() + "/" + entry.getValue());
					if (null != entry.getValue()) {
						docName = ReqAttrMap.get(entry.getKey());
						logger.debug("khushdil edit docname--> " + docName);
						// if (docName != null && docName.length() > 0) {
						only_docName = getFileName(docName);
						logger.debug("filename in supportive Documents"
								+ only_docName);
						logger.debug("dest_path in supportive Documents"
								+ dest_path);
						if (SaveUploadedFile(dest_path, only_docName,
								entry.getValue()))
							b_docUploaded = true;
						// }
					} else {
						logger.info("File is blank");
					}
				}
			} else {
				logger.info("there is no file to upload");
			}

		} catch (Exception e) {
			logger.info("Exception in SaveSupportiveDoc --> " + e.getMessage());
		}
		// return b_docUploaded;
		return true;

	}

	@Override
	public boolean isReportinngModeCorrect(String GIIN, String reportingYear,
			String reportingMode, String complianceType, String new_update) {
		boolean isSubmissionModeCorrect = false;
		String submissionMode = null;
		String SOAP_inxml = "";
		String option = "";
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		xmlvalues = new HashMap<String, String>();
		exe_websrv = new Execute_WebService();

		try {
			xmlvalues.put("Giin", GIIN);
			xmlvalues.put("ReportingYear", reportingYear);
			xmlvalues.put("ProcCalledFor", "SubmissionMode");
			xmlvalues.put("ProcCalledFrom", reportingMode); // Either XML or
			// manual
			xmlvalues.put("ComplianceType", complianceType);
			xmlvalues.put("TestOrActual", new_update);
			option = "ProcedureFATCAManualFormStatus";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {
				submissionMode = outptXMLlst.get(0);
				logger.info(outptXMLlst.get(0));
				logger.debug("mode from browser (Khushdil) --> "
						+ reportingMode);
				// Change for all by KD on 28072016 starts
				// adding in global variable to check later if user allowed or
				// not to upload file
				GlobalVariables.userAllowedtoUpload = submissionMode;
				logger.debug("userAllowedtoUpload in custom  --> "
						+ GlobalVariables.userAllowedtoUpload);

				if (submissionMode.equalsIgnoreCase("No Record Found")
						|| submissionMode.equalsIgnoreCase(reportingMode)
						|| submissionMode.equalsIgnoreCase("Allow")) {

					isSubmissionModeCorrect = true;
				}
				// Change for all by KD on 28072016 end
				logger.debug("isSubmissionModeCorrect (Khushdil) --> "
						+ isSubmissionModeCorrect);
			}

		} catch (Exception e) {
			logger.debug("Exception in isReportinngModeCorrect-->  "
					+ e.getMessage());
		}
		return isSubmissionModeCorrect;
	}

	// *********************************************************************************
	// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
	// Group : Application Projects1 (US)
	// Project : [Fatca Reporting St.Lucia]
	// Date Written : 09/06/2016
	// Date Modified :
	// Author : Khushdil Kaushik
	// Description : To generate registered users report
	// ***********************************************************************************/
	// change by shubham for pdf case on 26-10-2016


	public boolean generateReportFile(String file, String userType,
			String userGIIN, String reportType, String searchFor) {
		boolean bFileGenerated = false;
		FileWriter writer = null;
		String SOAP_inxml = "";
		String option = "";
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;

		exe_websrv = new Execute_WebService();
		FIRegistration fiUser = null;
		HashMap<Integer, FIRegistration> userData = null;
		try {
			xmlvalues = new HashMap<String, String>();
			userData = new HashMap<Integer, FIRegistration>();
			xmlvalues.put("userType", userType);
			xmlvalues.put("userGIIN", userGIIN);
			xmlvalues.put("searchFor", searchFor);
			xmlvalues.put("ReportType", reportType);
			xmlvalues.put("FromDate", "");
			xmlvalues.put("ToDate", "");
			xmlvalues.put("ReportCompliance", "");
			option = "ProcedureCustomReport";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);



			int n = 0;
			int i = 0;
			int k = 0;
			int j = 0;
			n = outptXMLlst.size() / 18;// 18 is number of columns in select
			String [][] content = new String [n][19];
			String temp="";
			if (!outptXMLlst.isEmpty()) {

				while (n > 0) {
					k=0;
					//fiUser = ObjectFactory.getFIRegistrationObject();
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setGiin(outptXMLlst.get(i++));

						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;


					}
					else {
						//fiUser.setGiin("");
						content[j][k]="";
						k++;						
						i++;
					}

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setUserEmailId(outptXMLlst.get(i++));
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
					}
					else {
						//fiUser.setUserEmailId("");
						content[j][k]="";
						k++;						
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{

						//fiUser.setUserName(outptXMLlst.get(i++));
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
					}
					else {
						//fiUser.setUserName("");
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setUserEmployeeID(outptXMLlst.get(i++));
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;

					}
					else {
						//fiUser.setUserEmployeeID("");
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						//fiUser.setUserDesignation(outptXMLlst.get(i++));
					}
					else {
						//fiUser.setUserDesignation("");
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						//fiUser.setUserPhone(outptXMLlst.get(i++));
					}
					else {
						//fiUser.setUserPhone("");
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						//fiUser.setUserCountry(outptXMLlst.get(i++));
					}
					else {
						//fiUser.setUserCountry("");
						content[j][k]="";
						k++;
						i++;
					}
					/* User Details Section */

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						//fiUser.setUserDocType1(outptXMLlst.get(i++));
					}
					else {
						//fiUser.setUserDocType1("");
						content[j][k]="";
						k++;
						i++;

					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						//fiUser.setUserDocType2(outptXMLlst.get(i++));
					}
					else {
						//fiUser.setUserDocType2("");
						content[j][k]="";
						k++;
						i++;
					}

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						//fiUser.setOperation(outptXMLlst.get(i++));
					}
					else {
						//fiUser.setOperation("");
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setFlag(outptXMLlst.get(i++));
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;

					}
					else {
						//fiUser.setFlag("");
						content[j][k]="";
						k++;
						i++;
					}
					/**/

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setUserAddress(outptXMLlst.get(i++));

						if(outptXMLlst.get(i).length()>100)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,100)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;

					}
					else {
						//fiUser.setUserAddress("");
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setUserDocType1File1(outptXMLlst.get(i++));
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
					}
					else {
						//fiUser.setUserDocType1File1("");
						content[j][k]="";
						k++;
						i++;
					}

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setUserDocType2File2(outptXMLlst.get(i++));
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
					}
					else {
						//fiUser.setUserDocType2File2("");
						content[j][k]="";
						k++;
						i++;

					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setUserGender(outptXMLlst.get(i++));
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;

					}
					else {
						//fiUser.setUserGender("");
						content[j][k]="";
						k++;
						i++;
					}

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//	fiUser.setUserDOB(outptXMLlst.get(i++));
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;

					}

					else {
						//fiUser.setUserDOB("");
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setUserRegisteredOn(outptXMLlst.get(i++));
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;


					}
					else {
						//fiUser.setUserRegisteredOn("");
						content[j][k]="";
						k++;
						i++;

					}

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setUserModifiedOn(outptXMLlst.get(i++));
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;

					}
					else {
						//fiUser.setUserModifiedOn("");
						content[j][k]="";
						k++;
						i++;
					}
					/*if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setFiName(outptXMLlst.get(i++));
						content[j][k]=outptXMLlst.get(i++);
						k++;

					}
					else {
						//fiUser.setFiName("");
						content[j][k]="";
						k++;
						i++;
					}



					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setFiEmail(outptXMLlst.get(i++));
						content[j][k]=outptXMLlst.get(i++);
						k++;

					}
					else {
						//fiUser.setFiEmail("");
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
					   //	fiUser.setFiContact(outptXMLlst.get(i++));
						content[j][k]=outptXMLlst.get(i++);
						k++;


					}
					else {
						//fiUser.setFiContact("");
						content[j][k]="";
						k++;
						i++;

						}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setFiCountry(outptXMLlst.get(i++));
						content[j][k]=outptXMLlst.get(i++);
						k++;

					}
					else {
						//fiUser.setFiCountry("");
						content[j][k]="";
						k++;
						i++;

					}

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						//fiUser.setFiAddress(outptXMLlst.get(i++));
						content[j][k]=outptXMLlst.get(i++);
						k++;

					}
					else {
						//fiUser.setFiAddress("");
						content[j][k]="";
						k++;
						i++;
					}
					 */
					//userData.put(n, fiUser);
					n--;
					j++;
				}

			}

			/*writer = new FileWriter(file);
			writer.append("User GIIN").append(",");

			 * writer.append("FI Name").append(",");
			 * writer.append("FI Address").append(",");
			 * writer.append("FI Contact").append(",");
			 * writer.append("FI Country").append(",");
			 * writer.append("FI Email").append(",");


			writer.append("User Name").append(",");
			writer.append("User Email").append(",");
			writer.append("User Address").append(",");
			writer.append("User Country").append(",");
			writer.append("User Designation").append(",");
			writer.append("User DOB").append(",");
			writer.append("User Employee ID").append(",");
			writer.append("User Gender").append(",");
			writer.append("User Modified On").append(",");
			writer.append("User Phone").append(",");
			writer.append("User Registered On").append(",");

			writer.append("User Status").append(",");
			writer.append("User Type").append(",");
			writer.append("Supportive Document1 Type").append(",");
			writer.append("Supportive Document1").append(",");
			writer.append("Supportive Document2 Type").append(",");
			writer.append("Supportive Document2").append("\n");*/
			/*int mapsize = userData.size();*/
			/*for (int j = mapsize; j >= 1; j--) {
				fiUser = userData.get(j);
				writer.append(fiUser.getGiin()).append(",");

			 * writer.append(fiUser.getFiName()).append(",");
			 * writer.append(fiUser.getFiAddress()).append(",");
			 * writer.append(fiUser.getFiContact()).append(",");
			 * writer.append(fiUser.getFiCountry()).append(",");
			 * writer.append(fiUser.getFiEmail()).append(",");

				writer.append(fiUser.getUserName()).append(",");
				writer.append(fiUser.getUserEmailId()).append(",");
				writer.append(fiUser.getUserAddress()).append(",");
				writer.append(fiUser.getUserCountry()).append(",");
				writer.append(fiUser.getUserDesignation()).append(",");
				writer.append(fiUser.getUserDOB()).append(",");
				writer.append(fiUser.getUserEmployeeID()).append(",");
				writer.append(fiUser.getUserGender()).append(",");
				writer.append(fiUser.getUserModifiedOn()).append(",");
				writer.append(fiUser.getUserPhone()).append(",");
				writer.append(fiUser.getUserRegisteredOn()).append(",");
				writer.append(fiUser.getFlag()).append(",");
				writer.append(fiUser.getOperation()).append(",");
				writer.append(fiUser.getUserDocType1()).append(",");
				writer.append(fiUser.getUserDocType1File1()).append(",");
				writer.append(fiUser.getUserDocType2()).append(",");
				writer.append(fiUser.getUserDocType2File2()).append("\n");
			}*/


			ArrayList<String> registeredUsers = new ArrayList<String>();
			registeredUsers.add("User GIIN");
			registeredUsers.add("User Email");
			registeredUsers.add("User Name");
			registeredUsers.add("User Employee ID");
			registeredUsers.add("User Designation");
			registeredUsers.add("User Phone");
			registeredUsers.add("User Country");
			registeredUsers.add("Supp. Doc.1");
			registeredUsers.add("Supp. Doc.2");
			registeredUsers.add("User Type");
			registeredUsers.add("User Status");
			registeredUsers.add("User Address");
			registeredUsers.add("Supp. Doc.1 Name");
			registeredUsers.add("Supp. Doc.2 Name");
			registeredUsers.add("User Gender");
			registeredUsers.add("User Date Of Birth");
			registeredUsers.add("User Registered On");
			registeredUsers.add("User Modified On");


			/*System.out.println("your file created in : "
					+ System.getProperty("user.dir"));
			 */
			new PDFTableGenerator().generatePDF(PDFSample.createContent(registeredUsers,content,file),file);
			logger.debug("File Generated");
			bFileGenerated = true;
		} catch (Exception e) {
			bFileGenerated = false;
			logger.debug("Custom:: generateReportFile:Excpetion "
					+ e.getMessage());
		} finally {
			try {
				/*writer.flush();
				writer.close();*/
			} catch (Exception e2) {
				bFileGenerated = false;
				logger.debug("Custom:: generateReportFile:finally Excpetion "
						+ e2.getMessage());
			}
		}
		return bFileGenerated;
	}

	// *********************************************************************************
	// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
	// Group : Application Projects1 (US)
	// Project : [Fatca Reporting St.Lucia]
	// Date Written : 13/06/2016
	// Date Modified :
	// Author : Khushdil Kaushik
	// Description : To generate registered users history report in a
	// date range
	// ***********************************************************************************/

	@Override
	public boolean generateUserHistReport(String file, String userType,
			String userGIIN, String reportType, String searchFor,
			String fromDate, String toDate) {
		boolean bFileGenerated = false;
		/*FileWriter writer = null;*/
		String SOAP_inxml = "";
		String option = "";
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;

		exe_websrv = new Execute_WebService();
		/*FIRegistration fiUser = null;
		HashMap<Integer, FIRegistration> userData = null;*/
		try {
			xmlvalues = new HashMap<String, String>();
			/*userData = new HashMap<Integer, FIRegistration>();*/
			xmlvalues.put("userType", userType);
			xmlvalues.put("userGIIN", userGIIN);
			xmlvalues.put("searchFor", searchFor);
			xmlvalues.put("ReportType", reportType);
			xmlvalues.put("FromDate", fromDate);
			xmlvalues.put("ToDate", toDate);
			xmlvalues.put("ReportCompliance", "");
			option = "ProcedureCustomReport";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);

			int n = 0;
			int i = 0;
			int k = 0;
			int j = 0;
			String temp="";
			n = outptXMLlst.size() / 8;// 8 is number of columns in select
			String [][] content = new String [n][8];
			if (!outptXMLlst.isEmpty()) {
				while (n > 0) {
					k=0;
					//fiUser = ObjectFactory.getFIRegistrationObject();
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						
					{
						// set value of Giin
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						
					}
											
					else {
						// set value of Giin
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						
					{
						// set value of FI Name
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						
					}
											
					else {
						// set value of FI Name
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						
					{
						// set value of FI Email
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						
					}
											
					else {
						// set value of FI Email
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						
					{
						// set value of Field Name
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						
					}
											
					else {
						// set value of Field Name
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						
					{
						// set value of Current Value
						if(outptXMLlst.get(i).length()>100)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,100)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						
					}
											
					else {
						// set value of Current Value
						content[j][k]="";
						k++;
						i++;
					}
					
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						
					{
						// set value of Older Value
						if(outptXMLlst.get(i).length()>100)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,100)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						
					}
											
					else {
						// set value of Older Value
						content[j][k]="";
						k++;
						i++;
					}
					
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						
					{
						// set value of OPeration Performed By
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						
					}
											
					else {
						// set value of OPeration Performed By
						content[j][k]="";
						k++;
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						
					{
						// set value of Modified On
						if(outptXMLlst.get(i).length()>26)
						{
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
						
					}
											
					else {
						// set value of Modified On
						content[j][k]="";
						k++;
						i++;
					}
					/*if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserName(outptXMLlst.get(i++));
					else {
						fiUser.setUserName("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserEmployeeID(outptXMLlst.get(i++));
					else {
						fiUser.setUserEmployeeID("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserDesignation(outptXMLlst.get(i++));
					else {
						fiUser.setUserDesignation("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserPhone(outptXMLlst.get(i++));
					else {
						fiUser.setUserPhone("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserCountry(outptXMLlst.get(i++));
					else {
						fiUser.setUserCountry("");
						i++;
					}
					 User Details Section 

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserDocType1(outptXMLlst.get(i++));
					else {
						fiUser.setUserDocType1("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserDocType2(outptXMLlst.get(i++));
					else {
						fiUser.setUserDocType2("");
						i++;
					}

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setOperation(outptXMLlst.get(i++));
					else {
						fiUser.setOperation("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setFlag(outptXMLlst.get(i++));
					else {
						fiUser.setFlag("");
						i++;
					}
					

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserAddress(outptXMLlst.get(i++));
					else {
						fiUser.setUserAddress("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserDocType1File1(outptXMLlst.get(i++));
					else {
						fiUser.setUserDocType1File1("");
						i++;
					}

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserDocType2File2(outptXMLlst.get(i++));
					else {
						fiUser.setUserDocType2File2("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserGender(outptXMLlst.get(i++));
					else {
						fiUser.setUserGender("");
						i++;
					}
					

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserDOB(outptXMLlst.get(i++));
					else {
						fiUser.setUserDOB("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserRegisteredOn(outptXMLlst.get(i++));
					else {
						fiUser.setUserRegisteredOn("");
						i++;
					}

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setUserModifiedOn(outptXMLlst.get(i++));
					else {
						fiUser.setUserModifiedOn("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setFiName(outptXMLlst.get(i++));
					else {
						fiUser.setFiName("");
						i++;
					}

					

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setFiEmail(outptXMLlst.get(i++));
					else {
						fiUser.setFiEmail("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setFiContact(outptXMLlst.get(i++));
					else {
						fiUser.setFiContact("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setFiCountry(outptXMLlst.get(i++));
					else {
						fiUser.setFiCountry("");
						i++;
					}

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setFiAddress(outptXMLlst.get(i++));
					else {
						fiUser.setFiAddress("");
						i++;
					}
					

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setComments(outptXMLlst.get(i++));
					else {
						fiUser.setComments("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setRejectedByName(outptXMLlst.get(i++));
					else {
						fiUser.setRejectedByName("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setRejectedByEmail(outptXMLlst.get(i++));
					else {
						fiUser.setRejectedByEmail("");
						i++;
					}

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						fiUser.setRejectedByUserType(outptXMLlst.get(i++));
					else {
						fiUser.setRejectedByUserType("");
						i++;
					}

					userData.put(n, fiUser);
					
*/					n--;
					j++;
				}
			}
			
			ArrayList<String> userHistReport = new ArrayList<String>();
			 userHistReport.add("User GIIN");
			 userHistReport.add("FI Name");
			 userHistReport.add("FI Email");
			 userHistReport.add("Field Name");
			 userHistReport.add("Current Value");
			 userHistReport.add("Older Value");
			 userHistReport.add("Operation Performed By");
			 userHistReport.add("Modified On");
		
			/*writer = new FileWriter(file);
			writer.append("User GIIN").append(",");
			writer.append("FI Name").append(",");
			writer.append("FI Address").append(",");
			writer.append("FI Contact").append(",");
			writer.append("FI Country").append(",");
			writer.append("FI Email").append(",");
			writer.append("User Name").append(",");
			writer.append("User Email").append(",");
			writer.append("User Address").append(",");
			writer.append("User Country").append(",");
			writer.append("User Designation").append(",");
			writer.append("User DOB").append(",");
			writer.append("User Employee ID").append(",");
			writer.append("User Gender").append(",");
			writer.append("User Modified On").append(",");
			writer.append("User Phone").append(",");
			writer.append("User Registered On").append(",");
			writer.append("User Status").append(",");
			writer.append("User Type").append(",");
			writer.append("Supportive Document1 Type").append(",");
			writer.append("Supportive Document1").append(",");
			writer.append("Supportive Document2 Type").append(",");
			writer.append("Supportive Document2").append(",");
			writer.append("Superior Comments").append(",");
			writer.append("Rejected By User Name").append(",");
			writer.append("Rejected By User Email").append(",");
			writer.append("Rejected By User Type").append("\n");
			int mapsize = userData.size();
			for (int j = mapsize; j >= 1; j--) {
				fiUser = userData.get(j);
				writer.append(fiUser.getGiin()).append(",");
				writer.append(fiUser.getFiName()).append(",");
				writer.append(fiUser.getFiAddress()).append(",");
				writer.append(fiUser.getFiContact()).append(",");
				writer.append(fiUser.getFiCountry()).append(",");
				writer.append(fiUser.getFiEmail()).append(",");
				writer.append(fiUser.getUserName()).append(",");
				writer.append(fiUser.getUserEmailId()).append(",");
				writer.append(fiUser.getUserAddress()).append(",");
				writer.append(fiUser.getUserCountry()).append(",");
				writer.append(fiUser.getUserDesignation()).append(",");
				writer.append(fiUser.getUserDOB()).append(",");
				writer.append(fiUser.getUserEmployeeID()).append(",");
				writer.append(fiUser.getUserGender()).append(",");
				writer.append(fiUser.getUserModifiedOn()).append(",");
				writer.append(fiUser.getUserPhone()).append(",");
				writer.append(fiUser.getUserRegisteredOn()).append(",");
				writer.append(fiUser.getFlag()).append(",");
				writer.append(fiUser.getOperation()).append(",");
				writer.append(fiUser.getUserDocType1()).append(",");
				writer.append(fiUser.getUserDocType1File1()).append(",");
				writer.append(fiUser.getUserDocType2()).append(",");
				writer.append(fiUser.getUserDocType2File2()).append(",");
				writer.append(fiUser.getComments()).append(",");
				writer.append(fiUser.getRejectedByName()).append(",");
				writer.append(fiUser.getRejectedByEmail()).append(",");
				writer.append(fiUser.getRejectedByUserType()).append("\n");
			}*/
			 
			 new PDFTableGenerator().generatePDF(PDFSample.createContent(userHistReport,content,file),file);
			logger.debug("Users History File Generated");
			bFileGenerated = true;
		} catch (Exception e) {
			bFileGenerated = false;
			logger.debug("Custom:: generateReportFile:Excpetion "
					+ e.getMessage());
		} finally {
			try {
				/*writer.flush();
				writer.close();*/
			} catch (Exception e2) {
				bFileGenerated = false;
				logger.debug("Custom:: generateReportFile:finally Excpetion "
						+ e2.getMessage());
			}
		}
		return bFileGenerated;
	}

	// *********************************************************************************
	// NEWGEN SOFTWARE TECHNOLOGIES LIMITED
	// Group : Application Projects1 (US)
	// Project : [Fatca Reporting St.Lucia]
	// Date Written : 15/06/2016
	// Date Modified :
	// Author : Khushdil Kaushik
	// Description : To generate transaction history report for the
	// selected year range
	// ***********************************************************************************/

	@Override
	public boolean generateTransactionHist(String file, String userType,
			String userGIIN, String reportType, String searchFor,
			String fromDate, String toDate) {
		boolean bFileGenerated = false;
		FileWriter writer = null;
		String SOAP_inxml = "";
		String option = "";
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;

		exe_websrv = new Execute_WebService();
		/*TransactionXMLBean tranHistObj = null;
		HashMap<Integer, TransactionXMLBean> transactionData = null;*/
		try {
			xmlvalues = new HashMap<String, String>();
			//transactionData = new HashMap<Integer, TransactionXMLBean>();
			xmlvalues.put("userType", userType);
			xmlvalues.put("userGIIN", userGIIN);
			xmlvalues.put("searchFor", searchFor);
			xmlvalues.put("ReportType", reportType);
			xmlvalues.put("FromDate", fromDate);
			xmlvalues.put("ToDate", toDate);
			xmlvalues.put("ReportCompliance", "");
			option = "ProcedureCustomReport";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);


			int n = 0;
			int i = 0;
			int k = 0;
			int j = 0;



			n = outptXMLlst.size() / 6;// 6 is number of columns in select
			String [][] content = new String [n][7];
			String temp="";
			if (!outptXMLlst.isEmpty()) {

				while (n > 0) {
					k=0;
					//tranHistObj = ObjectFactory.getTransactionXMLBeanObject();
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{

						if(outptXMLlst.get(i).length()>26)
						{
							//tranHistObj.setFileStage(outptXMLlst.get(i++));
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;


					}
					else {
						//fiUser.setFileStage("");
						content[j][k]="";
						k++;						
						i++;
					}


					/* User Details Section */
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						if(outptXMLlst.get(i).length()>26)
						{
							//tranHistObj.setFileStatus(outptXMLlst.get(i++));
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
					}

					else {
						content[j][k]="";
						k++;						
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						if(outptXMLlst.get(i).length()>26)
						{
							//tranHistObj.setUploadedDate(outptXMLlst.get(i++));
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
					}
					else {
						content[j][k]="";
						k++;						
						i++;

					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						if(outptXMLlst.get(i).length()>26)
						{
							//tranHistObj.setUploadedBy_userGIIN(outptXMLlst.get(i++));
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;
					}

					else {
						content[j][k]="";
						k++;						
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)

					{
						if(outptXMLlst.get(i).length()>26)
						{
							//tranHistObj.setUploadedBy(outptXMLlst.get(i++));
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
							k++;
					}
					else {
						content[j][k]="";
						k++;						
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
					{
						if(outptXMLlst.get(i).length()>26)
						{
							//tranHistObj.setReportingYear(outptXMLlst.get(i++));
							temp=outptXMLlst.get(i++);
							temp=temp.substring(0,26)+"...";
							content[j][k]=temp;
						}	
						else	
							content[j][k]=outptXMLlst.get(i++);
						k++;

					}
					else {
						content[j][k]="";
						k++;						
						i++;
					}

					/*transactionData.put(n, tranHistObj);*/
					j++;
					n--;
				}

			}
			ArrayList<String> transactionHistUSers = new ArrayList<String>();
			transactionHistUSers.add("File Stage");
			transactionHistUSers.add("File Status");
			transactionHistUSers.add("Transaction Date");
			transactionHistUSers.add("User Giin");
			transactionHistUSers.add("User Email");
			transactionHistUSers.add("Reporting Year");
			

			new PDFTableGenerator().generatePDF(PDFSample.createContent(transactionHistUSers,content,file),file);

			/**/
			/*writer = new FileWriter(file);
			writer.append("Reporting Year").append(",");
			writer.append("File Uploaded By").append(",");
			writer.append("Usre Giin").append(",");
			writer.append("File Uploaded On").append(",");
			writer.append("File Stage").append(",");
			writer.append("File Status").append("\n");*/

			// File f = new File(file);
			/*int mapsize = transactionData.size();
			for (int j = mapsize; j >= 1; j--) {
				tranHistObj = transactionData.get(j);
				writer.append(tranHistObj.getReportingYear()).append(",");
				writer.append(tranHistObj.getUploadedBy()).append(",");
				writer.append(tranHistObj.getUploadedBy_userGIIN()).append(",");
				writer.append(tranHistObj.getUploadedDate()).append(",");
				writer.append(tranHistObj.getFileStage()).append(",");
				writer.append(tranHistObj.getFileStatus()).append("\n");
			}*/

			logger.debug("Transaction History File Generated");
			bFileGenerated = true;
		} catch (Exception e) {
			bFileGenerated = false;
			logger.debug("Custom:: generateReportFile:Excpetion "
					+ e.getMessage());
		} finally {
			try {
				/*writer.flush();
				writer.close();*/
			} catch (Exception e2) {
				bFileGenerated = false;
				logger.debug("Custom:: generateReportFile:finally Excpetion "
						+ e2.getMessage());
			}
		}
		return bFileGenerated;
	}


}