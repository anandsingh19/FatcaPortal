package com.newgen.DAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.newgen.bean.FrequentlyAskedQue;
import com.newgen.bean.GlobalVariables;
import com.newgen.factory.ObjectFactory;
import com.newgen.interfaces.FAQContactAble;
import com.newgen.util.Execute_WebService;

public class FAQDao implements FAQContactAble {
	private static Logger logger = Logger.getLogger(FAQDao.class);

	@Override
	public Map<Integer, FrequentlyAskedQue> getFAQ() {

		String SOAP_inxml = "";
		String option = "";
		Map<Integer, FrequentlyAskedQue> faqMap = new HashMap<Integer, FrequentlyAskedQue>();
		FrequentlyAskedQue frequentlyAskedQueObj = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		int counter = 1;
		try {
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("CallFor", "FAQ");
			option = "ProcedureFatcaSelectFaqQue";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {

				int n = 0;
				int i = 0;

				n = outptXMLlst.size() / 2;// 2 is number of columns in select
				while (n > 0) {
					frequentlyAskedQueObj = ObjectFactory
							.getFrequentlyAskedQueObject();
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						frequentlyAskedQueObj.setQuetion(outptXMLlst.get(i++));
					else {
						frequentlyAskedQueObj.setQuetion("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						frequentlyAskedQueObj.setAnswer(outptXMLlst.get(i++));
					else {
						frequentlyAskedQueObj.setAnswer("");
						i++;
					}
					faqMap.put(counter, frequentlyAskedQueObj);

					n--;
					counter++;
				}
			}

		} catch (Exception e) {
			logger.debug("Exception in FAQDao-->  " + e.getMessage());
		}
		return faqMap;
	}
	
	@Override
	public List<String> getContactData() {

		String SOAP_inxml = "";
		String option = "";
		List<String> contactPageDataList = new ArrayList<String>();
		// FrequentlyAskedQue frequentlyAskedQueObj = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		try {
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("CallFor", "CONTACTUS");
			option = "ProcedureFatcaSelectFaqQue";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {

				int n = 0;
				int i = 0;

				n = outptXMLlst.size() / 1;// 1 is number of columns in select
				while (n > 0) {

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						contactPageDataList.add(outptXMLlst.get(i++));
					else {
						contactPageDataList.add("");
						i++;
					}
					n--;
				}
			}
		} catch (Exception e) {
			logger.debug("Exception in FAQDao-->  " + e.getMessage());
		}
		return contactPageDataList;
	}

}
