package com.newgen.DAO;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.newgen.bean.FIRegistration;
import com.newgen.bean.GlobalVariables;
import com.newgen.bean.LoginUser;
import com.newgen.factory.ObjectFactory;
import com.newgen.interfaces.UserEditable;
import com.newgen.interfaces.Validatable;
import com.newgen.util.Execute_WebService;

public class EditUserDAO implements UserEditable, Validatable {
	private static Logger logger = Logger.getLogger(EditUserDAO.class);

	@Override
	public FIRegistration getuserDetails(String emailID) {

		logger.info("Inside EditUserDAO");
		FIRegistration fiuser = null;
		fiuser = ObjectFactory.getFIRegistrationObject();
		String SOAP_inxml = null;
		String option = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;

		try {
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("emailID", emailID);
			option = "ProcedureUserSelect";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);

			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);

			int i = 0;

			if (!outptXMLlst.isEmpty()) {
				fiuser.setGiin(outptXMLlst.get(i++));
				fiuser.setFiName(outptXMLlst.get(i++));
				// fiuser.setFiName(outptXMLlst.get(i).equalsIgnoreCase("null")
				// ? "" : outptXMLlst.get(i++));

				fiuser.setFiEmail(outptXMLlst.get(i++));
				fiuser.setFiCountry(outptXMLlst.get(i++));
				fiuser.setFiAddress(outptXMLlst.get(i++));
				fiuser.setUserEmailId(outptXMLlst.get(i++));
				fiuser.setUserName(outptXMLlst.get(i++));
				fiuser.setUserAddress(outptXMLlst.get(i++));

				fiuser.setUserEmployeeID(outptXMLlst.get(i++));
				fiuser.setUserDesignation(outptXMLlst.get(i++));
				fiuser.setUserPhone(outptXMLlst.get(i++));
				fiuser.setUserCountry(outptXMLlst.get(i++));
				fiuser.setUserDocType1(outptXMLlst.get(i++));
				fiuser.setUserDocType2(outptXMLlst.get(i++));
				fiuser.setOperation(outptXMLlst.get(i++));

				fiuser.setUserDocType1File1(outptXMLlst.get(i++));
				fiuser.setUserDocType2File2(outptXMLlst.get(i++));
				fiuser.setUploadFileLocation(outptXMLlst.get(i++));
				logger.debug("file location ----arbind  "
						+ fiuser.getUploadFileLocation());
				fiuser.setPassword(outptXMLlst.get(i++));
				fiuser.setInType(outptXMLlst.get(i++));
				fiuser.setUserDOB(outptXMLlst.get(i++));
				fiuser.setUserGender(outptXMLlst.get(i++));

			}

		} catch (Exception ex) {
			logger.info("EditUserDAO:getFilerInformation:There is some exception in getGIINDetails function "
					+ ex.getMessage());
		}
		return fiuser;

	}

	@Override
	public boolean isEdit(String emailID, String password) {
		String hashValue = null;
		boolean flag = false;
		String SOAP_inxml = null;
		String option = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		try {

			hashValue = ObjectFactory.getCustomObject().getHashValue(password);
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("Password", hashValue);
			xmlvalues.put("Email", emailID);
			option = "ProcedureMyInfoEdit";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (outptXMLlst.get(0).equalsIgnoreCase("SUCCESS"))
				flag = true;

		} catch (Exception ex) {
			logger.info("EditUserDAO:isEdit:There is some exception in isEdit function "
					+ ex.getMessage());
		}

		return flag;
	}

	@Override
	public boolean validate(Object obj) {
		// TODO Auto-generated method stub
		LoginDAO login = ObjectFactory.getLoginObject();
		boolean flag = false;
		LoginUser user = (LoginUser) obj;
		String status = login.isAuthenticated(user);
		if (status.equalsIgnoreCase("success"))
			flag = true;
		return flag;
	}
	/*
	 * public boolean validate(Object obj) { LoginUser user = (LoginUser) obj;
	 * boolean flag = false;
	 * 
	 * if ((user.getUserEmail() == null || user.getUserEmail().length() == 0) &&
	 * (user.getPassword() == null || user.getPassword().length() == 0)) {
	 * logger.info("user and password Does not match."); } else { String status
	 * = isAuthenticated(user); if (status.equalsIgnoreCase("success")) flag =
	 * true; } return flag; }
	 */
	@Override
	public boolean validateOldPassword(LoginUser user) {
		// TODO Auto-generated method stub
		LoginDAO login = ObjectFactory.getLoginObject();
		boolean flag = false;
		String status = login.isOldPasswordAuthenticated(user);
		if (status.equalsIgnoreCase("success"))
			flag = true;
		return flag;
	}
	
}
