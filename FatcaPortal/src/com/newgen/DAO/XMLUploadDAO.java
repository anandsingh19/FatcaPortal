package com.newgen.DAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.newgen.bean.GlobalVariables;
import com.newgen.interfaces.XMLUploadable;
import com.newgen.util.Execute_WebService;

public class XMLUploadDAO implements XMLUploadable {
	private static Logger logger = Logger.getLogger(XMLUploadDAO.class);

	private static final int bytesInMB = 1024 * 1024; // 1MB

	@Override
	public void Insertdata(String FileName, String FileType, String FileSize,
			String FileVersion, String FileUploadBy, String FileUploadDate,
			String FileStatus, String FileUploadPath, String LogDownloadPath,
			String ReportingType, String ReportingYear, String stage,
			String giin, String complianceType) throws SQLException,
			ClassNotFoundException {

		String SOAP_inxml = null;
		String option = null;

		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;

		try {

			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();

			xmlvalues.put("FileName", FileName);
			xmlvalues.put("FileType", FileType);
			xmlvalues.put("FileSize", FileSize);
			xmlvalues.put("FileVersion", FileVersion);
			xmlvalues.put("FileUploadBy", FileUploadBy);
			xmlvalues.put("FileUploadDate", FileUploadDate);
			xmlvalues.put("FileStatus", FileStatus);
			xmlvalues.put("FileUploadPath", FileUploadPath);
			xmlvalues.put("LogDownloadPath", LogDownloadPath);
			xmlvalues.put("ReportingType", ReportingType);
			xmlvalues.put("ReportingYear", ReportingYear);
			xmlvalues.put("stage", stage);
			xmlvalues.put("giin", giin);
			xmlvalues.put("compliancaType", complianceType);
			xmlvalues.put("callingType", "");			

			option = "ProcedureXMLUploadInsertion";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();

			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {
				logger.info(outptXMLlst.get(0));
			}
		} catch (Exception e) {
			logger.info("Error  While inserting Data!" + e);

		}

	}

	@Override
	public int getFileVersion(String userEmail, String sFilename) {
		int newVersion = 0;
		String SOAP_inxml = null;
		String option = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;

		try {

			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();

			xmlvalues.put("userEmail", userEmail);
			xmlvalues.put("sFilename", sFilename);

			option = "ProcedureXMLUpload";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();

			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);

			if (!outptXMLlst.isEmpty()) {
				newVersion = (Integer.parseInt(outptXMLlst.get(0)) + 1);
			}

			if (newVersion != 0) {

				xmlvalues = new HashMap<String, String>();
				exe_websrv = new Execute_WebService();
				xmlvalues.put("Version", String.valueOf(newVersion));
				xmlvalues.put("UserEmail", userEmail);
				xmlvalues.put("Filename", sFilename);

				option = "ProcedureXMLUploadHistory";

				SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
				outptXMLlst = new ArrayList<String>();

				outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
						GlobalVariables.WSDL_String);

				if (!outptXMLlst.isEmpty()) {
					logger.info(outptXMLlst.get(0));
				}
			}

		} catch (Exception e) {
			logger.info("Exception in getFileVersion --> " + e.getMessage());
		}

		return newVersion;
	}
}
