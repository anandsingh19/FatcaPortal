package com.newgen.DAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.newgen.bean.GlobalVariables;
import com.newgen.bean.TransactionXMLBean;
import com.newgen.interfaces.ReportableStatus;
import com.newgen.util.Execute_WebService;

public class ReportStatusDAO implements ReportableStatus {
	private static Logger logger = Logger.getLogger(ReportStatusDAO.class);

	@Override
	public Map<String, Object> getUsertransactionDetails(
			HttpServletRequest request, HttpServletResponse response,
			String complianceType, String reportingYear)
			throws ServletException {

		int batchSize = 0;
		int pageCount = 0;
		String totalCount = null;
		String username = null;
		String userGiin = null;
		String nextFlag = "True";
		String pageNo = request.getParameter("pageCount");
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag = false;

		String SOAP_inxml = "";
		String option = "";

		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;

		if (pageNo == null || pageNo.equalsIgnoreCase("0")
				|| pageNo.length() == 0)
			pageCount = 0;
		else
			pageCount = Integer.parseInt(pageNo);
		List<TransactionXMLBean> XMLBeans = new ArrayList<TransactionXMLBean>();
		try {
			batchSize = GlobalVariables.batchSize;

			HttpSession session = request.getSession(false);
			if (session != null) {

				username = (String) session.getAttribute("UserEmail");
				userGiin = (String) session.getAttribute("UserGIIN");
				xmlvalues = new HashMap<String, String>();
				exe_websrv = new Execute_WebService();
				xmlvalues.put("Username", username);
				xmlvalues.put("UserGiin", userGiin);
				xmlvalues.put("ComplianceType", complianceType);
				xmlvalues.put("ReportingYear", reportingYear);
				option = "ProcedureReportServletCount";
				SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
				outptXMLlst = new ArrayList<String>();
				outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
						GlobalVariables.WSDL_String);
				if (!outptXMLlst.isEmpty()) {
					totalCount = outptXMLlst.get(0);
				}

				xmlvalues = new HashMap<String, String>();
				// exe_websrv = new Execute_WebService();

				xmlvalues.put("BatchSize", batchSize + "");
				xmlvalues.put("PageCount", pageCount + "");
				xmlvalues.put("Username", username);
				xmlvalues.put("UserGiin", userGiin);
				xmlvalues.put("ComplianceType", complianceType);
				xmlvalues.put("ReportingYear", reportingYear);
				option = "ProcedureReportServletBatch";

				SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
				outptXMLlst = new ArrayList<String>();
				outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
						GlobalVariables.WSDL_String);
				int n = outptXMLlst.size() / 9;
				int i = 0;
				while (n > 0) {
					TransactionXMLBean beanObj = new TransactionXMLBean();
					// i++;// for number in table not required here
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						beanObj.setFiStage(outptXMLlst.get(i++));
					else {
						beanObj.setFiStage("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						beanObj.setFiStatus(outptXMLlst.get(i++));
					else {
						beanObj.setFiStatus("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						beanObj.setFileName(outptXMLlst.get(i++));
					else {
						beanObj.setFileName("");
						i++;
					}

					// /beanObj.setFiStage(outptXMLlst.get(i++));
					// beanObj.setFiStatus(outptXMLlst.get(i++));
					// beanObj.setFileName(outptXMLlst.get(i++));
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						beanObj.setUploadedBy(outptXMLlst.get(i++));
					else {
						beanObj.setUploadedBy("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						beanObj.setFiletype(outptXMLlst.get(i++));
					else {
						beanObj.setFiletype("");
						i++;
					}
					// beanObj.setUploadedBy(outptXMLlst.get(i++));
					// beanObj.setFiletype(outptXMLlst.get(i++));

					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						beanObj.setFileStage(outptXMLlst.get(i++));
					else {
						beanObj.setFileStage("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						beanObj.setFileStatus(outptXMLlst.get(i++));
					else {
						beanObj.setFileStatus("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						beanObj.setUploadedDate(outptXMLlst.get(i++));
					else {
						beanObj.setUploadedDate("");
						i++;
					}
					if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
							&& outptXMLlst.get(i).length() != 0)
						beanObj.setHtmlFilepath(outptXMLlst.get(i++));
					else {
						beanObj.setHtmlFilepath("");
						i++;
					}
					// beanObj.setFileStage(outptXMLlst.get(i++));
					// beanObj.setFileStatus(outptXMLlst.get(i++));
					// beanObj.setUploadedDate(outptXMLlst.get(i++));
					// beanObj.setHtmlFilepath(outptXMLlst.get(i++));
					XMLBeans.add(beanObj);
					n--;
				}

				if (totalCount != null && totalCount.length() != 0) {
					int count = Integer.parseInt(totalCount);
					if ((batchSize * (pageCount + 1)) < count) {
						nextFlag = "True";
					} else
						nextFlag = "False";
				}

				map.put("XMLArrayList", XMLBeans);
				map.put("nextFlag", nextFlag);
				map.put("pageCount", pageCount);
				map.put("totalCount", totalCount);
				flag = true;
				map.put("status", flag);
			} else {
				map.put("status", flag);
			}

		} catch (Exception e) {
			logger.info("getUsertransactionDetails::Exception in servlet--> "
					+ e.getMessage());

		}

		return map;

	}
}
