package com.newgen.DAO;

import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Schema;
import javax.xml.XMLConstants;
import javax.xml.transform.sax.SAXSource;

import org.apache.log4j.Logger;
import org.xml.sax.InputSource;

import javax.xml.validation.Validator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
//import java.io.*;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.ErrorHandler;

import com.newgen.bean.GlobalVariables;
import com.newgen.controller.XMLUpload;
import com.newgen.factory.ObjectFactory;

public class XMLValidation {
	private static Logger logger = Logger.getLogger(XMLValidation.class);
	private static int errorCount = 0;
	private static String FileFullName = "";
	private static String FileName = "Log.txt";
	private static Long FileSize = null;
	private static String formattedDate = "";
	private static String FileType = "";
	private static Schema schema = null;

	public static String ReadXML(String User, String User_giin,
			String filename, String type, String year, int version,
			String stage, String InputXMLPath, String failXMLPath,
			String xmlType) throws IOException, SQLException,
			ClassNotFoundException {
		String responseStatus = "Failure";
		XMLUploadDAO XMLUploadDAOObj = null;
		String logPath = null;
		Date date = null;
		boolean schemaLoaded = false;
		try {
			FileUploadOperation fileUploadOperationObj = new FileUploadOperation();
			XMLUploadDAOObj = ObjectFactory.getXMLUploadDaoObject();
			logPath = failXMLPath;
			logger.debug("log path in read xml --> " + logPath);
			date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
			formattedDate = sdf.format(date);
			String TemplateXSDPath = GlobalVariables.xSDTemplate;
			String crsXSDPath = GlobalVariables.crsXSDPath;
			File localFile2 = new File(InputXMLPath + File.separator + filename);
			FileFullName = localFile2.getName();
			FileSize = localFile2.length();
			String strfileSize = FileSize.toString();
			FileName = FileFullName.substring(0, FileFullName.length() - 4);
			if ("crs".equalsIgnoreCase(xmlType)) {
				// if (schema == null)
				logger.debug("loading CRS Schema");
				schemaLoaded = loadSchema(crsXSDPath);
			} else if ("fatca".equalsIgnoreCase(xmlType)) {
				// if (schema == null)
				logger.debug("loading FATCA Schema");
				schemaLoaded = loadSchema(TemplateXSDPath);
			}
			if (schemaLoaded) {
				try {
					validateXml(schema, InputXMLPath + File.separator
							+ FileName + ".xml");
					responseStatus = "Success";
					//InputXMLPath = GlobalVariables.correctXmlLocation;
				} catch (ValidationException ex) {
					responseStatus = "Failure";
					logger.info("inside ValidationException");
					fileUploadOperationObj.convertHTML(FileName);
					//InputXMLPath = GlobalVariables.failedXmlLocation;
				}
				errorCount = 0;
				XMLUploadDAOObj.Insertdata(FileName + ".xml", FileType,
						strfileSize, String.valueOf(version), User,
						formattedDate, responseStatus, InputXMLPath
								+ File.separator + FileName + ".xml", logPath
								+ File.separator + FileName + ".html", type,
						year, stage, User_giin, xmlType);
			}
			else{
				responseStatus = "SchemaFailure";
			}
			
		} catch (Exception fe) {

			logger.info("Exception -->" + fe.getMessage());
			responseStatus = "Failure";

		}
		return responseStatus;
	}

	public static boolean validateXml(Schema schema, String xmlName)
			throws ValidationException {
		boolean fileValidated = false;
		FileInputStream inputStream = null;
		InputSource inputsrc = null;
		try {

			logger.info(" :: Inside validateXml ::");

			// creating a Validator instance
			Validator validator = schema.newValidator();

			// setting my own error handler
			validator.setErrorHandler(new MyErrorHandler());

			// preparing the XML file as a SAX source
			inputStream = new FileInputStream(xmlName);
			inputsrc = new InputSource(inputStream);
			SAXSource source = new SAXSource(inputsrc);

			// validating the SAX source against the schema
			validator.validate(source);

			if (errorCount > 0) {
				logger.info("<table><tr><td><font size=3><strong>Failed with errors: "
						+ errorCount + "</strong></font></td></tr></table>");
				throw new ValidationException();

			} else {
				logger.info("XML Validated Successfully !!");
				fileValidated = true;
			}

		} catch (Exception e) {
			throw new ValidationException(e);
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
					inputStream = null;
				}

			} catch (Exception ex) {
				logger.debug("Exception in validateXml finally "
						+ ex.getMessage());
			}
		}
		return fileValidated;
	}

	public static boolean loadSchema(String name) {
		boolean schemaLoaded = false;
		try {
			logger.debug("inside loadSchema");
			String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
			SchemaFactory factory = SchemaFactory.newInstance(language);
			schema = factory.newSchema(new File(name));
			schemaLoaded = true;
		} catch (Exception e) {
			schemaLoaded = false;
			logger.info("Exception in loadSchema--> " + e.getMessage());
		}
		return schemaLoaded;

	}

	private static class MyErrorHandler implements ErrorHandler {

		@Override
		public void warning(SAXParseException e) throws SAXException {
			FileUploadOperation.generateLog(
					"<table><tr><td><strong>Warning: </strong></td></tr>",
					FileName);
			printException(e);
		}

		@Override
		public void error(SAXParseException e) throws SAXException {
			if (errorCount == 0) {
				FileUploadOperation
						.generateLog(
								"<head><style type=text/css>* {font-family: Arial, Helvetica, sans-serif;}h1{text-align:center;}TABLE {WIDTH:100%;}caption {caption-side: top; text-align: left; font-weight: bold;}tr.greyBG {background-color: #888888; color: white;}tr.ltBlueBG {background-color: #3366FF; color: white;}tr.Black {  background-color: #000000; color: white;}</style></head>",
								FileName);
				FileUploadOperation
						.generateLog(
								"<table style=font-family: Arial, Helvetica, sans-serif; ><tr><td align=center><strong><font size=+3>XML Validation Report</font></strong></td></tr></table><br>",
								FileName);
				FileUploadOperation
						.generateLog(
								"<table style=font-family: Arial, Helvetica, sans-serif; ><tr><td><strong>File Name :</strong> "
										+ FileFullName
										+ " </td></tr></table><br>", FileName);
				FileUploadOperation
						.generateLog(
								"<table style=font-family: Arial, Helvetica, sans-serif; > <tr><td><strong>Type of File :</strong> Test</td></tr></table><br>",
								FileName);
				FileUploadOperation
						.generateLog(
								"<table style=font-family: Arial, Helvetica, sans-serif;  ><tr><td><strong>File size in bytes:</strong> "
										+ FileSize + "</td></tr></table><br>",
								FileName);
				FileUploadOperation
						.generateLog(
								"<table style=font-family: Arial, Helvetica, sans-serif; ><tr><td><strong>File Upload Date and Time: </strong> "
										+ formattedDate
										+ "</td></tr></table><br>", FileName);
			}
			System.out
					.println("<tablestyle=font-family: Arial, Helvetica, sans-serif;  ><tr><td>XML Error Description : </td></tr>");
			FileUploadOperation
					.generateLog(
							"<table style=font-family: Arial, Helvetica, sans-serif;  ><tr><td><strong>XML Error Description : </strong></td></tr>",
							FileName);
			printException(e);
		}

		@Override
		public void fatalError(SAXParseException e) throws SAXException {
			System.out.println("<table ><tr><td>Fatal error:  </td></tr>");
			FileUploadOperation
					.generateLog(
							"<tablestyle=font-family: Arial, Helvetica, sans-serif;  ><tr><td><strong>Fatal error:  </strong></td></tr>",
							FileName);
			printException(e);
		}

		private void printException(SAXParseException e) {
			errorCount++;
			logger.info("<tr><td>Error at Input XML Line number: "
					+ e.getLineNumber() + "</td></tr>");
			FileUploadOperation.generateLog(
					"<tr><td>Error at Input XML Line number: <font size=+1><strong>"
							+ e.getLineNumber() + "</strong></font></td></tr>",
					FileName);
			logger.info("<tr><td>Invalid XML Error Message: <font color=red>"
					+ e.getMessage() + "</font></td></tr></table>");
			FileUploadOperation.generateLog(
					"<tr><td>Invalid XML Error Message: <font color=red>"
							+ e.getMessage() + "</font></td></tr></table><br>",
					FileName);

		}
	}

}

class ValidationException extends Exception {
	private static Logger logger = Logger.getLogger(ValidationException.class);

	String exception1 = "";

	ValidationException(Exception e) {
		super(e);
		try {

			exception1 = "Exception in validation \n\n" + e;

			// System.out.println("ValidationException" + exception1);
			// logger.info("ValidationException--> " + exception1);
		} catch (Exception e2) {
			logger.info("ValidationException e2 --> " + e2.getMessage());

		}
	}

	ValidationException() {

	}

}
