package com.newgen.DAO;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.newgen.bean.GlobalVariables;
import com.newgen.bean.LoginUser;
import com.newgen.bean.OTP;
import com.newgen.factory.ObjectFactory;
import com.newgen.interfaces.Customable;
import com.newgen.interfaces.GenerateOTPable;
import com.newgen.interfaces.Validatable;
import com.newgen.util.Execute_WebService;

public class GenearateOTPDAO implements Validatable, GenerateOTPable {

	private static Logger logger = Logger.getLogger(GenearateOTPDAO.class);

	@Override
	public boolean validate(Object obj) {
		// TODO Auto-generated method stub
		boolean flag = false;
		OTP otpObject = (OTP) obj;

		if (otpObject.getEvent().equalsIgnoreCase("generateOTP")) {
			String otp = generatOTPValue(otpObject);
			Customable custom = ObjectFactory.getCustomObject();
			logger.debug("otp result:" + otp);
			String hasCodevalue = null;
			if (!otp.equalsIgnoreCase("EXIST")
					&& !otp.equalsIgnoreCase("NOTEXIST")) {
				hasCodevalue = custom.getHashValue(otp);
				if (saveOTP(hasCodevalue, otpObject.getEmail(),
						otpObject.getSourceType()))
					flag = true;
			}
		}

		else if (otpObject.getEvent().equalsIgnoreCase("authenticateOTP")) {
			String newOTP = otpObject.getOtp();
			String hasCodevalue = null;
			Customable custom = ObjectFactory.getCustomObject();
			hasCodevalue = custom.getHashValue(newOTP);
			if (isOTPvalidated(hasCodevalue, otpObject.getEmail(),
					otpObject.getSourceType()))
				flag = true;

		}

		return flag;
	}

	@Override
	public String generatOTPValue(OTP obj) {
		// TODO Auto-generated method stub
		logger.info("Inside generatOTPValue");
		logger.info("Inside generatOTPValue");
		String OTP = null;
		String SOAP_inxml = null;
		String option = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		try {
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("ProcessInstanceId", "");
			xmlvalues.put("GIIN", obj.getEmail());
			xmlvalues.put("TriggerType", obj.getSourceType());
			option = "Procedure_NG_SP_IRS_FATCA_EMAIL";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty())
				OTP = outptXMLlst.get(0);
			logger.info("otp is --> " + OTP);

		} catch (Exception e) {
			logger.info("Exception in sendOTP --> " + e.getMessage());
		}
		return OTP;
	}

	@Override
	public boolean saveOTP(String hashCode, String emaillID, String oTPSource) {
		// TODO Auto-generated method stub

		logger.info("saving user details in logged user table..");
		boolean flag = false;

		String SOAP_inxml = null;
		String option = null;

		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;

		try {

			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();

			xmlvalues.put("emailID", emaillID);
			xmlvalues.put("hashCode", hashCode);
			xmlvalues.put("oTPSource", oTPSource);
			xmlvalues.put("oTPTenure",
					String.valueOf(GlobalVariables.OTPexpiry));
			xmlvalues.put("type", "INSERT");
			option = "ProcedureValidateOTP";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();

			// System.out.println("Inputxml:  " + SOAP_inxml);
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (outptXMLlst.get(0).equalsIgnoreCase("SUCCESS")) {
				logger.info("GenearateOTPDAO:saveOTP:Inserted SuccessFully in logger user table...");
				flag = true;
			}
		} catch (Exception ex) {
			logger.info("GenearateOTPDAO:saveOTP():Exception:There is some exception please contact your system admin or try after some time"
					+ ex.getMessage());
		}
		return flag;

	}

	@Override
	public boolean isOTPvalidated(String hashCode, String emailID,
			String oTPSource) {

		logger.info("inside isOTPvalidated");
		logger.info("validating OTP..");
		boolean flag = false;

		String SOAP_inxml = null;
		String option = null;

		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;

		try {
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();

			xmlvalues.put("emailID", emailID);
			xmlvalues.put("hashCode", hashCode);
			xmlvalues.put("oTPSource", oTPSource);
			xmlvalues.put("oTPTenure",
					String.valueOf(GlobalVariables.OTPexpiry));
			//
			xmlvalues.put("type", "SELECT");
			option = "ProcedureValidateOTP";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();

			// System.out.println("Inputxml:  " + SOAP_inxml);
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {
				if (Integer.parseInt(outptXMLlst.get(0)) > 0)
					flag = true;
			}
		} catch (Exception ex) {
			logger.info("GenearateOTPDAO:isOTPvalidated:Exception:There is some exception please contact your system admin or try after some time"
					+ ex.getMessage());
		}

		return flag;

	}

	@Override
	public boolean isOTPExist(OTP obj) {
		logger.info("inside isOTPExist");

		boolean flag = false;

		String SOAP_inxml = null;
		String option = null;

		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		try {
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();

			xmlvalues.put("emailID", obj.getEmail());
			xmlvalues.put("hashCode", "");
			xmlvalues.put("oTPSource", obj.getSourceType());
			xmlvalues.put("oTPTenure",
					String.valueOf(GlobalVariables.OTPexpiry));
			//
			xmlvalues.put("type", "CheckOTPExist");
			option = "ProcedureValidateOTP";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();

			// System.out.println("Inputxml:  " + SOAP_inxml);
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {
				if (Integer.parseInt(outptXMLlst.get(0)) > 0)
					flag = true;
			}

		} catch (Exception e) {
			logger.info("GenearateOTPDAO:isOTPExist:Exception:There is some exception please contact your system admin or try after some time"
					+ e.getMessage());
		}
		return flag;
	}

	@Override
	public boolean validateOldPassword(LoginUser user) {
		// TODO Auto-generated method stub
		return false;
	}

}
