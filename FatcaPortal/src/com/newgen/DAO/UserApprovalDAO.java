package com.newgen.DAO;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.newgen.bean.FIRegistration;
import com.newgen.bean.GlobalVariables;
import com.newgen.bean.UserDetailsforEdit;
import com.newgen.factory.ObjectFactory;
import com.newgen.interfaces.Customable;
import com.newgen.interfaces.UserApprovable;
import com.newgen.util.Execute_WebService;

//import java.sql.Date;

public class UserApprovalDAO implements UserApprovable {
	private static Logger logger = Logger.getLogger(UserApprovalDAO.class);

	/* SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss"); */

	@Override
	public org.json.JSONArray getFI(String userType, String userGIIN,
			int pageNo, String event, String searchFor, String searchIn) {
		int counter = -1;
		FIRegistration selectedUser = null;
		org.json.JSONObject jsonObj = null;
		org.json.JSONArray jsonArray = new org.json.JSONArray();

		String SOAP_inxml = null;
		String option = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		try {

			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("userType", userType);
			xmlvalues.put("userGiin", userGIIN);
			xmlvalues.put("pageNo", String.valueOf(pageNo));
			xmlvalues.put("batchSize",
					String.valueOf(GlobalVariables.batchSize));
			xmlvalues.put("action", event);
			xmlvalues.put("searchFor", searchFor);
			xmlvalues.put("searchIn", searchIn);

			option = "ProcedureUserApprovalSelect";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();

			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);

			int n = 0;
			int i = 0;

			n = outptXMLlst.size() / 28;// 28 is number of columns in select
			while (n > 0) {
				counter++;
				selectedUser = ObjectFactory.getFIRegistrationObject();
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setGiin(outptXMLlst.get(i++));
				else {
					selectedUser.setGiin("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setFiName(outptXMLlst.get(i++));
				else {
					selectedUser.setFiName("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setFiEmail(outptXMLlst.get(i++));
				else {
					selectedUser.setFiEmail("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setFiCountry(outptXMLlst.get(i++));
				else {
					selectedUser.setFiCountry("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setFiAddress(outptXMLlst.get(i++));
				else {
					selectedUser.setFiAddress("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserEmailId(outptXMLlst.get(i++));
				else {
					selectedUser.setUserEmailId("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserName(outptXMLlst.get(i++));
				else {
					selectedUser.setUserName("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserEmployeeID(outptXMLlst.get(i++));
				else {
					selectedUser.setUserEmployeeID("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserDesignation(outptXMLlst.get(i++));
				else {
					selectedUser.setUserDesignation("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserPhone(outptXMLlst.get(i++));
				else {
					selectedUser.setUserPhone("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserCountry(outptXMLlst.get(i++));
				else {
					selectedUser.setUserCountry("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserDocType1(outptXMLlst.get(i++));
				else {
					selectedUser.setUserDocType1("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserDocType2(outptXMLlst.get(i++));
				else {
					selectedUser.setUserDocType2("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0) {
					selectedUser.setOperation(outptXMLlst.get(i++));

				} else {
					selectedUser.setOperation("");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setFlag(outptXMLlst.get(i++));
				else {
					selectedUser.setFlag("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setComments(outptXMLlst.get(i++));
				else {
					selectedUser.setComments("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserAddress(outptXMLlst.get(i++));
				else {
					selectedUser.setUserAddress("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUploadFileLocation(outptXMLlst.get(i++));
				else {
					selectedUser.setUploadFileLocation("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserDocType1File1(outptXMLlst.get(i++));
				else {
					selectedUser.setUserDocType1File1("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserDocType2File2(outptXMLlst.get(i++));
				else {
					selectedUser.setUserDocType2File2("");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setIsUserLocked(outptXMLlst.get(i++));
				else {
					selectedUser.setIsUserLocked("");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setComplianceType(outptXMLlst.get(i++));
				else {
					selectedUser.setComplianceType("");
					i++;
				}

				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setInType(outptXMLlst.get(i++));
				else {
					selectedUser.setInType("");
					i++;
				}
				
				
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserDOB(outptXMLlst.get(i++));
				else {
					selectedUser.setUserDOB("");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserGender(outptXMLlst.get(i++));
				else {
					selectedUser.setUserGender("");
					i++;
				}
				
				
				
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserRegisteredOn(outptXMLlst.get(i++));
				else {
					selectedUser.setUserRegisteredOn("");
					i++;
				}
				if (!(outptXMLlst.get(i).equalsIgnoreCase("null"))
						&& outptXMLlst.get(i).length() != 0)
					selectedUser.setUserModifiedOn(outptXMLlst.get(i++));
				else {
					selectedUser.setUserModifiedOn("");
					i++;
				}
				i++;// because we have add extra column row number for batching
					// which is not required

				selectedUser.setUserID(String.valueOf(counter));

				jsonObj = new org.json.JSONObject(selectedUser);
				jsonArray.put(jsonObj);

				n--;
			}

			logger.info(jsonArray.toString());
			// out.print(jsonArray);

		} catch (Exception e) {
			logger.info("getFI:Exception" + e.getMessage());
		}
		return jsonArray;
	}

	@Override
	public int isApproved(String emailID, String comments, String sAdminStatus,
			String sFiUserName, String userTypetoUpdate, String editUserGIIN,
			String isUserLocked, HttpSession session) {

		int errorUpd = 0;
		String SOAP_inxml = null;
		String option = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;

		try {

			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("Comments", comments);
			xmlvalues.put("UserName", sFiUserName);
			xmlvalues.put("UserEmail", emailID);
			xmlvalues.put("ApproverName",
					(String) session.getAttribute("UserName"));
			xmlvalues.put("ApproverEmail",
					(String) session.getAttribute("UserEmail"));
			xmlvalues
					.put("UserType", (String) session.getAttribute("UserType"));
			xmlvalues.put("Status", sAdminStatus);
			xmlvalues.put("UserEditType", userTypetoUpdate);
			xmlvalues.put("EditUserGIIN", editUserGIIN);
			xmlvalues.put("IsUserLocked", isUserLocked);

			option = "ProcedureUserApprovalUpdate";

			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();

			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);

			if (!outptXMLlst.isEmpty()) {
				logger.info(outptXMLlst.get(0));
				errorUpd = 1;

			}

		} catch (Exception ex) {
			logger.info("There is some exception in closing connection in exception block of function isapproved"
					+ ex.getMessage());

		}
		return errorUpd;
	}

	public Map<String, Integer> getPageCount(String userType, String userGIIN,
			String pageNo, String event, String searchFor, String searchIn) {

		int totalCount = 0;
		String SOAP_inxml = null;
		String option = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		Map<String, Integer> pageDetails = new HashMap<String, Integer>();
		try {

			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("userType", userType);
			xmlvalues.put("userGiin", userGIIN);
			xmlvalues.put("pageNo", String.valueOf(1));
			xmlvalues.put("batchSize",
					String.valueOf(GlobalVariables.batchSize));
			xmlvalues.put("action", event);
			xmlvalues.put("searchFor", searchFor);
			xmlvalues.put("searchIn", searchIn);
			option = "ProcedureUserApprovalSelect";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);

			totalCount = Integer.parseInt(outptXMLlst.get(0));
			pageDetails.put("totalCount", totalCount);
			pageDetails.put("currentPage", 1);

		} catch (Exception ex) {

		}
		return pageDetails;
	}

	@Override
	public int isApprovedUser(FIRegistration userToUpdate,
			Map<String, String> ReqAttrMap,
			Map<String, InputStream> fileInStrmMap, HttpSession session) {
		String uploadFileDestPath = null;
		int errorUpd = 0;
		String SOAP_inxml = null;
		String option = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		Customable custom = ObjectFactory.getCustomObject();
		try {

			uploadFileDestPath = getUploadedFileLocation(userToUpdate
					.getUserEmailId());
			GlobalVariables.file_DBLocation = uploadFileDestPath;
			if (custom.saveReplacedDoc(userToUpdate, fileInStrmMap, ReqAttrMap)) {

				/* call save doc */

				xmlvalues = new HashMap<String, String>();
				exe_websrv = new Execute_WebService();

				xmlvalues.put("Comments", userToUpdate.getComments());
				xmlvalues.put("UserName", userToUpdate.getUserName());
				xmlvalues.put("UserEmail", userToUpdate.getUserEmailId());
				xmlvalues.put("ApproverName",
						(String) session.getAttribute("UserName"));
				xmlvalues.put("ApproverEmail",
						(String) session.getAttribute("UserEmail"));
				xmlvalues.put("UserType",
						(String) session.getAttribute("UserType"));
				xmlvalues.put("Status", userToUpdate.getFlag());
				xmlvalues.put("UserEditType", userToUpdate.getOperation());
				xmlvalues.put("EditUserGIIN", userToUpdate.getGiin());
				xmlvalues.put("IsUserLocked", userToUpdate.getIsUserLocked());
				/**/
				logger.debug("khushdil IsUserLocked--> "
						+ userToUpdate.getIsUserLocked());
				xmlvalues.put("FiEmail", userToUpdate.getFiEmail());
				xmlvalues.put("FiName", userToUpdate.getFiName());
				xmlvalues.put("FiAddress", userToUpdate.getFiAddress());
				xmlvalues.put("FiCountry", userToUpdate.getFiCountry());

				xmlvalues.put("UserAddress", userToUpdate.getUserAddress());
				xmlvalues.put("UserPhone", userToUpdate.getUserPhone());
				xmlvalues.put("UserCountry", userToUpdate.getUserCountry());
				xmlvalues.put("UserEmpID", userToUpdate.getUserEmployeeID());
				xmlvalues.put("UserDesignation",
						userToUpdate.getUserDesignation());
				
				xmlvalues.put("UserDOB", userToUpdate.getUserDOB());
				xmlvalues.put("UserGender", userToUpdate.getUserGender());

				xmlvalues.put("UserDocType1", userToUpdate.getUserDocType1());
				xmlvalues.put("UserDocType2", userToUpdate.getUserDocType2());
				xmlvalues.put("UserFile1", userToUpdate.getUserDocType1File1());
				xmlvalues.put("UserFile2", userToUpdate.getUserDocType2File2());
				xmlvalues.put("UserFileLocation",
						GlobalVariables.file_DBLocation);
				xmlvalues.put("InType", userToUpdate.getInType());
				logger.debug("UserFileLocation--> "
						+ GlobalVariables.file_DBLocation);
				logger.debug("UserFile1--> "
						+ userToUpdate.getUserDocType1File1());
				logger.debug("UserFile2--> "
						+ userToUpdate.getUserDocType2File2());
				/**/
				option = "ProcedureUserApprovalUpdate";

				SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
				outptXMLlst = new ArrayList<String>();

				outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
						GlobalVariables.WSDL_String);

				if (!outptXMLlst.isEmpty()) {
					logger.info(outptXMLlst.get(0));
					errorUpd = 1;

				}// comment
			} else {
				logger.debug("error in uploading replaced file");

			}

		} catch (Exception ex) {
			logger.info("There is some exception in closing connection in exception block of function isapproved"
					+ ex.getMessage());

		}
		return errorUpd;
	}

	@Override
	public String getUploadedFileLocation(String userEmail) {

		String SOAP_inxml = null;
		String option = null;
		HashMap<String, String> xmlvalues = null;
		ArrayList<String> outptXMLlst = null;
		Execute_WebService exe_websrv = null;
		String uploadDestPath = null;
		try {
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("UserEmail", userEmail);
			option = "ProcedureFATCAUploadDestPath";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);

			if (!outptXMLlst.isEmpty()) {
				logger.info(outptXMLlst.get(0));
				uploadDestPath = outptXMLlst.get(0);
			}
		} catch (Exception e) {
			logger.info("There is some exception in getUploadedFileLocation()"
					+ e.getMessage());
		}
		return uploadDestPath;
	}

	@Override
	public boolean isLastFIAdmin(String giin) {
		boolean isLastFiAdmin = false;
		try {
			String SOAP_inxml = null;
			String option = null;
			HashMap<String, String> xmlvalues = null;
			ArrayList<String> outptXMLlst = null;
			Execute_WebService exe_websrv = null;
			xmlvalues = new HashMap<String, String>();
			exe_websrv = new Execute_WebService();
			xmlvalues.put("GIIN", giin);
			xmlvalues.put("ProcCallingFor", "isLastFIAdmin");
			option = "ProcedureFATCAMaxFIadminCount";
			SOAP_inxml = exe_websrv.generatexml(xmlvalues, option);
			outptXMLlst = new ArrayList<String>();
			outptXMLlst = exe_websrv.returnWebService(SOAP_inxml,
					GlobalVariables.WSDL_String);
			if (!outptXMLlst.isEmpty()) {
				if (outptXMLlst.get(0).equalsIgnoreCase("true")) {
					isLastFiAdmin = true;
					logger.debug("isLastFiAdmin in getFiAdminCount Dao --> "
							+ isLastFiAdmin);
				}
			}
		} catch (Exception e) {
			logger.debug("Exception in isLastFIAdmin --> " + e.getMessage());
		}
		return isLastFiAdmin;
	}
}
