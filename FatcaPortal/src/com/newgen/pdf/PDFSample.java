package com.newgen.pdf;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.newgen.bean.FIRegistration;

public class PDFSample {

	// Page configuration
	private static PDRectangle PAGE_SIZE = PDPage.PAGE_SIZE_A0;


	private static final float MARGIN = 20;
	private static final boolean IS_LANDSCAPE = true;

	// Font configuration
	private static final PDFont TEXT_FONT = PDType1Font.HELVETICA;

	private static final float FONT_SIZE = 10;
	private static final float FONT_SIZE_HEADER=14;

	// Table configuration
	private static final float ROW_HEIGHT = 15;
	private static final float CELL_MARGIN = 2;

	/*  public static void main(String[] args) throws IOException, COSVisitorException {
        new PDFTableGenerator().generatePDF(createContent());
    }
	 */
	public static Table createContent(ArrayList<String> headerList,String content[][],String file) {
		// Total size of columns must not be greater than table width.
		List<Column> columns = new ArrayList<Column>();
		if(file.contains("TransactionHistory"))
			PAGE_SIZE = PDPage.PAGE_SIZE_A4;
		else if(file.contains("UsersHistory"))
			PAGE_SIZE = PDPage.PAGE_SIZE_A2;
		else        	
			PAGE_SIZE = PDPage.PAGE_SIZE_A0;
		
		for(String headerName : headerList )
		{
			if(headerName.contains("Current Value") || headerName.contains("Older Value") || headerName.contains("User Address"))
				columns.add(new Column(headerName,650));
			else
				columns.add(new Column(headerName,135));
				

		}

		/* String [][] content = new String [userData.size()][headerList.size()];
        for(int i =0;i<userData.size();i++)
        {
        	for()
        	for(int j =0,n=userData.size();j<headerList.size();j++,n--)
        	{
        		FIRegistration fiuser = null;
        		fiuser = (FIRegistration) userData.get(Integer.valueOf(n));
        		content[i][j] = fiuser.get;
        	}
        }*/

		float tableHeight = IS_LANDSCAPE ? PAGE_SIZE.getWidth() - (2 * MARGIN) : PAGE_SIZE.getHeight() - (2 * MARGIN);


		Table table = new TableBuilder()
		.setCellMargin(CELL_MARGIN)
		.setColumns(columns)
		.setContent(content)
		.setHeight(tableHeight)
		.setNumberOfRows(content.length)
		.setRowHeight(ROW_HEIGHT)
		.setMargin(MARGIN)
		.setPageSize(PAGE_SIZE)
		.setLandscape(IS_LANDSCAPE)
		.setTextFont(TEXT_FONT)
		
		.setFontSize(FONT_SIZE)
		//.setFontSizeHeader(FONT_SIZE_HEADER)
		.build();
		return table;
	}   
}
