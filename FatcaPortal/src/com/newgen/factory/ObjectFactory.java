package com.newgen.factory;

import com.newgen.DAO.Custom;
import com.newgen.DAO.EditUserDAO;
import com.newgen.DAO.FAQDao;
import com.newgen.DAO.GenearateOTPDAO;
import com.newgen.DAO.LoginDAO;
import com.newgen.DAO.RegistrationDAO;
import com.newgen.DAO.ReportStatusDAO;
import com.newgen.DAO.TransactionBatchDAO;
import com.newgen.DAO.UploadManualDAO;
import com.newgen.DAO.UserApprovalDAO;
import com.newgen.DAO.XMLUploadDAO;
import com.newgen.bean.FIRegistration;
import com.newgen.bean.FrequentlyAskedQue;
import com.newgen.bean.LatestXMLBean;
import com.newgen.bean.LoginUser;
import com.newgen.bean.OTP;
//import com.newgen.util.ClsUtil;
import com.newgen.bean.TransactionXMLBean;

public class ObjectFactory {

	public ObjectFactory() {

		// Default Constructor;
	}

	public static RegistrationDAO getRegistrationDAOObject() {

		return new RegistrationDAO();
	}

	public static GenearateOTPDAO getGenearateOTPDAObject() {

		return new GenearateOTPDAO();
	}

	public static OTP getOTPbject() {

		return new OTP();
	}

	public static FIRegistration getFIRegistrationObject() {

		return new FIRegistration();
	}

	public static Custom getCustomObject() {

		return new Custom();
	}
	

	public static EditUserDAO getEditUserDAOObject() {

		return new EditUserDAO();
	}

	public static LoginUser getLoginUserObject() {

		return new LoginUser();
	}

	public static LoginDAO getLoginObject() {

		return new LoginDAO();
	}

	public static XMLUploadDAO getXMLUploadDaoObject() {

		return new XMLUploadDAO();
	}

	public static ReportStatusDAO getReportStatusDAoObject() {

		return new ReportStatusDAO();
	}

	/*public static ClsUtil getClsUtilObject() {

		return new ClsUtil();
	}*/

	public static UploadManualDAO getUploadManualDAOObject() {

		return new UploadManualDAO();
	}
	
	public static LatestXMLBean getLatestXMLBeanObject() {

		return new LatestXMLBean();
	}
	public static TransactionBatchDAO getTransactionBatchDAOObject() {

		return new TransactionBatchDAO();
	}
	public static UserApprovalDAO getUserApprovalDAOObject() {

		return new UserApprovalDAO();
	}
	
	public static FAQDao getFaqDaoObject() {

		return new FAQDao();
	}
	
	public static FrequentlyAskedQue getFrequentlyAskedQueObject() {

		return new FrequentlyAskedQue();
	}
	
	public static TransactionXMLBean getTransactionXMLBeanObject() {

		return new TransactionXMLBean();
	}

}
