package com.newgen.bean;

public class ManualOwnerGridBean {
	
	private  String accountNumber;
	private  String entityName;
	private  String ownerName;
	private  String ownerTin;
	public ManualOwnerGridBean(){
		
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getOwnerTin() {
		return ownerTin;
	}
	public void setOwnerTin(String ownerTin) {
		this.ownerTin = ownerTin;
	}
	
	
	

}
