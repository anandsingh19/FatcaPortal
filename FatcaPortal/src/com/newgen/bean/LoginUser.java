package com.newgen.bean;

public class LoginUser {

	private String userEmail;
	private String password;
	private String userName;
	private String userType;
	private String giin;
	private String userFlag;
	private String userSessionID;
	private String userComplianceType;

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGiin() {
		return giin;
	}

	public void setGiin(String giin) {
		this.giin = giin;
	}

	public String getUserFlag() {
		return userFlag;
	}

	public void setUserFlag(String userFlag) {
		this.userFlag = userFlag;
	}

	public String getUserSessionID() {
		return userSessionID;
	}

	public void setUserSessionID(String userSessionID) {
		this.userSessionID = userSessionID;
	}

	public String getUserComplianceType() {
		return userComplianceType;
	}

	public void setUserComplianceType(String userComplianceType) {
		this.userComplianceType = userComplianceType;
	}

	
}
