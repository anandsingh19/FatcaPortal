package com.newgen.bean;

public class UserDatails {
	private  String userName;
	private  String userPassword;
	private  String userType;
	private  String isActiveUser;
	private  String GIIN;
	private  String FI_Name;
	private  String FI_Email;
	private  String FI_Contact;
	private  String FI_ZIP;
	private  String FI_Country;
	private  String FI_Address;
	private  String User_Email;
	private  String User_Name;
	private  String User_Emp_Id;
	private  String User_Emp_Designation;
	private  String Office_Contact;
	private  String User_Emp_Phone;
	private  String Zip_Postal_Code;
	private  String User_Emp_Country;
	private  String password;
	private  String Operation;
	private  String confirmPassword;
	private  String Address;
	private  String User_Registration_LifeRay;
	private  String doctype2;
	private  String doctype1;
	private  String doctype2_path;
	private  String doctype1_path;
	private  String doc_path;
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getIsActiveUser() {
		return isActiveUser;
	}
	public void setIsActiveUser(String isActiveUser) {
		this.isActiveUser = isActiveUser;
	}
	public String getGIIN() {
		return GIIN;
	}
	public void setGIIN(String gIIN) {
		GIIN = gIIN;
	}
	public String getFI_Name() {
		return FI_Name;
	}
	public void setFI_Name(String fI_Name) {
		FI_Name = fI_Name;
	}
	public String getFI_Email() {
		return FI_Email;
	}
	public void setFI_Email(String fI_Email) {
		FI_Email = fI_Email;
	}
	public String getFI_Contact() {
		return FI_Contact;
	}
	public void setFI_Contact(String fI_Contact) {
		FI_Contact = fI_Contact;
	}
	public String getFI_ZIP() {
		return FI_ZIP;
	}
	public void setFI_ZIP(String fI_ZIP) {
		FI_ZIP = fI_ZIP;
	}
	public String getFI_Country() {
		return FI_Country;
	}
	public void setFI_Country(String fI_Country) {
		FI_Country = fI_Country;
	}
	public String getFI_Address() {
		return FI_Address;
	}
	public void setFI_Address(String fI_Address) {
		FI_Address = fI_Address;
	}
	public String getUser_Email() {
		return User_Email;
	}
	public void setUser_Email(String user_Email) {
		User_Email = user_Email;
	}
	public String getUser_Name() {
		return User_Name;
	}
	public void setUser_Name(String user_Name) {
		User_Name = user_Name;
	}
	public String getUser_Emp_Id() {
		return User_Emp_Id;
	}
	public void setUser_Emp_Id(String user_Emp_Id) {
		User_Emp_Id = user_Emp_Id;
	}
	public String getUser_Emp_Designation() {
		return User_Emp_Designation;
	}
	public void setUser_Emp_Designation(String user_Emp_Designation) {
		User_Emp_Designation = user_Emp_Designation;
	}
	public String getOffice_Contact() {
		return Office_Contact;
	}
	public void setOffice_Contact(String office_Contact) {
		Office_Contact = office_Contact;
	}
	public String getUser_Emp_Phone() {
		return User_Emp_Phone;
	}
	public void setUser_Emp_Phone(String user_Emp_Phone) {
		User_Emp_Phone = user_Emp_Phone;
	}
	public String getZip_Postal_Code() {
		return Zip_Postal_Code;
	}
	public void setZip_Postal_Code(String zip_Postal_Code) {
		Zip_Postal_Code = zip_Postal_Code;
	}
	public String getUser_Emp_Country() {
		return User_Emp_Country;
	}
	public void setUser_Emp_Country(String user_Emp_Country) {
		User_Emp_Country = user_Emp_Country;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getOperation() {
		return Operation;
	}
	public void setOperation(String operation) {
		Operation = operation;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getUser_Registration_LifeRay() {
		return User_Registration_LifeRay;
	}
	public void setUser_Registration_LifeRay(String user_Registration_LifeRay) {
		User_Registration_LifeRay = user_Registration_LifeRay;
	}
	public String getDoctype2() {
		return doctype2;
	}
	public void setDoctype2(String doctype2) {
		this.doctype2 = doctype2;
	}
	public String getDoctype1() {
		return doctype1;
	}
	public void setDoctype1(String doctype1) {
		this.doctype1 = doctype1;
	}
	public String getDoctype2_path() {
		return doctype2_path;
	}
	public void setDoctype2_path(String doctype2_path) {
		this.doctype2_path = doctype2_path;
	}
	public String getDoctype1_path() {
		return doctype1_path;
	}
	public void setDoctype1_path(String doctype1_path) {
		this.doctype1_path = doctype1_path;
	}
	public String getDoc_path() {
		return doc_path;
	}
	public void setDoc_path(String doc_path) {
		this.doc_path = doc_path;
	}
	
	
	

	

}
