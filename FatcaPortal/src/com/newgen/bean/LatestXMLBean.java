package com.newgen.bean;

public class LatestXMLBean {
	private String fileName;
	private String fileStage;
	private String uploadedDate;
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileStage() {
		return fileStage;
	}
	public void setFileStage(String fileStage) {
		this.fileStage = fileStage;
	}
	public String getUploadedDate() {
		return uploadedDate;
	}
	public void setUploadedDate(String uploadedDate) {
		this.uploadedDate = uploadedDate;
	}

}
