package com.newgen.bean;

public class TransactionXMLBean {
	private String fileName;
	private String uploadedBy;
	private String filetype;
	private String fileStage;
	private String fileStatus;
	private String uploadedDate;
	private String htmlFilepath;
	private String fiStage;
	private String fiStatus;
	private String reportingYear;
	private String uploadedBy_userGIIN;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getUploadedBy() {
		return uploadedBy;
	}
	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}
	public String getFiletype() {
		return filetype;
	}
	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}
	public String getFileStage() {
		return fileStage;
	}
	public void setFileStage(String fileStage) {
		this.fileStage = fileStage;
	}
	public String getFileStatus() {
		return fileStatus;
	}
	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}
	public String getUploadedDate() {
		return uploadedDate;
	}
	public void setUploadedDate(String uploadedDate) {
		this.uploadedDate = uploadedDate;
	}
	public String getHtmlFilepath() {
		return htmlFilepath;
	}
	public void setHtmlFilepath(String htmlFilepath) {
		this.htmlFilepath = htmlFilepath;
	}
	public String getFiStage() {
		return fiStage;
	}
	public void setFiStage(String fiStage) {
		this.fiStage = fiStage;
	}
	public String getFiStatus() {
		return fiStatus;
	}
	public void setFiStatus(String fiStatus) {
		this.fiStatus = fiStatus;
	}
	public String getReportingYear() {
		return reportingYear;
	}
	public void setReportingYear(String reportingYear) {
		this.reportingYear = reportingYear;
	}
	public String getUploadedBy_userGIIN() {
		return uploadedBy_userGIIN;
	}
	public void setUploadedBy_userGIIN(String uploadedBy_userGIIN) {
		this.uploadedBy_userGIIN = uploadedBy_userGIIN;
	}
	
	
}
