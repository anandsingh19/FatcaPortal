package com.newgen.bean;

public class FIRegistration {
	
	
	private String giin;
	private String fiName;
	private String fiCountry;
	private String fiAddress;
	private String fiEmail;
	private String fiPhone;
	private String userName;
	private String userEmailId;
	private String userCountry;
	private String userAddress;
	private String userPhone;
	private String userEmployeeID;
	private String userDesignation;
	private String userPassword;
	private String userDocType1;
	private String userDocType1File1;
	private String userDocType2;
	private String userDocType2File2;
	private String uploadFileLocation;
	private String password;
	private String operation;
	private String flag;
	private String comments;
	private String userID;
	private String isUserLocked;
	
	private String complianceType;
	private String inType;
	private String userGender;
	private String userDOB;
	private String userRegisteredOn;
	private String userModifiedOn;
	private String fiContact;
	private String rejectedByName;
	private String rejectedByEmail;
	private String rejectedByUserType;
	
	public String getGiin() {
		return giin;
	}
	public void setGiin(String giin) {
		this.giin = giin;
	}
	public String getFiName() {
		return fiName;
	}
	public void setFiName(String fiName) {
		this.fiName = fiName;
	}
	public String getFiCountry() {
		return fiCountry;
	}
	public void setFiCountry(String fiCountry) {
		this.fiCountry = fiCountry;
	}
	public String getFiAddress() {
		return fiAddress;
	}
	public void setFiAddress(String fiAddress) {
		this.fiAddress = fiAddress;
	}
	public String getFiEmail() {
		return fiEmail;
	}
	public void setFiEmail(String fiEmail) {
		this.fiEmail = fiEmail;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserEmailId() {
		return userEmailId;
	}
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}
	public String getUserCountry() {
		return userCountry;
	}
	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}
	public String getUserAddress() {
		return userAddress;
	}
	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getUserEmployeeID() {
		return userEmployeeID;
	}
	public void setUserEmployeeID(String userEmployeeID) {
		this.userEmployeeID = userEmployeeID;
	}
	public String getUserDesignation() {
		return userDesignation;
	}
	public void setUserDesignation(String userDesignation) {
		this.userDesignation = userDesignation;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getUserDocType1() {
		return userDocType1;
	}
	public void setUserDocType1(String userDocType1) {
		this.userDocType1 = userDocType1;
	}
	public String getUserDocType2() {
		return userDocType2;
	}
	public void setUserDocType2(String userDocType2) {
		this.userDocType2 = userDocType2;
	}
	public String getUserDocType1File1() {
		return userDocType1File1;
	}
	public void setUserDocType1File1(String userDocType1File1) {
		this.userDocType1File1 = userDocType1File1;
	}
	public String getUserDocType2File2() {
		return userDocType2File2;
	}
	public void setUserDocType2File2(String userDocType2File2) {
		this.userDocType2File2 = userDocType2File2;
	}
	public String getUploadFileLocation() {
		return uploadFileLocation;
	}
	public void setUploadFileLocation(String uploadFileLocation) {
		this.uploadFileLocation = uploadFileLocation;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getIsUserLocked() {
		return isUserLocked;
	}
	public void setIsUserLocked(String isUserLocked) {
		this.isUserLocked = isUserLocked;
	}
	public String getFiPhone() {
		return fiPhone;
	}
	public void setFiPhone(String fiPhone) {
		this.fiPhone = fiPhone;
	}
	public String getComplianceType() {
		return complianceType;
	}
	public void setComplianceType(String complianceType) {
		this.complianceType = complianceType;
	}
	public String getInType() {
		return inType;
	}
	public void setInType(String inType) {
		this.inType = inType;
	}
	public String getUserGender() {
		return userGender;
	}
	public void setUserGender(String userGender) {
		this.userGender = userGender;
	}
	public String getUserDOB() {
		return userDOB;
	}
	public void setUserDOB(String userDOB) {
		this.userDOB = userDOB;
	}
	public String getUserRegisteredOn() {
		return userRegisteredOn;
	}
	public void setUserRegisteredOn(String userRegisteredOn) {
		this.userRegisteredOn = userRegisteredOn;
	}
	public String getUserModifiedOn() {
		return userModifiedOn;
	}
	public void setUserModifiedOn(String userModifiedOn) {
		this.userModifiedOn = userModifiedOn;
	}
	public String getFiContact() {
		return fiContact;
	}
	public void setFiContact(String fiContact) {
		this.fiContact = fiContact;
	}
	public String getRejectedByName() {
		return rejectedByName;
	}
	public void setRejectedByName(String rejectedByName) {
		this.rejectedByName = rejectedByName;
	}
	public String getRejectedByEmail() {
		return rejectedByEmail;
	}
	public void setRejectedByEmail(String rejectedByEmail) {
		this.rejectedByEmail = rejectedByEmail;
	}
	public String getRejectedByUserType() {
		return rejectedByUserType;
	}
	public void setRejectedByUserType(String rejectedByUserType) {
		this.rejectedByUserType = rejectedByUserType;
	}
	
	
	

}
