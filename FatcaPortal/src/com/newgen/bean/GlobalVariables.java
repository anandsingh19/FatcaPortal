package com.newgen.bean;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class GlobalVariables {
	public static String projectName;
	public static String sharedLocation;
	public static String file_DBLocation;
	public static String JndiName;
	public static int batchSize = 4;
	public static String fileName = "FATCA.XML";
	// public static String loginfailedErrorMsg =
	// "The Email and Password does not match.";

	public static String WSDL_String;
	public static String EmailIntiateSP = "{call NG_SP_IRS_FATCA_EMAIL(?,?,?)}";
	// public static String uploadXmlLocation;
	public static String correctXmlLocation;
	public static String failedXmlLocation;
	public static int memoryThreshold;
	public static int maxFileSize;
	public static int maxRequestSize;
	public static String xSDTemplate;
	public static String hTMLFileLocation;
	public static String uploadLocation;
	// public static String
	// notExistUserMsg="The user does exist in the system.";
	//Changes made by Alok on 21-11-2016 Start
	public static int supportiveDoc1MaXSize;
	public static int supportiveDoc2MaXSize;
	//Changes made by Alok on 21-11-2016 End
	public static String supportiveDocType;
	public static int OTPexpiry;
	// public static String supportiveDocTypeMsg = null;
	public static String successPath;
	public static int reportingStartYear = 2014;
	public static String alreadyLogin = "User Already loged in";
	public static Map<String, String> loginUserMap = new HashMap<String, String>();
	public static Map<String, String> loginUserIPv4Map = new HashMap<String, String>();
	public static String countryOnForm;
	public static String supportedBrowsers;
	public static String crsXSDPath;
	public static int iMaxFiAdmin;
	public static String luciaUSIGA;
	public static String helpLink1_label;
	public static String helpLink2_label;
	public static String helpLink3_label;
	public static String helpLink1_url;
	public static String helpLink2_url;
	public static String helpLink3_url;
	public static String customReportPath;

	public static String helpLink4_label;
	public static String helpLink5_label;
	public static String helpLink4_url;
	public static String helpLink5_url;
	/* Change for All by shubham on 27072016,28072016 'code added' - starts */
	public static String userManualPath;
	public static String userAllowedtoUpload;
	/* Change for All by shubham on 27072016,28072016 'code added' - end */





}
