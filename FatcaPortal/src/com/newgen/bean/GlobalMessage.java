package com.newgen.bean;

public class GlobalMessage {
	public static final String OTP_SUCCESS_MESSAGE = "El PIN de 6 d�gitos se ha enviado correctamente en tu ID de correo electr�nico";
	public static final String NEW_ACCOUNT_CREATED = "Te has registrado exitosamente. Inicie sesi�n con nuevo nombre de usuario y contrase�a";
	public static final String CONNECTION_FAILED = "Error de conexi�n. Por favor, int�ntelo de nuevo m�s tarde. Si todav�a no funciona, p�ngase en contacto con el administrador";
	public static final String USER_PASSWORD_MISMATCH = "Combinaci�n de usuario y contrase�a no coincidente";
	public static final String USER_BLOCKED = "Inicie sesi�n despu�s de la aprobaci�n de ADMIN";
	public static final String BLANK_USER_PASSWORD = "El nombre de usuario y la contrase�a no pueden estar en blanco";
	public static final String FILE_NOT_FOUND = "El archivo no existe en el servidor. Haz clic aqu� para regresar";
	public static final String USER_LOCKED_MSG = "Su cuenta ha sido bloqueada. Por favor, int�ntelo de nuevo m�s tarde.";
	public static final String LOGIN_ATTEMPT = "N�mero de intentos dejados : ";
	public static final String LOGIN_FAILED ="Correo electr�nico y contrase�a no coinciden.";
	public static final String IN_ACTIVE_USR_MSG = "Inicie sesi�n despu�s de la aprobaci�n de ADMIN";
	public static final String UN_AUTHORISED_USER = "Usted no es un usuario autorizado";
	public static final String ACCOUNT_LOCKED = "Su cuenta ha sido bloqueada. Int�ntelo de nuevo m�s tarde o p�ngase en contacto con Admin";
	public static final String INVALID_FILE_FORMAT = "Formato de archivo inv�lido...";
	public static final String PLEASE_SELECT_FILE = "Seleccione una ubicaci�n de archivo...";
/*Change for All by shubham on 27072016 starts*/
	/*public static final String UPLOAD_SUCCESS_MSG = "File Uploaded Successfully...";*/
	public static final String UPLOAD_SUCCESS_MSG = "Validaci�n de archivos en curso. Compruebe el estado en la p�gina Estado del informe.";
	/*Change for All by shubham on 27072016 end*/
	public static final String UPLOAD_FAIL_MSG = "Archivo cargado con alg�n error";
	public static final String REPORT_SUBMISSION_MANUAL_MSG = "No se puede enviar manualmente. Cargue el archivo xml";
	public static final String REPORT_SUBMISSION_XML_MSG = "No puede cargar archivos XML. Utilice la opci�n Manual Online para enviar el informe.";
	public static final String SCHEMA_FAIL_MSG = "Hay un error al cargar el esquema XSD. P�ngase en contacto con el administrador";
	/*CHANGE for all  BY shubham on 28072016 "code added"--starts*/
	public static final String XML_SUBMISSION_XML_MSG = "Subida fallida. La fecha l�mite para la presentaci�n ha pasado.";	
	/*CHANGE for all  BY shubham on 28072016 --end*/	


	
	
}

