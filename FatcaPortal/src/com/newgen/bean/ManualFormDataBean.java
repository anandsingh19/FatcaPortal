package com.newgen.bean;

public class ManualFormDataBean {

	private  String fiGIIn;
	private  String sponGIIN;
	private  String interGIIN;
	
	public ManualFormDataBean(){
		
	}

	public String getFiGIIn() {
		return fiGIIn;
	}

	public void setFiGIIn(String fiGIIn) {
		this.fiGIIn = fiGIIn;
	}

	public String getSponGIIN() {
		return sponGIIN;
	}

	public void setSponGIIN(String sponGIIN) {
		this.sponGIIN = sponGIIN;
	}

	public String getInterGIIN() {
		return interGIIN;
	}

	public void setInterGIIN(String interGIIN) {
		this.interGIIN = interGIIN;
	}
	
	
}



