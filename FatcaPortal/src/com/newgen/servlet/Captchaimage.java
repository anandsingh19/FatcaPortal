/*------------------------------------------------------------------------------------------------------
 -                                     NEWGEN SOFTWARE TECHNOLOGIES LIMITED
 Group                                                       : Application -Project1
 Project/Product                                             : [Inland Revenue Department - Antigua ]
 Application                                                 : Fatca Reporting Solution (Fatca Portal)
 Module                                                      : user login
 File Name                                                   : Captchaimage
 Author                                                      : Khushdil Kaushik
 Date (DD/MM/YYYY)                                           : 30/10/2015
 Description                                                 : user authentication check
 -------------------------------------------------------------------------------------------------------
 CHANGE HISTORY
 -------------------------------------------------------------------------------------------------------
 Problem No/CR No   Change Date   	Changed By   			 Change Description
 ------------------------------------------------------------------------------------------------------*/

package com.newgen.servlet;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.newgen.DAO.RegistrationDAO;

public class Captchaimage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(Captchaimage.class);

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");

	}

	public Captchaimage() {
		super();

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		logger.info("inside doget in captcha image");
		response.setContentType("image/jpg");

		int iTotalChars = 6;

		int iHeight = 40;
		int iWidth = 158;

		Font fntStyle1 = new Font("Arial", 1, 30);
		Font fntStyle2 = new Font("Verdana", 1, 20);

		Random randChars = new Random();
		String sImageCode = Long.toString(Math.abs(randChars.nextLong()), 36)
				.substring(0, iTotalChars);

		BufferedImage biImage = new BufferedImage(iWidth, iHeight, 1);

		Graphics2D g2dImage = (Graphics2D) biImage.getGraphics();

		int iCircle = 15;
		g2dImage.fillRect(0, 0, iWidth, iHeight);
		for (int i = 0; i < iCircle; ++i) {
			g2dImage.setColor(new Color(224, 224, 224));
			int iRadius = (int) (Math.random() * iHeight / 2.0D);
			int iX = (int) (Math.random() * iWidth - iRadius);
			int iY = (int) (Math.random() * iHeight - iRadius);

			g2dImage.fillRoundRect(iX, iY, iRadius * 2, iRadius * 2, 100, 100);
		}
		g2dImage.setFont(fntStyle1);
		for (int i = 0; i < iTotalChars; ++i) {
			/*g2dImage.setColor(new Color(randChars.nextInt(255), randChars
					.nextInt(255), randChars.nextInt(255)));*/
			g2dImage.setColor(new Color(0, 0, 0));

			if (i % 2 == 0)
				g2dImage.drawString(sImageCode.substring(i, i + 1), 25 * i, 24);
			else {
				g2dImage.drawString(sImageCode.substring(i, i + 1), 25 * i, 35);
			}
		}
		HttpSession session = request.getSession(true);

		session.setAttribute("dns_security_code", sImageCode);

		OutputStream osImage = response.getOutputStream();
		ImageIO.write(biImage, "jpeg", osImage);
		osImage.flush();
		osImage.close();

		g2dImage.dispose();

		//System.out.println("Captcha code sent back");
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		processRequest(request, response);

	}

}
