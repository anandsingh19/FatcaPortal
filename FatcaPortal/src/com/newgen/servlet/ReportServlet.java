package com.newgen.servlet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.newgen.bean.GlobalVariables;
//import com.newgen.util.PageGenerator;
import com.newgen.util.ReadPropertyFile;
import java.util.Map;

/**
 * Servlet implementation class ReportServlet
 */
public class ReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	 private static Logger logger = Logger.getLogger(ReportServlet.class);
	public ReportServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		PropertyConfigurator.configure("log4j_CRS.properties");
		long starttime = System.currentTimeMillis();
		HttpSession session = null;
		logger.info("Inside Report Servlet");
		try {
			ReadPropertyFile.readGeneralInfo();
			/*PageGenerator gen = new PageGenerator();
			Map<String, ArrayList> mapInvoiceFields = gen
					.readInvoiceXML(getServletContext()
							.getRealPath(GlobalVariables.fileName));
			getServletContext().setAttribute("MapFATCAPageDetails",
					mapInvoiceFields);

			Process proc = null;
			String classPath = null;
			String fileToCompile = null;
			String strCommandLine = "";
			String command = System.getenv("JAVA_HOME") + "\\bin\\javac -cp ";
			try {
				gen.generateBean(
						(HashMap) getServletContext().getAttribute(
								"MapFATCAPageDetails"),
						getServletContext()
								.getRealPath(
										"WEB-INF/classes/com/newgen/bean/FIRegistrationBean.java"));

				classPath = getServletContext().getRealPath(
						"\\WEB-INF\\classes");
						
						logger.info("Compiling FIRegistrationBean.java");
				fileToCompile = getServletContext()
						.getRealPath(
								"WEB-INF\\classes\\com\\newgen\\bean\\FIRegistrationBean.java");
				strCommandLine = command + "\"" + classPath + "\"" + " "
						+ fileToCompile;
				logger.info("FIRegistrationBean Bean");

				proc = Runtime.getRuntime().exec(strCommandLine);
				logger.info("Compiling Servlet");
				classPath = classPath + ";"
						+ getServletContext().getRealPath("\\WEB-INF\\lib\\*");
				fileToCompile = getServletContext().getRealPath(
						"WEB-INF\\classes\\com\\newgen\\servlet\\*.java");

				strCommandLine = command + "\"" + classPath + "\"" + " "
						+ fileToCompile;
				//ClsLogger.CreateLogs("Compiling Servlet", strCommandLine, "D");
				logger.info("Compiling Servlet"+strCommandLine);
				proc = Runtime.getRuntime().exec(strCommandLine);

				gen.genJSPPage(
						getServletContext().getRealPath("JSP/Registration.jsp"),
						(HashMap) getServletContext().getAttribute(
								"MapFATCAPageDetails"));
				// exitValue = proc.exitValue();
			} catch (Exception exp) {
				logger.info("Exception:"+exp.getMessage());
			}
*/
			

		} catch (Exception e) {

			logger.info("ReportServlet Exception in Report Servlet : "+e.getMessage());

		}

		long endTime = System.currentTimeMillis();
		long totaltime = endTime - starttime;
		
		logger.info("ReportServlet Total Time Taken in Report Servlet is : "+totaltime);

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}
}
